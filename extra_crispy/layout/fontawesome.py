
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from crispy_forms.layout import LayoutObject, TEMPLATE_PACK
from crispy_forms.utils import flatatt
from django.template.loader import render_to_string


class FontAwesome(LayoutObject):
    template = 'extra_crispy/fontawesome.html'

    def __init__(self, icon, font_size=None, template=None, **kwargs):
        self.icon = icon
        if template:
            self.template = template

        if font_size:
            cur_style = getattr(kwargs, 'style', '')
            if not cur_style.endswith(';'):
                cur_style += ';'

            style = '%s %s' % (cur_style, 'font-size: %dpx' % (font_size, ), )

            kwargs['style'] = style

        self.css_class = kwargs.pop('css_class', None)
        self.css_id = kwargs.pop('css_id', None)

        self.attrs = flatatt(kwargs)

    def render(self, form, form_style, context, template_pack=TEMPLATE_PACK):
        return render_to_string(self.template, {'self': self})
