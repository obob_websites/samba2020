# simple makefile to simplify repetetive build env management tasks under posix

# caution: testing won't work on windows, see README

flake:
	@if command -v flake8 > /dev/null; then \
		echo "Running flake8"; \
		flake8 --count home samba_web search extra wagtail_selenium; \
	else \
		echo "flake8 not found, please install it!"; \
		exit 1; \
	fi;
	@echo "flake8 passed"

update_deps:
	rm -rf .venv
	mamba env create -p ./.venv
	rm -rf node_modules
	npm install
	./.venv/bin/seleniumbase install chromedriver 114
	./manage.py get_js_packages
	./manage.py get_pypi_packages

webpack_dev:
	 npx webpack --watch -c webpack.dev.js

webpack_prod:
	 npx webpack -c webpack.prod.js

create_venv:
	mamba create -p ./.venv python

pytest_durations:
	pytest -n 3 --reruns 3 --store-durations


COPYRIGHT_CMD ?= docker run --rm --volume `pwd`:$(COPYRIGHT_OUTPUT_DIR) osterman/copyright-header:latest
COPYRIGHT_LICENSE ?= GPL3
COPYRIGHT_HOLDER ?= Thomas Hartmann <thomas.hartmann@th-ht.de>
COPYRIGHT_YEAR ?= 2019-2021
COPYRIGHT_SOFTWARE ?= samba2020
COPYRIGHT_SOFTWARE_DESCRIPTION ?= Webpage for https://samba.ccns.sbg.ac.at
COPYRIGHT_OUTPUT_DIR ?= /usr/src
COPYRIGHT_WORD_WRAP ?= 75
COPYRIGHT_PATHS ?= home:extra:extra_crispy:assets:pytest_plugins:samba_web:wagtail_selenium

remove-copyright:
	$(COPYRIGHT_CMD) \
	  --license $(COPYRIGHT_LICENSE)  \
	  --remove-path $(COPYRIGHT_PATHS) \
	  --guess-extension \
	  --copyright-holder '$(COPYRIGHT_HOLDER)' \
	  --copyright-software '$(COPYRIGHT_SOFTWARE)' \
	  --copyright-software-description '$(COPYRIGHT_SOFTWARE_DESCRIPTION)' \
	  --copyright-year $(COPYRIGHT_YEAR) \
	  --word-wrap $(COPYRIGHT_WORD_WRAP) \
	  --output-dir $(COPYRIGHT_OUTPUT_DIR)

add-copyright:
	$(COPYRIGHT_CMD) \
	  --license $(COPYRIGHT_LICENSE)  \
	  --add-path $(COPYRIGHT_PATHS) \
	  --guess-extension \
	  --copyright-holder '$(COPYRIGHT_HOLDER)' \
	  --copyright-software '$(COPYRIGHT_SOFTWARE)' \
	  --copyright-software-description '$(COPYRIGHT_SOFTWARE_DESCRIPTION)' \
	  --copyright-year $(COPYRIGHT_YEAR) \
	  --word-wrap $(COPYRIGHT_WORD_WRAP) \
	  --output-dir $(COPYRIGHT_OUTPUT_DIR)
