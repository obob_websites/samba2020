
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.models import AnonymousUser, Group, Permission
from django.urls import reverse
from django.contrib.auth import get_user_model
from selenium.webdriver.common.by import By
from allauth.account.models import EmailAddress
from selenium.webdriver.support.select import Select
import home.factories
import wagtailmenus.models
import requests


class SambaPytestHelper(object):
    def __init__(self, live_server, selenium):
        self.live_server = live_server
        self.selenium = selenium

    def logout(self):
        self.selenium.open(self.live_server + reverse('account_logout'))

    def login(self, username, password=None):
        if password is None and isinstance(username, dict):
            password = username['password']
            username = username['email']

        self.logout()
        self.selenium.open(self.live_server + reverse('alt-login'))
        self.selenium.update_text('#id_username', username)
        self.selenium.update_text('#id_password', password)
        self.selenium.submit('#id_username')

    def login_admin(self):
        admin_allauth_email = EmailAddress(email='admin@example.com',
                                           user=get_user_model().objects.get(
                                               username='admin'))
        admin_allauth_email.verified = True
        admin_allauth_email.save()
        self.login('admin@example.com', 'password')

    def create_site_in_browser(self, root_page,
                               is_default_site=False,
                               hostname='localhost',
                               port=80,
                               site_name='Test Site'):
        self.selenium.click('Settings', by=By.LINK_TEXT)
        self.selenium.click('Sites', by=By.LINK_TEXT)
        self.selenium.click('ADD A SITE', by=By.LINK_TEXT)

        self.selenium.update_text('input#id_hostname', hostname)
        self.selenium.update_text('input#id_port', str(port))
        self.selenium.update_text('input#id_site_name', site_name)

        default_site_cb = self.selenium.find_elements(
            'input#id_is_default_site')[0]

        if default_site_cb.is_selected() != is_default_site:
            self.selenium.js_click('input#id_is_default_site')

        self.selenium.click('#id_root_page-chooser > div > button')
        self.selenium.update_text('#id_q', root_page.title)
        self.selenium.click_link_text(root_page.title)
        self.selenium.click('input[value="Save"]')

    def assert_page_in_menu(self, page):
        self.selenium.open(str(self.live_server))

        self.selenium.assert_text(page.title, '.nav-item')

    def assert_streamfield_blocks_present(self, blocks):
        for cur_block in blocks:
            text_to_assert = cur_block.value
            text_to_assert = getattr(text_to_assert, 'source', text_to_assert)

            self.selenium.assert_text(text_to_assert)

    def goto_url(self, url):
        self.selenium.open(self.live_server + url)

    def goto_page(self, page):
        self.goto_url(page.url)

    def get_cookies(self):
        cookies_raw = self.selenium.driver.get_cookies()
        cookies = {i['name']: i['value'] for i in cookies_raw}

        return cookies

    def get_response_from_url(self, url):
        return requests.get(self.live_server + url,
                            cookies=self.get_cookies())

    def download_from_url(self, url, file):
        response = self.get_response_from_url(url)
        with open(file, 'wb') as f:
            f.write(response.content)

    def assert_option_in_select(self, selector, option):
        select_element = Select(
            self.selenium.get_element(selector))
        social_select_options = [x.text for x in select_element.options]

        assert option in social_select_options

    def assert_option_not_in_select(self, selector, option):
        select_element = Select(
            self.selenium.get_element(selector))
        social_select_options = [x.text for x in select_element.options]

        assert option not in social_select_options

    def add_page_to_menu(self, page, exclude_if_unauthorized):
        menu = wagtailmenus.models.MainMenu.objects.first()
        page.show_in_menus = True
        page.save()
        home.factories.MenuItem.create(
            menu=menu,
            exclude_from_menu_when_not_authorized=exclude_if_unauthorized,
            link_page=page
        )
        
    def login_with_role(self, role, conference, conference_user=None):
        self.logout()
        this_user = AnonymousUser
        if role == 'conference':
            self.login(conference_user['user_info'])
            this_user = conference_user['user']
        elif role == 'manager':
            this_user = home.factories.UserWithConfirmedEmail(
                raw_password='pw'
            )

            conference['conference_page'].manager_group.user_set.add(
                this_user)
            self.login(this_user.email, 'pw')
        elif role == 'reviewer':
            this_user = home.factories.UserWithConfirmedEmail(
                raw_password='pw'
            )

            conference['conference_page'].abstract_reviewer_group.user_set.add(
                this_user)
            self.login(this_user.email, 'pw')
        elif role == 'extended_reviewer':
            this_user = home.factories.UserWithConfirmedEmail(
                raw_password='pw'
            )

            (conference['conference_page']
             .extended_abstract_reviewer_group.user_set.add(this_user))
            self.login(this_user.email, 'pw')
        elif role == 'admin':
            this_user = home.factories.UserWithConfirmedEmail(
                raw_password='pw'
            )
            permission = Permission.objects.get(codename='access_admin')
            this_user.user_permissions.add(permission)
            self.login(this_user.email, 'pw')
        elif role == 'superuser':
            this_user = get_user_model().objects.create_superuser('newadmin', 'admin@example.com', 'pw')
            self.login('admin@example.com', 'pw')

        return this_user
