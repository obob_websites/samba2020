
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from faker import Faker
import re


class SambaIntegrationHelpers(object):
    def __init__(self, samba):
        self.samba = samba
        self.faker = Faker()

    def register_user(self,
                      email=None,
                      first_name=None,
                      last_name=None,
                      password=None):
        if email is None:
            email = self.faker.email()

        if first_name is None:
            first_name = self.faker.first_name()

        if last_name is None:
            last_name = self.faker.last_name()

        if password is None:
            password = self.faker.password()

        self.samba.logout()
        self.samba.goto_url('/')
        self.samba.selenium.click_link('Login')
        self.samba.selenium.click_link('sign up')

        self.samba.selenium.update_text('#id_email', email)
        self.samba.selenium.update_text('#id_first_name', first_name)
        self.samba.selenium.update_text('#id_last_name', last_name)
        self.samba.selenium.update_text('#id_password1', password)
        self.samba.selenium.update_text('#id_captcha_1', 'PASSED')

        self.samba.selenium.submit('#id_email')

        return {
            'email': email,
            'first_name': first_name,
            'last_name': last_name,
            'password': password
        }

    def confirm_email_address(self, mail_object):
        urls = re.findall(r'(https?://\S+)', mail_object.body)
        self.samba.selenium.open(urls[0])

