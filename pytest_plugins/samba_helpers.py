
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import os
from pathlib import Path

import pytest
import seleniumbase
import wagtail.core.models

from faker import Faker
from wagtail.core.utils import get_supported_content_language_variant

import home.models
import home.factories
import home.misc.ror
import home.misc.studyfieldandmodel
import wagtail_factories
from django.conf import settings
from django.core.management import call_command

from pytest_plugins.helpers.samba_pytest import SambaPytestHelper
from pytest_plugins.helpers.samba_integration import SambaIntegrationHelpers
from pyinstrument import Profiler


def pytest_addoption(parser):
    parser.addoption('--django-debug', action='store_true')


@pytest.fixture(scope='session', autouse=True)
def init_test_session(tmpdir_factory, django_db_blocker, request):
    """General init fixture for all pytests."""
    settings.STATIC_ROOT = str(tmpdir_factory.mktemp('static'))
    settings.TESTING = False
    settings.INSTALLED_APPS += [
        'extra.expose_to_template.tests.helpers'
    ]
    settings.CSRF_COOKIE_SECURE = False
    settings.SESSION_COOKIE_SECURE = False

    settings.DEBUG = request.config.getoption('--django-debug')

    with django_db_blocker.unblock():
        call_command('makemigrations',
                     'expose_to_template_test_app')
    call_command('collectstatic', interactive=False)

    if not os.getenv('SAMBA_KEY_DIR'):
        call_command('generate_key')

    yield

    # shutil.rmtree(os.environ['TEST_BASE_DIR'])


@pytest.fixture(scope='module')
def samba(live_server):
    this_sel = seleniumbase.BaseCase('__init__')
    this_sel.setUp()

    yield SambaPytestHelper(live_server, this_sel)
    this_sel.tearDown()


@pytest.fixture(scope='module')
def samba_integration(samba):
    yield SambaIntegrationHelpers(samba)


@pytest.fixture(scope='function')
def default_data(transactional_db):
    hp = home.factories.IndexPageFactory.create(
        title='Index Page',
        body__0__paragraph__value='I am a paragraph!',
    )
    site = wagtail_factories.SiteFactory(root_page=hp,
                                         is_default_site=True)

    data = {
        'index_page': hp,
        'site': site,
    }

    yield data


@pytest.fixture(scope='function')
def conference(transactional_db, default_data):
    default_data['conference_page'] = home.factories.ConferenceFactory.create(
        parent=default_data['index_page'],
        payment_confirmation_body__0__paragraph__value='Payment confirmed',  # noqa
        participant_confirmation_body__0__paragraph__value='Participation confirmed',  # noqa
    )

    registration_page = home.factories.RegisterAndAbstractOverview.create(
        parent=default_data['conference_page'],
        not_logged_in__0__paragraph__value='Not logged in',
        registration_not_yet_started__0__paragraph__value='Registration has not yet started',  # noqa
        registration_open_not_logged_in__0__paragraph__value='Registration open! But you are not logged in',  # noqa
        registration_open_not_registered__0__paragraph__value='Registration open! But you have not registered',  # noqa
        registration_full__0__paragraph__value='Conference Full',
        registration_deadline_passed__0__paragraph__value='Registration deadline is over',  # noqa
        registered_payment_pending__0__paragraph__value='You are registered but have not paid.',  # noqa
        registered_payment_complete__0__paragraph__value='You are registered and paid everything.',  # noqa
        registered_pay_at_conference__0__paragraph__value='You pay at the conference',  # noqa
        abstract_submission_open_not_logged_in__0__paragraph__value='Abstract submission open. not logged in',  # noqa
        abstract_submission_open_not_submitted__0__paragraph__value='Abstract submission is open but no abstract was submitted',  # noqa
        abstract_submission_limit_reached__0__paragraph__value='Abstract Submission Full',  # noqa
        abstract_submission_deadline_passed__0__paragraph__value='Abstract deadline has passed',  # noqa
        abstract_pending__0__paragraph__value='Abstract review pending',
        abstract_accepted__0__paragraph__value='Abstract accepted',
        abstract_rejected__0__paragraph__value='Abstract rejected',
        abstract_poster_number_display__0__paragraph__value='Poster number: {{poster_number}}',
        extended_abstract_submission_deadline_passed__0__paragraph__value='Deadline for extended abstract has passed',  # noqa
        extended_abstract_submission_requirements_not_fulfilled__0__paragraph__value='You must be fully registered and have '  # noqa
                                                                                     'your abstract accepted in order to '  # noqa
                                                                                     'submit the extended abstract',  # noqa
        extended_abstract_submission_open_not_submitted__0__paragraph__value='Submit Extended Abstract here',  # noqa
        extended_abstract_submission_open_not_logged_in__0__paragraph__value='Extended Abstract submission open but not logged in',  # noqa
        extended_abstract_pending__0__paragraph__value='Extended Abstract under Review',  # noqa
        extended_abstract_accepted__0__paragraph__value='Extended Abstract accepted',  # noqa
        extended_abstract_rejected__0__paragraph__value='Extended Abstract rejected',  # noqa
        registered_checked_in__0__paragraph__value='You have checked in',
    )

    register_form_page = home.factories.RegisterAndAbstractFormPage.create(
        parent=registration_page,
        intro__0__paragraph__value='Hello Form',
        form='home.views.onpage.registration.NewRegistration'
    )

    abstract_form_page = home.factories.RegisterAndAbstractFormPage.create(
        parent=registration_page,
        intro__0__paragraph__value='Hello Abstract Form',
        form='home.views.onpage.registration.NewAbstract',
        title='Abstract Form Page'
    )

    ex_abstract_form_page = home.factories.RegisterAndAbstractFormPage.create(
        parent=registration_page,
        intro__0__paragraph__value='Hello Extended Abstract Form',
        form='home.views.onpage.registration.NewExtendedAbstract',
        title='Extended Abstract Form Page'
    )

    old_content = registration_page \
        .registration_open_not_registered
    x = home.factories.registration.LinkToFormBlockFactory(
        form_page=register_form_page,
        button_caption='Register here'
    )

    old_content.append(('link_to_form', x))

    registration_page.registration_open_not_registered = old_content

    old_content = registration_page \
        .abstract_submission_open_not_submitted
    x = home.factories.registration.LinkToFormBlockFactory(
        form_page=abstract_form_page,
        button_caption='Submit abstract here'
    )

    old_content.append(('link_to_form', x))

    registration_page.abstract_submission_open_not_submitted = old_content

    old_content = registration_page \
        .extended_abstract_submission_open_not_submitted
    x = home.factories.registration.LinkToFormBlockFactory(
        form_page=ex_abstract_form_page,
        button_caption='Submit extended abstract here'
    )

    old_content.append(('link_to_form', x))

    registration_page.extended_abstract_submission_open_not_submitted = old_content  # noqa

    registration_page.save()

    management_base_page = home.factories.ManagementBasePage.create(
        parent=default_data['conference_page']
    )

    registration_management_page = \
        home.factories.RegistrationManagementPage.create(
            parent=management_base_page
        )

    abstract_management_page = home.factories.AbstractManagementPage.create(
        parent=management_base_page
    )

    extended_abstracts_management_page = (
        home.factories.ExtendedAbstractManagementPage.create(
            parent=management_base_page
        ))

    email_participants_page = (
        home.factories.EmailParticipantsPage.create(
            parent=management_base_page
        )
    )

    review_base_page = home.factories.ReviewParentPage.create(
        parent=default_data['conference_page']
    )

    review_abstracts_page = home.factories.ReviewAbstractsPage.create(
        parent=review_base_page
    )

    review_extended_abstracts_page = \
        home.factories.ReviewExtendedAbstractsPage.create(
            parent=review_base_page
        )

    refund_policy_page = home.factories.RefundPolicyPage.create(
        parent=default_data['conference_page'],
        body__0__paragraph__value='No refunds given',
    )

    privacy_policy_page = home.factories.PrivacyPolicyPage.create(
        parent=default_data['conference_page'],
        body__0__paragraph__value='We steal all your data',
    )

    abstract_overview_page = home.factories.AbstractsPage.create(
        parent=default_data['conference_page']
    )

    checkin_page = home.factories.CheckinPage(
        parent=default_data['conference_page']
    )

    default_data['registration_page'] = registration_page
    default_data['form_pages'] = {
        'register': register_form_page,
        'abstract': abstract_form_page,
        'extended_abstract': ex_abstract_form_page
    }
    default_data['registration_management_page'] = registration_management_page
    default_data['abstract_management_page'] = abstract_management_page
    default_data['extended_abstracts_management_page'] = extended_abstracts_management_page  # noqa
    default_data['management_base_page'] = management_base_page
    default_data['email_participants_page'] = email_participants_page
    default_data['review_base_page'] = review_base_page
    default_data['review_pages'] = {
        'abstracts': review_abstracts_page,
        'extended_abstracts': review_extended_abstracts_page
    }
    default_data['refund_policy_page'] = refund_policy_page
    default_data['privacy_policy_page'] = privacy_policy_page
    default_data['abstract_overview_page'] = abstract_overview_page
    default_data['checkin_page'] = checkin_page

    yield default_data


@pytest.fixture(scope='function')
def conference_user(samba):
    faker = Faker()

    password = faker.password()
    user = home.factories.UserWithConfirmedEmail(raw_password=password)

    user_info = {
        'email': user.email,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'password': password
    }

    samba.login(user.email, password)

    yield {
        'user_info': user_info,
        'user': user
    }


@pytest.fixture(scope='function', autouse=True)
def delete_wagtail_stuff(transactional_db):
    wagtail.core.models.Site.objects.all().delete()
    home.models.IndexPage.objects.all().delete()
    wagtail.core.models.Page.objects.all().delete()

    wagtail.core.models.Locale.objects.get_or_create(
        language_code=get_supported_content_language_variant(settings.LANGUAGE_CODE),
    )

    root = wagtail.core.models.Page.objects.create(
        title="Root",
        slug='root',
        path='0001',
        depth=1,
        numchild=0,
        url_path='/',
    )


@pytest.fixture(scope='function', autouse=True)
def add_initial_data(transactional_db):
    home.misc.studyfieldandmodel.add_studyfield_data(
        home.models.StudyField,
        100
    )
    home.misc.studyfieldandmodel.add_studymethod_data(
        home.models.StudyMethod,
        100
    )

    home.misc.ror.add_ror_data(home.models.Affiliation, 100)


@pytest.fixture()
def profile(request):
    PROFILE_ROOT = Path('.profiles')
    # Turn profiling on
    profiler = Profiler(interval=0.001)
    profiler.start()

    yield  # Run test

    profiler.stop()
    PROFILE_ROOT.mkdir(exist_ok=True)
    results_file = PROFILE_ROOT / f"{request.node.name}.html"
    with open(results_file, "w", encoding="utf-8") as f_html:
        f_html.write(profiler.output_html())



