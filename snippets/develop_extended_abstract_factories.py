import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "samba_web.settings")

import django
django.setup()
import home.factories
import wagtail_factories
import home.factories.abstracts

from django import test
test.utils.setup_test_environment()
from django.db import connection
db = connection.creation.create_test_db()


hp = home.factories.IndexPageFactory.create(
    title='Index Page',
    body__0__paragraph__value='I am a paragraph!',
)
site = wagtail_factories.SiteFactory(root_page=hp,
                                     is_default_site=True)

conference = home.factories.ConferenceFactory.create(
    parent=hp,
    body__0__paragraph__value='I am a paragraph!',
)

extended_abstract = home.factories.abstracts.ExtendedAbstract.create(
    parent_conference=conference,
    create_registration_and_pay=True
)
