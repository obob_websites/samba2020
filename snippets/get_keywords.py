import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "samba_web.settings")

import django
django.setup()

from django.conf import settings

import requests
from bs4 import BeautifulSoup as bs
import csv

url = 'https://www.neuroscience.cam.ac.uk/directory/keyword.php?n=1'
r = requests.get(url)

soup = bs(r.text, 'html5lib')

all_links = soup.find('div', 'invisibox').find_all('a')[1:]

keywords = [l.text for l in all_links]

with open(os.path.join(
            os.path.dirname(settings.PROJECT_DIR),
            'home/data/keywords.csv'), 'w') as f:
    writer = csv.writer(f)
    for key in keywords:
        writer.writerow([key])


