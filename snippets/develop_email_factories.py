import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "samba_web.settings")

import django
django.setup()
import home.factories
import wagtail_factories

from django import test
test.utils.setup_test_environment()
from django.db import connection
db = connection.creation.create_test_db()


hp = home.factories.IndexPageFactory.create(
    title='Index Page',
    body__0__paragraph__value='I am a paragraph!',
)
site = wagtail_factories.SiteFactory(root_page=hp,
                                     is_default_site=True)

email = home.factories.EmailFactory.create(
        subject='TestSubject',
        body='Test Body'
    )

email_settings = home.factories.EmailsSiteSettings.create(
    site=site,
    email_address_confirmed=email
)

email2 = home.factories.EmailFactory.create()

#print(email_settings.email_address_confirmed)

