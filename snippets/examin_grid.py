import bz2
import json

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "samba_web.settings")

import django
django.setup()

from django.conf import settings

from django import test
test.utils.setup_test_environment()
from django.db import connection
db = connection.creation.create_test_db()

import home.models
from home.misc.grid import add_grid_data

add_grid_data(home.models.Affiliation)

