
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import tempfile
import os
from pathlib import Path

cur_tmp_folder = tempfile.mkdtemp()
cache_tmp_folder = Path(tempfile.gettempdir()) / 'samba_db'

os.environ['TEST_BASE_DIR'] = os.path.join(cur_tmp_folder, 'samba')
Path(os.environ['TEST_BASE_DIR']).mkdir(parents=True, exist_ok=True)

from ..settings import *  # noqa

INSTALLED_APPS += ['extra.expose_to_template.tests.expose_to_template_test_app']  # noqa

os.symlink(os.path.join(PROJECT_DIR, '..', 'node_modules'), os.path.join(BASE_DIR, 'node_modules'))  # noqa

cache_tmp_folder.mkdir(exist_ok=True)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': str(Path(cache_tmp_folder, 'testdb.sqlite3')),
        'TEST': {
            'NAME': str(Path(cache_tmp_folder, 'testdb.sqlite3'))
        }
    }
}
