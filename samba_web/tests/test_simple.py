
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import pytest
import datetime
import wagtail_factories
from django.utils.formats import date_format

import home.factories


@pytest.mark.django_db
def test_access_index(samba, admin_user):

    hp = home.factories.IndexPageFactory.create(
        title='Index Page',
        body__0__paragraph__value='I am a paragraph!',
    )
    wagtail_factories.SiteFactory(root_page=hp,
                                  is_default_site=True)

    samba.selenium.open(samba.live_server + '/')

    samba.selenium.assert_text('Index Page')
    samba.selenium.assert_text('I am a paragraph!')


@pytest.mark.django_db
def test_make_index_with_faker(samba, admin_user):
    samba.login_admin()

    factory_kwargs = dict(
        title='My Page',
        body__0__paragraph__dummy=None,
        body__1__paragraph__dummy=None,
    )

    hp = home.factories.IndexPageFactory.create(**factory_kwargs)
    hp_object = hp
    wagtail_factories.SiteFactory(root_page=hp,
                                  is_default_site=True)

    for do_logout in (False, True):
        if do_logout:
            samba.logout()
        else:
            samba.selenium.open(samba.live_server + '/admin')
            samba.selenium.assert_text('Welcome to')

        samba.selenium.open(samba.live_server + '/')

        samba.assert_streamfield_blocks_present(hp_object.body)


@pytest.mark.django_db
@pytest.mark.parametrize('factory_class', [
    home.factories.NormalPageFactory,
    home.factories.ConferenceFactory,
    home.factories.SpeakerIndexFactory])
def test_forbidden_pages_under_root(samba, admin_user, factory_class):
    samba.login_admin()

    object = factory_class.build()
    add_url = factory_class.get_add_url(object, None)

    samba.selenium.open(samba.live_server + add_url)

    samba.selenium.assert_text(
        'Sorry, you do not have permission to access this area.')


@pytest.mark.django_db
@pytest.mark.parametrize('factory_class', [
    home.factories.NormalPageFactory,
    home.factories.ConferenceFactory
])
def test_add_pages_under_index(samba, admin_user,
                               factory_class):
    samba.login_admin()

    conf_start_date = datetime.date(2020, 9, 8)

    index_factory_kwargs = dict(
        title='My Page',
        body__0__paragraph__dummy=None,
        body__1__paragraph__dummy=None,
    )

    sub_pages_factory_kwargs = {
        home.factories.NormalPageFactory: {
            'title': 'My Sub Page',
            'body__0__paragraph__dummy': None
        },
        home.factories.ConferenceFactory: {
            'start_date': conf_start_date,
            'body__0__paragraph__value': 'Welcome to {{ conference_title }}. '
                                         'From: {{ start_date }} to:'
                                         ' {{ end_date }}'
        }

    }

    assert_in_body = {
        home.factories.ConferenceFactory:
            'Welcome to ChaCha 2020. '
            'From: %s to: %s' % (
                date_format(conf_start_date),
                date_format(conf_start_date + datetime.timedelta(3)))
    }

    cur_subpage_factory_kwargs = sub_pages_factory_kwargs[factory_class]

    index_page = home.factories.IndexPageFactory.create(
        **index_factory_kwargs)

    wagtail_factories.SiteFactory(root_page=index_page,
                                  is_default_site=True)

    sub_page = factory_class.create(
        parent=index_page,
        **cur_subpage_factory_kwargs)

    sub_page_obj = sub_page

    samba.selenium.open(samba.live_server + sub_page.url)

    for cur_block in sub_page_obj.body:
        if factory_class in assert_in_body:
            text_to_assert = assert_in_body[factory_class]
        else:
            text_to_assert = cur_block.value
            text_to_assert = getattr(text_to_assert, 'source', text_to_assert)

        samba.selenium.assert_text(text_to_assert)

    print('done')
