#  Copyright (c) 2016-2021, Thomas Hartmann
#
#  This file is part of the SAMBA2020 Project
#  (https://gitlab.com/obob_websites/samba2020)
#
#  SAMBA2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SAMBA2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SAMBA2020. If not, see <http://www.gnu.org/licenses/>.
from django.core.management import call_command


def test_about_page(samba):
    call_command('get_js_packages')
    call_command('get_pypi_packages')
    samba.goto_url('/samba_home_misc/about/')
    samba.selenium.assert_text('Django')
    samba.selenium.assert_text('gitpython')
    samba.selenium.assert_text('bootstrap-cookie-alert')
    samba.selenium.assert_text_not_visible(
        'Sorry, there seems to be an error. Please try again soon.'
    )
