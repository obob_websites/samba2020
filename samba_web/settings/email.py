
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import os

EMAIL_HOST = os.getenv('SAMBA_EMAIL_HOST', None)
DEFAULT_FROM_EMAIL = os.getenv('SAMBA_FROM_EMAIL', 'root@localhost')
SERVER_EMAIL = os.getenv('SAMBA_SERVER_EMAIL', DEFAULT_FROM_EMAIL)

if EMAIL_HOST is None:
    EMAIL_LOG_BACKEND = 'django.core.mail.backends.console.EmailBackend'
else:
    EMAIL_LOG_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_BACKEND = 'email_log.backends.EmailBackend'
