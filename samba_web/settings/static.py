# Copyright (c) 2016-2019, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project,
# see: https://gitlab.com/obob/obob_subjectdb/
#
#    samba2020 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    samba2020 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import os
from pathlib import Path

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(PROJECT_DIR, "static"),
    Path(os.path.dirname(PROJECT_DIR), 'webpack_assets'),
    Path(os.path.dirname(PROJECT_DIR), 'node_modules/@fortawesome/fontawesome-free/svgs')
]

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

STATIC_ROOT = os.getenv("SAMBA_STATIC_DIR",
                        os.path.join(PERSISTENT_DIR, 'static'))

if not os.path.exists(STATIC_ROOT):
    os.makedirs(STATIC_ROOT)
