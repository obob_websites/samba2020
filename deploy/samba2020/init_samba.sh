#!/usr/bin/env bash

# Copyright (c) 2019-2020, Thomas Hartmann
#
# This file is part of the SAMBA2020 Project, see: https://gitlab.com/obob_websites/samba2020
#
#    samba2020 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    samba2020 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with samba. If not, see <http://www.gnu.org/licenses/>.

if ! [ -f $SAMBA_KEY_DIR/samba.crt ] && ! [ -f $SAMBA_KEY_DIR/samba.key ]; then
    openssl req -x509 -newkey rsa:2048 -passout pass:none -out $SAMBA_KEY_DIR/samba.crt -keyout $SAMBA_KEY_DIR/samba.key -subj '/CN=www.test.com'
    openssl rsa -in $SAMBA_KEY_DIR/samba.key -out $SAMBA_KEY_DIR/samba.key -passin pass:none
fi

if ! [ -f $SAMBA_KEY_DIR/django_secret_key ]; then
    openssl rand -base64 50 | tr -d '\n' > $SAMBA_KEY_DIR/django_secret_key
fi

if ! [ -f $SAMBA_KEY_DIR/dhparam.pem ]; then
    openssl dhparam -out $SAMBA_KEY_DIR/dhparam.pem 2048
fi

./manage.py migrate && ./manage.py crontab add