
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import time

from dal import autocomplete
from django.contrib.contenttypes.models import ContentType
from django.urls import reverse
from django import forms
from django.forms.models import model_to_dict
from factory.base import OptionDefault
from factory.django import DjangoOptions
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.core import models
from wagtail.core.blocks import field_block
from itertools import chain


class CreateInSeleniumMixinFactoryOptions(DjangoOptions):
    def _build_default_options(self):
        options = super()._build_default_options()
        options.append(
            OptionDefault('do_not_auto_handle', [], inherit=True)
        )

        return options


class CreateInSeleniumMixin(object):

    _options_class = CreateInSeleniumMixinFactoryOptions

    @classmethod
    def create_in_browser(cls, samba, *args, **kwargs):
        obj = cls.get_template_object(*args, **kwargs)
        form = cls.get_form(*args, **kwargs)
        samba.goto_url(cls.get_create_url(*args, **kwargs))

        submit_field_str = ''
        for cur_field_str in form._meta.fields:
            if hasattr(cls._meta, 'do_not_auto_handle'):
                if cur_field_str in cls._meta.do_not_auto_handle:
                    continue
            cur_field = form.base_fields[cur_field_str]
            if not submit_field_str:
                submit_field_str = cur_field_str

            if isinstance(cur_field, forms.BooleanField):
                cls.handle_booleanfield(cur_field_str, obj, samba)
            elif isinstance(cur_field.widget, (autocomplete.ModelSelect2,
                                               autocomplete.ModelSelect2Multiple)):  # noqa
                continue
            elif isinstance(cur_field, forms.ChoiceField):
                cls.handle_choicefield(cur_field_str, obj, samba)
            elif isinstance(cur_field, forms.FileField):
                cls.handle_filefield(cur_field_str, obj, samba)
            else:
                cls.handle_defaultfield(cur_field_str, obj, samba)

        cls.handle_further_fields(samba, obj, *args, **kwargs)

        samba.selenium.submit('#id_%s' % (submit_field_str, ))

        n_retries = 5
        t_sleep = 0.5

        created_object = None

        for retry in range(n_retries):
            try:
                created_object = cls._meta.model.objects.order_by('-id')[0]
                break
            except IndexError:
                time.sleep(t_sleep)

        return created_object

    @classmethod
    def get_template_object(cls, *args, **kwargs):
        return model_to_dict(cls.build(*args, **kwargs))

    @classmethod
    def get_create_url(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def get_form(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def handle_defaultfield(cls, cur_field_str, obj, samba):
        samba.selenium.update_text('#id_%s' % (cur_field_str, ),
                                   obj[cur_field_str])

    @classmethod
    def handle_booleanfield(cls, cur_field_str, obj, samba):
        el = samba.selenium.get_element('#id_%s' % (cur_field_str, ))
        if el.is_selected() != obj[cur_field_str]:
            samba.selenium.js_click('#id_%s' % (cur_field_str, ))

    @classmethod
    def handle_choicefield(cls, cur_field_str, obj, samba):
        samba.selenium.select_option_by_value(
            '#id_%s' % (cur_field_str, ),
            obj[cur_field_str]
        )

    @classmethod
    def handle_filefield(cls, cur_field_str, obj, samba):
        cur_file = obj[cur_field_str]
        file_input = samba.selenium.get_element(cur_field_str,
                                                by=By.NAME)
        file_input.send_keys(cur_file.file.name)

    @classmethod
    def handle_further_fields(cls, samba, obj, *args, **kwargs):
        pass

    @staticmethod
    def to_dict(instance):
        opts = instance._meta
        data = {}
        for f in chain(opts.concrete_fields, opts.private_fields):
            data[f.name] = f.value_from_object(instance)
        for f in opts.many_to_many:
            data[f.name] = [i.id for i in f.value_from_object(instance)]
        for f in opts.related_objects:
            if f.one_to_many:
                val = getattr(instance, '%s_set' % (f.name, )).all()
                data[f.name] = [CreateInSeleniumMixin.to_dict(i) for i in val]
        return data


class WagtailCreateInSeleniumMixin(object):
    @classmethod
    def get_add_url(cls, object, parent):
        if parent is None:
            parent = models.Page.objects.get(slug='root')

        content_type_for_obj = ContentType.objects.get_for_model(object)

        content_type_app_name = content_type_for_obj.app_label
        content_type_name = content_type_for_obj.model

        return reverse(
            'wagtailadmin_pages:add',
            args=[
                content_type_app_name,
                content_type_name,
                parent.id,
            ])

    @classmethod
    def create_in_browser(cls, samba, *args, return_created_object=False,
                          **kwargs):
        object = cls.build(*args, **kwargs)

        parent = kwargs.get('parent', None)

        samba.selenium.open(samba.live_server + cls.get_add_url(object,
                                                                parent))

        for cur_cpanel in object.content_panels:
            if isinstance(cur_cpanel, FieldPanel):
                cls._handle_streamfieldpanel(samba, cur_cpanel, object)
            else:
                val = getattr(object, cur_cpanel.field_name)
                if val is not None:
                    raise NotImplementedError('Not Implemented right now!')

        time.sleep(0.3)
        samba.selenium.click('footer > nav > ul > li.actions > div > div')
        samba.selenium.click(
            'footer > nav > ul > li.actions > div > ul > '
            'li:nth-child(1) > button[name="action-publish"]')
        samba.selenium.wait_for_element_visible(
            'div > main > div > div.messages > ul > li')

        n_retries = 5
        t_sleep = 0.5

        for retry in range(n_retries):
            try:
                created_page = models.Page.objects.filter(
                    title=object.title).order_by('id')[0]
                break
            except IndexError:
                time.sleep(t_sleep)

        if return_created_object:
            return created_page, object
        else:
            return created_page

    @classmethod
    def _handle_streamfieldpanel(cls, samba, cpanel, object):
        all_fieldsets = samba.selenium.find_elements('fieldset')
        cur_field_name = cpanel.field_name
        content_to_insert = getattr(object, cur_field_name)

        only_one_block = len(
            getattr(object, cur_field_name).stream_block.child_blocks) == 1

        for cur_fieldset in all_fieldsets:
            try:
                cur_fieldset.find_element_by_css_selector(
                    'input#%s-count' % (cur_field_name,)
                )

                for idx, cur_content in enumerate(content_to_insert):
                    if not only_one_block or idx == 0:
                        block_type = cur_content.block_type
                        btn_css = 'button.action-add-block-%s' % (block_type,)
                        btn = cur_fieldset.find_elements_by_css_selector(
                            btn_css)[-1]
                        WebDriverWait(samba.selenium.driver, 10).until(
                            lambda x: btn.is_displayed)
                        btn.location_once_scrolled_into_view
                        samba.selenium.driver.execute_script(
                            'arguments[0].click();',
                            btn)

                    if isinstance(cur_content.block, field_block.TextBlock):
                        cls._handle_textblock(samba,
                                              cur_field_name,
                                              idx,
                                              cur_content)
                    elif isinstance(cur_content.block,
                                    field_block.RichTextBlock):
                        cls._handle_richtextblock(samba,
                                                  cur_field_name,
                                                  idx,
                                                  cur_content)

                    add_element_css = 'button#%s-%d-appendmenu-openclose' % (
                        cur_field_name,
                        idx)

                    if not only_one_block or \
                            idx < (len(content_to_insert) - 1):
                        samba.selenium.show_element(add_element_css)
                        samba.selenium.js_click(add_element_css)

                    if not only_one_block:
                        new_chooser_css = \
                            '#%s-%d-appendmenu div.c-sf-add-panel' % (
                                cur_field_name,
                                idx)

                        samba.selenium.wait_for_element_visible(
                            new_chooser_css)

            except NoSuchElementException:
                pass
        pass

    @classmethod
    def _handle_textblock(cls, samba, field_name, idx, content):
        inputfield_css = 'textarea#%s-%d-value' % (field_name, idx)
        samba.selenium.update_text(inputfield_css, content.value)

        return inputfield_css

    @classmethod
    def _handle_richtextblock(cls, samba, field_name, idx, content):
        inputfield_css = '#%s-%d-container ' % (field_name, idx)
        inputfield_css += 'div.DraftEditor-root'
        where_to_type_css = inputfield_css + ' .public-DraftEditor-content'

        samba.selenium.click(inputfield_css, delay=0.3)

        samba.selenium.update_text(where_to_type_css, content.value.source,
                                   retry=True)

        return inputfield_css

    @classmethod
    def _handle_fieldpanel(cls, samba, cpanel, object):
        all_fieldsets = samba.selenium.find_elements('fieldset')
        cur_field_name = cpanel.field_name
        content_to_insert = getattr(object, cur_field_name)

        # try to find correct fieldset
        for cur_fieldset in all_fieldsets:
            try:
                result = cur_fieldset.find_element_by_css_selector(
                    'input#id_%s' % (cur_field_name,))

                result.send_keys(str(content_to_insert))
                break
            except NoSuchElementException:
                pass
