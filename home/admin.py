
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from dal import autocomplete
from django.contrib import admin
from django.forms import ModelForm, ModelMultipleChoiceField, ModelChoiceField
from django.urls import reverse
from queryable_properties.admin import QueryablePropertiesAdmin

from . import models


class AbstractAuthorForm(ModelForm):
    class Meta:
        model = models.AbstractAuthor
        fields = '__all__'

    affiliations = ModelMultipleChoiceField(
        queryset=models.Affiliation.objects.all(),
        widget=autocomplete.ModelSelect2Multiple()
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['affiliations'].widget.url = reverse(
            'affiliation-complete')


class RegistrationForm(ModelForm):
    class Meta:
        model = models.Registration
        fields = '__all__'

    affiliation = ModelChoiceField(
        queryset=models.Affiliation.objects.all(),
        widget=autocomplete.ModelSelect2()
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['affiliation'].widget.url = reverse(
            'affiliation-complete'
        )


class AbstractAuthorAdmin(admin.TabularInline):
    model = models.AbstractAuthor
    form = AbstractAuthorForm


class AbstractAdmin(admin.ModelAdmin):
    inlines = [AbstractAuthorAdmin]
    search_fields = ['user__first_name',
                     'user__last_name',
                     'title']
    list_filter = ['conference',
                   'review_status']


class PaymentAdmin(admin.TabularInline):
    model = models.Payment


class RegistrationAdmin(QueryablePropertiesAdmin):
    model = models.Registration
    form = RegistrationForm
    inlines = [PaymentAdmin]

    list_filter = [
        'conference',
        'payment_status'
    ]
    search_fields = ['user__first_name',
                     'user__last_name']


class AffiliationForm(ModelForm):
    class Meta:
        model = models.Affiliation
        fields = '__all__'

    parent = ModelChoiceField(
        queryset=models.Affiliation.objects.all(),
        widget=autocomplete.ModelSelect2(),
        required=False
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['parent'].widget.url = reverse(
            'affiliation-complete'
        )


class AffiliationAdmin(admin.ModelAdmin):
    model = models.Affiliation
    form = AffiliationForm
    search_fields = ['institution_primary_name',
                     'affiliationalternativename__affiliation__institution_primary_name']  # noqa
    list_display = ['string_for_dal',
                    'city',
                    'country']

    def get_search_results(self, request, queryset, search_term):
        qs = models.Affiliation.objects.filter_by_names_and_acronyms(search_term)  # noqa
        return qs, False


admin.site.register(models.ExtendedAbstract)
admin.site.register(models.Registration, RegistrationAdmin)
admin.site.register(models.Abstract, AbstractAdmin)
admin.site.register(models.Affiliation, AffiliationAdmin)
