$(() => {
    const $previewDiv = $('.preview-panel');
    const $form = $('#page-edit-form');
    const previewUrl = $previewDiv.data('action');
    console.log(previewUrl);
    let autoUpdatePreviewDataTimeout = -1;

    function setPreviewData() {
        return $.ajax({
            url: previewUrl,
            method: 'POST',
            data: new FormData($form[0]),
            processData: false,
            contentType: false
        });
    }

    const payment_preview_button = $('.payment-conf-preview ');
    const payment_preview_url = payment_preview_button.data('preview-url')

    payment_preview_button.on('click', function(e) {
        e.preventDefault();
        setPreviewData().done((data) => {
            const previewWindow = window.open('', payment_preview_url);
            previewWindow.focus();
            previewWindow.document.location = payment_preview_url;
        }).fail(() => {
            console.log('something went wrong');
        });
    });

    const participation_preview_button = $('.participation-conf-preview ');
    const participation_preview_url = participation_preview_button.data('preview-url')

    participation_preview_button.on('click', function(e) {
        e.preventDefault();
        setPreviewData().done((data) => {
            const previewWindow = window.open('', participation_preview_url);
            previewWindow.focus();
            previewWindow.document.location = participation_preview_url;
        }).fail(() => {
            console.log('something went wrong');
        });
    });
});