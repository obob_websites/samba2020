#  samba2020 - Webpage for https://samba.ccns.sbg.ac.at
#  Copyright (C) 2022. Thomas Hartmann <thomas.hartmann@th-ht.de>
#
#  This file is part of samba2020.
#
#  samba2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  samba2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
import re

from django.template import Library
from django.template.defaultfilters import stringfilter
from django.utils.functional import keep_lazy_text
from django.utils.html import escape
from django.utils.safestring import SafeData, mark_safe
from django.utils.text import normalize_newlines


register = Library()


@keep_lazy_text
def linebreaks_to_para(value, autoescape=False):
    """Convert newlines into <p> and <br>s."""
    value = normalize_newlines(value)
    paras = re.split('\n{1,}', str(value))
    if autoescape:
        paras = ['<p>%s</p>' % escape(p) for p in paras]
    else:
        paras = ['<p>%s</p>' % p for p in paras]
    return '\n\n'.join(paras)


@register.filter("linebreaks_to_p", is_safe=True, needs_autoescape=True)
@stringfilter
def linebreaks_to_p_filter(value, autoescape=True):
    """
    Replace line breaks in plain text with appropriate HTML; a single
    newline becomes an HTML line break (``<br>``) and a new line
    followed by a blank line becomes a paragraph break (``</p>``).
    """
    autoescape = autoescape and not isinstance(value, SafeData)
    return mark_safe(linebreaks_to_para(value, autoescape))
