
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from classytags.core import Options, Tag
from classytags.helpers import InclusionTag
from classytags.arguments import Argument
from django import template

register = template.Library()


class RenderRegistrationOverviewBodyBlockList(InclusionTag):
    name = 'render_registration_bodyblocklist'
    options = Options(
        Argument('blocklist', required=True)
    )

    template = 'samba_home/templatetags/render_registration_blocklist.html'

    def get_context(self, context, blocklist):
        context['should_render'] = blocklist.should_render(context)
        context['blocklist'] = blocklist

        return context


class RenderRegistrationOverviewBlock(InclusionTag):
    name = 'render_registration_block'
    options = Options(
        Argument('block', required=True),
    )

    template = 'samba_home/templatetags/render_registration_block.html'

    def get_context(self, context, block):
        context['should_render'] = block.stream_block.final_display(context)

        return context


class RenderDetailObject(Tag):
    name = 'render_detail_object'
    options = Options(
        Argument('object', required=True),
    )

    def render_tag(self, context, object):
        return str(object.render(context))


register.tag(RenderRegistrationOverviewBlock)
register.tag(RenderDetailObject)
register.tag(RenderRegistrationOverviewBodyBlockList)
