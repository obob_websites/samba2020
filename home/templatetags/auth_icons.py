
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from classytags.core import Options
from classytags.helpers import InclusionTag
from classytags.arguments import Argument
from django import template
from django.template.loader import select_template

register = template.Library()


class RenderAuthIconOrText(InclusionTag):
    name = 'render_auth_icon_or_text'
    options = Options(
        Argument('provider', required=True)
    )

    def get_context(self, context, provider):
        context['provider'] = provider

        return context

    def get_template(self, context, provider):
        template_prefix = 'samba_home/templatetags/auth_icons/'
        template = select_template(
            [
                '%s%s.html' % (template_prefix, provider.name),
                '%sdefault.html' % (template_prefix, )
            ]
        )

        return template.origin.template_name


register.tag(RenderAuthIconOrText)
