
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail.core import hooks
from django.urls import reverse

from ..models import SambaMainMenuItem


@hooks.register('menus_modify_primed_menu_items', order=-1)
def add_login_logout_item(menu_items, request, parent_page,
                          original_menu_tag, menu_instance,
                          current_level, **kwargs):
    if current_level == 1:
        if request.user.is_authenticated:
            menu_items.append({
                'href': '%s?next=%s' % (reverse('account_logout'),
                                        request.path),
                'text': 'Logout',
                'show_in_menu': True
            })
        else:
            menu_items.append({
                'href': '%s?next=%s' % (reverse('account_login'),
                                        request.path),
                'text': 'Login',
                'show_in_menu': True
            })

    return menu_items


@hooks.register('menus_modify_primed_menu_items')
def add_request_object(menu_items, request, *args, **kwargs):
    for item in menu_items:
        if isinstance(item, SambaMainMenuItem):
            item.request = request

    return menu_items
