
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail.images.formats import (Format,
                                    register_image_format,
                                    unregister_image_format)
from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe
from bs4 import BeautifulSoup
from datauri import DataURI


class PDFFormat(Format):
    def image_to_html(self, image, *args, **kwargs):
        tag = super().image_to_html(image, *args, **kwargs)
        uri = DataURI.from_file(image.file.file.name)
        tag_soup = BeautifulSoup(tag, 'html5lib').img

        tag_soup.attrs['src'] = uri
        return mark_safe(str(tag_soup))


unregister_image_format('fullwidth')
unregister_image_format('left')
unregister_image_format('right')

register_image_format(Format(
    'fullwidth',
    _('Full width'),
    'richtext-image full-width img-fluid mx-auto d-block',
    'width-900'))

register_image_format(Format(
    'left',
    _('Left-aligned'),
    'richtext-image left img-fluid d-block',
    'width-500'))

register_image_format(PDFFormat(
    'pdf',
    'PDF',
    'richtext-image img-fluid d-block',
    'width-800'
))
