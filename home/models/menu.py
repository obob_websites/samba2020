
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import FieldPanel
from wagtailmenus.models import AbstractMainMenuItem


class SambaMainMenuItem(AbstractMainMenuItem):
    menu = ParentalKey(
        'wagtailmenus.MainMenu',
        on_delete=models.CASCADE,
        related_name='samba_menu_items'
    )

    exclude_from_menu_when_not_authorized = models.BooleanField(default=False)

    panels = AbstractMainMenuItem.panels + [
        FieldPanel('exclude_from_menu_when_not_authorized')
    ]

    def _can_user_view_page(self, page, request):
        for restriction in page.get_view_restrictions():
            if not restriction.accept_request(request):
                return False

        return True

    def show_in_menu(self):
        if self.link_page and self.exclude_from_menu_when_not_authorized:
            show = self._can_user_view_page(self.link_page, self.request)
            if hasattr(self.link_page.specific, 'show_in_menu_test_func'):
                show = show and self.link_page.specific.show_in_menu_test_func(
                    self.request)

            return show

        return True
