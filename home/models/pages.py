
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.http import HttpResponse
from wagtail.admin.edit_handlers import FieldPanel, PageChooserPanel

from extra.expose_to_template.mixins import ExposeToTemplatePageMixin
from . import SambaPage
from home.models.mixins import NotRootMixin, RestrictMenuToGroupMixin
from home.models.utils import get_default_streamfield
from django.db import models
from django.shortcuts import redirect


class IndexPage(SambaPage):
    body = get_default_streamfield(blank=True)
    redirect_to_conference = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
    )

    content_panels = SambaPage.content_panels + [
        FieldPanel('body'),
        PageChooserPanel('redirect_to_conference',
                         ['samba_home.Conference'])
    ]

    show_in_menus = True
    show_in_menus_default = True
    parent_page_types = ['wagtailcore.Page']
    max_count_per_parent = 1
    subpage_types = ['samba_home.NormalPage',
                     'samba_home.Conference',
                     'samba_home.PreviousConferencesPage']

    def serve(self, request, *args, **kwargs):
        if self.redirect_to_conference is None:
            return super().serve(request, *args, **kwargs)

        raw_url = self.redirect_to_conference.url_path
        if not raw_url.startswith('/'):
            raw_url = '/' + raw_url

        redirect_url = raw_url.replace(self.url_path, '/')
        return redirect(redirect_url)


class NormalPage(NotRootMixin, ExposeToTemplatePageMixin, SambaPage):
    body = get_default_streamfield()

    content_panels = SambaPage.content_panels + [
        FieldPanel('body')
    ]


class PreviousConferencesPage(SambaPage):
    subpage_types = ['samba_home.Conference']
    parent_page_types = ['samba_home.IndexPage']
    max_count = 1

    def serve(self, request, *args, **kwargs):
        if not self.get_children().exists():
            return HttpResponse('Please add at least one subpage')
        else:
            return redirect(self.get_children()[0].url)


class SubPagesPage(RestrictMenuToGroupMixin, SambaPage):
    class Meta:
        abstract = True

    @staticmethod
    def can_user_view_page(page, request):
        for restriction in page.get_view_restrictions():
            if not restriction.accept_request(request):
                return False

        return True

    def serve(self, request, *args, **kwargs):
        for cur_page in self.get_children():
            cur_specific_page = cur_page.specific
            can_redirect = self.can_user_view_page(
                cur_specific_page,
                request)
            if hasattr(cur_page, 'show_in_menu_test_func'):
                can_redirect = can_redirect and \
                    cur_specific_page.show_in_menu_test_func(request)

            if can_redirect:
                return redirect(cur_specific_page.url)

        return HttpResponse('Please add at least one subpage')
