
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail.core.fields import StreamField

from extra.expose_to_template.blocks import ExposedRichTextBlock
from extra.blocks.blocks import CardBlock, BootstrapRowBlock
from wagtail.contrib.table_block.blocks import TableBlock


def get_default_streamfield(blank=False, ignore_tag=None):
    sf = StreamField(block_types=[
        ('paragraph', ExposedRichTextBlock(ignore_tag=ignore_tag)),
        ('table', TableBlock(table_options={
            'renderer': 'html',
        })),
        ('card', CardBlock(ignore_tag=ignore_tag)),
        ('row', BootstrapRowBlock(ignore_tag=ignore_tag))
    ],
        blank=blank)

    return sf
