# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
import datetime

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.functional import cached_property
from djmoney.models.validators import MinMoneyValidator

from home.signals import payment_status_changed
from .. import Affiliation
from ...modelfields import DefaultMoneyField
from queryable_properties.managers import QueryablePropertiesManager
from queryable_properties.properties import queryable_property
from sql_util.aggregates import SubquerySum
from sql_util.utils import Exists


class Registration(models.Model):
    objects = QueryablePropertiesManager()

    datetime_added = models.DateTimeField(auto_now_add=True)
    conference = models.ForeignKey('samba_home.Conference',
                                   on_delete=models.CASCADE)
    user = models.ForeignKey(get_user_model(),
                             on_delete=models.CASCADE)
    degree = models.CharField(max_length=255)
    affiliation = models.ForeignKey(Affiliation,
                                    on_delete=models.SET_NULL,
                                    null=True)
    has_read_refund_policy = models.BooleanField(default=False)
    consents_to_data_processing = models.BooleanField(default=False)
    pay_at_conference = models.BooleanField(default=False)

    send_first_payment_reminder_date = models.DateField(blank=True, null=True)
    send_second_payment_reminder_date = models.DateField(blank=True, null=True)
    unregister_date = models.DateField(blank=True, null=True)
    discount = DefaultMoneyField(default=0, validators=[
        MinMoneyValidator(0),
    ])
    ticket = models.ForeignKey('ConferenceTicket', on_delete=models.CASCADE)
    ticket_options = models.ManyToManyField('ConferenceTicketOption',
                                            blank=True,
                                            null=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.pk is not None:
            self.__last_payment_status = self.payment_status
        else:
            self.__last_payment_status = 'unknown'

    @queryable_property(annotation_based=True)
    @classmethod
    def total_amount(cls):
        return models.Case(
            models.When(
                Exists('ticket_options'),
                then=(SubquerySum('ticket__ticket_price') + SubquerySum('ticket_options__option_price') - models.F('discount'))  # noqa
            ),
            default=(
                SubquerySum('ticket__ticket_price') - models.F('discount')
            ),
            output_field=DefaultMoneyField()
        )

    @queryable_property(annotation_based=True)
    @classmethod
    def amount_paid(cls):
        return models.Case(
            models.When(Exists('payment'),
                        then=SubquerySum('payment__amount')),
            default=models.Value(0),
            output_field=DefaultMoneyField()
        )

    @queryable_property(annotation_based=True)
    @classmethod
    def fees_paid(cls):
        return models.Case(
            models.When(Exists('payment'), then=SubquerySum('payment__fee')),
            default=models.Value(0),
            output_field=DefaultMoneyField()
        )

    @queryable_property(annotation_based=True)
    @classmethod
    def amount_owed(cls):
        return models.ExpressionWrapper(
            models.functions.Round(models.F('total_amount') - models.F('amount_paid') + models.F('fees_paid'), 2),  # noqa
            output_field=DefaultMoneyField()
        )

    @queryable_property(annotation_based=True)
    @classmethod
    def payment_status(cls):
        return models.Case(
            models.When(
                models.lookups.Exact(models.F('total_amount'), 0),
                then=models.Value('complete')
            ),
            models.When(
                models.lookups.LessThanOrEqual(models.F('amount_owed'), 0),
                then=models.Value('complete')
            ),
            models.When(pay_at_conference=True,
                        then=models.Value('pay_at_conference')),
            default=models.Value('pending')
        )

    @queryable_property(annotation_based=True)
    def can_checkin(self):
        return models.Case(
            models.When(models.Q(
                payment_status='complete') & Exists(
                'checkin', negated=True), then=models.Value(True)),
            default=models.Value(False),
            output_field=models.BooleanField()
        )
        return self.payment_status == 'complete' and not hasattr(self, 'checkin')  # noqa

    @queryable_property(annotation_based=True)
    def display_for_checkin(self):
        return models.Case(
            models.When(
                models.Q(payment_status__in=('complete', 'pay_at_conference')) & Exists('checkin', negated=True),  # noqa
                then=models.Value(True)
            ),
            default=models.Value(False),
            output_field=models.BooleanField()
        )

    @cached_property
    def abstract(self):
        from .. import Abstract
        return Abstract.objects.filter(user=self.user,
                                       conference=self.conference).first()

    @cached_property
    def extended_abstract(self):
        from .. import ExtendedAbstract
        return ExtendedAbstract.objects.filter(
            base_abstract__user=self.user,
            base_abstract__conference=self.conference).first()

    @cached_property
    def management_page(self):
        from .. import RegistrationManagementPage
        management_page = (RegistrationManagementPage.objects
                           .descendant_of(self.conference)
                           .first())
        return management_page

    @cached_property
    def full_name(self):
        return '%s %s' % (self.user.first_name, self.user.last_name)

    @cached_property
    def is_checked_in(self):
        return hasattr(self, 'checkin')

    @property
    def is_checked_in_str(self):
        if self.is_checked_in:
            return 'Yes'
        else:
            return 'No'

    @queryable_property
    def ticket_options_for_html(self):
        return ', '.join([str(x) for x in self.ticket_options.all()])

    def clean(self):
        if not self.has_read_refund_policy:
            raise ValidationError('Refund policy must be accepted')

        if not self.consents_to_data_processing:
            raise ValidationError(
                'Consent to data processing must be provided')

    def save(self, *args, **kwargs):
        if self.pk is None:
            new_registration = True
        else:
            new_registration = False

        if new_registration:
            if Registration.objects.filter(
                    conference=self.conference, user=self.user).exists():
                raise ValidationError('Cannot register twice')
            if not self.conference.can_register_for_conference:
                raise ValidationError('Conference registration not possible')

            self._reset_payment_deadlines()

        super().save(*args, **kwargs)
        self.conference.participant_group.user_set.add(self.user)

        if self.total_amount.amount == 0 and new_registration:
            payment_status_changed.send(sender=self.__class__,
                                        registration=self,
                                        new_payment_status='complete')
        elif self.__last_payment_status != self.payment_status:
            self.__last_payment_status = self.payment_status
            payment_status_changed.send(sender=self.__class__,
                                        registration=self,
                                        new_payment_status=self.payment_status)

    def get_payment_status_display(self):
        status_dict = {
            'complete': 'Payment complete',
            'pending': 'Pending',
            'pay_at_conference': 'Pay at Conference'
        }

        return status_dict[self.payment_status]

    def _reset_payment_deadlines(self):
        today = datetime.date.today()

        if self.conference.payment_first_warning_after_n_days:
            self.send_first_payment_reminder_date = (
                today + datetime.timedelta(
                    self.conference.payment_first_warning_after_n_days)
            )

        if self.conference.payment_second_warning_after_n_days:
            self.send_second_payment_reminder_date = (
                today + datetime.timedelta(
                    self.conference.payment_second_warning_after_n_days)
            )

        if self.conference.payment_unregister_after_n_days:
            self.unregister_date = (today + datetime.timedelta(
                self.conference.payment_unregister_after_n_days))

    def check_payment_status_update(self, new_payment_status=None):
        if new_payment_status is None:
            new_payment_status = self.__last_payment_status
        if new_payment_status != self.payment_status:
            self.__last_payment_status = self.payment_status
            payment_status_changed.send(sender=self.__class__,
                                        registration=self,
                                        new_payment_status=self.payment_status)

    def __str__(self):
        return 'Registration for %s by %s %s' % (
            self.conference,
            self.user.first_name,
            self.user.last_name
        )
