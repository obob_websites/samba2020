
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

class BodyBlockList(object):
    def __init__(self, *blocks, heading=None, render_condition=None):
        self.heading = heading
        self.blocks = blocks
        self.prepared_blocks = None
        self.page = None
        self.render_condition = render_condition

    def should_render(self, context):
        if self.prepared_blocks is None:
            raise RuntimeError('prepare must be run first!')

        if callable(self.render_condition) and not self.render_condition(self):
            return False

        for cur_block in self.prepared_blocks:
            if cur_block.stream_block.final_display(context):
                return True

        return False

    def prepare(self, page):
        self.prepared_blocks = [getattr(page, block) for block in self.blocks]
        self.page = page


def get_form_choices():
    import home.views.onpage
    all_views = home.views.onpage.OnPageViewMixin.get_all_onpage_views()

    return [('.'.join([x.__module__, x.__name__]),
             x.__name__) for x in all_views]
