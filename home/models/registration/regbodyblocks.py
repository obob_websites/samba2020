
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
from django.core.exceptions import ObjectDoesNotExist
from wagtail.core import blocks
from wagtail.core.rich_text import RichText

from extra.expose_to_template.blocks import ExposedRichTextBlock
from .blocks import LinkToFormBlock
import home.models


class RegistrationBodyBlockBase(blocks.StreamBlock):
    class Meta:
        not_show_when_showing = []
        only_show_when_showing = []

    paragraph = ExposedRichTextBlock()
    link_to_form = LinkToFormBlock()

    context = None

    @classmethod
    def final_display(cls, context):
        cls.context = context
        show = cls.display()
        for not_show_block in cls._meta_class.not_show_when_showing:
            show = show and not not_show_block.final_display(context)

        for only_show_block in cls._meta_class.only_show_when_showing:
            show = show and only_show_block.final_display(context)

        return show

    @classmethod
    def display(cls):
        return True

    @classmethod
    def get_conference(cls):
        return cls.context['page'].conference

    @classmethod
    def get_user(cls):
        return cls.context['user']

    def render_form(self, value, *args, **kwargs):
        if len(value) == 0:
            value = None

        return super().render_form(value, *args, **kwargs)


class LoggedInBlock(RegistrationBodyBlockBase):
    class Meta:
        default = (
            ('paragraph', RichText('User is not logged in')),
        )

    @classmethod
    def display(cls):
        return cls.context['request'].user.is_authenticated


class RegistrationNotYetStartedBlock(RegistrationBodyBlockBase):
    class Meta:
        default = (
            ('paragraph', RichText('Registration has not started yet')),
        )

    @classmethod
    def display(cls):
        return not cls.get_conference().registration_open


class RegistrationDeadlinePassedBlock(RegistrationBodyBlockBase):
    class Meta:
        not_show_when_showing = [RegistrationNotYetStartedBlock]
        default = (
            ('paragraph', RichText('Deadline is over')),
        )

    @classmethod
    def display(cls):
        return (cls.get_conference().registration_deadline_passed and
                not cls.get_conference().is_user_registered(cls.get_user()))


class RegistrationOpenNotRegisteredBlock(LoggedInBlock):
    class Meta:
        default = (
            ('paragraph', RichText('Registration is open but not registered')),
        )

    @classmethod
    def display(cls):
        return (super().display() and
                cls.get_conference().can_register_for_conference and
                not cls.get_conference().is_user_registered(cls.get_user()))


class RegistrationFullBlock(RegistrationBodyBlockBase):
    class Meta:
        not_show_when_showing = [RegistrationNotYetStartedBlock]
        default = (
            ('paragraph', RichText('Registration Full')),
        )

    @classmethod
    def display(cls):
        return (super().display() and
                not cls.get_conference().tickets_left_for_conference and
                not cls.get_conference().registration_deadline_passed and
                not cls.get_conference().is_user_registered(cls.get_user()))


class NotLoggedInBlock(RegistrationBodyBlockBase):
    class Meta:
        not_show_when_showing = [
            RegistrationNotYetStartedBlock,
        ]
        default = (
            ('paragraph', RichText('Not logged in')),
        )

    @classmethod
    def display(cls):
        return (not cls.context['request'].user.is_authenticated and
                cls.get_conference().registration_open)


class RegistrationOpenAndNotLoggedInBlock(RegistrationBodyBlockBase):
    class Meta:
        only_show_when_showing = [
            NotLoggedInBlock
        ]
        default = (
            ('paragraph', RichText('Registration open. not logged in')),
        )

    @classmethod
    def display(cls):
        return (super().display() and
                cls.get_conference().can_register_for_conference)


class RegisteredButNotPaidBlock(LoggedInBlock):
    class Meta:
        default = (
            ('paragraph', RichText('Please pay')),
        )

    @classmethod
    def display(cls):
        return (super().display() and
                cls.get_conference().is_user_registered(cls.get_user()) and
                not cls.get_conference().has_user_paid(cls.get_user()))


class RegisteredAndPaidBlock(LoggedInBlock):
    class Meta:
        default = (
            ('paragraph', RichText('Thank you for paying')),
        )

    @classmethod
    def display(cls):
        from home.models import Registration

        if not super().display():
            return False

        conf = cls.get_conference()
        user = cls.get_user()

        qs = Registration.objects.filter(conference=conf,
                                         user=user)

        if not qs.exists():
            return False

        is_complete = qs.first().payment_status == 'complete'
        has_checked_in = qs.first().is_checked_in

        return (cls.get_conference().has_user_paid(cls.get_user()) and
                is_complete and not has_checked_in)


class RegisteredAndCheckedIn(LoggedInBlock):
    class Meta:
        default = (
            ('paragraph', RichText('You have checked in')),
        )

    @classmethod
    def display(cls):
        from home.models import Registration

        if not super().display():
            return False

        conf = cls.get_conference()
        user = cls.get_user()

        qs = Registration.objects.filter(conference=conf,
                                         user=user)

        if not qs.exists():
            return False

        is_complete = qs.first().payment_status == 'complete'
        has_checked_in = qs.first().is_checked_in

        return (cls.get_conference().has_user_paid(cls.get_user()) and
                is_complete and has_checked_in)


class RegisteredAndPayAtConferenceBlock(LoggedInBlock):
    class Meta:
        default = (
            ('paragraph', RichText('You pay at the conference')),
        )
        not_show_when_showing = [
            RegisteredAndPaidBlock,
            RegisteredAndCheckedIn
        ]

    @classmethod
    def display(cls):
        return (super().display() and
                cls.get_conference().has_user_paid(cls.get_user()))


class AbstractSubmissionOpenNotLoggedInBlock(RegistrationBodyBlockBase):
    class Meta:
        only_show_when_showing = [
            NotLoggedInBlock
        ]
        default = (
            ('paragraph', RichText('Abstract submission open. not logged in')),
        )

    @classmethod
    def display(cls):
        return (super().display() and
                cls.get_conference().can_submit_abstract(cls.get_user()))


class AbstractSubmissionOpenNotSubmittedBlock(LoggedInBlock):
    class Meta:
        default = (
            ('paragraph', RichText('Abstract submission is open but '
                                   'no abstract was submitted')),
        )

    @classmethod
    def display(cls):
        return (super().display() and
                cls.get_conference().can_submit_abstract(cls.get_user()))


class AbstractSubmissionDeadlinePassed(RegistrationBodyBlockBase):
    class Meta:
        not_show_when_showing = [
            RegistrationNotYetStartedBlock,
        ]
        default = (
            ('paragraph', RichText('Abstract deadline has passed')),
        )

    @classmethod
    def display(cls):
        return (super().display() and
                not cls.get_conference().can_submit_abstract(
                    cls.get_user()) and
                cls.get_conference().abstract_deadline_passed and
                not cls.get_conference().has_user_submitted_abstract(
                    cls.get_user()))


class AbstractSubmissionLimitReached(RegistrationBodyBlockBase):
    class Meta:
        not_show_when_showing = [
            RegistrationNotYetStartedBlock,
            RegistrationFullBlock,
            AbstractSubmissionDeadlinePassed
        ]
        default = (
            ('paragraph', RichText('Abstract Submission Full')),
        )

    @classmethod
    def display(cls):
        conf = cls.get_conference()
        user = cls.get_user()

        return (super().display() and
                not conf.can_submit_abstract(user) and
                conf.n_abstract_submission_left <= 0)


class AbstractPendingBlock(LoggedInBlock):
    class Meta:
        default = (
            ('paragraph', RichText('Abstract review pending')),
        )

    @classmethod
    def display(cls):
        return (super().display() and
                cls.get_conference().has_user_submitted_abstract(
                    cls.get_user()) and
                cls.get_conference().get_user_abstract_status(
                    cls.get_user()) == 'pending')


class AbstractAcceptedBlock(LoggedInBlock):
    class Meta:
        default = (
            ('paragraph', RichText('Abstract accepted')),
        )

    @classmethod
    def display(cls):
        return (super().display() and
                cls.get_conference().has_user_submitted_abstract(
                    cls.get_user()) and
                cls.get_conference().get_user_abstract_status(
                    cls.get_user()) == 'accepted')


class AbstractRejectedBlock(LoggedInBlock):
    class Meta:
        default = (
            ('paragraph', RichText('Abstract rejected')),
        )

    @classmethod
    def display(cls):
        return (super().display() and
                cls.get_conference().has_user_submitted_abstract(
                    cls.get_user()) and
                cls.get_conference().get_user_abstract_status(
                    cls.get_user()) == 'rejected')


class AbstractPosterNumberBlock(LoggedInBlock):
    class Meta:
        only_show_when_showing = [AbstractAcceptedBlock]
        default = (
            ('paragraph', RichText('Your poster number...')),
        )

    @classmethod
    def display(cls):
        if not super().display():
            return False

        conf = cls.get_conference()
        user = cls.get_user()

        try:
            abstract = home.models.Abstract.objects.get(user=user,
                                                        conference=conf)
        except ObjectDoesNotExist:
            return False

        return abstract.poster_number is not None


class ExtendedAbstractDeadlinePassedBlock(RegistrationBodyBlockBase):
    class Meta:
        not_show_when_showing = [RegistrationNotYetStartedBlock]
        default = (
            ('paragraph', RichText('Deadline for extended abstract'
                                   ' has passed')),
        )

    @classmethod
    def display(cls):
        conf = cls.get_conference()
        user = cls.get_user()

        return (super().display() and
                not conf.can_submit_extended_abstract(user) and
                conf.extended_abstract_deadline_passed and
                not conf.has_user_submitted_extended_abstract(user))


class ExtendedAbstractMustRegisterPayAndHaveAbstractAcceptedBlock(LoggedInBlock):  # noqa
    class Meta:
        default = (
            ('paragraph', RichText('You must be fully registered and have '
                                   'your abstract accepted in order to '
                                   'submit the extended abstract')),
        )
        not_show_when_showing = [
            ExtendedAbstractDeadlinePassedBlock,
            RegistrationNotYetStartedBlock
        ]

    @classmethod
    def display(cls):
        conf = cls.get_conference()
        user = cls.get_user()

        return (super().display() and
                not conf.can_submit_extended_abstract(user) and
                (not conf.get_user_abstract_status(user) == 'accepted' or
                 not conf.has_user_paid(user)))


class ExtendedAbstractOpenNotSubmittedBlock(LoggedInBlock):
    class Meta:
        default = (
            ('paragraph', RichText('Submit Extended Abstract here')),
        )

    @classmethod
    def display(cls):
        conf = cls.get_conference()
        user = cls.get_user()

        return (super().display() and
                conf.can_submit_extended_abstract(user))


class ExtendedAbstractOpenNotLoggedInBlock(RegistrationBodyBlockBase):
    class Meta:
        default = (
            ('paragraph', RichText('Extended Abstract submission open '
                                   'but not logged in')),
        )
        only_show_when_showing = [
            NotLoggedInBlock
        ]
        not_show_when_showing = [
            ExtendedAbstractDeadlinePassedBlock
        ]

    @classmethod
    def display(cls):
        conf = cls.get_conference()

        return (super().display() and
                conf.registration_open)


class ExtendedAbstractUnderReviewBlock(LoggedInBlock):
    class Meta:
        default = (
            ('paragraph', RichText('Extended Abstract under Review')),
        )

    @classmethod
    def display(cls):
        conf = cls.get_conference()
        user = cls.get_user()

        return (super().display() and
                conf.get_user_extended_abstract_status(user) == 'pending')


class ExtendedAbstractAcceptedBlock(LoggedInBlock):
    class Meta:
        default = (
            ('paragraph', RichText('Extended Abstract accepted')),
        )

    @classmethod
    def display(cls):
        conf = cls.get_conference()
        user = cls.get_user()

        return (super().display() and
                conf.get_user_extended_abstract_status(user) == 'accepted')


class ExtendedAbstractRejectedBlock(LoggedInBlock):
    class Meta:
        default = (
            ('paragraph', RichText('Extended Abstract rejected')),
        )

    @classmethod
    def display(cls):
        conf = cls.get_conference()
        user = cls.get_user()

        return (super().display() and
                conf.get_user_extended_abstract_status(user) == 'rejected')
