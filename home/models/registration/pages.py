
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import importlib

from django.db import models
from django.db.models import BooleanField
from django.utils.functional import cached_property
from wagtail.admin.edit_handlers import FieldPanel, \
    TabbedInterface, ObjectList
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.core.fields import StreamField
from ..base import SambaPage

from extra.expose_to_template.mixins import ExposeToTemplatePageMixin
from extra.expose_to_template.mixins.expose_to_template import \
    ObjectListWithHelp
from home.models import get_default_streamfield
from home.models.registration.regbodyblocks import (
    NotLoggedInBlock,
    RegistrationNotYetStartedBlock,
    RegistrationFullBlock, RegistrationDeadlinePassedBlock,
    RegistrationOpenAndNotLoggedInBlock, RegistrationOpenNotRegisteredBlock,
    RegisteredButNotPaidBlock, RegisteredAndPaidBlock,
    AbstractSubmissionOpenNotLoggedInBlock,
    AbstractSubmissionOpenNotSubmittedBlock, AbstractSubmissionDeadlinePassed,
    AbstractPendingBlock, AbstractAcceptedBlock, AbstractRejectedBlock,
    ExtendedAbstractDeadlinePassedBlock,
    ExtendedAbstractMustRegisterPayAndHaveAbstractAcceptedBlock,
    ExtendedAbstractOpenNotSubmittedBlock,
    ExtendedAbstractOpenNotLoggedInBlock, ExtendedAbstractUnderReviewBlock,
    ExtendedAbstractAcceptedBlock, ExtendedAbstractRejectedBlock,
    AbstractSubmissionLimitReached, RegisteredAndPayAtConferenceBlock,
    AbstractPosterNumberBlock, RegisteredAndCheckedIn)
from .utils import get_form_choices, BodyBlockList
from ..exposeitems import (ExposePaymentConfirmationButton,
                           ExposeParticipationConfirmationButton)


class RegisterAndAbstractOverview(ExposeToTemplatePageMixin, SambaPage):
    parent_page_types = ['samba_home.Conference']
    subpage_types = ['samba_home.RegisterAndAbstractFormPage']
    max_count_per_parent = 1

    not_logged_in = StreamField(NotLoggedInBlock(),
                                blank=True)
    registration_not_yet_started = StreamField(
        RegistrationNotYetStartedBlock(),
        blank=True)
    registration_open_not_registered = StreamField(
        RegistrationOpenNotRegisteredBlock(),
        blank=True)
    registration_open_not_logged_in = StreamField(
        RegistrationOpenAndNotLoggedInBlock(),
        blank=True)
    registration_full = StreamField(RegistrationFullBlock(),
                                    blank=True)
    registration_deadline_passed = StreamField(
        RegistrationDeadlinePassedBlock(),
        blank=True)
    registered_payment_pending = StreamField(
        RegisteredButNotPaidBlock(),
        blank=True
    )
    registered_payment_complete = StreamField(
        RegisteredAndPaidBlock(),
        blank=True
    )
    registered_pay_at_conference = StreamField(
        RegisteredAndPayAtConferenceBlock(),
        blank=True
    )
    registered_checked_in = StreamField(
        RegisteredAndCheckedIn(),
        blank=True
    )
    abstract_submission_open_not_logged_in = StreamField(
        AbstractSubmissionOpenNotLoggedInBlock(),
        blank=True
    )
    abstract_submission_open_not_submitted = StreamField(
        AbstractSubmissionOpenNotSubmittedBlock(),
        blank=True
    )
    abstract_submission_deadline_passed = StreamField(
        AbstractSubmissionDeadlinePassed(),
        blank=True
    )
    abstract_submission_limit_reached = StreamField(
        AbstractSubmissionLimitReached(),
        blank=True
    )
    abstract_pending = StreamField(
        AbstractPendingBlock(),
        blank=True
    )
    abstract_accepted = StreamField(
        AbstractAcceptedBlock(),
        blank=True
    )
    abstract_rejected = StreamField(
        AbstractRejectedBlock(),
        blank=True
    )
    abstract_poster_number_display = StreamField(
        AbstractPosterNumberBlock(),
        blank=True
    )
    extended_abstract_submission_deadline_passed = StreamField(
        ExtendedAbstractDeadlinePassedBlock(),
        blank=True
    )
    extended_abstract_submission_requirements_not_fulfilled = StreamField(
        ExtendedAbstractMustRegisterPayAndHaveAbstractAcceptedBlock(),
        blank=True
    )
    extended_abstract_submission_open_not_submitted = StreamField(
        ExtendedAbstractOpenNotSubmittedBlock(),
        blank=True
    )
    extended_abstract_submission_open_not_logged_in = StreamField(
        ExtendedAbstractOpenNotLoggedInBlock(),
        blank=True
    )
    extended_abstract_pending = StreamField(
        ExtendedAbstractUnderReviewBlock(),
        blank=True
    )
    extended_abstract_accepted = StreamField(
        ExtendedAbstractAcceptedBlock(),
        blank=True
    )
    extended_abstract_rejected = StreamField(
        ExtendedAbstractRejectedBlock(),
        blank=True
    )

    show_extended_abstract_content = BooleanField(default=True)

    content_panels = SambaPage.content_panels + [
        FieldPanel('not_logged_in'),
    ]

    registration_panels = [
        FieldPanel('registration_not_yet_started'),
        FieldPanel('registration_open_not_registered'),
        FieldPanel('registration_open_not_logged_in'),
        FieldPanel('registration_full'),
        FieldPanel('registration_deadline_passed'),
        FieldPanel('registered_payment_pending'),
        FieldPanel('registered_payment_complete'),
        FieldPanel('registered_checked_in'),
        FieldPanel('registered_pay_at_conference')
    ]

    abstract_panels = [
        FieldPanel('abstract_submission_limit_reached'),
        FieldPanel('abstract_submission_open_not_logged_in'),
        FieldPanel('abstract_submission_open_not_submitted'),
        FieldPanel('abstract_submission_deadline_passed'),
        FieldPanel('abstract_pending'),
        FieldPanel('abstract_accepted'),
        FieldPanel('abstract_rejected'),
        FieldPanel('abstract_poster_number_display')
    ]

    extended_abstract_panels = [
        FieldPanel('show_extended_abstract_content'),
        FieldPanel('extended_abstract_submission_open_not_logged_in'),
        FieldPanel('extended_abstract_submission_open_not_submitted'),
        FieldPanel('extended_abstract_submission_deadline_passed'),
        FieldPanel('extended_abstract_submission_requirements_not_fulfilled'),  # noqa
        FieldPanel('extended_abstract_pending'),
        FieldPanel('extended_abstract_accepted'),
        FieldPanel('extended_abstract_rejected'),
    ]

    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Content'),
        ObjectListWithHelp(registration_panels,
                           heading='Registration Content'),
        ObjectListWithHelp(abstract_panels, heading='Abstract Content'),
        ObjectListWithHelp(extended_abstract_panels,
                           heading='Extended Abstract Content'),
        ObjectList(SambaPage.promote_panels, heading='Promote'),
        ObjectList(SambaPage.settings_panels, heading='Settings',
                   classname="settings"),
    ])

    body_blocks_raw = [
        BodyBlockList(
            'registration_not_yet_started'),
        BodyBlockList('not_logged_in'),
        BodyBlockList(
            'registration_open_not_registered',
            'registration_open_not_logged_in',
            'registration_full',
            'registration_deadline_passed',
            'registered_payment_pending',
            'registered_payment_complete',
            'registered_pay_at_conference',
            'registered_checked_in',
            heading='Registration'),
        BodyBlockList(
            'abstract_submission_open_not_logged_in',
            'abstract_submission_open_not_submitted',
            'abstract_submission_limit_reached',
            'abstract_submission_deadline_passed',
            'abstract_pending',
            'abstract_accepted',
            'abstract_poster_number_display',
            'abstract_rejected',
            heading='Abstract'),
        BodyBlockList(
            'extended_abstract_submission_deadline_passed',
            'extended_abstract_submission_requirements_not_fulfilled',
            'extended_abstract_submission_open_not_submitted',
            'extended_abstract_submission_open_not_logged_in',
            'extended_abstract_pending',
            'extended_abstract_accepted',
            'extended_abstract_rejected',
            heading='Poster Prize',
            render_condition=lambda b: b.page.show_extended_abstract_content
        )
    ]

    expose_fields = ExposeToTemplatePageMixin.expose_fields + [
        ExposePaymentConfirmationButton(),
        ExposeParticipationConfirmationButton(),
    ]

    @property
    def body_blocks(self):
        for block in self.body_blocks_raw:
            block.prepare(self)

        return self.body_blocks_raw

    @cached_property
    def conference(self):
        return self.get_parent().specific


class RegisterAndAbstractFormPage(ExposeToTemplatePageMixin,
                                  RoutablePageMixin,
                                  SambaPage):
    parent_page_types = ['samba_home.RegisterAndAbstractOverview']
    subpage_types = []

    intro = get_default_streamfield(blank=True)
    form = models.CharField(max_length=255,
                            blank=True,
                            null=True)

    content_panels = SambaPage.content_panels + [
        FieldPanel('intro'),
        FieldPanel('form')
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._meta.get_field('form').choices = get_form_choices()

    @route(r'^$')
    def show_form(self, request):
        view_mod_name, x, view_class_name = self.form.rpartition('.')
        view_mod = importlib.import_module(view_mod_name)
        view_class = getattr(view_mod, view_class_name)
        view = view_class()
        view.setup(request)
        if not view.template_name:
            view.template_name = self.get_template(request)
        if view.extra_context is None:
            view.extra_context = {}

        view.extra_context.update(self.get_context(request))

        return view.dispatch(request)
