
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django_currentuser.db.models import CurrentUserField
from home.modelfields import DefaultMoneyField


class Payment(models.Model):
    datetime_added = models.DateTimeField(auto_now_add=True)
    created_by = CurrentUserField()
    registration = models.ForeignKey('Registration',
                                     on_delete=models.CASCADE)
    amount = DefaultMoneyField()
    fee = DefaultMoneyField()
    kind_of_payment = models.CharField(max_length=255,
                                       choices=(
                                           ('transfer', 'Bank Transfer'),
                                           ('cash', 'Cash')
                                       ),
                                       blank=False)

    notes = models.TextField(blank=True)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.registration.check_payment_status_update()

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        self.registration.check_payment_status_update()
