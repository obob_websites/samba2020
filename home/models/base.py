
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.core.models import Page as WagtailPage
from django.db import models


class SambaPage(WagtailPage):
    class Meta:
        abstract = True

    show_title_on_page = models.BooleanField(default=True)
    turn_table_links_into_buttons = models.BooleanField(default=False)

    misc_panels = [
        FieldPanel('show_title_on_page', classname='full'),
        FieldPanel('turn_table_links_into_buttons', classname='full')
    ]

    settings_panels = WagtailPage.settings_panels + [
        MultiFieldPanel(misc_panels, heading='Misc')
    ]
