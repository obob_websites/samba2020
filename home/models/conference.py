
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from wagtail.admin.panels import InlinePanel

from home.edit_handlers import PreviewPDFsEditHandler

from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import Max
from django.utils.text import slugify
from wagtail.admin.edit_handlers import FieldPanel, \
    TabbedInterface, ObjectList
from wagtail.core.blocks import StreamBlock
from wagtail.core.fields import StreamField
from wagtailmenus.models import MenuPageMixin
from wagtailmenus.panels import menupage_panel
from .base import SambaPage
from wagtailgeowidget.panels import LeafletPanel

import home.models
from extra.expose_to_template.mixins import ExposeToTemplatePageMixin, \
    ExposeContextItem
from extra.expose_to_template.mixins.expose_to_template import \
    ObjectListWithHelp
from home.models import NotRootMixin, get_default_streamfield
from extra.blocks.blocks import EmailBlock, FooterBlock
from home.models.exposeitems import (ExposeSpeakersToContext,
                                     ExposeFullnameToContext,
                                     ExposeEmailToContext,
                                     ExposePaymentDetails, ExposeAmountOwed,
                                     ExposeAmountPaid, ExposeFeesDeducted,
                                     ExposeAbstractTitle,
                                     ExposeAbstractReviewNote,
                                     ExposeLocationMap, ExposePaymentDeadline,
                                     ExposePosterNumber,
                                     ExposeRegistrationAffiliation,
                                     ExposeAbstractAuthors,
                                     ExposeAbstractAffiliations,
                                     ExposeBoughtTickets,
                                     ExposeBoughtTicketOptions,
                                     ExposeTotalAmount)


class Conference(NotRootMixin, ExposeToTemplatePageMixin,
                 MenuPageMixin, SambaPage):
    start_date = models.DateField()
    end_date = models.DateField()

    body = get_default_streamfield(blank=True)

    location = models.CharField(max_length=255, blank=True, null=True)

    registration_start_date = models.DateField()
    registration_deadline = models.DateField()

    payment_details = get_default_streamfield(blank=True,
                                              ignore_tag='payment_details')

    abstract_deadline = models.DateField()
    abstract_max_number_of_words = models.IntegerField(validators=(
        MinValueValidator(1),
    ))

    extended_abstract_deadline = models.DateField()
    extended_abstract_max_number_of_words = models.IntegerField(validators=(
        MinValueValidator(1),
    ))

    conference_number_of_tickets = models.IntegerField(validators=(
        MinValueValidator(1),
    ))
    abstract_max_number_of_submissions = models.IntegerField(validators=(
        MinValueValidator(1),
    ))

    email_has_registered = StreamField(
        EmailBlock(blank=True),
        blank=True)
    email_payment_complete = StreamField(EmailBlock(blank=True), blank=True)
    email_pay_at_conference = StreamField(EmailBlock(blank=True), blank=True)
    email_abstract_submitted = StreamField(EmailBlock(blank=True),
                                           blank=True)
    email_abstract_accepted = StreamField(EmailBlock(blank=True), blank=True)
    email_abstract_rejected = StreamField(EmailBlock(blank=True), blank=True)
    email_poster_number_assigned = StreamField(EmailBlock(blank=True),
                                               blank=True)
    email_extended_abstract_submitted = StreamField(EmailBlock(blank=True),
                                                    blank=True)
    email_extended_abstract_accepted = StreamField(EmailBlock(blank=True),
                                                   blank=True)
    email_extended_abstract_rejected = StreamField(EmailBlock(blank=True),
                                                   blank=True)
    email_first_payment_warning = StreamField(EmailBlock(blank=True),
                                              blank=True)
    email_second_payment_warning = StreamField(EmailBlock(blank=True),
                                               blank=True)
    email_unregister_no_payment = StreamField(EmailBlock(blank=True),
                                              blank=True)
    email_checked_in = StreamField(EmailBlock(blank=True), blank=True)

    pdf_header_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    payment_confirmation_body = get_default_streamfield(blank=True)
    participant_confirmation_body = get_default_streamfield(blank=True)
    pdf_footer = StreamField(
        StreamBlock([
            ('footer', FooterBlock())
        ], max_num=1),
        blank=True)

    payment_first_warning_after_n_days = models.IntegerField(
        validators=(MinValueValidator(0), ),
        default=0
    )

    payment_second_warning_after_n_days = models.IntegerField(
        validators=(MinValueValidator(0), ),
        default=0
    )

    payment_unregister_after_n_days = models.IntegerField(
        validators=(MinValueValidator(0), ),
        default=0
    )

    content_panels = SambaPage.content_panels + [
        FieldPanel('start_date'),
        FieldPanel('end_date'),
        LeafletPanel('location'),
        FieldPanel('body')
    ]

    settings_panels = SambaPage.settings_panels + [
        menupage_panel
    ]

    email_panels = [
        FieldPanel('email_has_registered'),
        FieldPanel('email_pay_at_conference'),
        FieldPanel('email_payment_complete'),
        FieldPanel('email_abstract_submitted'),
        FieldPanel('email_abstract_accepted'),
        FieldPanel('email_abstract_rejected'),
        FieldPanel('email_poster_number_assigned'),
        FieldPanel('email_extended_abstract_submitted'),
        FieldPanel('email_extended_abstract_accepted'),
        FieldPanel('email_extended_abstract_rejected'),
        FieldPanel('email_first_payment_warning'),
        FieldPanel('email_second_payment_warning'),
        FieldPanel('email_unregister_no_payment'),
        FieldPanel('email_checked_in'),
    ]

    registration_panels = [
        FieldPanel('registration_start_date'),
        FieldPanel('registration_deadline'),
        FieldPanel('conference_number_of_tickets'),
        FieldPanel('payment_details'),
        FieldPanel('payment_first_warning_after_n_days'),
        FieldPanel('payment_second_warning_after_n_days'),
        FieldPanel('payment_unregister_after_n_days'),
    ]

    ticket_panels = [
        InlinePanel('conference_tickets', heading='Conference Tickets',
                    label='Conference Ticket'),
        InlinePanel('conference_options', heading='Ticket Options',
                    label='Option')
    ]

    abstract_panels = [
        FieldPanel('abstract_max_number_of_submissions'),
        FieldPanel('abstract_deadline'),
        FieldPanel('abstract_max_number_of_words'),
        FieldPanel('extended_abstract_deadline'),
        FieldPanel('extended_abstract_max_number_of_words')
    ]

    pdfs_panels = [
        FieldPanel('pdf_header_image'),
        FieldPanel('payment_confirmation_body'),
        FieldPanel('participant_confirmation_body'),
        FieldPanel('pdf_footer'),
        PreviewPDFsEditHandler()
    ]

    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Content'),
        ObjectList(ticket_panels,
                   heading='Tickets'),
        ObjectListWithHelp(registration_panels,
                           heading='Registration Settings'),
        ObjectList(abstract_panels, heading='Abstract Settings'),
        ObjectList(email_panels, heading='Emails'),
        ObjectListWithHelp(pdfs_panels, heading='PDFs'),
        ObjectList(SambaPage.promote_panels, heading='Promote'),
        ObjectList(settings_panels, heading='Settings',
                   classname="settings"),
    ])

    expose_fields = ExposeToTemplatePageMixin.expose_fields + [
        ExposeFullnameToContext(),
        ExposeEmailToContext(),
        ExposeContextItem(tag='start_date',
                          description='Start date of the conference.'),
        ExposeContextItem(tag='end_date',
                          description='End date of the conference'),
        ExposeContextItem(tag='conference_title',
                          description='Title of the conference',
                          getfun_or_field='title'),
        ExposeSpeakersToContext(),
        ExposePaymentDetails(),
        ExposeContextItem(tag='registration_start_date',
                          description='Start date of the registration'),
        ExposeContextItem(tag='registration_deadline',
                          description='Registration deadline'),
        ExposeContextItem(tag='abstract_deadline',
                          description='Abstract Deadline'),
        ExposeContextItem(tag='abstract_max_number_of_words',
                          description='Maximum number of words in abstract'),
        ExposeContextItem(tag='extended_abstract_deadline',
                          description='Extended Abstract Deadline'),
        ExposeContextItem(tag='extended_abstract_max_number_of_words',
                          description='Maximum number of words in extended '
                                      'abstract'),
        ExposeAmountOwed(),
        ExposeAmountPaid(),
        ExposeFeesDeducted(),
        ExposeAbstractTitle(),
        ExposeAbstractAuthors(),
        ExposeAbstractAffiliations(),
        ExposeAbstractReviewNote(),
        ExposeLocationMap(),
        ExposePaymentDeadline(),
        ExposePosterNumber(),
        ExposeRegistrationAffiliation(),
        ExposeBoughtTickets(),
        ExposeBoughtTicketOptions(),
        ExposeTotalAmount()
    ]

    parent_page_types = ['samba_home.IndexPage',
                         'samba_home.PreviousConferencesPage']

    def clean(self):
        if self.start_date is None:
            raise ValidationError(
                'The start date must be set.'
            )

        if self.end_date is None:
            raise ValidationError(
                'The end date must be set.'
            )

        if self.end_date <= self.start_date:
            raise ValidationError(
                'The start date must be before the end date.')

        if self.payment_first_warning_after_n_days == 0:
            if self.payment_second_warning_after_n_days > 0:
                raise ValidationError('Second Payment warning must be zero '
                                      'if first one is zero')
            if self.payment_unregister_after_n_days > 0:
                raise ValidationError('Payment unregister must be zero '
                                      'if first payment warning is zero')
        else:
            if (self.payment_unregister_after_n_days <=
                    self.payment_first_warning_after_n_days):
                raise ValidationError('Payment unregister must be later than '
                                      'first payment warning')
            if self.payment_second_warning_after_n_days > 0:
                if (self.payment_unregister_after_n_days <=
                        self.payment_second_warning_after_n_days):
                    raise ValidationError('Payment unregister must be later '
                                          'than second payment warning')
                if (self.payment_second_warning_after_n_days <=
                        self.payment_first_warning_after_n_days):
                    raise ValidationError('Second payment warning must be '
                                          'later than the first')

        super().clean()

    @property
    def base_group_name(self):
        return slugify(self.title)

    def _create_group_name(self, suffix):
        return '%s_%s' % (self.base_group_name, suffix)

    @property
    def participant_group(self):
        return Group.objects.get_or_create(
            name=self._create_group_name('participant')
        )[0]

    @property
    def manager_group(self):
        return Group.objects.get_or_create(
            name=self._create_group_name('manager')
        )[0]

    @property
    def abstract_reviewer_group(self):
        return Group.objects.get_or_create(
            name=self._create_group_name('abstract_reviewer')
        )[0]

    @property
    def extended_abstract_reviewer_group(self):
        return Group.objects.get_or_create(
            name=self._create_group_name('extended_abstract_reviewer')
        )[0]

    @property
    def n_registered_for_conference(self):
        return self.registration_set.count()

    @property
    def n_registered_and_paid_for_conference(self):
        return home.models.Registration.objects.filter(
            conference=self,
            payment_status__in=(
                'complete', 'pay_at_conference')).count()

    @property
    def n_checked_in(self):
        return self.registration_set.filter(checkin__isnull=False).count()

    @property
    def n_remaining_for_conference(self):
        return (self.conference_number_of_tickets -
                self.n_registered_for_conference)

    @property
    def tickets_left_for_conference(self):
        return (self.n_registered_for_conference <
                self.conference_number_of_tickets)

    @property
    def registration_deadline_passed(self):
        return datetime.date.today() > self.registration_deadline

    @property
    def registration_open(self):
        return datetime.date.today() >= self.registration_start_date

    @property
    def abstract_deadline_passed(self):
        return datetime.date.today() > self.abstract_deadline

    @property
    def n_abstracts_submitted_and_accepted(self):
        return (home.models.Abstract.objects
                .filter(conference=self,
                        review_status__in=('pending', 'accepted'))
                .count())

    @property
    def n_abstract_submission_left(self):
        return (self.abstract_max_number_of_submissions -
                self.n_abstracts_submitted_and_accepted)

    @property
    def extended_abstract_deadline_passed(self):
        return datetime.date.today() > self.extended_abstract_deadline

    def can_submit_extended_abstract(self, user=None):
        if user is None:
            return False

        rval = (not self.extended_abstract_deadline_passed and
                self.has_user_paid(user) and
                self.get_user_abstract_status(user) == 'accepted' and
                not self.has_user_submitted_extended_abstract(user))

        return rval

    def can_submit_abstract(self, user=None):
        rval = (self.registration_open and
                not self.abstract_deadline_passed)

        if user is None or not self.is_user_registered(user):
            rval = rval and self.can_register_for_conference

        rval = rval and not self.has_user_submitted_abstract(user)

        rval = rval and self.n_abstract_submission_left > 0

        return rval

    def can_user_register(self, user=None):
        if user is None:
            return False

        return (self.can_register_for_conference and
                not self.is_user_registered(user))

    @property
    def can_register_for_conference(self):
        return (self.registration_open and
                not self.registration_deadline_passed and
                self.tickets_left_for_conference)

    def is_user_registered(self, user):
        if user.id is None:
            return False

        return home.models.Registration.objects \
            .filter(conference=self, user=user).exists()

    def has_user_paid(self, user):
        if user.id is None:
            return False

        qs = home.models.Registration.objects.filter(conference=self,
                                                     user=user)

        if not qs.exists():
            return False

        return qs.first().payment_status in ('pay_at_conference', 'complete')

    def has_user_submitted_abstract(self, user):
        if user.id is None:
            return False

        return home.models.Abstract.objects.filter(
            conference=self, user=user
        ).exists()

    def get_user_abstract_status(self, user):
        if user.id is None:
            return None

        abstract = home.models.Abstract.objects.filter(
            conference=self, user=user
        ).first()

        if not abstract:
            return None

        return abstract.review_status

    def has_user_submitted_extended_abstract(self, user):
        if user.id is None:
            return False

        return home.models.ExtendedAbstract.objects.filter(
            base_abstract__conference=self, base_abstract__user=user
        ).exists()

    def get_user_extended_abstract_status(self, user):
        if user.id is None:
            return None

        ex_abstract = home.models.ExtendedAbstract.objects.filter(
            base_abstract__conference=self, base_abstract__user=user
        ).first()

        if not ex_abstract:
            return None

        return ex_abstract.review_status

    @property
    def highest_poster_number(self):
        n = home.models.Abstract.objects.filter(
            conference=self
        ).aggregate(Max('poster_number'))['poster_number__max']

        if n is None:
            n = 0

        return n
