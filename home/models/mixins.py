
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
from wagtailmenus.models import MenuPageMixin
from wagtailmenus.panels import menupage_panel

from home.models import SambaPage


class NotRootMixin(object):
    @classmethod
    def can_create_at(cls, parent):
        # You can only create one of these!
        return super().can_create_at(parent) and parent.title != 'Root'


class ServeViewMixin(object):
    def get_extra_context(self, request):
        context = self.get_context(request)
        if hasattr(self, 'extra_context') and self.extra_context is not None:
            context.update(self.extra_context)

        return context

    def serve_view(self, view_class, request, *args, **kwargs):
        view = view_class()
        view.setup(request, *args, **kwargs)
        if (not hasattr(view, 'extra_context') or
                view_class.extra_context is None):
            view.extra_context = {}

        view.extra_context.update(self.get_extra_context(request))

        return view.dispatch(request)


class RestrictMenuToGroupMixin(MenuPageMixin):
    class Meta:
        abstract = True

    menu_allowed_group = None

    settings_panels = SambaPage.settings_panels + [
        menupage_panel
    ]

    def show_in_menu_test_func(self, request):
        if self.menu_allowed_group is None:
            return True

        if isinstance(self.menu_allowed_group, str):
            self.menu_allowed_group = [self.menu_allowed_group]

        groups = [getattr(self.get_parent().specific, g)
                  for g in self.menu_allowed_group]

        for group in groups:
            if group.user_set.all().filter(pk=request.user.pk).exists():
                return True

        return False
