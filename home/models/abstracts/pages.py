
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
from extra.expose_to_template.mixins import ExposeToTemplatePageMixin
from .. import SambaPage, get_default_streamfield
from ..pages import SubPagesPage
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.admin.edit_handlers import FieldPanel


class ReviewParentPage(SubPagesPage):
    parent_page_types = ['samba_home.Conference']
    subpage_types = ['samba_home.ReviewAbstractsPage',
                     'samba_home.ReviewExtendedAbstractsPage']
    max_count_per_parent = 1
    menu_allowed_group = ['abstract_reviewer_group',
                          'extended_abstract_reviewer_group']

    def modify_submenu_items(self, menu_items, request, **kwargs):
        menu_items = super().modify_submenu_items(menu_items, request=request,
                                                  **kwargs)

        menu_items = [m for m in menu_items if m.show_in_menu_test_func(request)]  # noqa

        return menu_items


class ReviewAbstractsPage(RoutablePageMixin, SambaPage):
    parent_page_types = ['samba_home.ReviewParentPage']
    subpage_types = []
    max_count_per_parent = 1

    def get_extra_context(self, request):
        context = self.get_context(request)
        context['conference'] = context['page'].get_parent().get_parent()
        context['title_prefix'] = 'Review'

        return context

    def show_in_menu_test_func(self, request):
        abstract_reviewer_group = \
            self.get_parent().get_parent().specific.abstract_reviewer_group
        return abstract_reviewer_group.user_set.all().filter(
            pk=request.user.pk).exists()

    @route(r'^$')
    def show_abstracts(self, request):
        from home.views.registrationmanagement import AbstractsReviewListView
        view = AbstractsReviewListView()
        view.request = request
        view.extra_context = self.get_extra_context(request)
        view.queryset = view.model.objects.filter(review_status='pending')

        return view.dispatch(request)

    @route(r'^review_abstract/(\d+)/$')
    def review_abstract(self, request, pk):
        from home.views.registrationmanagement import ReviewAbstractView
        view = ReviewAbstractView()
        view.setup(request, pk=pk)
        view.extra_context = self.get_extra_context(request)

        return view.dispatch(request)


class ReviewExtendedAbstractsPage(RoutablePageMixin, SambaPage):
    parent_page_types = ['samba_home.ReviewParentPage']
    subpage_types = []
    max_count_per_parent = 1

    def get_extra_context(self, request):
        context = self.get_context(request)
        context['conference'] = context['page'].get_parent().get_parent()
        context['title_prefix'] = 'Review'

        return context

    def show_in_menu_test_func(self, request):
        abstract_reviewer_group = (self.get_parent()
                                   .get_parent()
                                   .specific
                                   .extended_abstract_reviewer_group)
        return abstract_reviewer_group.user_set.all().filter(
            pk=request.user.pk).exists()

    @route(r'^$')
    def show_abstracts(self, request):
        from home.views.registrationmanagement import (
            ExtendedAbstractsReviewListView)
        view = ExtendedAbstractsReviewListView()
        view.request = request
        view.extra_context = self.get_extra_context(request)
        view.queryset = view.model.objects.filter(review_status='pending')

        return view.dispatch(request)

    @route(r'^review_abstract/(\d+)/$')
    def review_abstract(self, request, pk):
        from home.views.registrationmanagement import (
            ReviewExtendedAbstractView)
        view = ReviewExtendedAbstractView()
        view.setup(request, pk=pk)
        view.extra_context = self.get_extra_context(request)

        return view.dispatch(request)


class AbstractsPage(ExposeToTemplatePageMixin, SambaPage):
    class Meta:
        verbose_name = 'Poster Abstact Overview'

    parent_page_types = ['samba_home.Conference']
    subpage_types = []
    max_count_per_parent = 1

    intro = get_default_streamfield(True)

    content_panels = SambaPage.content_panels + [
        FieldPanel('intro')
    ]

    def serve(self, request, *args, **kwargs):
        from home.views.abstract_page import AbstractPageView
        request.is_preview = getattr(request, 'is_preview', False)

        context = self.get_context(request, *args, **kwargs)

        view = AbstractPageView()
        view.setup(request)
        view.conference = context['page'].get_parent()
        view.extra_context = context

        return view.dispatch(request)
