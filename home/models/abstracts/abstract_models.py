
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db import models
from django.utils.functional import cached_property
from django.template.loader import render_to_string
from django.core.validators import RegexValidator
from private_storage.fields import PrivateFileField
from private_storage.storage.files import PrivateFileSystemStorage

from home.modelfields import AuthorToAffiliationField
from home.models import Affiliation
from home.signals import (abstract_submitted, extended_abstract_submitted,
                          abstract_accepted, abstract_rejected,
                          extended_abstract_accepted,
                          extended_abstract_rejected, poster_number_assigned)
import PIL

superscript_map = {
    "0": "⁰", "1": "¹", "2": "²", "3": "³", "4": "⁴", "5": "⁵", "6": "⁶",
    "7": "⁷", "8": "⁸", "9": "⁹", "a": "ᵃ", "b": "ᵇ", "c": "ᶜ", "d": "ᵈ",
    "e": "ᵉ", "f": "ᶠ", "g": "ᵍ", "h": "ʰ", "i": "ᶦ", "j": "ʲ", "k": "ᵏ",
    "l": "ˡ", "m": "ᵐ", "n": "ⁿ", "o": "ᵒ", "p": "ᵖ", "q": "۹", "r": "ʳ",
    "s": "ˢ", "t": "ᵗ", "u": "ᵘ", "v": "ᵛ", "w": "ʷ", "x": "ˣ", "y": "ʸ",
    "z": "ᶻ", "A": "ᴬ", "B": "ᴮ", "C": "ᶜ", "D": "ᴰ", "E": "ᴱ", "F": "ᶠ",
    "G": "ᴳ", "H": "ᴴ", "I": "ᴵ", "J": "ᴶ", "K": "ᴷ", "L": "ᴸ", "M": "ᴹ",
    "N": "ᴺ", "O": "ᴼ", "P": "ᴾ", "Q": "Q", "R": "ᴿ", "S": "ˢ", "T": "ᵀ",
    "U": "ᵁ", "V": "ⱽ", "W": "ᵂ", "X": "ˣ", "Y": "ʸ", "Z": "ᶻ", "+": "⁺",
    "-": "⁻", "=": "⁼", "(": "⁽", ")": "⁾"}

superscript_trans = str.maketrans(
    ''.join(superscript_map.keys()),
    ''.join(superscript_map.values()))


class Abstract(models.Model):
    user = models.ForeignKey(get_user_model(),
                             on_delete=models.CASCADE)
    conference = models.ForeignKey('samba_home.Conference',
                                   on_delete=models.CASCADE)

    title = models.CharField(max_length=255)
    content = models.TextField()
    review_status = models.CharField(max_length=255,
                                     choices=(
                                         ('pending', 'Pending'),
                                         ('accepted', 'Accepted'),
                                         ('rejected', 'Rejected')
                                     ),
                                     default='pending')
    reviewed_by = models.ForeignKey(get_user_model(),
                                    on_delete=models.CASCADE,
                                    blank=True,
                                    null=True,
                                    related_name='reviewed_by_user')
    review_notes = models.TextField(blank=True)

    poster_number = models.PositiveIntegerField(blank=True, null=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__last_review_status = self.review_status

    def get_affiliations_indexed_list(self):
        affiliation_list = list()
        for cur_author in self.abstractauthor_set.all().order_by('position'):
            aff_pks = cur_author.authortoaffiliation_set.all().order_by(
                'order').values_list('affiliation', flat=True)
            affiliations = [Affiliation.objects.get(pk=pk) for pk in aff_pks]

            for cur_affiliation in affiliations:
                if cur_affiliation not in affiliation_list:
                    affiliation_list.append(cur_affiliation)

        return affiliation_list

    def get_authors_indexed_list(self):
        authors = dict()
        affiliation_list = self.get_affiliations_indexed_list()

        for cur_author in self.abstractauthor_set.all().order_by('position'):
            this_aff_list = []
            for cur_affiliation in cur_author.affiliations.all():
                aff_index = affiliation_list.index(cur_affiliation) + 1
                this_aff_list.append(aff_index)

            this_aff_list.sort()
            authors[cur_author.full_name] = this_aff_list

        return authors

    def get_authors_indexed_string(self):
        authors = self.get_authors_indexed_list()

        authors_strings_list = []
        for author, aff_idxs in authors.items():
            aff_str = ','.join([str(s) for s in aff_idxs])\
                .translate(superscript_trans)
            author_str = author + aff_str
            authors_strings_list.append(author_str)

        return ', '.join(authors_strings_list)

    def get_affiliations_indexed_string(self):
        affiliations = self.get_affiliations_indexed_list()

        affiliations_strings_list = []
        for aff_idx, aff in enumerate(affiliations):
            aff_string = (str(aff_idx + 1).translate(superscript_trans) +
                          str(aff))
            affiliations_strings_list.append(aff_string)

        return ', '.join(affiliations_strings_list)

    def save(self, *args, **kwargs):
        new_poster_number = False
        if self.pk is None:
            new_abstract = True
        else:
            new_abstract = False

        if new_abstract:
            if Abstract.objects.filter(conference=self.conference,
                                       user=self.user).exists():
                raise ValidationError('Cannot submit two abstracts.')

            if not self.conference.can_submit_abstract(self.user):
                raise ValidationError('Abstract submission not possible.')

        if self.review_status != self.__last_review_status or self.pk is None:
            if self.review_status == 'accepted':
                abstract_accepted.send(sender=self.__class__,
                                       abstract=self)
                if (self.registration is not None and
                        self.registration.payment_status in ('complete',
                                                             'pay_at_conference') and   # noqa
                        self.poster_number is None):
                    self.poster_number = self.conference.highest_poster_number + 1  # noqa
                    new_poster_number = True
            if self.review_status == 'rejected':
                abstract_rejected.send(sender=self.__class__,
                                       abstract=self)

        super().save(*args, **kwargs)
        self.conference.participant_group.user_set.add(self.user)

        if new_abstract:
            abstract_submitted.send(sender=self.__class__,
                                    abstract=self)

        if new_poster_number:
            poster_number_assigned.send(sender=self.__class__,
                                        abstract=self)

    def clean(self):
        if self.review_status == 'rejected':
            if not self.review_notes:
                raise ValidationError('Notes must be filled out if the '
                                      'abstract is rejected')

    def __str__(self):
        return 'Abstract "%s" for %s by %s %s' % (
            self.title,
            self.conference,
            self.user.first_name,
            self.user.last_name
        )

    @cached_property
    def management_page(self):
        from .. import AbstractManagementPage
        management_page = (AbstractManagementPage.objects
                           .descendant_of(self.conference)
                           .first())
        return management_page

    @cached_property
    def registration(self):
        from .. import Registration
        return Registration.objects.filter(
            conference=self.conference,
            user=self.user
        ).first()

    @property
    def extended_abstract(self):
        try:
            return self.extendedabstract
        except ObjectDoesNotExist:
            return None

    @property
    def html_indexed_authors(self):
        return render_to_string(
            'samba_home/abstract_detail/authors.html',
            self._generate_context(self.get_authors_indexed_list())
        )

    @property
    def html_indexed_affiliations(self):
        return render_to_string(
            'samba_home/abstract_detail/affiliations.html',
            self._generate_context(self.get_affiliations_indexed_list())
        )

    def _generate_context(self, o):
        return dict(detail_object=o)


class AbstractAuthor(models.Model):
    full_name = models.CharField(max_length=255, validators=[
        RegexValidator(',', inverse_match=True,
                       message='Please enter only one author per field '
                               'and use the + sign to add more!')
    ])
    affiliations = AuthorToAffiliationField(Affiliation,
                                            through='AuthorToAffiliation')
    abstract = models.ForeignKey(Abstract,
                                 on_delete=models.CASCADE)
    position = models.PositiveIntegerField()

    def __str__(self):
        return self.full_name

    def save(self, *args, **kwargs):
        return super().save(*args, **kwargs)


class AuthorToAffiliation(models.Model):
    author = models.ForeignKey(AbstractAuthor, on_delete=models.CASCADE)
    affiliation = models.ForeignKey(Affiliation, on_delete=models.CASCADE)
    order = models.IntegerField()


def upload_extended_abstract_figure_to(instance, filename):
    return 'figures/%s/%s/%s' % (instance.conference.slug,
                                 instance.user.username,
                                 filename)


class ExtendedAbstract(models.Model):
    base_abstract = models.OneToOneField(Abstract,
                                         on_delete=models.CASCADE)
    content = models.TextField()
    figure = PrivateFileField(upload_to=upload_extended_abstract_figure_to,
                              max_file_size=10 * 1024 * 1024,
                              storage=PrivateFileSystemStorage(
                                  base_url='extendedabstract-figure'))
    study_method = models.ManyToManyField('samba_home.StudyMethod')
    study_field = models.ManyToManyField('samba_home.StudyField')
    review_status = models.CharField(max_length=255,
                                     choices=(
                                         ('pending', 'Pending'),
                                         ('accepted', 'Accepted'),
                                         ('rejected', 'Rejected')
                                     ),
                                     default='pending')
    review_notes = models.TextField(blank=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__last_review_status = self.review_status

    @cached_property
    def user(self):
        return self.base_abstract.user

    @cached_property
    def conference(self):
        return self.base_abstract.conference

    @cached_property
    def management_page(self):
        from .. import ExtendedAbstractManagementPage
        management_page = (ExtendedAbstractManagementPage.objects
                           .descendant_of(self.conference)
                           .first())
        return management_page

    def clean(self):
        super().clean()
        try:
            img = PIL.Image.open(self.figure)
        except (FileNotFoundError, PIL.UnidentifiedImageError):
            raise ValidationError('The uploaded file is not an image.')

        if img.format not in ('PNG', 'JPEG'):
            raise ValidationError('Only .png or jpg images are accepted.')

    def save(self, *args, **kwargs):
        if self.pk is None:
            new_abstract = True
        else:
            new_abstract = False

        if new_abstract:
            if not self.conference.can_submit_extended_abstract(self.user):
                raise ValidationError(
                    'Extended Abstract Submission not possible!')

        if self.review_status != self.__last_review_status:
            if self.review_status == 'accepted':
                extended_abstract_accepted.send(sender=self.__class__,
                                                extended_abstract=self)
            if self.review_status == 'rejected':
                extended_abstract_rejected.send(sender=self.__class__,
                                                extended_abstract=self)

        super().save(*args, **kwargs)

        if new_abstract:
            extended_abstract_submitted.send(sender=self.__class__,
                                             extended_abstract=self)

    def __str__(self):
        return 'Extended abstract "%s" for %s by %s %s' % (
            self.base_abstract.title,
            self.conference,
            self.user.first_name,
            self.user.last_name
        )
