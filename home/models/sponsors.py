
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail.admin.edit_handlers import FieldPanel
from .base import SambaPage
from wagtail.core.fields import StreamField
from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock
from home.models.utils import get_default_streamfield
from extra.expose_to_template.mixins import ExposeToTemplatePageMixin


class SponsorBlock(blocks.StructBlock):
    name = blocks.CharBlock()
    logo = ImageChooserBlock()
    url = blocks.URLBlock()

    class Meta:
        label = 'Sponsor'
        icon = 'fa-building'
        template = 'samba_home/blocks/sponsor_list/sponsor.html'


class SponsorLevelBlock(blocks.StructBlock):
    level_name = blocks.CharBlock()
    sponsors = blocks.ListBlock(SponsorBlock())

    class Meta:
        label = 'Sponsor Level'
        icon = 'fa-money'
        template = 'samba_home/blocks/sponsor_list/sponsor_level.html'


class SponsorsListPage(ExposeToTemplatePageMixin, SambaPage):
    parent_page_types = ['samba_home.Conference']
    subpage_types = []
    max_count_per_parent = 1

    intro = get_default_streamfield(True)

    sponsor_list = StreamField([
        ('sponsor_level', SponsorLevelBlock())
    ],
        blank=True)

    content_panels = SambaPage.content_panels + [
        FieldPanel('intro'),
        FieldPanel('sponsor_list')
    ]
