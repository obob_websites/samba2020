
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.core.models import Site

from .base import SambaPage
from .site_settings import GeneralSiteSettings
from home.models.utils import get_default_streamfield
from django.db import models
from home.forms.wagtailmodelforms import AdminPageAutoSlugForm
from extra.expose_to_template.mixins import ExposeToTemplatePageMixin


class SpeakersIndexPage(ExposeToTemplatePageMixin, SambaPage):
    parent_page_types = ['samba_home.Conference']
    subpage_types = ['samba_home.SpeakerPage']
    max_count_per_parent = 1

    intro = get_default_streamfield(True)

    content_panels = SambaPage.content_panels + [
        FieldPanel('intro')
    ]

    @property
    def sorted_speakers(self):
        return SpeakerPage.objects.child_of(self).live().order_by('full_name')\
            .specific()


class SpeakerPage(SambaPage):
    parent_page_types = ['samba_home.SpeakersIndexPage']
    subpage_types = []

    full_name = models.CharField(max_length=255)
    speaker_url = models.URLField(blank=True)
    talk_title = models.CharField(max_length=255, verbose_name='Title', blank=True)
    photo_copyright = models.CharField(max_length=255, blank='True')
    abstract = get_default_streamfield(True)
    biography = get_default_streamfield(True)
    picture = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    affiliation_name = models.CharField(max_length=255, blank=True)
    affiliation_link = models.URLField(blank=True)

    content_panels = [
        FieldPanel('full_name'),
        FieldPanel('picture'),
        FieldPanel('photo_copyright'),
        MultiFieldPanel([FieldPanel('affiliation_name'),
                         FieldPanel('affiliation_link')],
                        heading='Affiliation'),
        FieldPanel('speaker_url'),
        FieldPanel('biography'),
        FieldPanel('talk_title'),
        FieldPanel('abstract')
    ]

    base_form_class = AdminPageAutoSlugForm
    show_in_menus = True
    show_in_menus_default = True

    def full_clean(self, *args, **kwargs):
        self.title = self.full_name
        super().full_clean(*args, **kwargs)

    @property
    def profile_picture(self):
        if self.picture:
            return self.picture
        else:
            site = Site.objects.get(is_default_site=True)
            settings = GeneralSiteSettings.for_site(site)
            return settings.fallback_speaker_image
