
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.utils.html import format_html
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.core.blocks import StreamBlock
from wagtail.core.fields import StreamField
from wagtail.admin.edit_handlers import FieldPanel

from extra.expose_to_template.mixins import ExposeContextItem
from extra.blocks.blocks import FooterBlock


class ExposedLink(ExposeContextItem):
    def __init__(self, tag, description, orig_ctx_item):
        super().__init__(tag, description,
                         lambda o, c: self._getfun(o, c, orig_ctx_item))

    def _getfun(self, instance, context, orig_ctx_item):
        link = context.get(orig_ctx_item, None)
        if link is None:
            return ''

        return format_html('<a href="{}">{}</a>', link, link)


@register_setting
class GeneralSiteSettings(BaseSetting):
    top_logo = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    favicon = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    page_footer = StreamField(
        StreamBlock([
            ('footer', FooterBlock())
        ], max_num=1),
        blank=True)

    fallback_speaker_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    panels = [
        FieldPanel('top_logo'),
        FieldPanel('favicon'),
        FieldPanel('page_footer'),
        FieldPanel('fallback_speaker_image')
    ]
