
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.core.exceptions import ObjectDoesNotExist
from django.db import models


class AffiliationManager(models.Manager):
    def filter_by_names_and_acronyms(self, name):
        all_names = name.split(' ')
        qs = self.get_queryset()
        final_qs = qs

        for cur_name in all_names:
            by_name = qs.filter(institution_primary_name__icontains=cur_name)
            by_alt_name = qs.filter(
                affiliationalternativename__alternative_name__icontains=cur_name)  # noqa
            by_ac = qs.filter(affiliationacronym__acronym__icontains=cur_name)

            final_qs = final_qs & (by_name | by_alt_name | by_ac)

        final_qs = final_qs.annotate(models.Count('id'))
        final_qs = final_qs.distinct()
        final_qs.order_by('id__count')

        return final_qs


class Affiliation(models.Model):
    institution_primary_name = models.CharField(max_length=255)
    source = models.CharField(max_length=255, blank=False, null=False)
    ror_id = models.CharField(max_length=255, blank=True,
                              null=True, unique=True)
    city = models.CharField(max_length=255, blank=True)
    country = models.CharField(max_length=255, blank=True)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True)

    objects = AffiliationManager()

    def __str__(self):
        rval = self.english_name

        if self.city not in rval:
            rval = f'{rval}, {self.city}'

        return f'{rval}, {self.country}'

    @property
    def english_name(self):
        rval = str(self.institution_primary_name)
        try:
            rval = str(
                self.affiliationalternativename_set.get(
                    language_code='en'
                )
            )
        except ObjectDoesNotExist:
            pass

        if self.parent:
            rval = f'{rval}, {self.parent.english_name}'

        return rval

    @property
    def string_for_dal(self):
        rval = str(self.institution_primary_name)

        try:
            rval = '%s - %s' % (rval, self.english_name)
        except ObjectDoesNotExist:
            pass

        if self.city not in rval:
            rval = f'{rval}, {self.city}'

        return f'{rval}, {self.country}'


class AffiliationAlternativeName(models.Model):
    alternative_name = models.CharField(max_length=255)
    affiliation = models.ForeignKey(Affiliation, on_delete=models.CASCADE)
    language_code = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return str(self.alternative_name)


class AffiliationAcronym(models.Model):
    acronym = models.CharField(max_length=255)
    affiliation = models.ForeignKey(Affiliation, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.acronym)
