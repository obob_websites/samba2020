#  Copyright (c) 2016-2021, Thomas Hartmann
#
#  This file is part of the SAMBA2020 Project
#  (https://gitlab.com/obob_websites/samba2020)
#
#  SAMBA2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SAMBA2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SAMBA2020. If not, see <http://www.gnu.org/licenses/>.
from django.db import models
from django_currentuser.db.models import CurrentUserField
from django.core.exceptions import ValidationError
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from home.models import Registration
from . import SambaPage
from .mixins import RestrictMenuToGroupMixin, ServeViewMixin
from extra.blocks.blocks import EmailBlock


def can_checkin(value):
    if not Registration.objects.get(id=value).can_checkin:
        raise ValidationError('Cannot do checkin.')


class Checkin(models.Model):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__is_new = self.id is None

    datetime_added = models.DateTimeField(auto_now_add=True)
    created_by = CurrentUserField()
    registration = models.OneToOneField(Registration,
                                        on_delete=models.CASCADE,
                                        validators=[can_checkin])

    def save(self, *args, **kwargs):
        self.full_clean(exclude=('created_by', ))
        if self.__is_new:
            EmailBlock.send(
                self.registration.conference.email_checked_in,
                [self.registration.user.email],
                extra_context=self.registration.conference.get_expose_context(
                    {'user': self.registration.user})
            )
        return super().save(*args, **kwargs)


class CheckinPage(RestrictMenuToGroupMixin, RoutablePageMixin,
                  ServeViewMixin, SambaPage):
    parent_page_types = ['samba_home.Conference']
    subpage_types = []
    max_count_per_parent = 1

    def get_extra_context(self, request):
        context = super().get_extra_context(request)
        context['conference'] = context['page'].get_parent()
        context['title_prefix'] = 'Manage'

        return context

    def show_in_menu_test_func(self, request):
        manager_group = self.get_parent().specific.manager_group
        return manager_group.user_set.all().filter(
            pk=request.user.pk).exists()

    @route(r'^$')
    def show_participants_to_checkin(self, request):
        from home.views.checkin import ShowParticipantsToCheckin
        return self.serve_view(ShowParticipantsToCheckin, request)
