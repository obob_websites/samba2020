
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from django.utils.safestring import SafeString

from wagtailgeowidget.helpers import geosgeometry_str_to_struct

import home.models
from extra.expose_to_template.mixins import ExposeContextItem


class ExposeNeedsRegistration(ExposeContextItem):
    def get_registration(self, instance, context):
        if 'registration' in context:
            return context['registration']

        if 'user' not in context or isinstance(context['user'], AnonymousUser):
            return None

        try:
            return home.models.Registration.objects.get(
                conference=instance,
                user=context['user']
            )
        except ObjectDoesNotExist:
            return None


class ExposeSpeakersToContext(ExposeContextItem):
    template = 'samba_home/exposed_tags/speaker_list.html'

    def __init__(self, tag='speakers',
                 description='List of speakers'):

        super().__init__(
            tag=tag,
            description=description,
            getfun_or_field=self._get_speaker_list,
            is_valid=self._has_speakers
        )

    def _get_speaker_list(self, object, context):
        speaker_idx_page = object.get_descendants().type(
            home.models.SpeakersIndexPage)[0]

        all_speakers = home.models.SpeakerPage.objects.child_of(speaker_idx_page)\
            .live().order_by('full_name').specific()

        return render_to_string(self.template, {'speakers': all_speakers})

    def _has_speakers(self, object, context):
        try:
            return object.get_descendants().type(
                home.models.SpeakersIndexPage).exists()
        except ValueError as e:
            if str(e) == 'Cannot use None as a query value':
                return False
            raise e


class ExposeFullnameToContext(ExposeContextItem):
    def __init__(self, tag='full_name',
                 description='Full Name of current user'):
        super().__init__(tag=tag,
                         description=description,
                         getfun_or_field=self._get_full_name)

    def _get_full_name(self, object, context):
        if 'user' not in context:
            return ''

        if isinstance(context['user'], AnonymousUser):
            return ''

        return '%s %s' % (context['user'].first_name,
                          context['user'].last_name)


class ExposeEmailToContext(ExposeContextItem):
    def __init__(self, tag='email',
                 description='Email of current user'):
        super().__init__(tag=tag,
                         description=description,
                         getfun_or_field=self._get_email)

    def _get_email(self, object, context):
        if 'user' not in context:
            return ''

        if isinstance(context['user'], AnonymousUser):
            return ''

        return context['user'].email


class ExposePaymentDetails(ExposeContextItem):
    def __init__(self, tag='payment_details',
                 description='Payment details'):
        super().__init__(tag=tag,
                         description=description,
                         getfun_or_field=self._get_payment_info)

    def _get_payment_info(self, object, context):
        return object.payment_details.stream_block.render(
            object.payment_details, context)


class ExposeRegistrationAffiliation(ExposeNeedsRegistration):
    def __init__(self, tag='affiliation',
                 description='Affiliation of the current user'):
        super().__init__(tag=tag,
                         description=description,
                         getfun_or_field=self._get_affiliation)

    def _get_affiliation(self, instance, context):
        registration = self.get_registration(instance, context)
        if not registration:
            return ''

        return registration.affiliation


class ExposeAmountOwed(ExposeNeedsRegistration):
    def __init__(self, tag='amount_owed',
                 description='Amount still owed'):
        super().__init__(tag=tag,
                         description=description,
                         getfun_or_field=self._get_amount_owed)

    def _get_amount_owed(self, instance, context):
        registration = self.get_registration(instance, context)
        if not registration:
            return ''

        return registration.amount_owed


class ExposeAmountPaid(ExposeNeedsRegistration):
    def __init__(self, tag='amount_paid',
                 description='Amount already paid'):
        super().__init__(tag=tag,
                         description=description,
                         getfun_or_field=self._get_amount_paid)

    def _get_amount_paid(self, instance, context):

        registration = self.get_registration(instance, context)
        if not registration:
            return ''

        return registration.amount_paid


class ExposeFeesDeducted(ExposeNeedsRegistration):
    def __init__(self, tag='fees_deducted',
                 description='Fees deducted'):
        super().__init__(tag=tag,
                         description=description,
                         getfun_or_field=self._get_fees_paid)

    def _get_fees_paid(self, instance, context):
        registration = self.get_registration(instance, context)
        if not registration:
            return ''

        return registration.fees_paid


class ExposeAbstractTitle(ExposeContextItem):
    def __init__(self, tag='abstract_title',
                 description='Abstract Title'):
        super().__init__(tag=tag,
                         description=description,
                         getfun_or_field=self._get_abstract_title)

    def _get_abstract_title(self, instance, context):
        if isinstance(context['user'], AnonymousUser):
            return ''

        try:
            return home.models.Abstract.objects.get(
                conference=instance,
                user=context['user']
            ).title
        except ObjectDoesNotExist:
            return ''


class ExposeAbstractReviewNote(ExposeContextItem):
    def __init__(self, tag='abstract_review_note',
                 description='Abstract reviewers notes'):
        super().__init__(tag=tag,
                         description=description,
                         getfun_or_field=self._get_abstract_notes)

    def _get_abstract_notes(self, instance, context):
        if isinstance(context['user'], AnonymousUser):
            return ''

        try:
            return home.models.Abstract.objects.get(
                conference=instance,
                user=context['user']
            ).review_notes
        except ObjectDoesNotExist:
            return ''


class ExposeAbstractAuthors(ExposeContextItem):
    def __init__(self, tag='abstract_authors',
                 description='Abstract Authors'):
        super().__init__(tag=tag,
                         description=description,
                         getfun_or_field=self._get_abstract_authors)

    def _get_abstract_authors(self, instance, context):
        if isinstance(context['user'], AnonymousUser):
            return ''

        try:
            abstract = home.models.Abstract.objects.get(
                conference=instance,
                user=context['user']
            )
            return abstract.get_authors_indexed_string()
        except ObjectDoesNotExist:
            return ''


class ExposeAbstractAffiliations(ExposeContextItem):
    def __init__(self, tag='abstract_affiliations',
                 description='Abstract Affiliations'):
        super().__init__(tag=tag,
                         description=description,
                         getfun_or_field=self._get_abstract_affiliations)

    def _get_abstract_affiliations(self, instance, context):
        if isinstance(context['user'], AnonymousUser):
            return ''

        try:
            abstract = home.models.Abstract.objects.get(
                conference=instance,
                user=context['user']
            )
            return abstract.get_affiliations_indexed_string()
        except ObjectDoesNotExist:
            return ''


class ExposePaymentConfirmationButton(ExposeContextItem):
    template = 'samba_home/exposed_tags/payment_confirmation_button.html'

    def __init__(self, tag='payment_confirmation_button',
                 description='Payment confirmation button'):
        super().__init__(tag=tag,
                         description=description,
                         getfun_or_field=self._get_payment_confirmation_button)

    def _get_payment_confirmation_button(self, instance, context):
        if not context['user'] or isinstance(context['user'], AnonymousUser):
            return ''
        try:
            reg_pk = home.models.Registration.objects.get(
                conference=instance.get_parent(),
                user=context['user']
            ).pk
        except ObjectDoesNotExist:
            return ''

        return render_to_string(self.template, {'pk': reg_pk})


class ExposeParticipationConfirmationButton(ExposeContextItem):
    template = 'samba_home/exposed_tags/participation_confirmation_button.html'

    def __init__(self, tag='participation_confirmation_button',
                 description='Participation confirmation button'):
        super().__init__(
            tag=tag,
            description=description,
            getfun_or_field=self._get_participation_confirmation_button
        )

    def _get_participation_confirmation_button(self, instance, context):
        if not context['user'] or isinstance(context['user'], AnonymousUser):
            return ''
        try:
            reg_pk = home.models.Registration.objects.get(
                conference=instance.get_parent(),
                user=context['user']
            ).pk
        except ObjectDoesNotExist:
            return ''

        return render_to_string(self.template, {'pk': reg_pk})


class ExposeLocationMap(ExposeContextItem):
    template = 'samba_home/exposed_tags/location_map.html'

    def __init__(self, tag='location_map',
                 description='Map of the conference location'):

        super().__init__(
            tag=tag,
            description=description,
            getfun_or_field=self._get_locationmap
        )

    def _get_locationmap(self, instance, context):
        if not instance.location:
            return ''

        location = geosgeometry_str_to_struct(instance.location)
        if location is None:
            return ''

        location['conference_title'] = instance.title

        return render_to_string(self.template, location)


class ExposePaymentDeadline(ExposeContextItem):
    def __init__(self, tag='payment_deadline',
                 description='Payment Deadline'):
        super().__init__(
            tag=tag,
            description=description,
            getfun_or_field=self._get_payment_deadline
        )

    def _get_payment_deadline(self, instance, context):
        if isinstance(context['user'], AnonymousUser):
            return ''

        try:
            registration = home.models.Registration.objects.get(
                conference=instance,
                user=context['user']
            )
        except ObjectDoesNotExist:
            return ''

        return registration.unregister_date


class ExposePosterNumber(ExposeContextItem):
    def __init__(self, tag='poster_number',
                 description='Poster Number'):
        super().__init__(
            tag=tag,
            description=description,
            getfun_or_field=self._get_poster_number
        )

    def _get_poster_number(self, instance, context):
        if isinstance(context['user'], AnonymousUser):
            return ''

        try:
            abstract = home.models.Abstract.objects.get(
                conference=instance,
                user=context['user']
            )
        except ObjectDoesNotExist:
            return ''

        n_poster = abstract.poster_number
        if n_poster is None:
            n_poster = ''

        return n_poster


class ExposeBoughtTickets(ExposeContextItem):
    def __init__(self, tag='ticket_bought',
                 description='Bought Ticket'):
        super().__init__(
            tag=tag,
            description=description,
            getfun_or_field=self._get_bought_ticket
        )

    def _get_bought_ticket(self, instance, context):
        if isinstance(context['user'], AnonymousUser):
            return ''

        try:
            registration = home.models.Registration.objects.get(
                conference=instance,
                user=context['user']
            )
        except ObjectDoesNotExist:
            return ''

        return registration.ticket


class ExposeBoughtTicketOptions(ExposeContextItem):
    def __init__(self, tag='ticket_options',
                 description='Selected Ticket Options'):
        super().__init__(
            tag=tag,
            description=description,
            getfun_or_field=self._get_ticket_options
        )

    def _get_ticket_options(self, instance, context):
        if isinstance(context['user'], AnonymousUser):
            return ''

        try:
            registration = home.models.Registration.objects.get(
                conference=instance,
                user=context['user']
            )
        except ObjectDoesNotExist:
            return ''

        html = '<ul>'
        for o in registration.ticket_options.all():
            html += f'<li>{str(o)}</li>'

        html += '</ul>'

        return SafeString(html)


class ExposeTotalAmount(ExposeNeedsRegistration):
    def __init__(self, tag='total_conference_fee',
                 description='Total Conference Fee'):
        super().__init__(
            tag=tag,
            description=description,
            getfun_or_field=self._get_total_amount
        )

    def _get_total_amount(self, instance, context):
        registration = self.get_registration(instance, context)
        if not registration:
            return ''

        return registration.total_amount
