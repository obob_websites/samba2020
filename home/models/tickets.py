from modelcluster.models import ClusterableModel
from wagtail.admin.panels import FieldPanel
from wagtail.models import Orderable
from modelcluster.fields import ParentalKey
from django.db import models
from django.core.exceptions import ValidationError
from queryable_properties.managers import QueryablePropertiesManager
from queryable_properties.properties import queryable_property, RangeCheckProperty
import datetime

from home.modelfields import DefaultMoneyField


class ConferenceTicket(ClusterableModel, Orderable):
    objects = QueryablePropertiesManager()

    name = models.CharField(max_length=255)
    ticket_price = DefaultMoneyField()
    available = models.BooleanField(default=True)
    not_before = models.DateField(blank=True, null=True)
    not_after = models.DateField(blank=True, null=True)

    ticket_in_time_range = RangeCheckProperty('not_before', 'not_after', datetime.date.today,
                                              include_boundaries=True, include_missing=True)

    page = ParentalKey('Conference',
                       on_delete=models.CASCADE,
                       related_name='conference_tickets')

    panels = [
        FieldPanel('name'),
        FieldPanel('ticket_price'),
        FieldPanel('not_before'),
        FieldPanel('not_after'),
        FieldPanel('available'),
    ]

    @queryable_property(annotation_based=True)
    @classmethod
    def tickets_sold(cls):
        return models.Count('registration')

    def save(self, *args, **kwargs):
        if self.id is not None:
            old_payment_status = {r.pk: r.payment_status for r in self.registration_set.all()}  # noqa
        super().save(*args, **kwargs)
        if self.id is not None:
            for reg in self.registration_set.all():
                reg.check_payment_status_update(old_payment_status[reg.pk])

    def __str__(self):
        return f'{self.name} ({self.ticket_price})'

    def clean(self):
        if self.not_before and self.not_after:
            if self.not_after < self.not_before:
                raise ValidationError(
                    'Not before must be earlier than Not After'
                )

        super().clean()


class ConferenceTicketOption(Orderable):
    objects = QueryablePropertiesManager()

    name = models.CharField(max_length=255)
    option_price = DefaultMoneyField()

    available = models.BooleanField(default=True)
    limit = models.IntegerField(blank=True, null=True)

    page = ParentalKey('Conference',
                       on_delete=models.CASCADE,
                       related_name='conference_options')

    panels = [
        FieldPanel('name'),
        FieldPanel('option_price'),
        FieldPanel('available'),
        FieldPanel('limit')
    ]

    @queryable_property(annotation_based=True)
    @classmethod
    def ticket_options_sold(cls):
        return models.Count('registration')

    @queryable_property(annotation_based=True)
    @classmethod
    def ticket_options_available(cls):
        return models.Case(
            models.When(
                models.lookups.GreaterThanOrEqual(models.F('limit'), 1),
                (models.F('limit') - models.Count('registration'))
            ),
            default=models.Value(99999)
        )

    def save(self, *args, **kwargs):
        if self.id is not None:
            old_payment_status = {r.pk: r.payment_status for r in self.registration_set.all()}  # noqa
        super().save(*args, **kwargs)
        if self.id is not None:
            for reg in self.registration_set.all():
                reg.check_payment_status_update(old_payment_status[reg.pk])

    def __str__(self):
        return f'{self.name} ({self.option_price})'
