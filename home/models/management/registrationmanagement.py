
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail.contrib.routable_page.models import RoutablePageMixin, route

from home.models.mixins import ServeViewMixin
from home.models import SambaPage


class RegistrationManagementPage(RoutablePageMixin, ServeViewMixin, SambaPage):
    parent_page_types = ['samba_home.ManagementParentPage']
    subpage_types = []
    max_count_per_parent = 1

    def get_extra_context(self, request):
        context = self.get_context(request)
        context['conference'] = context['page'].get_parent().get_parent()

        return context

    def show_in_menu_test_func(self, request):
        manager_group = self.get_parent().get_parent().specific.manager_group
        return manager_group.user_set.all().filter(
            pk=request.user.pk).exists()

    @route(r'^$')
    def show_registrations(self, request):
        from home.views.registrationmanagement import Registrations
        return self.serve_view(Registrations, request)

    @route(r'^registration/(\d+)/$')
    def show_registration_detail(self, request, pk):
        from home.views.registrationmanagement import RegistrationDetail
        return self.serve_view(RegistrationDetail, request, pk=pk)

    @route(r'^add_payment/(\d+)/$')
    def add_payment(self, request, pk_registration):
        from home.views.registrationmanagement import NewPayment
        return self.serve_view(NewPayment, request,
                               pk_registration=pk_registration)

    @route(r'^download_excel/$')
    def download_excel(self, request):
        from home.views.registrationmanagement import DownloadExcel
        return self.serve_view(DownloadExcel, request)
