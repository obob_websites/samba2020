
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail.contrib.routable_page.models import RoutablePageMixin, route

from home.models import SambaPage


class EmailParticipantsPage(RoutablePageMixin, SambaPage):
    parent_page_types = ['samba_home.ManagementParentPage']
    subpage_types = []
    max_count_per_parent = 1

    def show_in_menu_test_func(self, request):
        manager_group = self.get_parent().get_parent().specific.manager_group
        return manager_group.user_set.all().filter(
            pk=request.user.pk).exists()

    def get_extra_context(self, request):
        context = self.get_context(request)
        context['conference'] = context['page'].get_parent().get_parent()
        context['title_prefix'] = 'Email'

        return context

    def dispatch_view(self, view, request):
        view_instance = view()
        view_instance.request = request
        view_instance.extra_context = self.get_extra_context(request)

        return view_instance.dispatch(request)

    @route(r'^$')
    def choose_recipients(self, request):
        from home.views.registrationmanagement.email import ChooseRecipients

        return self.dispatch_view(ChooseRecipients, request)

    @route(r'^write_email/$')
    @route(r'^write_email/(?P<user_id>[0-9]*)')
    def write_email(self, request, user_id=None):
        from home.views.registrationmanagement.email import WriteEmail

        if user_id:
            request.session['email_recipients'] = [user_id]

        return self.dispatch_view(WriteEmail, request)

    @route(r'^verify_email/$')
    def verify_email(self, request):
        from home.views.registrationmanagement.email import VerifyEmail

        return self.dispatch_view(VerifyEmail, request)

    @route(r'^email_result/$')
    def email_result(self, request):
        from home.views.registrationmanagement.email import EmailSuccess

        return self.dispatch_view(EmailSuccess, request)
