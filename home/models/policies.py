
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from .base import SambaPage
from extra.expose_to_template.mixins import ExposeToTemplatePageMixin
from home.models import get_default_streamfield, FieldPanel


class PrivacyPolicyPage(ExposeToTemplatePageMixin, SambaPage):
    parent_page_types = ['samba_home.Conference']
    subpage_types = []
    max_count_per_parent = 1
    template = 'samba_home/normal_page.html'

    body = get_default_streamfield()

    content_panels = SambaPage.content_panels + [
        FieldPanel('body')
    ]


class RefundPolicyPage(ExposeToTemplatePageMixin, SambaPage):
    parent_page_types = ['samba_home.Conference']
    subpage_types = []
    max_count_per_parent = 1
    template = 'samba_home/normal_page.html'

    body = get_default_streamfield()

    content_panels = SambaPage.content_panels + [
        FieldPanel('body')
    ]
