
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
import home.factories
import home.models
import pytest
from faker import Faker
from django.utils.text import slugify
from selenium.webdriver.common.by import By


@pytest.mark.django_db
def test_simple_sponsor(samba, admin_user, conference):
    faker = Faker()
    sponsor_list_page = home.factories.SponsorListFactory.create(
        parent=conference['conference_page'],
        title=faker.word(),
        intro__0__paragraph__dummy=None,
        sponsor_list__0__sponsor_level__sponsors__0__name='TH Inc',
        sponsor_list__1__sponsor_level__sponsors__0__name='TH2 Inc',
        sponsor_list__1__sponsor_level__sponsors__1__name='TH3 Inc',
    )

    samba.goto_url(sponsor_list_page.url)

    samba.selenium.assert_text(sponsor_list_page.title)
    samba.assert_streamfield_blocks_present(sponsor_list_page.intro)

    for idx, cur_sp_block in enumerate(sponsor_list_page.sponsor_list):
        cur_block = cur_sp_block.value
        samba.selenium.assert_text(cur_block['level_name'])

        assert len(cur_block['sponsors']) == idx + 1

        for cur_sponsor in cur_block['sponsors']:
            logo_img = samba.selenium.find_element(
                '#logo-%s' % (slugify(cur_sponsor['name'], )))
            assert logo_img.get_attribute('alt') == cur_sponsor['name']
            parent = logo_img.find_element(By.XPATH, '..')
            assert parent.tag_name == 'a'
            assert parent.get_attribute('href') == cur_sponsor['url']
