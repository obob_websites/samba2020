
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import math
import time
from faker import Faker

from django.core.exceptions import ValidationError, ObjectDoesNotExist

import home.factories
import home.models
import pytest
from selenium.webdriver.support.ui import Select


def test_abstract_in_selenium(samba, conference, conference_user, admin_user,
                              mailoutbox):
    home.factories.Abstract.create_in_browser(
        samba,
        conference=conference['conference_page'],
        user=conference_user['user'])

    assert home.models.Abstract.objects.count() == 1
    assert (conference_user['user'] in
            conference['conference_page'].participant_group.user_set.all())

    assert len(mailoutbox) == 2
    assert 'Thanks for submitting your abstract' in mailoutbox[1].body
    assert conference_user['user_info']['first_name'] in mailoutbox[1].body
    assert conference_user['user_info']['last_name'] in mailoutbox[1].body
    assert conference['conference_page'].title in mailoutbox[1].body


def test_cannot_submit_two_abstracts(samba, conference, conference_user):
    home.factories.Abstract.create_in_browser(
        samba,
        conference=conference['conference_page'],
        user=conference_user['user'])

    with pytest.raises(ValidationError):
        home.factories.Abstract.create_in_browser(
            samba,
            conference=conference['conference_page'],
            user=conference_user['user'])

    assert home.models.Abstract.objects.count() == 1


@pytest.mark.django_db
@pytest.mark.parametrize('user', [None, 'conference', 'manager', 'reviewer',
                                  'extended_reviewer'])
def test_permissions(samba, conference, conference_user, user):
    home.factories.Abstract.create(conference=conference['conference_page'],
                                   user=conference_user['user'])

    conference['registration_management_page'].show_in_menus = True

    samba.add_page_to_menu(
        conference['review_base_page'],
        exclude_if_unauthorized=True
    )

    samba.login_with_role(user, conference, conference_user)

    samba.goto_url(conference['review_pages']['abstracts'].url)

    if user is None:
        samba.selenium.assert_text_not_visible('Review abstracts')
        samba.selenium.assert_text_not_visible('Review',
                                               '.nav-item')
        samba.selenium.assert_text('Login')
    elif user in ('conference', 'manager', 'extended_reviewer'):
        samba.selenium.assert_text('Forbidden')
    else:
        samba.selenium.assert_text('Review abstracts')
        samba.selenium.assert_text('Review', '.nav-item')

    samba.goto_url(
        conference['review_pages']['abstracts'].url +
        conference['review_pages']['abstracts'].reverse_subpage(
            'review_abstract',
            args=[home.models.Abstract.objects.all()[0].pk]))

    if user is None:
        samba.selenium.assert_text_not_visible(
            'Review %s' % (conference['conference_page'].title, ))
        samba.selenium.assert_text_not_visible('Review',
                                               '.nav-item')
        samba.selenium.assert_text('Login')
    elif user in ('conference', 'manager', 'extended_reviewer'):
        samba.selenium.assert_text('Forbidden')
    else:
        samba.selenium.assert_text(
            'Review %s' % (conference['conference_page'].title,)
        )
        samba.selenium.assert_text('Review', '.nav-item')


@pytest.mark.parametrize('reject', [True, False])
def test_must_add_note_if_abstract_rejected(samba, conference, conference_user,
                                            reject):
    samba.logout()
    abstract = home.factories.Abstract.create(
        conference=conference['conference_page'],
        user=conference_user['user'])

    this_user = home.factories.UserWithConfirmedEmail(
        raw_password='pw'
    )

    conference['conference_page'].abstract_reviewer_group.user_set.add(
        this_user)

    if reject:
        abstract.review_status = 'rejected'
    else:
        abstract.review_status = 'accepted'

    abstract.reviewed_by = this_user

    if reject:
        with pytest.raises(ValidationError):
            abstract.full_clean()
    else:
        abstract.full_clean()


@pytest.mark.parametrize('accept_abstract', [True, False])
def test_abstract_submission_workflow(samba, conference, conference_user,
                                      mailoutbox, accept_abstract):
    reviewer = home.factories.UserWithConfirmedEmail(
        raw_password='pw'
    )

    conference['conference_page'].abstract_reviewer_group.user_set.add(
        reviewer)

    home.factories.Abstract.create_in_browser(
        samba,
        conference=conference['conference_page'],
        user=conference_user['user'])

    assert home.models.Abstract.objects.count() == 1
    assert (conference_user['user'] in
            conference['conference_page'].participant_group.user_set.all())

    assert len(mailoutbox) == 2
    assert 'Thanks for submitting your abstract' in mailoutbox[1].body
    assert conference_user['user_info']['first_name'] in mailoutbox[1].body
    assert conference_user['user_info']['last_name'] in mailoutbox[1].body
    assert conference['conference_page'].title in mailoutbox[1].body

    samba.goto_url(conference['registration_page'].url)

    samba.selenium.assert_text_not_visible('Abstract submission is open but '
                                           'no abstract was submitted')
    samba.selenium.assert_text('Abstract review pending')

    samba.login(reviewer.email, 'pw')

    samba.goto_url(conference['review_pages']['abstracts'].url)
    samba.selenium.click_link('Review')

    if accept_abstract:
        samba.selenium.select_option_by_text('#id_review_status',
                                             'Accepted')
    else:
        samba.selenium.select_option_by_text('#id_review_status',
                                             'Rejected')

    samba.selenium.update_text('#id_review_notes', 'These are notes.')
    samba.selenium.submit('#id_review_notes')

    assert len(mailoutbox) == 3
    mail_body = mailoutbox[2].body
    if accept_abstract:
        assert 'Abstract accepted' in mail_body
    else:
        assert 'Abstract rejected' in mail_body

    assert conference_user['user_info']['first_name'] in mail_body
    assert conference_user['user_info']['last_name'] in mail_body
    assert conference['conference_page'].title in mail_body

    samba.login(conference_user['user_info'])
    samba.goto_url(conference['registration_page'].url)

    samba.selenium.assert_text_not_visible('Abstract submission is open but '
                                           'no abstract was submitted')
    samba.selenium.assert_text_not_visible('Abstract review pending')
    if accept_abstract:
        samba.selenium.assert_text('Abstract accepted')
        samba.selenium.assert_text_not_visible('Abstract rejected')
    else:
        samba.selenium.assert_text('Abstract rejected')
        samba.selenium.assert_text_not_visible('Abstract accepted')


@pytest.mark.parametrize('deadline_ok', [True, False])
@pytest.mark.parametrize('already_submitted', [True, False])
@pytest.mark.parametrize('registration_open, registered, registration_deadline_ok',  # noqa
                         [
                             (False, False, False),
                             (True, True, False),
                             (True, False, False),
                             (True, False, True)
                         ])
@pytest.mark.parametrize('abstract_submissions_full', [True, False])
@pytest.mark.parametrize('in_browser', [True, False])
def test_abstract_allowed_forbidden(samba, conference, mailoutbox,
                                    registration_open, deadline_ok,
                                    in_browser, already_submitted,
                                    registered, registration_deadline_ok,
                                    abstract_submissions_full):
    today = datetime.date.today()
    conf = conference['conference_page']
    user = home.factories.UserWithConfirmedEmail.create(raw_password='pw')
    samba.login(user.email, 'pw')

    is_ok = (deadline_ok and registration_open and not already_submitted and
             (registered or registration_deadline_ok) and
             not abstract_submissions_full)

    assert conf.n_abstracts_submitted_and_accepted == 0
    assert conf.n_abstract_submission_left == 15

    if already_submitted:
        home.factories.Abstract.create(
            conference=conf,
            user=user
        )
        assert conf.n_abstracts_submitted_and_accepted == 1
        assert conf.n_abstract_submission_left == 14

    home.factories.Abstract.create_batch(
        size=10,
        conference=conf,
        review_status='rejected'
    )

    if already_submitted:
        assert conf.n_abstracts_submitted_and_accepted == 1
        assert conf.n_abstract_submission_left == 14
    else:
        assert conf.n_abstracts_submitted_and_accepted == 0
        assert conf.n_abstract_submission_left == 15

    if abstract_submissions_full:
        home.factories.Abstract.create_batch(
            size=math.floor(conf.n_abstract_submission_left / 2),
            conference=conf,
            review_status='accepted'
        )

        home.factories.Abstract.create_batch(
            size=conf.n_abstract_submission_left,
            conference=conf,
            review_status='pending'
        )

        assert conf.n_abstract_submission_left == 0
        assert conf.n_abstracts_submitted_and_accepted == 15

    if not deadline_ok:
        conf.abstract_deadline = today + datetime.timedelta(-1)
        conf.save()

    if not registration_open:
        conf.registration_start_date = today + datetime.timedelta(1)
        conf.save()

    if registered:
        home.factories.Registration.create(
            conference=conf,
            user=user
        )

    if not registration_deadline_ok:
        conf.registration_deadline = today + datetime.timedelta(-1)
        conf.save()

    if in_browser:
        samba.goto_url(conference['registration_page'].url)
        if is_ok:
            samba.selenium.assert_text('Abstract submission is open')
        else:
            samba.selenium.assert_text_not_visible('Abstract submission '
                                                   'is open')
        samba.goto_url(conference['form_pages']['abstract'].url)
        samba.selenium.assert_text_not_visible('Internal server error')

        if is_ok:
            samba.selenium.assert_text('Abstract Form Page')
            home.factories.Abstract.create_in_browser(
                samba,
                conference=conf,
                user=user
            )
            samba.selenium.assert_text_not_visible('Internal server error')
            samba.selenium.assert_text_not_visible(
                'Abstract Submission not possible!')

        else:
            samba.selenium.assert_text_not_visible('Abstract Form Page')
            samba.selenium.assert_text('Abstract Submission not possible!')

    else:
        if is_ok:
            home.factories.Abstract.create(
                conference=conf,
                user=user,
            )
        else:
            with pytest.raises((ValidationError, ObjectDoesNotExist)):
                home.factories.Abstract.create(
                    conference=conf,
                    user=user,
                )

    if is_ok:
        mail_body = mailoutbox[-1].body
        assert 'Thanks for submitting your abstract' in mail_body
        assert user.first_name in mail_body
        assert user.last_name in mail_body
        assert conference['conference_page'].title in mail_body


@pytest.mark.parametrize('registration_open', [True, False])
@pytest.mark.parametrize('deadline_ok', [True, False])
@pytest.mark.parametrize('conference_full', [True, False])
@pytest.mark.parametrize('abstract_submissions_full', [True, False])
def test_notlogged_in_messages(samba, conference,
                               registration_open, deadline_ok,
                               conference_full, abstract_submissions_full):
    today = datetime.date.today()
    conf = conference['conference_page']

    if abstract_submissions_full:
        home.factories.Abstract.create_batch(
            size=math.floor(conf.n_abstract_submission_left / 2),
            conference=conf,
            review_status='accepted'
        )

        home.factories.Abstract.create_batch(
            size=conf.n_abstract_submission_left,
            conference=conf,
            review_status='pending'
        )

    if conference_full:
        home.factories.Registration.create_batch(
            size=conf.n_remaining_for_conference,
            conference=conf
        )

    if not deadline_ok:
        conf.abstract_deadline = today + datetime.timedelta(-1)
        conf.save()

    if not registration_open:
        conf.registration_start_date = today + datetime.timedelta(1)
        conf.save()

    samba.goto_url(conference['registration_page'].url)

    if registration_open:
        samba.selenium.assert_text('Not logged in')
    else:
        samba.selenium.assert_text_not_visible('Not logged in')
        samba.selenium.assert_text_not_visible('Abstract submission open. '
                                               'not logged in')
        samba.selenium.assert_text_not_visible('Abstract deadline has passed')
        samba.selenium.assert_text_not_visible('Conference Full')
        samba.selenium.assert_text('Registration has not yet started')
        return

    if (registration_open and deadline_ok and not conference_full and
            not abstract_submissions_full):
        samba.selenium.assert_text('Abstract submission open. not logged in')
        samba.selenium.assert_text_not_visible('Abstract deadline has passed')
        samba.selenium.assert_text_not_visible('Conference Full')
        samba.selenium.assert_text_not_visible('Abstract Submission Full')
        samba.selenium.assert_text_not_visible(
            'Registration has not yet started')
    elif not deadline_ok:
        samba.selenium.assert_text_not_visible('Abstract submission open. not '
                                               'logged in')
        samba.selenium.assert_text_not_visible('Abstract Submission Full')
        samba.selenium.assert_text('Abstract deadline has passed')
        samba.selenium.assert_text_not_visible(
            'Registration has not yet started')
    elif conference_full:
        samba.selenium.assert_text('Conference Full')
        samba.selenium.assert_text_not_visible(
            'Registration has not yet started')
        samba.selenium.assert_text_not_visible('Abstract submission open. '
                                               'not logged in')
        samba.selenium.assert_text_not_visible('Abstract deadline has passed')
        samba.selenium.assert_text_not_visible('Abstract Submission Full')
    elif abstract_submissions_full:
        samba.selenium.assert_text_not_visible('Conference Full')
        samba.selenium.assert_text_not_visible(
            'Registration has not yet started')
        samba.selenium.assert_text_not_visible('Abstract submission open. '
                                               'not logged in')
        samba.selenium.assert_text_not_visible('Abstract deadline has passed')
        samba.selenium.assert_text('Abstract Submission Full')


def test_prefill_author(samba, conference, conference_user):
    samba.goto_url(conference['form_pages']['abstract'].url)

    c_user = conference_user['user_info']

    fn = '%s %s' % (c_user['first_name'], c_user['last_name'])

    assert (samba.selenium.get_attribute(
        '#id_abstractauthor_set-0-full_name', 'value') == fn)

    registration = home.factories.Registration.create(
        conference=conference['conference_page'],
        user=conference_user['user']
    )

    samba.goto_url(conference['form_pages']['abstract'].url)
    assert (samba.selenium.get_attribute(
        '#id_abstractauthor_set-0-full_name', 'value') == fn)

    selected_affiliation = Select(
        samba.selenium.get_element('#id_abstractauthor_set-0-affiliations'))

    assert (selected_affiliation.all_selected_options[0].text ==
            str(registration.affiliation))


@pytest.mark.parametrize('abstract_submissions_full', [True, False])
def test_abstract_submissions_limit_reached_messages(
        samba, conference, conference_user,
        abstract_submissions_full):
    conf = conference['conference_page']
    if abstract_submissions_full:
        home.factories.Abstract.create_batch(
            size=math.floor(conf.n_abstract_submission_left / 2),
            conference=conf,
            review_status='accepted'
        )

        home.factories.Abstract.create_batch(
            size=conf.n_abstract_submission_left,
            conference=conf,
            review_status='pending'
        )

    samba.goto_url(conference['registration_page'].url)
    if abstract_submissions_full:
        samba.selenium.assert_text('Abstract Submission Full')
        samba.selenium.assert_text_not_visible(
            'Abstract submission is open but no abstract was submitted'
        )

    else:
        samba.selenium.assert_text_not_visible('Abstract Submission Full')
        samba.selenium.assert_text(
            'Abstract submission is open but no abstract was submitted'
        )


def test_abstract_prevent_multi_authors(samba, conference,
                                        conference_user):
    fake = Faker()
    samba.goto_page(conference['form_pages']['abstract'])
    samba.selenium.update_text('#id_title', fake.sentence())
    samba.selenium.update_text('#id_content', fake.paragraph())
    samba.selenium.update_text('#id_abstractauthor_set-0-full_name',
                               '{}, {}'.format(
                                   fake.name(),
                                   fake.name()
                               ))
    samba.selenium.click('#div_id_abstractauthor_set-0-affiliations')
    time.sleep(1)
    samba.selenium.click(
        '#select2-id_abstractauthor_set-0-affiliations-results > li'
    )
    samba.selenium.submit('form')

    assert home.models.Abstract.objects.count() == 0


def test_abstract_author_position(samba, conference, conference_user):
    conf = conference['conference_page']
    abs = home.factories.Abstract(conference=conf)

    for idx_author, author in enumerate(abs.abstractauthor_set.all()):
        assert author.position == idx_author

    abs.delete()

    home.factories.Abstract.create_in_browser(
        samba=samba,
        conference=conf,
        user=conference_user['user']
    )

    abs2 = home.models.Abstract.objects.first()

    for idx_author, author in enumerate(abs2.abstractauthor_set.all()):
        assert author.position == idx_author
