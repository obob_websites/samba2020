
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import time

from django.contrib.auth import get_user_model
import home.factories
import pytest
import datetime


@pytest.mark.parametrize('deadline_passed', [False, True])
@pytest.mark.parametrize('fillup_conference', [False, True])
def test_registration_login_lost_password(samba_integration,
                                          samba,
                                          conference,
                                          conference_user,
                                          mailoutbox,
                                          deadline_passed,
                                          fillup_conference):
    today = datetime.date.today()
    manager_user = home.factories.UserWithConfirmedEmail(
        raw_password='pw'
    )
    conference['conference_page'].manager_group.user_set.add(manager_user)

    user_info = conference_user['user_info']

    samba.selenium.assert_text('Logout')

    user = get_user_model().objects.get(email=user_info['email'])
    assert (user not in
           conference['conference_page'].participant_group.user_set.all())

    samba.logout()

    samba.login(user_info['email'], user_info['password'])

    samba.selenium.assert_text('Logout')

    samba.goto_url(conference['registration_page'].url)
    samba.selenium.click_link('Register here')
    samba.selenium.update_text('#id_degree', 'Dr.')
    samba.selenium.click(
        '#div_id_affiliation > div > span')
    samba.selenium.update_text(
        'span.select2-search.select2-search--dropdown > input',
        'newcastle')
    time.sleep(1)
    samba.selenium.click('#select2-id_affiliation-results > li')
    samba.selenium.js_click('#id_ticket_0')
    samba.selenium.js_click('#id_has_read_refund_policy')
    samba.selenium.js_click('#id_consents_to_data_processing')
    samba.selenium.submit('#id_degree')

    assert (user in
            conference['conference_page'].participant_group.user_set.all())

    assert len(mailoutbox) == 1
    assert 'Thanks for registering.' in mailoutbox[0].body
    assert user_info['first_name'] in mailoutbox[0].body
    assert user_info['last_name'] in mailoutbox[0].body
    assert conference['conference_page'].title in mailoutbox[0].body

    if fillup_conference:
        home.factories.Registration.create_batch(
            size=conference['conference_page'].n_remaining_for_conference,
            conference=conference['conference_page']
        )

    if deadline_passed:
        conference['conference_page'].registration_deadline = today + datetime.timedelta(-1)  # noqa
        conference['conference_page'].save()

    samba.logout()
    samba.goto_url(conference['registration_page'].url)

    if deadline_passed:
        samba.selenium.assert_text('Registration deadline is over')

    if fillup_conference and not deadline_passed:
        samba.selenium.assert_text('Conference Full')

    samba.login(user_info['email'], user_info['password'])

    samba.goto_url(conference['registration_page'].url)
    samba.selenium.assert_text('You are registered but have not paid.')
    samba.selenium.assert_text_not_visible(
        'You are registered and paid everything.')
    samba.selenium.assert_text_not_visible('Registration deadline is over')
    samba.selenium.assert_text_not_visible('Conference Full')

    samba.login(manager_user.email, 'pw')
    samba.goto_url(conference['registration_management_page'].url)

    samba.selenium.assert_text(user_info['first_name'])
    samba.selenium.assert_text(user_info['last_name'])

    samba.selenium.click_link('View Details')
    samba.selenium.click_link('Add payment')

    samba.selenium.update_text(
        '#id_amount_0',
        str(
            conference['conference_page'].conference_tickets.first().ticket_price  # noqa
        )
    )

    samba.selenium.update_text('#id_kind_of_payment',
                               'Bank Transfer')

    samba.selenium.submit('#id_amount_0')

    samba.selenium.assert_text('Payment complete')

    samba.login(user_info['email'], user_info['password'])

    samba.goto_url(conference['registration_page'].url)
    samba.selenium.assert_text('You are registered and paid everything.')
    samba.selenium.assert_text_not_visible(
        'You are registered but have not paid.')
    samba.selenium.assert_text_not_visible('Registration deadline is over')
    samba.selenium.assert_text_not_visible('Conference Full')

    assert 'Thanks for paying.' in mailoutbox[-1].body
    assert user_info['first_name'] in mailoutbox[-1].body
    assert user_info['last_name'] in mailoutbox[-1].body
    assert conference['conference_page'].title in mailoutbox[-1].body
