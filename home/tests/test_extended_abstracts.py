
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db import IntegrityError

import home.factories
import home.models
import pytest
import datetime


def test_extended_abstract_in_selenium(samba, conference, conference_user,
                                       admin_user,
                                       mailoutbox):
    home.factories.Registration.create(
        conference=conference['conference_page'],
        user=conference_user['user'],
        payment_status='complete'
    )
    home.factories.ExtendedAbstract.create_in_browser(
        samba,
        parent_conference=conference['conference_page'],
        parent_user=conference_user['user'])

    assert home.models.Abstract.objects.count() == 1
    assert home.models.ExtendedAbstract.objects.count() == 1
    assert (conference_user['user'] in
            conference['conference_page'].participant_group.user_set.all())

    mail_body = mailoutbox[-1].body
    assert 'Extended Abstract submitted' in mail_body
    assert conference_user['user_info']['first_name'] in mail_body
    assert conference_user['user_info']['last_name'] in mail_body
    assert conference['conference_page'].title in mail_body


@pytest.mark.parametrize('deadline_ok', [True, False])
@pytest.mark.parametrize('registered, registration_paid',
                         [
                             (True, 'complete'),
                             (True, 'pay_at_conference'),
                             (True, False),
                             (False, False)
                         ])
@pytest.mark.parametrize('abstract_submitted, abstract_accepted',
                         [
                             (True, True),
                             (True, False),
                             (False, False)
                         ])
@pytest.mark.parametrize('in_browser', [True, False])
def test_extended_abstract_allowed_forbidden(
        samba, conference, mailoutbox,
        deadline_ok,
        registered, registration_paid,
        abstract_submitted, abstract_accepted,
        in_browser):

    today = datetime.date.today()
    conf = conference['conference_page']
    user = home.factories.UserWithConfirmedEmail.create(raw_password='pw')
    samba.login(user.email, 'pw')

    registration_paid_bool = registration_paid in ('complete',
                                                   'pay_at_conference')

    is_ok = (deadline_ok and registered and registration_paid_bool and
             abstract_submitted and abstract_accepted)

    if not deadline_ok:
        conf.extended_abstract_deadline = today + datetime.timedelta(-1)
        conf.save()

    if registered:
        registration = home.factories.Registration.create(
            conference=conf,
            user=user
        )
        if registration_paid == 'complete':
            home.factories.Payment.create(
                registration=registration,
                amount=registration.amount_owed.amount
            )
        elif registration_paid == 'pay_at_conference':
            registration.pay_at_conference = True
            registration.save()
            registration.refresh_from_db()

    abstract = None

    if abstract_submitted:
        abstract = home.factories.Abstract.create(
            conference=conf,
            user=user
        )
        if abstract_accepted:
            abstract.review_status = 'accepted'
            abstract.save()

    if in_browser:
        samba.goto_url(conference['registration_page'].url)
        if is_ok:
            samba.selenium.assert_text('Submit Extended Abstract here')
            samba.selenium.assert_text_not_visible('Extended Abstract '
                                                   'submission not possible')
        else:
            samba.selenium.assert_text_not_visible('Submit Extended '
                                                   'Abstract here')
            if not deadline_ok:
                samba.selenium.assert_text('Deadline for extended abstract '
                                           'has passed')
                samba.selenium.assert_text_not_visible(
                    'You must be fully registered and have '
                    'your abstract accepted in order to '
                    'submit the extended abstract')
            elif not registration_paid_bool or not abstract_accepted:
                samba.selenium.assert_text_not_visible(
                    'Deadline for extended abstract '
                    'has passed')
                samba.selenium.assert_text(
                    'You must be fully registered and have '
                    'your abstract accepted in order to '
                    'submit the extended abstract')
            else:
                samba.selenium.assert_text('Extended Abstract '
                                           'submission not possible')

        samba.goto_url(conference['form_pages']['extended_abstract'].url)
        samba.selenium.assert_text_not_visible('Internal server error')
        if is_ok:
            samba.selenium.assert_text('Extended Abstract Form Page')
            home.factories.ExtendedAbstract.create_in_browser(
                samba,
                parent_conference=conf,
                parent_user=user,
                base_abstract=abstract
            )
            samba.selenium.assert_text_not_visible('Internal server error')
            samba.selenium.assert_text_not_visible(
                'Extended Abstract Submission not possible!')
        else:
            samba.selenium.assert_text_not_visible('Extended '
                                                   'Abstract Form Page')
            samba.selenium.assert_text(
                'Extended Abstract Submission not possible!')
    else:
        if is_ok:
            home.factories.ExtendedAbstract.create(
                parent_conference=conf,
                parent_user=user,
                base_abstract=abstract
            )
        else:
            with pytest.raises((ValidationError, ObjectDoesNotExist)):
                home.factories.ExtendedAbstract.create(
                    parent_conference=conf,
                    parent_user=user,
                    base_abstract=abstract
                )

    if is_ok:
        mail_body = mailoutbox[-1].body
        assert 'Extended Abstract submitted' in mail_body
        assert user.first_name in mail_body
        assert user.last_name in mail_body
        assert conference['conference_page'].title in mail_body


@pytest.mark.parametrize('in_browser', [True, False])
def test_cannot_submit_two_extended_abstracts(samba, conference, in_browser):
    conf = conference['conference_page']
    user = home.factories.UserWithConfirmedEmail.create(raw_password='pw')
    home.factories.Registration.create(
        conference=conf,
        user=user,
        payment_status='complete'
    )
    samba.login(user.email, 'pw')

    ex_abstract = home.factories.ExtendedAbstract.create(
        parent_conference=conf,
        parent_user=user,
    )

    if in_browser:
        samba.goto_url(conference['form_pages']['extended_abstract'].url)
        samba.selenium.assert_text_not_visible('Internal server error')
        samba.selenium.assert_text_not_visible('Extended '
                                               'Abstract Form Page')
        samba.selenium.assert_text(
            'Extended Abstract Submission not possible!')
    else:
        with pytest.raises((ValidationError, ObjectDoesNotExist,
                            IntegrityError)):
            home.factories.ExtendedAbstract.create(
                parent_conference=conf,
                parent_user=user,
                base_abstract=ex_abstract.base_abstract
            )


@pytest.mark.parametrize('registration_open', [True, False])
@pytest.mark.parametrize('deadline_ok', [True, False])
@pytest.mark.parametrize('conference_full', [True, False])
def test_notlogged_in_messages(samba, conference,
                               registration_open, deadline_ok,
                               conference_full):

    today = datetime.date.today()
    conf = conference['conference_page']

    if conference_full:
        home.factories.Registration.create_batch(
            size=conf.n_remaining_for_conference,
            conference=conf
        )

    if not deadline_ok:
        conf.extended_abstract_deadline = today + datetime.timedelta(-1)
        conf.save()

    if not registration_open:
        conf.registration_start_date = today + datetime.timedelta(1)
        conf.save()

    samba.goto_url(conference['registration_page'].url)

    if registration_open:
        samba.selenium.assert_text('Not logged in')
    else:
        samba.selenium.assert_text_not_visible('Not logged in')
        samba.selenium.assert_text_not_visible('Extended Abstract submission '
                                               'open but not logged in')
        samba.selenium.assert_text_not_visible(
            'Deadline for extended abstract has passed')
        samba.selenium.assert_text_not_visible('Conference Full')
        samba.selenium.assert_text('Registration has not yet started')
        return

    if registration_open and deadline_ok and not conference_full:
        samba.selenium.assert_text('Extended Abstract submission open '
                                   'but not logged in')
        samba.selenium.assert_text_not_visible(
            'Deadline for extended abstract has passed')
        samba.selenium.assert_text_not_visible(
            'Registration has not yet started')
        samba.selenium.assert_text_not_visible('Conference Full')
    elif not deadline_ok:
        samba.selenium.assert_text_not_visible('Extended Abstract submission '
                                               'open but not logged in')
        samba.selenium.assert_text('Deadline for extended abstract has passed')
        samba.selenium.assert_text_not_visible(
            'Registration has not yet started')
    elif conference_full:
        samba.selenium.assert_text('Conference Full')
        samba.selenium.assert_text_not_visible(
            'Registration has not yet started')
        if registration_open:
            samba.selenium.assert_text('Extended Abstract '
                                       'submission '
                                       'open but not logged in')
        else:
            samba.selenium.assert_text_not_visible('Extended Abstract '
                                                   'submission '
                                                   'open but not logged in')
        samba.selenium.assert_text_not_visible(
            'Deadline for extended abstract has passed')


@pytest.mark.parametrize('accept_abstract', [True, False])
def test_extended_abstract_flow(samba, conference, mailoutbox,
                                accept_abstract):
    conf = conference['conference_page']
    user = home.factories.UserWithConfirmedEmail.create(raw_password='pw')

    reviewer = home.factories.UserWithConfirmedEmail(
        raw_password='pw'
    )

    conf.extended_abstract_reviewer_group.user_set.add(reviewer)

    home.factories.Registration.create(
        conference=conf,
        user=user,
        payment_status='complete'
    )

    abstract = home.factories.Abstract.create(
        conference=conf,
        user=user,
        review_status='accepted'
    )

    samba.login(user.email, 'pw')
    samba.goto_url(conference['registration_page'].url)

    samba.selenium.click_link('Submit extended abstract here')

    samba.selenium.assert_text('Hello Extended Abstract Form')

    extended_abstract = home.factories.ExtendedAbstract.create_in_browser(
        samba,
        parent_conference=conf,
        parent_user=user,
        base_abstract=abstract
    )

    mail_body = mailoutbox[-1].body
    assert 'Extended Abstract submitted' in mail_body
    assert user.first_name in mail_body
    assert user.last_name in mail_body
    assert conf.title in mail_body

    samba.goto_url(conference['registration_page'].url)
    samba.selenium.assert_text_not_visible('Submit extended abstract here')
    samba.selenium.assert_text('Extended Abstract under Review')

    samba.login(reviewer.email, 'pw')

    samba.goto_url(conference['review_pages']['extended_abstracts'].url)
    samba.selenium.click_link('Review')

    samba.selenium.assert_text(
        '\n'.join(extended_abstract.content.splitlines()), 'div#content')

    if accept_abstract:
        samba.selenium.select_option_by_text('#id_review_status',
                                             'Accepted')
    else:
        samba.selenium.select_option_by_text('#id_review_status',
                                             'Rejected')

    samba.selenium.submit('#id_review_notes')

    mail_body = mailoutbox[-1].body
    if accept_abstract:
        assert 'Extended Abstract accepted' in mail_body
    else:
        assert 'Extended Abstract rejected' in mail_body

    assert user.first_name in mail_body
    assert user.last_name in mail_body
    assert conf.title in mail_body

    samba.login(user.email, 'pw')
    samba.goto_url(conference['registration_page'].url)

    samba.selenium.assert_text_not_visible('Submit Extended Abstract here')
    samba.selenium.assert_text_not_visible('Extended Abstract under Review')
    if accept_abstract:
        samba.selenium.assert_text('Extended Abstract accepted')
        samba.selenium.assert_text_not_visible('Extended Abstract rejected')
    else:
        samba.selenium.assert_text('Extended Abstract rejected')
        samba.selenium.assert_text_not_visible('Extended Abstract accepted')


@pytest.mark.django_db
@pytest.mark.parametrize('user', [None, 'conference', 'manager', 'reviewer',
                                  'extended_reviewer'])
def test_permissions(samba, conference, conference_user, user):
    home.factories.ExtendedAbstract.create(
        parent_conference=conference['conference_page'],
    )

    conference['registration_management_page'].show_in_menus = True

    samba.add_page_to_menu(
        conference['review_base_page'],
        exclude_if_unauthorized=True
    )

    samba.login_with_role(user, conference, conference_user)

    samba.goto_url(conference['review_pages']['extended_abstracts'].url)

    if user is None:
        samba.selenium.assert_text_not_visible('Review extended abstracts')
        samba.selenium.assert_text_not_visible('Review',
                                               '.nav-item')
        samba.selenium.assert_text('Login')
    elif user in ('conference', 'manager', 'reviewer'):
        samba.selenium.assert_text('Forbidden')
    else:
        samba.selenium.assert_text('Review extended abstracts')
        samba.selenium.assert_text('Review', '.nav-item')

    samba.goto_url(
        conference['review_pages']['extended_abstracts'].url +
        conference['review_pages']['extended_abstracts'].reverse_subpage(
            'review_abstract',
            args=[home.models.ExtendedAbstract.objects.all()[0].pk]))

    if user is None:
        samba.selenium.assert_text_not_visible(
            'Review %s' % (conference['conference_page'].title, ))
        samba.selenium.assert_text_not_visible('Review',
                                               '.nav-item')
        samba.selenium.assert_text('Login')
    elif user in ('conference', 'manager', 'reviewer'):
        samba.selenium.assert_text('Forbidden')
    else:
        samba.selenium.assert_text(
            'Review %s' % (conference['conference_page'].title,)
        )
        samba.selenium.assert_text('Review', '.nav-item')
