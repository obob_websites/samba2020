
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import home.factories


def test_behavior_without_data(samba, conference):
    conf = conference['conference_page']
    user = home.factories.UserWithConfirmedEmail.create(raw_password='pw')

    conf.manager_group.user_set.add(user)
    conf.abstract_reviewer_group.user_set.add(user)
    conf.extended_abstract_reviewer_group.user_set.add(user)

    samba.login(user.email, 'pw')

    samba.goto_url(conference['registration_management_page'].url)
    samba.selenium.assert_text('Manage registrations for')

    samba.goto_url(conference['abstract_management_page'].url)
    samba.selenium.assert_text('Manage abstracts for')

    samba.goto_url(conference['extended_abstracts_management_page'].url)
    samba.selenium.assert_text('Manage extended abstracts for')

    samba.goto_url(conference['review_pages']['abstracts'].url)
    samba.selenium.assert_text('Review abstracts for')

    samba.goto_url(conference['review_pages']['extended_abstracts'].url)
    samba.selenium.assert_text('Review extended abstracts')
