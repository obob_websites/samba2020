#  Copyright (c) 2016-2021, Thomas Hartmann
#
#  This file is part of the SAMBA2020 Project
#  (https://gitlab.com/obob_websites/samba2020)
#
#  SAMBA2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SAMBA2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SAMBA2020. If not, see <http://www.gnu.org/licenses/>.
import pytest

import home.models
from home.factories import (UserWithUnpaidRegistration,
                            UserWithConfirmedEmail,
                            Abstract,
                            Payment,
                            Registration)


def test_abstract_overview_when_empty(samba, conference):
    samba.goto_page(conference['abstract_overview_page'])
    samba.selenium.assert_text('Poster Abstracts')


@pytest.mark.parametrize('free_conference', [True, False])
def test_abstract_overview_with_abstracts(samba, conference,
                                          free_conference):
    pay_users_1 = [0, 4]
    abstract_review_users = [5, 8, 4, 1]
    pay_users_2 = [1, 7]

    users_with_poster_number = []

    conf = conference['conference_page']
    if free_conference:
        for ticket in conf.conference_tickets.all():
            ticket.ticket_price = 0
            ticket.save()

        for ticket_option in conf.conference_options.all():
            ticket_option.option_price = 0
            ticket_option.save()

    users = UserWithUnpaidRegistration.create_batch(
        size=10, parent_conference=conf, raw_password='pw')
    non_registered_user = UserWithConfirmedEmail.create(raw_password='pw')

    abstracts = [Abstract(user=u, conference=conf) for u in users]
    unregistered_abstract = Abstract(user=non_registered_user,
                                     conference=conf)

    for abstract in abstracts:
        assert abstract.poster_number is None

    assert unregistered_abstract.poster_number is None

    if not free_conference:
        for idx_u in pay_users_1:
            reg = users[idx_u].registration_set.filter(conference=conf).first()
            Payment(registration=reg, amount=reg.amount_owed)

    for idx_u in abstract_review_users:
        abstract = abstracts[idx_u]
        abstract.review_status = 'accepted'
        abstract.save()

        if free_conference:
            users_with_poster_number.append(idx_u)
        else:
            if idx_u in pay_users_1:
                users_with_poster_number.append(idx_u)

    unregistered_abstract.review_status = 'accepted'
    unregistered_abstract.save()

    if not free_conference:
        for idx_u in pay_users_2:
            reg = users[idx_u].registration_set.filter(conference=conf).first()
            Payment(registration=reg, amount=reg.amount_owed)

            if idx_u in abstract_review_users and idx_u not in pay_users_1:
                users_with_poster_number.append(idx_u)

    tmp_reg = Registration.create(conference=conf, user=non_registered_user)
    if not free_conference:
        Payment(registration=tmp_reg, amount=reg.amount_owed)

    unregistered_abstract.refresh_from_db()
    users_with_poster_number.append(10)

    [a.refresh_from_db() for a in abstracts]
    users.append(non_registered_user)
    abstracts.append(unregistered_abstract)

    samba.goto_page(conference['abstract_overview_page'])

    for cur_abstract in home.models.Abstract.objects.all():
        should_be_there = False

        if (
                cur_abstract.poster_number is not None and
                cur_abstract.review_status == 'accepted' and
                cur_abstract.registration is not None and
                cur_abstract.registration.payment_status == 'complete'
        ):
            should_be_there = True

        if should_be_there:
            samba.selenium.assert_text(
                '{}) {}'.format(cur_abstract.poster_number, cur_abstract.title)
            )
        else:
            samba.selenium.assert_text_not_visible(cur_abstract.title)
