
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
import math
import random

import factory.django
from django.db.utils import IntegrityError

import pytest
from moneyed import Money

import home.factories
import home.models
import datetime
import tempfile
import pandas as pd
import itertools

import home.signals


@pytest.mark.django_db
@pytest.mark.parametrize('user', [None, 'conference', 'manager', 'reviewer',
                                  'extended_reviewer'])
def test_permissions_registrations(samba, conference, conference_user, user):
    conference['registration_management_page'].show_in_menus = True

    samba.add_page_to_menu(
        conference['registration_management_page'],
        exclude_if_unauthorized=True
    )

    samba.add_page_to_menu(
        conference['abstract_management_page'],
        exclude_if_unauthorized=True
    )

    samba.add_page_to_menu(
        conference['extended_abstracts_management_page'],
        exclude_if_unauthorized=True
    )

    home.factories.ExtendedAbstract.create(
        parent_conference=conference['conference_page'],
    )

    samba.login_with_role(user, conference, conference_user)

    samba.goto_url(conference['registration_management_page'].url)

    if user is None:
        samba.selenium.assert_text_not_visible('Manage Registrations')
        samba.selenium.assert_text_not_visible('Manage Registrations',
                                               '.nav-item')
        samba.selenium.assert_text('Login')
    elif user in ('conference', 'reviewer', 'extended_reviewer'):
        samba.selenium.assert_text('Forbidden')
    else:
        samba.selenium.assert_text('Manage Registrations')
        samba.selenium.assert_text('Manage Registrations', '.nav-item')

    samba.goto_url(
        conference['registration_management_page'].url +
        conference['registration_management_page'].reverse_subpage(
            'show_registration_detail',
            args=[home.models.Registration.objects.first().pk]))

    if user is None:
        samba.selenium.assert_text_not_visible('Manage')
        samba.selenium.assert_text_not_visible('Manage Registrations',
                                               '.nav-item')
        samba.selenium.assert_text('Login')
    elif user in ('conference', 'reviewer', 'extended_reviewer'):
        samba.selenium.assert_text('Forbidden')
    else:
        samba.selenium.assert_text('Manage')
        samba.selenium.assert_text('Manage Registrations', '.nav-item')

    with tempfile.NamedTemporaryFile(suffix='xls') as tmpfile:
        samba.download_from_url(
            conference['registration_management_page'].url +
            conference['registration_management_page'].reverse_subpage(
                'download_excel'),
            tmpfile.name
        )

        if user is None or user in (
                'conference', 'reviewer', 'extended_reviewer'
        ):
            with pytest.raises(ValueError):
                pd.read_excel(tmpfile.name)
        else:
            pd.read_excel(tmpfile.name)

    samba.goto_url(conference['abstract_management_page'].url)

    if user is None:
        samba.selenium.assert_text_not_visible('Manage Abstracts')
        samba.selenium.assert_text('Login')
    elif user in ('conference', 'reviewer', 'extended_reviewer'):
        samba.selenium.assert_text('Forbidden')
    else:
        samba.selenium.assert_text('Manage abstracts')
        samba.selenium.assert_text('Manage Abstracts')

    samba.goto_url(
        conference['abstract_management_page'].url +
        conference['abstract_management_page'].reverse_subpage(
            'show_abstract_detail',
            args=[home.models.Abstract.objects.first().pk]))

    if user is None:
        samba.selenium.assert_text_not_visible('Manage')
        samba.selenium.assert_text_not_visible('Manage Abstracts')
        samba.selenium.assert_text('Login')
    elif user in ('conference', 'reviewer', 'extended_reviewer'):
        samba.selenium.assert_text('Forbidden')
    else:
        samba.selenium.assert_text('Manage')
        samba.selenium.assert_text('Manage Abstracts')

    samba.goto_url(conference['extended_abstracts_management_page'].url)

    if user is None:
        samba.selenium.assert_text_not_visible('Manage Extended Abstracts')
        samba.selenium.assert_text('Login')
    elif user in ('conference', 'reviewer', 'extended_reviewer'):
        samba.selenium.assert_text('Forbidden')
    else:
        samba.selenium.assert_text('Manage extended abstracts')
        samba.selenium.assert_text('Manage Extended Abstracts')

    samba.goto_url(
        conference['extended_abstracts_management_page'].url +
        conference['extended_abstracts_management_page'].reverse_subpage(
            'show_abstract_detail',
            args=[home.models.ExtendedAbstract.objects.first().pk]))

    if user is None:
        samba.selenium.assert_text_not_visible('Manage')
        samba.selenium.assert_text_not_visible('Manage Extended Abstracts')
        samba.selenium.assert_text('Login')
    elif user in ('conference', 'reviewer', 'extended_reviewer'):
        samba.selenium.assert_text('Forbidden')
    else:
        samba.selenium.assert_text('Manage')
        samba.selenium.assert_text('Manage Extended Abstracts')


@pytest.mark.django_db
@pytest.mark.parametrize('user', [None, 'conference', 'manager', 'reviewer'])
def test_permissions_abstracts(samba, conference, conference_user, user):
    home.factories.Abstract.create(conference=conference['conference_page'],
                                   user=conference_user['user'])

    conference['abstract_management_page'].show_in_menus = True

    samba.add_page_to_menu(
        conference['abstract_management_page'],
        exclude_if_unauthorized=True
    )

    samba.login_with_role(user, conference, conference_user)

    samba.goto_url(conference['abstract_management_page'].url)

    if user is None:
        samba.selenium.assert_text_not_visible('Manage Abstracts')
        samba.selenium.assert_text_not_visible('Manage Abstracts',
                                               '.nav-item')
        samba.selenium.assert_text('Login')
    elif user in ('conference', 'reviewer'):
        samba.selenium.assert_text('Forbidden')
    else:
        samba.selenium.assert_text('Manage Abstracts')
        samba.selenium.assert_text('Manage Abstracts', '.nav-item')

    samba.goto_url(
        conference['abstract_management_page'].url +
        conference['abstract_management_page'].reverse_subpage(
            'show_abstract_detail',
            args=[home.models.Abstract.objects.all()[0].pk]))

    if user is None:
        samba.selenium.assert_text_not_visible(
            'Manage %s' % (conference['conference_page'].title,)
        )
        samba.selenium.assert_text_not_visible('Manage Abstracts',
                                               '.nav-item')
        samba.selenium.assert_text('Login')
    elif user in ('conference', 'reviewer'):
        samba.selenium.assert_text('Forbidden')
    else:
        samba.selenium.assert_text(
            'Manage %s' % (conference['conference_page'].title, )
        )
        samba.selenium.assert_text('Manage Abstracts', '.nav-item')


def test_extend_payment_deadline(samba, conference):
    conf = conference['conference_page']
    conf.payment_first_warning_after_n_days = 5
    conf.payment_second_warning_after_n_days = 10
    conf.payment_unregister_after_n_days = 15
    conf.save()

    registration = home.factories.Registration.create(
        conference=conf
    )

    today = datetime.date.today()

    assert registration.send_first_payment_reminder_date == (
        today + datetime.timedelta(days=5))
    assert registration.send_second_payment_reminder_date == (
        today + datetime.timedelta(days=10))
    assert registration.unregister_date == (
        today + datetime.timedelta(days=15))

    samba.login_with_role('manager', conference)

    samba.goto_url(
        conference['registration_management_page'].url +
        conference['registration_management_page'].reverse_subpage(
            'show_registration_detail',
            args=[registration.pk])
    )

    new_deadline = registration.unregister_date + datetime.timedelta(5)

    samba.selenium.click('#id_extend_deadline_button')
    samba.selenium.wait_for_element('#id_extend_deadline_until')
    samba.selenium.driver.execute_script(
        'document.querySelector("#id_extend_deadline_until").value = "%s"' % (
            new_deadline.isoformat(), ))
    samba.selenium.submit('#id_extend_deadline_until')

    registration.refresh_from_db()

    assert registration.send_first_payment_reminder_date == (
        today + datetime.timedelta(days=10))
    assert registration.send_second_payment_reminder_date == (
        today + datetime.timedelta(days=15))
    assert registration.unregister_date == (
        today + datetime.timedelta(days=20))


def test_excel_sheet_content(samba, conference, settings):
    settings.PASSWORD_HASHERS = [
        'django.contrib.auth.hashers.UnsaltedMD5PasswordHasher'
    ]
    settings.EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
    conf = conference['conference_page']
    conf.conference_number_of_tickets = 800
    conf.abstract_max_number_of_submissions = 800
    conf.save()
    conf.refresh_from_db()
    all_ticket_options = list(conf.conference_options.all())

    n_per_round = 2

    registration_status = ('not_registered', 'complete',
                           'unpaid', 'pay_at_conference')
    abstract_status = ('not_submitted', 'pending', 'accepted', 'rejected')
    extended_abstract_status = ('not_submitted', 'pending',
                                'accepted', 'rejected')
    ticket = list(conf.conference_tickets.all())
    ticket_options = [
        [],
        [all_ticket_options[0]],
        [all_ticket_options[1]],
        [all_ticket_options[0], all_ticket_options[1]]
    ]

    users = dict()

    for (cur_reg_status,
         cur_abstract_status,
         cur_ext_abstract_status,
         cur_ticket,
         cur_ticket_options) in itertools.product(registration_status,
                                                  abstract_status,
                                                  extended_abstract_status,
                                                  ticket,
                                                  ticket_options):  # noqa
        if cur_ext_abstract_status != 'not_submitted':
            if cur_abstract_status != 'accepted' or cur_reg_status not in ('complete',  # noqa
                                                                           'pay_at_conference'):  # noqa
                continue

        for cur_n in range(n_per_round):
            cur_u = None
            while cur_u is None:
                try:
                    cur_u = home.factories.UserWithConfirmedEmail()
                except IntegrityError:
                    pass

            with factory.django.mute_signals(home.signals.payment_status_changed,
                                             home.signals.abstract_submitted,
                                             home.signals.abstract_accepted,
                                             home.signals.abstract_rejected,
                                             home.signals.poster_number_assigned,
                                             home.signals.extended_abstract_accepted,
                                             home.signals.extended_abstract_rejected,
                                             home.signals.extended_abstract_submitted,
                                             home.signals.user_has_registered):
                if cur_reg_status != 'not_registered':
                    home.factories.Registration(
                        conference=conf,
                        user=cur_u,
                        payment_status=cur_reg_status,
                        ticket=cur_ticket,
                        ticket_options=cur_ticket_options,
                    )

                if cur_abstract_status != 'not_submitted':
                    abs = home.factories.Abstract(
                        conference=conf,
                        user=cur_u,
                        review_status=cur_abstract_status
                    )

                    if cur_ext_abstract_status != 'not_submitted':
                        home.factories.ExtendedAbstract(
                            parent_conference=conf,
                            parent_user=cur_u,
                            base_abstract=abs,
                            review_status=cur_ext_abstract_status
                        )

            cur_u_dict = {
                'registration_status': cur_reg_status,
                'abstract_status': cur_abstract_status,
                'extended_abstract_status': cur_ext_abstract_status
            }

            users[cur_u] = cur_u_dict

    samba.login_with_role('manager', conference=conference)

    with tempfile.NamedTemporaryFile(suffix='.xls') as tmpfile:
        samba.download_from_url(
            conference['registration_management_page'].url +
            conference['registration_management_page'].reverse_subpage(
                'download_excel'),
            tmpfile.name
        )

        df = pd.read_excel(tmpfile.name)

        for user, values in users.items():
            should_be_in_list = values['registration_status'] in ('complete',  # noqa
                                                                  'pay_at_conference')  # noqa

            if should_be_in_list:
                assert user.username in df['user_id'].values
                row = df[df['user_id'] == user.username].to_dict('records')[0]
                assert row['First Name'] == user.first_name
                assert row['Last Name'] == user.last_name

                cur_registration = home.models.Registration.objects.get(
                    pk=row['Registration ID'])  # noqa

                assert str(cur_registration.ticket) in row['Ticket']
                for cur_option in cur_registration.ticket_options.all():
                    assert str(cur_option) in row['Ticket Options']

                if values['registration_status'] == 'complete':
                    assert row['Payment Status'] == 'complete'
                    assert row['Amount to pay at registration'] == str(Money(0, 'Eur'))  # noqa
                if values['registration_status'] == 'pay_at_conference':
                    assert row['Payment Status'] == 'pay_at_conference'  # noqa
                    assert row['Amount to pay at registration'] == str(cur_registration.amount_owed)  # noqa

                assert row['Affiliation'] == str(cur_registration.affiliation)

                if values['abstract_status'] == 'accepted':
                    cur_abstract = home.models.Abstract.objects.get(pk=row['Abstract ID'])  # noqa
                    assert row['Abstract Title'] == cur_abstract.title
                    assert row['Abstract Content'] == cur_abstract.content.replace('\r', '')  # noqa
                    assert row['Poster Number'] == cur_abstract.poster_number
                    assert row['Abstract Authors'] == cur_abstract.get_authors_indexed_string()  # noqa
                    assert row['Abstract Affiliations'] == cur_abstract.get_affiliations_indexed_string()  # noqa

                else:
                    assert math.isnan(row['Abstract ID'])
                    assert math.isnan(row['Abstract Title'])
                    assert math.isnan(row['Abstract Content'])
                    assert math.isnan(row['Poster Number'])
                    assert math.isnan(row['Abstract Authors'])
                    assert math.isnan(row['Abstract Affiliations'])
                    assert math.isnan(row['Accepted for Poster Prize'])

                df.drop(df.loc[df['user_id'] == user.username].index,
                        inplace=True)

            else:
                assert user.username not in df['user_id'].values

        assert len(df) == 0


def test_ticket_and_ticket_options_on_reg_overview(samba, conference, settings):
    settings.PASSWORD_HASHERS = [
        'django.contrib.auth.hashers.UnsaltedMD5PasswordHasher'
    ]
    settings.EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
    conf = conference['conference_page']
    conf.conference_number_of_tickets = 800
    conf.abstract_max_number_of_submissions = 800
    conf.save()
    conf.refresh_from_db()
    all_ticket_options = list(conf.conference_options.all())

    ticket = list(conf.conference_tickets.all())
    ticket_options = [
        [],
        [all_ticket_options[0]],
        [all_ticket_options[1]],
        [all_ticket_options[0], all_ticket_options[1]]
    ]

    n_tickets_sold = {t: 0 for t in ticket}
    n_ticket_options_sold = {to: 0 for to in all_ticket_options}

    with factory.django.mute_signals(home.signals.payment_status_changed,
                                     home.signals.user_has_registered):
        for cur_ticket, cur_ticket_option in itertools.product(ticket, ticket_options):
            n_registrations = random.randint(1, 5)
            home.factories.Registration.create_batch(
                size=n_registrations,
                conference=conf,
                ticket=cur_ticket,
                ticket_options=cur_ticket_option
            )

            n_tickets_sold[cur_ticket] += n_registrations
            for to in cur_ticket_option:
                n_ticket_options_sold[to] += n_registrations

    samba.login_with_role('manager', conference)
    samba.goto_page(conference['registration_management_page'])

    for cur_ticket, n_sold in n_tickets_sold.items():
        samba.selenium.assert_text(f'{cur_ticket.name}: {cur_ticket.tickets_sold}')

    for cur_ticket_option, n_sold in n_ticket_options_sold.items():
        samba.selenium.assert_text(f'{cur_ticket_option.name}: {cur_ticket_option.ticket_options_sold}')
