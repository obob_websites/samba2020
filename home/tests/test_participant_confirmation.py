
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import pytest
from django.urls import reverse
import ftfy
import tempfile

from pdfminer.pdfparser import PDFSyntaxError
from endesive import pdf
from selenium.webdriver.common.by import By

import home.factories
import home.models
from wagtail.core.rich_text import RichText


@pytest.mark.parametrize('user', [None, 'conference', 'manager', 'reviewer',
                                  'conference_user', 'extended_reviewer',
                                  'owner'])
@pytest.mark.parametrize('checked_in', [True, False])
def test_participant_confirmation(samba, conference, user,
                                  conference_user, checked_in):
    conf = conference['conference_page']

    reg_user = home.factories.UserWithPayedRegistration.create(
        raw_password='pw',
        parent_conference=conf
    )

    registration = reg_user.registration_set.first()

    if checked_in:
        home.factories.Checkin(
            registration=registration
        )

    if user == 'conference':
        samba.login(conference_user['user_info'])
    elif user == 'manager':
        this_user = home.factories.UserWithConfirmedEmail(
            raw_password='pw'
        )

        conference['conference_page'].manager_group.user_set.add(
            this_user)
        samba.login(this_user.email, 'pw')
    elif user == 'reviewer':
        this_user = home.factories.UserWithConfirmedEmail(
            raw_password='pw'
        )

        conference['conference_page'].abstract_reviewer_group.user_set.add(
            this_user)
        samba.login(this_user.email, 'pw')
    elif user == 'extended_reviewer':
        this_user = home.factories.UserWithConfirmedEmail(
            raw_password='pw'
        )

        (conference['conference_page']
         .extended_abstract_reviewer_group.user_set.add(this_user))
        samba.login(this_user.email, 'pw')
    elif user == 'owner':
        samba.login(reg_user.email, 'pw')

    pdf_url = reverse('participation-confirmation', args=[registration.pk])

    with tempfile.NamedTemporaryFile(suffix='.pdf') as pdf_file:
        samba.download_from_url(pdf_url, pdf_file.name)
        if user in ('owner', 'manager') and checked_in:
            pdf_text = ftfy.fix_text(samba.selenium.get_pdf_text(
                pdf_file.name))
            assert 'Participation confirmed' in pdf_text
            assert 'Preview' not in pdf_text

            assert_text = '%s %s' % (
                reg_user.first_name,
                reg_user.last_name
            )
            assert assert_text in pdf_text

            with open(pdf_file.name, 'rb') as f:
                (hashok, signatureok, certok) = pdf.verify(f.read())[0]

            assert signatureok
            assert hashok

        else:
            with pytest.raises(PDFSyntaxError):
                samba.selenium.get_pdf_text(pdf_file.name)


def test_validate_participant_confirmation(samba, conference):
    conf = conference['conference_page']

    home.factories.UserWithPayedRegistration.create(
        raw_password='pw',
        parent_conference=conf
    )

    registration = home.models.Registration.objects.first()
    home.factories.Checkin(
        registration=registration
    )

    this_user = home.factories.UserWithConfirmedEmail(
        raw_password='pw'
    )

    conference['conference_page'].manager_group.user_set.add(
        this_user)
    samba.login(this_user.email, 'pw')

    pdf_url = reverse('participation-confirmation', args=[registration.pk])

    with tempfile.NamedTemporaryFile(suffix='.pdf') as pdf_file:
        samba.download_from_url(pdf_url, pdf_file.name)
        samba.logout()

        samba.goto_url(reverse('validate_pdf'))
        upload_pdf_input = samba.selenium.get_element('pdf_file', By.NAME)
        upload_pdf_input.send_keys(pdf_file.name)
        samba.selenium.click('#button-id-validate')
        samba.selenium.assert_text('PDF Validated', '#id_results', timeout=30)

        pdf_file.seek(50)
        pdf_file.write(b'0')
        pdf_file.flush()

        samba.goto_url(reverse('validate_pdf'))
        upload_pdf_input = samba.selenium.get_element('pdf_file', By.NAME)
        upload_pdf_input.send_keys(pdf_file.name)
        samba.selenium.click('#button-id-validate')
        samba.selenium.assert_text(
            'The content of this PDF may have been altered!',
            '#id_results', timeout=30)


def test_buttons(samba, conference):
    conf = conference['conference_page']
    user = home.factories.CheckedInUser(parent_conference=conf,
                                        raw_password='pw')
    registration = user.registration_set.first()
    reg_page = conference['registration_page']
    reg_page.registered_checked_in[0] = ('paragraph', RichText(
        '<p>{{ participation_confirmation_button }}</p>'
        '<p>{{ payment_confirmation_button }}</p>'
    ))
    reg_page.save()

    samba.login(user.email, 'pw')
    samba.goto_page(reg_page)
    samba.selenium.assert_text('Download Participation Confirmation')
    samba.selenium.assert_text('Download Payment Confirmation')
    part_conf_link = samba.selenium.get_link_attribute(
        'Download Participation Confirmation',
        'href'
    )
    assert part_conf_link == reverse('participation-confirmation',
                                     args=[registration.pk])
    payment_conf_link = samba.selenium.get_link_attribute(
        'Download Payment Confirmation',
        'href'
    )
    assert payment_conf_link == reverse('payment-confirmation',
                                        args=[registration.pk])
