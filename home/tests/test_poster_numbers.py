#  Copyright (c) 2016-2020, Thomas Hartmann
#
#  This file is part of the SAMBA2020 Project
#  (https://gitlab.com/obob_websites/samba2020)
#
#  SAMBA2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SAMBA2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SAMBA2020. If not, see <http://www.gnu.org/licenses/>.
import home.models
from ..factories import (UserWithUnpaidRegistration, Abstract,
                         Payment, UserWithConfirmedEmail, Registration,
                         UserWithPayedRegistration)
import pytest


@pytest.mark.parametrize('free_conference', [False, True])
def test_poster_number(samba, conference, admin_user, mailoutbox,
                       free_conference):
    pay_users_1 = [0, 4]
    pay_at_conference_users_1 = [10, 14]
    abstract_review_users = [5, 8, 4, 1, 15, 18, 14, 11]
    pay_users_2 = [1, 7]
    pay_at_conference_users_2 = [11, 17]

    users_with_poster_number = []

    conf = conference['conference_page']
    conf.abstract_max_number_of_submissions = 30
    conf.conference_number_of_tickets = 30
    conf.save()

    if free_conference:
        for ticket in conf.conference_tickets.all():
            ticket.ticket_price = 0
            ticket.save()

        for ticket_option in conf.conference_options.all():
            ticket_option.option_price = 0
            ticket_option.save()

    users = UserWithUnpaidRegistration.create_batch(
        size=20, parent_conference=conf, raw_password='pw')
    non_registered_user = UserWithConfirmedEmail.create(raw_password='pw')

    abstracts = [Abstract(user=u, conference=conf) for u in users]
    unregistered_abstract = Abstract(user=non_registered_user,
                                     conference=conf)

    for abstract in abstracts:
        assert abstract.poster_number is None

    assert unregistered_abstract.poster_number is None

    if not free_conference:
        for idx_u in pay_users_1:
            reg = users[idx_u].registration_set.filter(conference=conf).first()
            Payment(registration=reg, amount=reg.amount_owed)
        for idx_u in pay_at_conference_users_1:
            reg = users[idx_u].registration_set.filter(conference=conf).first()
            reg.pay_at_conference = True
            reg.save()

    for idx_u in abstract_review_users:
        abstract = abstracts[idx_u]
        abstract.review_status = 'accepted'
        abstract.save()

        if free_conference:
            users_with_poster_number.append(idx_u)
        else:
            if idx_u in pay_users_1 + pay_at_conference_users_1:
                users_with_poster_number.append(idx_u)

    for idx_user, cur_user in enumerate(users):
        abstract = home.models.Abstract.objects.get(user=cur_user)
        if idx_user in users_with_poster_number:
            assert abstract.poster_number is not None
        else:
            assert abstract.poster_number is None

    unregistered_abstract.review_status = 'accepted'
    unregistered_abstract.save()
    unregistered_abstract.refresh_from_db()

    assert unregistered_abstract.poster_number is None

    if not free_conference:
        for idx_u in pay_users_2:
            reg = users[idx_u].registration_set.filter(conference=conf).first()
            Payment(registration=reg, amount=reg.amount_owed)

            if idx_u in abstract_review_users and idx_u not in pay_users_1:
                users_with_poster_number.append(idx_u)

        for idx_u in pay_at_conference_users_2:
            reg = users[idx_u].registration_set.filter(conference=conf).first()
            reg.pay_at_conference = True
            reg.save()

            if idx_u in abstract_review_users and \
                    idx_u not in pay_at_conference_users_1:
                users_with_poster_number.append(idx_u)

    for idx_user, cur_user in enumerate(users):
        abstract = home.models.Abstract.objects.get(user=cur_user)
        if idx_user in users_with_poster_number:
            assert abstract.poster_number is not None
        else:
            assert abstract.poster_number is None

    tmp_reg = Registration.create(conference=conf, user=non_registered_user)
    if not free_conference:
        Payment(registration=tmp_reg, amount=tmp_reg.amount_owed)

    unregistered_abstract.refresh_from_db()
    users_with_poster_number.append(20)

    [a.refresh_from_db() for a in abstracts]
    users.append(non_registered_user)
    abstracts.append(unregistered_abstract)

    for idx_u, user in enumerate(users):
        cur_abstract = abstracts[idx_u]

        samba.login(user.email, 'pw')
        samba.goto_page(conference['registration_page'])

        if idx_u in users_with_poster_number:
            poster_nr = users_with_poster_number.index(idx_u) + 1
            assert cur_abstract.poster_number == poster_nr
            samba.selenium.assert_text('Poster number: %d' % (poster_nr, ))
            found_email = False

            for cur_mail in mailoutbox:
                if (cur_mail.to[0] == user.email and
                        'Poster number %d' % (poster_nr, ) in cur_mail.body):
                    found_email = True

            assert found_email
        else:
            samba.selenium.assert_text_not_visible('Poster number')


@pytest.mark.parametrize('pay_at_conference', [True, False])
def test_poster_number_constant_after_payment_change(samba, conference,
                                                     pay_at_conference):
    user1, user2 = UserWithPayedRegistration.create_batch(
        parent_conference=conference['conference_page'],
        size=2,
        registration__ticket_options=[]
    )

    user1_abstract = Abstract(user=user1,
                              conference=conference['conference_page'],
                              review_status='accepted')

    assert user1_abstract.poster_number == 1

    user2_abstract = Abstract(user=user2,
                              conference=conference['conference_page'],
                              review_status='accepted')

    assert user2_abstract.poster_number == 2

    user1_reg = user1.registration_set.first()
    user1_reg.ticket_options.add(conference['conference_page'].conference_options.first())  # noqa
    user1_reg.save()

    user1_reg = user1.registration_set.first()

    assert user1_reg.payment_status == 'pending'
    user1_abstract = user1.abstract_set.first()
    assert user1_abstract.poster_number == 1

    if pay_at_conference:
        user1_reg.pay_at_conference = True
        user1_reg.save()
    else:
        Payment(registration=user1_reg, amount=user1_reg.amount_owed)

    user1_reg = user1.registration_set.first()

    if not pay_at_conference:
        assert user1_reg.payment_status == 'complete'

    assert user1_reg.display_for_checkin

    user1_abstract = user1.abstract_set.first()
    assert user1_abstract.poster_number == 1
