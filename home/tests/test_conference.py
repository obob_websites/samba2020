
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import home.factories
import datetime

import home.models
import pytest
from django.core.exceptions import ValidationError
from django.urls import reverse
from contextlib import nullcontext as does_not_raise

import home.models.conference


def test_conference_start_date_must_be_before_end_date(
        samba, admin_user, default_data):
    samba.login_admin()
    samba.selenium.open(samba.live_server + '/admin/')

    conference = home.factories.ConferenceFactory.create(
        parent=default_data['index_page'],
        start_date=datetime.date.today() + datetime.timedelta(days=1),
        end_date=datetime.date.today() + datetime.timedelta(days=10)
    )

    assert home.models.conference.Conference.objects.filter(
        title=conference.title).exists()

    new_title = 'ChaCha 2022'

    with pytest.raises(ValidationError):
        conference = home.factories.conference.ConferenceFactory.create(
            parent=default_data['index_page'],
            title=new_title,
            start_date=datetime.date.today() + datetime.timedelta(days=10),
            end_date=datetime.date.today() + datetime.timedelta(days=1)
        )

    assert not home.models.conference.Conference.objects.filter(
        title=new_title).exists()


@pytest.mark.parametrize('first_warning', (0, 1, 5, 10))
@pytest.mark.parametrize('second_warning', (0, 1, 5, 10))
@pytest.mark.parametrize('unregister', (0, 1, 5, 10))
def test_conference_valid_payment_deadlines(samba, default_data,
                                            first_warning, second_warning,
                                            unregister):
    conf_title = 'ChaCha 2022'

    raise_assert = pytest.raises(ValidationError)
    is_good = False

    if 0 < first_warning < second_warning < unregister:
        is_good = True

    if first_warning == 0 and second_warning == 0 and unregister == 0:
        is_good = True

    if second_warning == 0 and 0 < first_warning < unregister:
        is_good = True

    if is_good:
        raise_assert = does_not_raise()

    with raise_assert:
        home.factories.conference.ConferenceFactory.create(
            parent=default_data['index_page'],
            title=conf_title,
            payment_first_warning_after_n_days=first_warning,
            payment_second_warning_after_n_days=second_warning,
            payment_unregister_after_n_days=unregister
        )

    assert home.models.Conference.objects.filter(
        title=conf_title).exists() == is_good


def test_automatic_conference_group(samba, conference):
    conference_p_group = conference['conference_page'].participant_group  # noqa


def test_ticket_limits(samba, conference):
    conference['conference_page'].conference_number_of_tickets = 30
    conference['conference_page'].save()

    social_ticket_option = conference['conference_page'].conference_options.all()[0]  # noqa
    social_ticket_option.limit = 20
    social_ticket_option.option_price = 70
    social_ticket_option.save()
    social_ticket_option.refresh_from_db()
    other_ticket_option = conference['conference_page'].conference_options.all()[1]  # noqa
    other_ticket_option.limit = None
    other_ticket_option.option_price = 90
    other_ticket_option.save()
    other_ticket_option.refresh_from_db()

    ticket = conference['conference_page'].conference_tickets.all()[0]
    ticket.ticket_price = 100
    ticket.save()
    ticket = conference['conference_page'].conference_tickets.all()[1]
    ticket.ticket_price = 150
    ticket.save()

    assert conference['conference_page'].n_registered_for_conference == 0
    assert conference['conference_page'].tickets_left_for_conference

    for cur_ticket in conference['conference_page'].conference_tickets.all():
        assert cur_ticket.tickets_sold == 0

    assert social_ticket_option.ticket_options_available == 20
    assert social_ticket_option.ticket_options_sold == 0

    assert other_ticket_option.ticket_options_available == 99999
    assert other_ticket_option.ticket_options_sold == 0

    samba.goto_url(home.factories.Registration.get_create_url(
        conference['conference_page']
    ))

    samba.selenium.assert_text('Login')

    registrations = home.factories.Registration.create_batch(
        size=3,
        conference=conference['conference_page'],
        ticket=conference['conference_page'].conference_tickets.all()[0],
        ticket_options=[]
    )

    for cur_reg in registrations:
        assert cur_reg.total_amount.amount == 100

    registrations = home.factories.Registration.create_batch(
        size=2,
        conference=conference['conference_page'],
        ticket=conference['conference_page'].conference_tickets.all()[1],
        ticket_options=[]
    )

    for cur_reg in registrations:
        assert cur_reg.total_amount.amount == 150

    assert conference['conference_page'].n_registered_for_conference == 5
    assert conference['conference_page'].tickets_left_for_conference

    assert conference['conference_page'].conference_tickets.all()[0].tickets_sold == 3  # noqa
    assert conference['conference_page'].conference_tickets.all()[1].tickets_sold == 2  # noqa

    social_ticket_option.refresh_from_db()
    other_ticket_option.refresh_from_db()

    assert social_ticket_option.ticket_options_available == 20
    assert social_ticket_option.ticket_options_sold == 0

    assert other_ticket_option.ticket_options_available == 99999
    assert other_ticket_option.ticket_options_sold == 0

    registrations = home.factories.Registration.create_batch(
        size=4,
        conference=conference['conference_page'],
        ticket=conference['conference_page'].conference_tickets.all()[0],
        ticket_options=[social_ticket_option]
    )

    for cur_reg in registrations:
        assert cur_reg.total_amount.amount == 170

    assert conference['conference_page'].n_registered_for_conference == 9
    assert conference['conference_page'].tickets_left_for_conference

    social_ticket_option.refresh_from_db()
    other_ticket_option.refresh_from_db()

    assert social_ticket_option.ticket_options_available == 16
    assert social_ticket_option.ticket_options_sold == 4

    assert other_ticket_option.ticket_options_available == 99999
    assert other_ticket_option.ticket_options_sold == 0

    registrations = home.factories.Registration.create_batch(
        size=2,
        conference=conference['conference_page'],
        ticket=conference['conference_page'].conference_tickets.all()[1],
        ticket_options=[other_ticket_option]
    )

    for cur_reg in registrations:
        assert cur_reg.total_amount.amount == 240

    assert conference['conference_page'].n_registered_for_conference == 11
    assert conference['conference_page'].tickets_left_for_conference

    social_ticket_option.refresh_from_db()
    other_ticket_option.refresh_from_db()

    assert social_ticket_option.ticket_options_available == 16
    assert social_ticket_option.ticket_options_sold == 4

    assert other_ticket_option.ticket_options_available == 99999
    assert other_ticket_option.ticket_options_sold == 2

    user = home.factories.UserWithConfirmedEmail(raw_password='pw')
    samba.login(user.email, 'pw')

    samba.goto_url(home.factories.Registration.get_create_url(
        conference['conference_page']
    ))

    samba.selenium.assert_text_not_visible(
        'Conference Registration not possible!')

    reg = home.factories.Registration.create_in_browser(
        samba,
        conference=conference['conference_page'],
        user=user,
        ticket=conference['conference_page'].conference_tickets.all()[0],
        ticket_options=[social_ticket_option]
    )

    assert reg.total_amount.amount == 170

    assert conference['conference_page'].n_registered_for_conference == 12

    social_ticket_option.refresh_from_db()
    other_ticket_option.refresh_from_db()

    assert social_ticket_option.ticket_options_available == 15
    assert social_ticket_option.ticket_options_sold == 5

    assert other_ticket_option.ticket_options_available == 99999
    assert other_ticket_option.ticket_options_sold == 2

    user = home.factories.UserWithConfirmedEmail(raw_password='pw')
    samba.login(user.email, 'pw')

    samba.goto_url(home.factories.Registration.get_create_url(
        conference['conference_page']
    ))

    samba.selenium.assert_text(str(social_ticket_option))
    samba.selenium.assert_text(str(other_ticket_option))

    home.factories.Registration.create_batch(
        size=15,
        conference=conference['conference_page'],
        ticket=conference['conference_page'].conference_tickets.all()[0],
        ticket_options=[social_ticket_option]
    )

    assert conference['conference_page'].n_registered_for_conference == 27
    assert conference['conference_page'].tickets_left_for_conference

    social_ticket_option.refresh_from_db()
    other_ticket_option.refresh_from_db()

    assert social_ticket_option.ticket_options_available == 0
    assert social_ticket_option.ticket_options_sold == 20

    assert other_ticket_option.ticket_options_available == 99999
    assert other_ticket_option.ticket_options_sold == 2

    user = home.factories.UserWithConfirmedEmail(raw_password='pw')
    samba.login(user.email, 'pw')

    samba.goto_url(home.factories.Registration.get_create_url(
        conference['conference_page']
    ))

    samba.selenium.assert_text_not_visible(str(social_ticket_option))
    samba.selenium.assert_text(str(other_ticket_option))

    home.factories.Registration.create_batch(
        size=3,
        conference=conference['conference_page'],
        ticket=conference['conference_page'].conference_tickets.all()[1]
    )

    assert conference['conference_page'].n_registered_for_conference == 30
    assert not conference['conference_page'].tickets_left_for_conference

    samba.goto_url(home.factories.Registration.get_create_url(
        conference['conference_page']
    ))

    samba.selenium.assert_text(
        'Conference Registration not possible!')

    with pytest.raises(ValidationError):
        home.factories.Registration.create(
            conference=conference['conference_page'])


def test_registration_start(samba, conference, conference_user):
    samba.logout()

    today = datetime.date.today()
    this_conference = conference['conference_page']
    registration_page = conference['registration_page']

    # If registration has not started yet, tell user to come back later.
    # No need to tell him/her to login...

    this_conference.registration_start_date = today + datetime.timedelta(1)
    this_conference.save()

    samba.goto_url(registration_page.url)
    samba.selenium.assert_text('Registration has not yet started')
    samba.selenium.assert_text_not_visible('Not logged in')

    # But if the user is logged in, tell the same thing

    samba.login(conference_user['user_info'])
    samba.goto_url(registration_page.url)
    samba.selenium.assert_text('Registration has not yet started')
    samba.selenium.assert_text_not_visible('Not logged in')
    samba.selenium.assert_text_not_visible('You must be fully registered and have '  # noqa
                                           'your abstract accepted in order to '  # noqa
                                           'submit the extended abstract')

    # logout again
    samba.logout()

    # If registration has started, tell the user that he/she needs
    # to login to register and/or see the status of their registration

    this_conference.registration_start_date = today + datetime.timedelta(-2)
    this_conference.save()

    samba.goto_url(registration_page.url)
    samba.selenium.assert_text('Registration open! But you are not logged in')
    samba.selenium.assert_text('Abstract submission open. not logged in')
    samba.selenium.assert_text('Not logged in')

    # If the conference is full, tell the user
    registrations = home.factories.Registration.create_batch(
        size=20,
        conference=this_conference
    )

    samba.goto_url(registration_page.url)
    samba.selenium.assert_text_not_visible(
        'Registration open! But you are not logged in')
    samba.selenium.assert_text_not_visible(
        'Abstract submission open. not logged in')
    samba.selenium.assert_text('Not logged in')
    samba.selenium.assert_text('Conference Full')

    # If the deadline has passed and the conference is full,
    # tell the user only that the deadline has passed
    this_conference.registration_deadline = today + datetime.timedelta(-1)
    this_conference.save()

    samba.goto_url(registration_page.url)
    samba.selenium.assert_text_not_visible(
        'Registration open! But you are not logged in')
    samba.selenium.assert_text_not_visible(
        'Abstract submission open. not logged in')
    samba.selenium.assert_text('Not logged in')
    samba.selenium.assert_text('Registration deadline is over')
    samba.selenium.assert_text_not_visible('Conference Full')

    # If the deadline has passed but the conference is not full,
    # tell the user that the deadline has passed

    for cur_reg in registrations:
        cur_reg.delete()

    samba.goto_url(registration_page.url)
    samba.selenium.assert_text_not_visible(
        'Registration open! But you are not logged in')
    samba.selenium.assert_text_not_visible(
        'Abstract submission open. not logged in')
    samba.selenium.assert_text('Not logged in')
    samba.selenium.assert_text('Registration deadline is over')
    samba.selenium.assert_text_not_visible('Conference Full')

    # If the user is logged in, registration is opened, deadline is ok
    # and the user has not registered, tell him/her to do so
    this_conference.registration_deadline = today + datetime.timedelta(3)
    this_conference.save()

    samba.login(conference_user['user_info'])
    samba.goto_url(registration_page.url)

    samba.selenium.assert_text(
        'Registration open! But you have not registered')
    samba.selenium.assert_link_text('Register here')
    samba.selenium.assert_text(
        'Abstract submission is open but no abstract was submitted'
    )
    samba.selenium.assert_link_text('Submit abstract here')

    # If the conference is full, tell the user
    registrations = home.factories.Registration.create_batch(
        size=20,
        conference=this_conference
    )

    samba.goto_url(registration_page.url)
    samba.selenium.assert_text_not_visible(
        'Registration open! But you have not registered')
    samba.selenium.assert_text('Conference Full')
    samba.selenium.assert_text_not_visible(
        'Abstract submission is open but no abstract was submitted'
    )

    # If the deadline has passed and the conference is full,
    # tell the user only that the deadline has passed
    this_conference.registration_deadline = today + datetime.timedelta(-1)
    this_conference.save()

    samba.goto_url(registration_page.url)
    samba.selenium.assert_text_not_visible(
        'Registration open! But you have not registered')
    samba.selenium.assert_text('Registration deadline is over')
    samba.selenium.assert_text_not_visible(
        'Abstract submission is open but no abstract was submitted'
    )
    samba.selenium.assert_text_not_visible('Conference Full')

    # If the deadline has passed but the conference is not full,
    # tell the user that the deadline has passed

    for cur_reg in registrations:
        cur_reg.delete()

    samba.goto_url(registration_page.url)
    samba.selenium.assert_text_not_visible(
        'Registration open! But you have not registered')
    samba.selenium.assert_text('Registration deadline is over')
    samba.selenium.assert_text_not_visible('Conference Full')
    samba.selenium.assert_text_not_visible(
        'Abstract submission is open but no abstract was submitted'
    )

    this_conference.registration_deadline = today + datetime.timedelta(3)
    this_conference.save()

    # Lets register the current user
    home.factories.Registration.create(
        user=conference_user['user'],
        conference=conference['conference_page'],
    )

    samba.goto_url(registration_page.url)

    samba.selenium.assert_text(
        'Abstract submission is open but no abstract was submitted'
    )
    samba.selenium.assert_link_text('Submit abstract here')
    samba.selenium.assert_text('You are registered but have not paid')
    samba.selenium.assert_text_not_visible('Abstract deadline has passed')

    # Setting back the conference deadline should not change this
    this_conference.registration_deadline = today + datetime.timedelta(-2)
    this_conference.save()

    samba.goto_url(registration_page.url)

    samba.selenium.assert_text(
        'Abstract submission is open but no abstract was submitted'
    )
    samba.selenium.assert_link_text('Submit abstract here')
    samba.selenium.assert_text('You are registered but have not paid')
    samba.selenium.assert_text_not_visible('Abstract deadline has passed')

    # also making the conference full should show the same
    this_conference.registration_deadline = today + datetime.timedelta(2)
    this_conference.save()
    home.factories.Registration.create_batch(
        size=19,
        conference=this_conference
    )
    this_conference.registration_deadline = today + datetime.timedelta(-2)
    this_conference.save()

    samba.goto_url(registration_page.url)

    samba.selenium.assert_text(
        'Abstract submission is open but no abstract was submitted'
    )
    samba.selenium.assert_link_text('Submit abstract here')
    samba.selenium.assert_text('You are registered but have not paid')
    samba.selenium.assert_text_not_visible('Abstract deadline has passed')

    # If the abstract submission deadline is over, this should have an effect
    this_conference.abstract_deadline = today + datetime.timedelta(-1)
    this_conference.save()

    samba.goto_url(registration_page.url)

    samba.selenium.assert_text_not_visible(
        'Abstract submission is open but no abstract was submitted'
    )
    samba.selenium.assert_text('You are registered but have not paid')
    samba.selenium.assert_text('Abstract deadline has passed')

    # and when we log out, this should still be the case
    samba.logout()
    samba.goto_url(registration_page.url)
    samba.selenium.assert_text('Abstract deadline has passed')


def test_misc_settings(samba, conference, admin_user):
    samba.login_with_role('superuser', conference, None)

    conf = conference['conference_page']
    edit_conf_url = reverse('wagtailadmin_pages:edit',
                            kwargs={'page_id': conf.id})
    samba.goto_url(edit_conf_url + '#tab-settings')

    samba.selenium.assert_text('Misc')
