
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import pytest
from django.core.exceptions import ObjectDoesNotExist, ValidationError

from ..factories import Registration, Payment, ConferenceFactory, ConferenceTicketFactory
import home.models
import home.forms.registration
import datetime
from freezegun import freeze_time
from home.cron.payment_reminder import process_payment_reminders


@pytest.mark.django_db
def test_registration_factory(samba, conference, conference_user, admin_user):
    samba.logout()
    samba.goto_url(conference['registration_page'].url)

    samba.selenium.assert_text('Not logged in')

    samba.login(conference_user['user_info'])

    samba.goto_url(conference['registration_page'].url)
    samba.selenium.assert_text('Registration open!')
    samba.selenium.assert_link_text('Register here')


@pytest.mark.django_db
def test_registration_factory_db(samba, conference, conference_user):
    Registration.create(
        user=conference_user['user'],
        conference=conference['conference_page']
    )


@pytest.mark.django_db
@pytest.mark.parametrize('has_read_refund', [True, False])
@pytest.mark.parametrize('consent_data', [True, False])
def test_needs_to_tick_consent_boxes(samba, conference, conference_user,
                                     has_read_refund, consent_data):
    Registration.create_in_browser(
        samba=samba,
        user=conference_user['user'],
        conference=conference['conference_page'],
        has_read_refund_policy=has_read_refund,
        consents_to_data_processing=consent_data
    )

    if consent_data and has_read_refund:
        assert home.models.Registration.objects.count() == 1
    else:
        assert home.models.Registration.objects.count() == 0


@pytest.mark.django_db
def test_datetime_added_field(samba, conference, conference_user):
    registration = Registration.create_in_browser(
        samba=samba,
        user=conference_user['user'],
        conference=conference['conference_page']
    )

    date_difference = datetime.datetime.now(
        datetime.timezone.utc) - registration.datetime_added

    assert date_difference.seconds < 60


@pytest.mark.django_db
def test_payments(samba, conference, ticket=0, ticket_options=[0]):
    today = datetime.date.today()

    conf = conference['conference_page']
    conf.payment_first_warning_after_n_days = 5
    conf.payment_second_warning_after_n_days = 10
    conf.payment_unregister_after_n_days = 15
    conf.save()

    ticket_object = conf.conference_tickets.all()[ticket]
    ticket_options_objects = []

    conference_fee = ticket_object.ticket_price
    ticket_options_fee = 0

    for idx, x in enumerate(conf.conference_options.all()):
        if idx in ticket_options:
            ticket_options_objects.append(x)
            ticket_options_fee += x.option_price

    conference_fee = conference_fee + ticket_options_fee

    registration = Registration.create(
        conference=conf,
        ticket=ticket_object,
        ticket_options=ticket_options_objects
    )

    registration = home.models.Registration.objects.get(id=registration.pk)

    assert registration.total_amount == conference_fee
    assert registration.amount_paid.amount == 0
    assert registration.amount_owed == conference_fee
    assert registration.fees_paid.amount == 0
    assert registration.payment_status == 'pending'
    assert registration.send_first_payment_reminder_date == (
        today + datetime.timedelta(days=5))
    assert registration.send_second_payment_reminder_date == (
        today + datetime.timedelta(days=10))
    assert registration.unregister_date == (
        today + datetime.timedelta(days=15))

    ticket_object.ticket_price = 200
    ticket_object.save()
    registration = home.models.Registration.objects.get(id=registration.pk)

    conference_fee = ticket_options_fee + ticket_object.ticket_price

    assert registration.total_amount == conference_fee
    assert registration.amount_paid.amount == 0
    assert registration.amount_owed == conference_fee
    assert registration.payment_status == 'pending'
    assert registration.fees_paid.amount == 0
    assert registration.send_first_payment_reminder_date == (
        today + datetime.timedelta(days=5))
    assert registration.send_second_payment_reminder_date == (
        today + datetime.timedelta(days=10))
    assert registration.unregister_date == (
        today + datetime.timedelta(days=15))

    Payment.create(
        registration=registration,
        amount=40
    )
    registration = home.models.Registration.objects.get(id=registration.pk)

    assert registration.total_amount == conference_fee
    assert registration.amount_paid.amount == 40
    assert registration.amount_owed.amount == conference_fee.amount - 40
    assert registration.payment_status == 'pending'
    assert registration.fees_paid.amount == 0
    assert registration.send_first_payment_reminder_date == (
        today + datetime.timedelta(days=5))
    assert registration.send_second_payment_reminder_date == (
        today + datetime.timedelta(days=10))
    assert registration.unregister_date == (
        today + datetime.timedelta(days=15))

    Payment.create(
        registration=registration,
        amount=160,
        fee=20
    )
    registration = home.models.Registration.objects.get(id=registration.pk)

    assert registration.total_amount == conference_fee
    assert registration.amount_paid.amount == 200
    assert registration.amount_owed.amount == conference_fee.amount - 200 + 20
    assert registration.payment_status == 'pending'
    assert registration.fees_paid.amount == 20
    assert registration.send_first_payment_reminder_date == (
        today + datetime.timedelta(days=5))
    assert registration.send_second_payment_reminder_date == (
        today + datetime.timedelta(days=10))
    assert registration.unregister_date == (
        today + datetime.timedelta(days=15))

    Payment.create(
        registration=registration,
        amount=registration.amount_owed
    )
    registration = home.models.Registration.objects.get(id=registration.pk)

    assert registration.total_amount == conference_fee
    assert registration.amount_paid.amount == conference_fee.amount + 20
    assert registration.amount_owed.amount == 0
    assert registration.payment_status == 'complete'
    assert registration.fees_paid.amount == 20
    assert registration.send_first_payment_reminder_date is None
    assert registration.send_second_payment_reminder_date is None
    assert registration.unregister_date is None


@pytest.mark.parametrize('registration_open', [True, False])
@pytest.mark.parametrize('deadline_ok', [True, False])
@pytest.mark.parametrize('already_registered', [True, False])
@pytest.mark.parametrize('conference_full', [True, False])
@pytest.mark.parametrize('in_browser', [True, False])
def test_registration_allowed_forbidden(samba, conference, mailoutbox,
                                        registration_open, deadline_ok,
                                        in_browser, already_registered,
                                        conference_full):
    today = datetime.date.today()
    conf = conference['conference_page']
    user = home.factories.UserWithConfirmedEmail.create(raw_password='pw')
    samba.login(user.email, 'pw')

    is_ok = (deadline_ok and registration_open and not already_registered and
             not conference_full)

    if already_registered:
        Registration.create(
            conference=conf,
            user=user
        )

    if conference_full:
        Registration.create_batch(
            size=conf.n_remaining_for_conference,
            conference=conf
        )

    if not deadline_ok:
        conf.registration_deadline = today + datetime.timedelta(-1)
        conf.save()

    if not registration_open:
        conf.registration_start_date = today + datetime.timedelta(1)
        conf.save()

    if in_browser:
        samba.goto_url(conference['registration_page'].url)
        if is_ok:
            samba.selenium.assert_text('Registration open!')
        else:
            samba.selenium.assert_text_not_visible('Registration open!')
        samba.goto_url(conference['form_pages']['register'].url)
        samba.selenium.assert_text_not_visible('Internal server error')

        if is_ok:
            samba.selenium.assert_text('Registration Form is here')
            Registration.create_in_browser(
                samba,
                conference=conf,
                user=user
            )
            samba.selenium.assert_text_not_visible('Internal server error')
            samba.selenium.assert_text_not_visible(
                'Conference Registration not possible!')

        else:
            samba.selenium.assert_text_not_visible('Registration Form is here')
            samba.selenium.assert_text('Conference Registration not possible!')

    else:
        if is_ok:
            Registration.create(
                conference=conf,
                user=user,
            )
        else:
            with pytest.raises((ValidationError, ObjectDoesNotExist)):
                Registration.create(
                    conference=conf,
                    user=user,
                )

    if is_ok:
        mail_body = mailoutbox[-1].body
        assert 'Thanks for registering.' in mail_body
        assert user.first_name in mail_body
        assert user.last_name in mail_body
        assert conference['conference_page'].title in mail_body


@pytest.mark.parametrize('registration_open', [True, False])
@pytest.mark.parametrize('deadline_ok', [True, False])
@pytest.mark.parametrize('conference_full', [True, False])
def test_notlogged_in_messages(samba, conference,
                               registration_open, deadline_ok,
                               conference_full):
    today = datetime.date.today()
    conf = conference['conference_page']

    if conference_full:
        Registration.create_batch(
            size=conf.n_remaining_for_conference,
            conference=conf
        )

    if not deadline_ok:
        conf.registration_deadline = today + datetime.timedelta(-1)
        conf.save()

    if not registration_open:
        conf.registration_start_date = today + datetime.timedelta(1)
        conf.save()

    samba.goto_url(conference['registration_page'].url)

    if registration_open:
        samba.selenium.assert_text('Not logged in')
    else:
        samba.selenium.assert_text_not_visible('Not logged in')
        samba.selenium.assert_text_not_visible('Registration open! '
                                               'But you are not logged in')
        samba.selenium.assert_text_not_visible('Registration deadline is over')
        samba.selenium.assert_text_not_visible('Conference Full')
        samba.selenium.assert_text('Registration has not yet started')
        return

    if registration_open and deadline_ok and not conference_full:
        samba.selenium.assert_text('Registration open! '
                                   'But you are not logged in')
        samba.selenium.assert_text_not_visible('Registration deadline is over')
        samba.selenium.assert_text_not_visible('Conference Full')
    elif not deadline_ok:
        samba.selenium.assert_text_not_visible('Registration open! '
                                               'But you are not logged in')
        samba.selenium.assert_text('Registration deadline is over')
        samba.selenium.assert_text_not_visible('Conference Full')
    elif conference_full:
        samba.selenium.assert_text_not_visible('Registration open! '
                                               'But you are not logged in')
        samba.selenium.assert_text_not_visible('Registration deadline is over')
        samba.selenium.assert_text('Conference Full')


def test_links_to_policy_pages(samba, conference, conference_user):
    samba.goto_url(conference['form_pages']['register'].url)

    samba.selenium.assert_link_text('privacy policy')
    samba.selenium.assert_link_text('refund policy')


def test_payment_reset_status(samba, conference):
    today = datetime.date.today()

    conf = conference['conference_page']
    conf.payment_first_warning_after_n_days = 5
    conf.payment_second_warning_after_n_days = 10
    conf.payment_unregister_after_n_days = 15
    conf.save()

    ticket = conf.conference_tickets.first()
    ticket_option = conf.conference_options.first()

    registration = Registration.create(
        conference=conf,
        ticket=ticket,
        ticket_options=[ticket_option]
    )

    assert registration.send_first_payment_reminder_date == (
        today + datetime.timedelta(days=5))
    assert registration.send_second_payment_reminder_date == (
        today + datetime.timedelta(days=10))
    assert registration.unregister_date == (
        today + datetime.timedelta(days=15))

    ticket.ticket_price = 100
    ticket.save()

    ticket_option.option_price = 20
    ticket_option.save()

    payment = Payment.create(
        registration=registration,
        amount=120
    )
    registration = home.models.Registration.objects.get(id=registration.id)

    assert registration.total_amount.amount == 120
    assert registration.amount_paid.amount == 120
    assert registration.amount_owed.amount == 0
    assert registration.payment_status == 'complete'
    assert registration.fees_paid.amount == 0

    assert registration.send_first_payment_reminder_date is None
    assert registration.send_second_payment_reminder_date is None
    assert registration.unregister_date is None

    payment.delete()

    registration = home.models.Registration.objects.get(id=registration.id)

    assert registration.total_amount.amount == 120
    assert registration.amount_paid.amount == 0
    assert registration.amount_owed.amount == 120
    assert registration.payment_status == 'pending'
    assert registration.fees_paid.amount == 0
    assert registration.send_first_payment_reminder_date == (
        today + datetime.timedelta(days=5))
    assert registration.send_second_payment_reminder_date == (
        today + datetime.timedelta(days=10))
    assert registration.unregister_date == (
        today + datetime.timedelta(days=15))

    Payment.create(
        registration=registration,
        amount=120
    )

    registration = home.models.Registration.objects.get(id=registration.id)

    assert registration.total_amount.amount == 120
    assert registration.amount_paid.amount == 120
    assert registration.amount_owed.amount == 0
    assert registration.payment_status == 'complete'
    assert registration.fees_paid.amount == 0
    assert registration.send_first_payment_reminder_date is None
    assert registration.send_second_payment_reminder_date is None
    assert registration.unregister_date is None

    Payment.create(
        registration=registration,
        amount=0,
        fee=10,
    )

    registration = home.models.Registration.objects.get(id=registration.id)

    assert registration.total_amount.amount == 120
    assert registration.amount_paid.amount == 120
    assert registration.amount_owed.amount == 10
    assert registration.payment_status == 'pending'
    assert registration.fees_paid.amount == 10
    assert registration.send_first_payment_reminder_date == (
        today + datetime.timedelta(days=5))
    assert registration.send_second_payment_reminder_date == (
        today + datetime.timedelta(days=10))
    assert registration.unregister_date == (
        today + datetime.timedelta(days=15))

    Payment.create(
        registration=registration,
        amount=10,
        fee=0,
    )

    registration = home.models.Registration.objects.get(id=registration.id)

    assert registration.total_amount.amount == 120
    assert registration.amount_paid.amount == 130
    assert registration.amount_owed.amount == 0
    assert registration.payment_status == 'complete'
    assert registration.fees_paid.amount == 10
    assert registration.send_first_payment_reminder_date is None
    assert registration.send_second_payment_reminder_date is None
    assert registration.unregister_date is None

    ticket.ticket_price = 110
    ticket.save()

    registration = home.models.Registration.objects.get(id=registration.id)

    assert registration.total_amount.amount == 130
    assert registration.amount_paid.amount == 130
    assert registration.amount_owed.amount == 10
    assert registration.payment_status == 'pending'
    assert registration.fees_paid.amount == 10
    assert registration.send_first_payment_reminder_date == (
        today + datetime.timedelta(days=5))
    assert registration.send_second_payment_reminder_date == (
        today + datetime.timedelta(days=10))
    assert registration.unregister_date == (
        today + datetime.timedelta(days=15))

    Payment.create(
        registration=registration,
        amount=10,
        fee=0,
    )

    registration = home.models.Registration.objects.get(id=registration.id)

    assert registration.total_amount.amount == 130
    assert registration.amount_paid.amount == 140
    assert registration.amount_owed.amount == 0
    assert registration.payment_status == 'complete'
    assert registration.fees_paid.amount == 10
    assert registration.send_first_payment_reminder_date is None
    assert registration.send_second_payment_reminder_date is None
    assert registration.unregister_date is None

    ticket_option.option_price = 40
    ticket_option.save()

    registration = home.models.Registration.objects.get(id=registration.id)

    assert registration.total_amount.amount == 150
    assert registration.amount_paid.amount == 140
    assert registration.amount_owed.amount == 20
    assert registration.payment_status == 'pending'
    assert registration.fees_paid.amount == 10
    assert registration.send_first_payment_reminder_date == (
        today + datetime.timedelta(days=5))
    assert registration.send_second_payment_reminder_date == (
        today + datetime.timedelta(days=10))
    assert registration.unregister_date == (
        today + datetime.timedelta(days=15))

    registration.discount = 20
    registration.save()

    registration = home.models.Registration.objects.get(id=registration.id)

    assert registration.total_amount.amount == 130
    assert registration.amount_paid.amount == 140
    assert registration.amount_owed.amount == 0
    assert registration.payment_status == 'complete'
    assert registration.fees_paid.amount == 10
    assert registration.send_first_payment_reminder_date is None
    assert registration.send_second_payment_reminder_date is None
    assert registration.unregister_date is None


def test_payment_blocks(samba, conference, conference_user, mailoutbox):
    today = datetime.date.today()

    conf = conference['conference_page']
    user = conference_user['user']

    conf.conference_fee = 100
    conf.payment_first_warning_after_n_days = 5
    conf.payment_second_warning_after_n_days = 10
    conf.payment_unregister_after_n_days = 15
    conf.save()

    registration = Registration.create(
        conference=conf,
        user=user
    )

    assert registration.send_first_payment_reminder_date == (
        today + datetime.timedelta(days=5))
    assert registration.send_second_payment_reminder_date == (
        today + datetime.timedelta(days=10))
    assert registration.unregister_date == (
        today + datetime.timedelta(days=15))

    samba.goto_url(conference['registration_page'].url)

    samba.selenium.assert_text('You are registered but have not paid.')
    samba.selenium.assert_text_not_visible(
        'You are registered and paid everything.')
    samba.selenium.assert_text_not_visible('You pay at the conference')

    registration.pay_at_conference = True
    registration.save()
    assert registration.send_first_payment_reminder_date is None
    assert registration.send_second_payment_reminder_date is None
    assert registration.unregister_date is None

    mail_body = mailoutbox[-1].body
    assert 'You pay the rest at the conference' in mail_body
    assert user.first_name in mail_body
    assert user.last_name in mail_body
    assert conf.title in mail_body

    samba.goto_url(conference['registration_page'].url)

    samba.selenium.assert_text_not_visible(
        'You are registered but have not paid.')
    samba.selenium.assert_text_not_visible(
        'You are registered and paid everything.')
    samba.selenium.assert_text('You pay at the conference')

    Payment.create(
        registration=registration,
        amount=30
    )

    samba.goto_url(conference['registration_page'].url)

    samba.selenium.assert_text_not_visible(
        'You are registered but have not paid.')
    samba.selenium.assert_text_not_visible(
        'You are registered and paid everything.')
    samba.selenium.assert_text('You pay at the conference')
    assert registration.send_first_payment_reminder_date is None
    assert registration.send_second_payment_reminder_date is None
    assert registration.unregister_date is None

    Payment.create(
        registration=registration,
        amount=registration.amount_owed
    )

    samba.goto_url(conference['registration_page'].url)

    samba.selenium.assert_text_not_visible(
        'You are registered but have not paid.')
    samba.selenium.assert_text(
        'You are registered and paid everything.')
    samba.selenium.assert_text_not_visible('You pay at the conference')

    mail_body = mailoutbox[-1].body
    assert 'Thanks for paying.' in mail_body
    assert user.first_name in mail_body
    assert user.last_name in mail_body
    assert conf.title in mail_body
    assert registration.send_first_payment_reminder_date is None
    assert registration.send_second_payment_reminder_date is None
    assert registration.unregister_date is None


@pytest.mark.parametrize(['first_warning', 'second_warning', 'unregister'],
                         ((0, 0, 0),
                          (1, 5, 10),
                          (1, 0, 10)))
def test_payment_deadlines(samba, default_data, mailoutbox,
                           first_warning, second_warning, unregister):
    conf_title = 'ChaCha 2022'

    today = datetime.date.today()

    conference = ConferenceFactory.create(
        parent=default_data['index_page'],
        title=conf_title,
        payment_first_warning_after_n_days=first_warning,
        payment_second_warning_after_n_days=second_warning,
        payment_unregister_after_n_days=unregister
    )

    registration = Registration.create(
        conference=conference
    )

    if first_warning == 0:
        assert registration.send_first_payment_reminder_date is None
    else:
        assert registration.send_first_payment_reminder_date == today + datetime.timedelta(days=first_warning)  # noqa

    if second_warning == 0:
        assert registration.send_second_payment_reminder_date is None
    else:
        assert registration.send_second_payment_reminder_date == today + datetime.timedelta(days=second_warning)  # noqa

    if unregister == 0:
        assert registration.unregister_date is None
    else:
        assert registration.unregister_date == today + datetime.timedelta(days=unregister)  # noqa

    mailoutbox.clear()

    if first_warning:
        with freeze_time(today + datetime.timedelta(days=first_warning)):
            process_payment_reminders()
            assert len(mailoutbox) == 1
            process_payment_reminders()
            assert len(mailoutbox) == 1

            mail_body = mailoutbox[-1].body
            assert 'First Payment Warning' in mail_body
            assert registration.user.first_name in mail_body
            assert registration.user.last_name in mail_body
            assert conf_title in mail_body

    mailoutbox.clear()

    if second_warning:
        with freeze_time(today + datetime.timedelta(days=second_warning)):
            process_payment_reminders()
            assert len(mailoutbox) == 1
            process_payment_reminders()
            assert len(mailoutbox) == 1

            mail_body = mailoutbox[-1].body
            assert 'Second Payment Warning' in mail_body
            assert registration.user.first_name in mail_body
            assert registration.user.last_name in mail_body
            assert conf_title in mail_body

    mailoutbox.clear()

    if unregister:
        with freeze_time(today + datetime.timedelta(days=unregister)):
            process_payment_reminders()
            assert len(mailoutbox) == 1
            process_payment_reminders()
            assert len(mailoutbox) == 1

            mail_body = mailoutbox[-1].body
            assert 'Unregistered because no payment' in mail_body
            assert registration.user.first_name in mail_body
            assert registration.user.last_name in mail_body
            assert conf_title in mail_body

            assert home.models.Registration.objects.count() == 0
    else:
        assert home.models.Registration.objects.count() == 1


def test_ticket_option_validation(samba, conference):
    user = home.factories.UserWithConfirmedEmail(raw_password='pw')
    form_class = home.forms.registration.RegistrationForm
    form_class.conference = conference['conference_page']
    form_class.user = user

    this_ticket_option = conference['conference_page'].conference_options.all()[0]  # noqa
    this_ticket_option.limit = 5

    form = form_class(data={
        'degree': 'Mr.',
        'affiliation': home.models.Affiliation.objects.first(),
        'has_read_refund_policy': 'on',
        'consents_to_data_processing': 'on',
        'ticket': str(conference['conference_page'].conference_tickets.all()[0].pk),  # noqa
        'ticket_options': [this_ticket_option]
    })

    form.data.update(form.initial)

    form.full_clean()
    assert form.is_valid()

    this_ticket_option.available = False
    this_ticket_option.save()

    form = form_class(data={
        'degree': 'Mr.',
        'affiliation': home.models.Affiliation.objects.first(),
        'has_read_refund_policy': 'on',
        'consents_to_data_processing': 'on',
        'ticket': str(conference['conference_page'].conference_tickets.all()[0].pk),  # noqa
        'ticket_options': [this_ticket_option]
    })

    form.data.update(form.initial)

    form.full_clean()
    assert not form.is_valid()

    this_ticket_option.available = True
    this_ticket_option.save()

    form = form_class(data={
        'degree': 'Mr.',
        'affiliation': home.models.Affiliation.objects.first(),
        'has_read_refund_policy': 'on',
        'consents_to_data_processing': 'on',
        'ticket': str(
            conference['conference_page'].conference_tickets.all()[0].pk),
        'ticket_options': [this_ticket_option]
    })

    form.data.update(form.initial)

    form.full_clean()
    assert form.is_valid()

    Registration.create_batch(
        size=5,
        conference=conference['conference_page'],
        ticket_options=[this_ticket_option]
    )

    form = form_class(data={
        'degree': 'Mr.',
        'affiliation': home.models.Affiliation.objects.first(),
        'has_read_refund_policy': 'on',
        'consents_to_data_processing': 'on',
        'ticket': str(
            conference['conference_page'].conference_tickets.all()[0].pk),
        'ticket_options': [this_ticket_option]
    })

    form.data.update(form.initial)

    form.full_clean()
    assert not form.is_valid()


def test_ticket_validation(samba, conference):
    user = home.factories.UserWithConfirmedEmail(raw_password='pw')
    conf = conference['conference_page']
    conf.conference_number_of_tickets = 5

    tickets = conf.conference_tickets.all()

    form_class = home.forms.registration.RegistrationForm
    form_class.conference = conf
    form_class.user = user

    for ticket in tickets:
        form = form_class(data={
            'degree': 'Mr.',
            'affiliation': home.models.Affiliation.objects.first(),
            'has_read_refund_policy': 'on',
            'consents_to_data_processing': 'on',
            'ticket': str(ticket.pk)
        })

        form.data.update(form.initial)

        form.full_clean()
        assert form.is_valid()

    for ticket in tickets:
        ticket.available = False
        ticket.save()

        for chosen_ticket in tickets:
            form = form_class(data={
                'degree': 'Mr.',
                'affiliation': home.models.Affiliation.objects.first(),
                'has_read_refund_policy': 'on',
                'consents_to_data_processing': 'on',
                'ticket': str(chosen_ticket.pk)
            })

            form.data.update(form.initial)

            form.full_clean()
            if chosen_ticket == ticket:
                assert not form.is_valid()
            else:
                assert form.is_valid()

        ticket.available = True
        ticket.save()

    Registration.create_batch(size=5, conference=conf)

    for ticket in tickets:
        form = form_class(data={
            'degree': 'Mr.',
            'affiliation': home.models.Affiliation.objects.first(),
            'has_read_refund_policy': 'on',
            'consents_to_data_processing': 'on',
            'ticket': str(ticket.pk)
        })

        form.data.update(form.initial)

        form.full_clean()
        assert not form.is_valid()


@pytest.mark.parametrize('time_shift_days', [0, -2, +2, -3, +3, +4])
def test_observe_ticket_date_limits(samba, conference, conference_user, time_shift_days):
    today = datetime.date.today()

    conf = conference['conference_page']
    conf.conference_tickets.set([
        ConferenceTicketFactory(name='Always good', page=conf),
        ConferenceTicketFactory(name='only_start_date', not_before=today - datetime.timedelta(days=2), page=conf),
        ConferenceTicketFactory(name='only_end_date', not_after=today + datetime.timedelta(days=2), page=conf),
        ConferenceTicketFactory(name='range', not_before=today, not_after=today + datetime.timedelta(days=3), page=conf)
    ])
    conf.registration_start_date = today - datetime.timedelta(days=20)
    conf.registration_deadline = today + datetime.timedelta(days=20)
    conf.save()

    form_class = home.forms.registration.RegistrationForm
    form_class.conference = conf
    form_class.user = conference_user['user']

    with freeze_time(today + datetime.timedelta(days=time_shift_days)):
        samba.login(conference_user['user_info'])
        samba.goto_page(conference['form_pages']['register'])

        should_be_there = ['Always good']
        should_not_be_there = []

        if time_shift_days >= -2:
            should_be_there.append('only_start_date')
        else:
            should_not_be_there.append('only_start_date')

        if time_shift_days <= 2:
            should_be_there.append('only_end_date')
        else:
            should_not_be_there.append('only_end_date')

        if 0 <= time_shift_days <= 3:
            should_be_there.append('range')
        else:
            should_not_be_there.append('range')

        for o in should_be_there:
            samba.selenium.assert_text(o)

        for o in should_not_be_there:
            samba.selenium.assert_text_not_visible(o)

        assert len(should_be_there + should_not_be_there) == 4

        for cur_ticket_name in should_be_there + should_not_be_there:
            cur_ticket = home.models.ConferenceTicket.objects.get(name=cur_ticket_name)

            form = form_class(data={
                'degree': 'Mr.',
                'affiliation': home.models.Affiliation.objects.first(),
                'has_read_refund_policy': 'on',
                'consents_to_data_processing': 'on',
                'ticket': str(cur_ticket.pk)
            })

            form.data.update(form.initial)

            form.full_clean()

            if cur_ticket_name in should_be_there:
                assert form.is_valid()
            else:
                assert not form.is_valid()
