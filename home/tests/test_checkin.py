#  Copyright (c) 2016-2021, Thomas Hartmann
#
#  This file is part of the SAMBA2020 Project
#  (https://gitlab.com/obob_websites/samba2020)
#
#  SAMBA2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SAMBA2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SAMBA2020. If not, see <http://www.gnu.org/licenses/>.
import pytest
import home.factories
import home.models
from django.core.exceptions import ValidationError
from selenium.webdriver.common.by import By


def test_can_checkin_db(samba, conference):
    conf = conference['conference_page']
    registration = home.factories.Registration(conference=conf)

    assert not registration.can_checkin
    assert not registration.display_for_checkin
    assert not hasattr(registration, 'checkin')

    home.factories.Payment(registration=registration,
                           amount=10)

    registration.refresh_from_db()

    assert not registration.can_checkin
    assert not registration.display_for_checkin
    assert not hasattr(registration, 'checkin')

    registration.pay_at_conference = True
    registration.save()

    assert not registration.can_checkin
    assert registration.display_for_checkin
    assert not hasattr(registration, 'checkin')

    payment = home.factories.Payment(
        registration=registration,
        amount=registration.amount_owed
    )

    registration.refresh_from_db()

    assert registration.can_checkin
    assert registration.display_for_checkin
    assert not hasattr(registration, 'checkin')

    payment.delete()
    registration.refresh_from_db()

    assert not registration.can_checkin
    assert registration.display_for_checkin
    assert not hasattr(registration, 'checkin')

    registration.pay_at_conference = False
    registration.save()

    assert not registration.can_checkin
    assert not registration.display_for_checkin
    assert not hasattr(registration, 'checkin')

    with pytest.raises(ValidationError):
        home.factories.Checkin(registration=registration)

    registration.checkin = None
    registration.save()

    home.factories.Payment(
        registration=registration,
        amount=registration.amount_owed
    )

    registration.refresh_from_db()
    assert registration.can_checkin
    assert registration.display_for_checkin
    assert not hasattr(registration, 'checkin')

    home.factories.Checkin(registration=registration)

    registration.refresh_from_db()
    assert registration.checkin

    assert not registration.can_checkin
    assert not registration.display_for_checkin


def test_checkin_only_from_correct_conference(samba, conference):
    samba.login_with_role('manager', conference)
    conf = conference['conference_page']
    other_conference = home.factories.ConferenceFactory(
        title='Rumba 2020'
    )

    good_participants = home.factories.UserWithPayedRegistration.create_batch(
        size=5,
        parent_conference=conf
    )
    bad_participants = home.factories.UserWithPayedRegistration.create_batch(
        size=5,
        parent_conference=other_conference
    )

    samba.goto_page(conference['checkin_page'])

    table = samba.selenium.get_element('table')
    rows = table.find_elements(By.TAG_NAME, 'tr')

    full_names = []
    for r in rows:
        cells = r.find_elements(By.TAG_NAME, 'td')
        if not cells:
            continue

        full_names.append('{} {}'.format(
            cells[1].text,
            cells[0].text
        ))

    for participant in good_participants:
        assert participant.get_full_name() in full_names

    for participant in bad_participants:
        assert participant.get_full_name() not in full_names


def test_only_participants_with_paid_or_payatconf_registrations(
        samba, conference
):
    samba.login_with_role('manager', conference)
    conf = conference['conference_page']

    good_participants = []
    bad_participants = []

    good_participants.append(
        home.factories.UserWithPayedRegistration(
            parent_conference=conf
        )
    )

    good_participants.append(
        home.factories.UserPaysRestAtConferenceRegistration(
            parent_conference=conf
        )
    )

    bad_participants.append(
        home.factories.UserWithUnpaidRegistration(
            parent_conference=conf
        )
    )

    abstract = home.factories.Abstract(conference=conf)

    bad_participants.append(
        abstract.user
    )

    samba.goto_page(conference['checkin_page'])

    table = samba.selenium.get_element('table')
    rows = table.find_elements(By.TAG_NAME, 'tr')

    full_names = []
    button_text = {}
    for r in rows:
        cells = r.find_elements(By.TAG_NAME, 'td')
        if not cells:
            continue

        full_names.append('{} {}'.format(
            cells[1].text,
            cells[0].text
        ))
        button_text[full_names[-1]] = cells[3].text

    for participant in good_participants:
        assert participant.get_full_name() in full_names
        reg = participant.registration_set.first()
        if reg.payment_status == 'complete':
            assert button_text[participant.get_full_name()] == 'Checkin'
        else:
            assert button_text[participant.get_full_name()] == 'Pay and ' \
                                                               'Checkin'

    for participant in bad_participants:
        assert participant.get_full_name() not in full_names


def test_checkin_gui(samba, conference):
    samba.login_with_role('manager', conference)
    conf = conference['conference_page']
    ticket = conf.conference_tickets.first()
    ticket.ticket_price = 100
    ticket.save()

    paid_participant = home.factories.UserWithPayedRegistration(
        parent_conference=conf,
        registration__ticket=ticket,
        registration__ticket_options=[]
    )
    pay_at_conf_participant = home.factories.UserPaysRestAtConferenceRegistration(  # noqa
        parent_conference=conf,
        registration__ticket=ticket,
        registration__ticket_options=[]
    )

    samba.goto_page(conference['checkin_page'])
    all_buttons = samba.selenium.get_element(
        'table'
    ).find_elements(By.TAG_NAME, 'button')
    button_dict = {b.get_attribute('data-full-name'): b for b in all_buttons}

    button_paid = button_dict[paid_participant.get_full_name()]
    assert button_paid.text == 'Checkin'
    button_paid.click()

    samba.selenium.assert_text(
        'Please confirm that you want to checkin {}!'.format(
            paid_participant.get_full_name()
        )
    )

    samba.selenium.get_element('#submit-id-submit_btn_checkin').click()

    paid_participant.refresh_from_db()
    assert hasattr(paid_participant.registration_set.first(), 'checkin')

    button_pay_at_conf = samba.selenium.get_element(
        'table'
    ).find_elements(By.TAG_NAME, 'button')[0]
    assert button_pay_at_conf.text == 'Pay and Checkin'
    button_pay_at_conf.click()

    assert pay_at_conf_participant.registration_set.first().amount_owed.amount == 100  # noqa

    amount_owed_str = str(
        pay_at_conf_participant.registration_set.first().amount_owed
    )\
        .replace("\xa0", " ")
    samba.selenium.assert_text(f'{pay_at_conf_participant.get_full_name()} '
                               f'needs to pay '
                               f'{amount_owed_str}!')

    submit_button = samba.selenium.get_element(
        '#submit-id-submit_btn_checkin_and_pay'
    )
    assert not submit_button.is_enabled()

    samba.selenium.get_element('#div_id_participant_has_paid > label').click()
    assert submit_button.is_enabled()
    submit_button.click()

    pay_at_conf_participant.refresh_from_db()
    assert hasattr(pay_at_conf_participant.registration_set.first(), 'checkin')
    assert pay_at_conf_participant.registration_set.first().payment_status == 'complete'  # noqa


def test_checkin_email_and_regoverview(samba, conference, mailoutbox):
    conf = conference['conference_page']
    participant = home.factories.UserWithPayedRegistration(
        parent_conference=conf,
        raw_password='pw'
    )

    samba.login(participant.email, 'pw')

    samba.goto_page(conference['registration_page'])
    samba.selenium.assert_text('You are registered and paid everything.')
    samba.selenium.assert_text_not_visible('You have checked in')

    mailoutbox.clear()
    home.factories.Checkin(
        registration=participant.registration_set.first()
    )

    assert len(mailoutbox) == 1
    assert mailoutbox[0].body == '{} has checked in for {}.'.format(
        participant.get_full_name(),
        conf.title
    )

    samba.goto_page(conference['registration_page'])
    samba.selenium.assert_text_not_visible(
        'You are registered and paid everything.'
    )
    samba.selenium.assert_text('You have checked in')
