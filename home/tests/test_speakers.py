
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import home.factories
import home.models
import pytest
import wagtail_factories
import factory


@pytest.mark.django_db
def test_cannot_add_speakerindex_at_index(samba, admin_user):
    samba.login_admin()

    index_factory_kwargs = dict(
        title='My Page',
        body__0__paragraph__dummy=None)

    index_page = home.factories.IndexPageFactory.create(
        **index_factory_kwargs)

    wagtail_factories.SiteFactory(root_page=index_page,
                                  is_default_site=True)

    speaker_idx_page = home.factories.SpeakerIndexFactory.build()
    add_url = home.factories.SpeakerIndexFactory.get_add_url(
        speaker_idx_page, index_page)

    samba.selenium.open(samba.live_server + add_url)

    samba.selenium.assert_text(
        'Sorry, you do not have permission to access this area.')


@pytest.mark.django_db
def test_add_speakerindex(samba, admin_user, conference):
    speaker_idx_page = home.factories.SpeakerIndexFactory.create(
        parent=conference['conference_page'],
        intro__0__paragraph__dummy=None
    )

    samba.selenium.open(samba.live_server + speaker_idx_page.url)

    samba.selenium.assert_text(speaker_idx_page.title)

    samba.assert_streamfield_blocks_present(speaker_idx_page.intro)


@pytest.mark.django_db
def test_add_speaker(samba, admin_user, conference):
    speaker_idx_page = home.factories.SpeakerIndexFactory.create(
        parent=conference['conference_page'],
        intro__0__paragraph__dummy=None
    )

    speaker_page = home.factories.SpeakerFactory.create(
        parent=speaker_idx_page,
        abstract__0__paragraph__dummy=None,
        biography__0__paragraph__dummy=None,
        picture=wagtail_factories.ImageFactory(
            file=factory.django.ImageField(color='red')
        )
    )

    link_text = speaker_page.full_name

    samba.goto_url(speaker_idx_page.url)
    samba.selenium.assert_link_text(link_text)

    speaker_link_url = samba.selenium.get_link_attribute(
        link_text, 'href')

    assert speaker_link_url == speaker_page.url

    samba.goto_url(speaker_page.url)

    samba.selenium.assert_text(speaker_page.full_name)
    samba.assert_streamfield_blocks_present(speaker_page.abstract)
    samba.assert_streamfield_blocks_present(speaker_page.biography)
    affiliation_link = samba.selenium.wait_for_link_text(
        speaker_page.affiliation_name)

    assert (affiliation_link.get_attribute(
        'href') == speaker_page.affiliation_link)

    speaker_hp_link = samba.selenium.wait_for_link_text('Homepage')

    assert (speaker_hp_link.get_attribute(
        'href') == speaker_page.speaker_url)
