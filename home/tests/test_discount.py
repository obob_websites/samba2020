#  samba2020 - Webpage for https://samba.ccns.sbg.ac.at
#  Copyright (C) 2022. Thomas Hartmann <thomas.hartmann@th-ht.de>
#
#  This file is part of samba2020.
#
#  samba2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  samba2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
from decimal import Decimal

import pytest
from django.core.exceptions import ValidationError

import home.factories
import home.models


def test_discount_db(samba, conference, mailoutbox):
    reg = home.factories.Registration(conference=conference['conference_page'])
    reg.ticket.ticket_price = 50
    reg.ticket.save()
    option = reg.ticket_options.first()
    option.option_price = 50
    option.save()

    reg = home.models.Registration.objects.get(id=reg.id)
    assert reg.amount_owed.amount == 100
    assert reg.payment_status == 'pending'

    assert len(mailoutbox) == 1

    reg.discount = 50
    reg.save()
    reg = home.models.Registration.objects.get(id=reg.id)

    assert reg.amount_owed.amount == 50
    assert reg.payment_status == 'pending'
    assert reg.amount_paid.amount == 0
    assert reg.total_amount.amount == 50

    assert len(mailoutbox) == 1

    reg.discount = 100
    reg.save()
    reg = home.models.Registration.objects.get(id=reg.id)

    assert reg.amount_owed.amount == 0
    assert reg.payment_status == 'complete'
    assert reg.amount_paid.amount == 0
    assert reg.total_amount.amount == 0

    assert len(mailoutbox) == 2
    assert mailoutbox[1].subject == 'You have paid!'


def test_do_not_allow_negative(samba, conference):
    reg = home.factories.Registration(conference=conference['conference_page'])

    reg.discount.amount = Decimal(20)
    reg.full_clean()

    with pytest.raises(ValidationError):
        reg.discount.amount = Decimal(-20)
        reg.full_clean()


def test_discount_button(samba, conference):
    reg = home.factories.Registration(conference=conference['conference_page'])
    reg.ticket.ticket_price = 50
    reg.ticket.save()
    option = reg.ticket_options.first()
    option.option_price = 50
    option.save()

    samba.login_with_role('manager', conference)

    samba.goto_url(
        conference['registration_management_page'].url +
        conference['registration_management_page'].reverse_subpage(
            'show_registration_detail', args=(reg.pk,))
    )

    samba.selenium.assert_element_present('#id_set_discount_button')

    home.factories.Payment(registration=reg,
                           amount=100)

    samba.goto_url(
        conference['registration_management_page'].url +
        conference['registration_management_page'].reverse_subpage(
            'show_registration_detail', args=(reg.pk,))
    )

    samba.selenium.assert_element_absent('#id_set_discount_button')

    reg.payment_set.all().delete()

    samba.goto_url(
        conference['registration_management_page'].url +
        conference['registration_management_page'].reverse_subpage(
            'show_registration_detail', args=(reg.pk,))
    )

    samba.selenium.assert_element_present('#id_set_discount_button')

    reg.pay_at_conference = True
    reg.save()

    samba.goto_url(
        conference['registration_management_page'].url +
        conference['registration_management_page'].reverse_subpage(
            'show_registration_detail', args=(reg.pk,))
    )

    samba.selenium.assert_element_present('#id_set_discount_button')


def test_enter_discount(samba, conference):
    reg = home.factories.Registration(conference=conference['conference_page'])
    reg.ticket.ticket_price = 50
    reg.ticket.save()
    option = reg.ticket_options.first()
    option.option_price = 50
    option.save()

    samba.login_with_role('manager', conference)

    samba.goto_url(
        conference['registration_management_page'].url +
        conference['registration_management_page'].reverse_subpage(
            'show_registration_detail', args=(reg.pk,))
    )

    samba.selenium.click('#id_set_discount_button')
    samba.selenium.clear('#id_discount_0')
    samba.selenium.add_text('#id_discount_0', '30')

    samba.selenium.submit('#id_discount_0')

    reg.refresh_from_db()

    assert reg.amount_owed.amount == 70
