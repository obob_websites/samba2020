
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import pytest
import home.factories
import home.models
import wagtailmenus.models
import time


@pytest.mark.django_db
def test_exluded_menus(samba, admin_user, conference):
    from django.conf import settings

    private_page = home.factories.NormalPageFactory.create(
        parent=conference['conference_page'],
        title='Private Page'
    )

    non_private_page = home.factories.NormalPageFactory.create(
        parent=conference['conference_page'],
        title='Public Page'
    )

    private_but_viewable_page = home.factories.NormalPageFactory.create(
        parent=conference['conference_page'],
        title='Semiprivate Page'
    )

    private_page.view_restrictions.create(
        restriction_type='login'
    )

    private_page.show_in_menus = True
    private_page.save()

    non_private_page.show_in_menus = True
    non_private_page.save()

    private_but_viewable_page.view_restrictions.create(
        restriction_type='login'
    )
    private_but_viewable_page.show_in_menus = True
    private_but_viewable_page.save()

    menu_model = getattr(settings, 'WAGTAILMENUS_MAIN_MENU_MODEL',
                         wagtailmenus.models.MainMenu)
    menu = menu_model.get_for_site(conference['site'])

    menu.add_menu_items_for_pages(
        home.models.NormalPage.objects.all())

    private_menu_entry = menu.get_menu_items_manager().filter(
        link_page__title='Private Page')[0]

    private_menu_entry.exclude_from_menu_when_not_authorized = True
    private_menu_entry.save()

    menu.save()

    samba.goto_url('/')
    time.sleep(3)
    samba.goto_url('/')

    samba.selenium.assert_text('Public Page')
    samba.selenium.assert_text('Semiprivate Page')
    samba.selenium.assert_text_not_visible('Private Page')

    samba.login_admin()

    samba.goto_url('/')

    samba.selenium.assert_text('Public Page')
    samba.selenium.assert_text('Private Page')
    samba.selenium.assert_text('Semiprivate Page')
