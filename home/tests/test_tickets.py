#  samba2020 - Webpage for https://samba.ccns.sbg.ac.at
#  Copyright (C) 2023. Thomas Hartmann <thomas.hartmann@th-ht.de>
#
#  This file is part of samba2020.
#
#  samba2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  samba2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
import pytest
from wagtail.rich_text import RichText
from selenium.webdriver.common.by import By

import home.factories
from home.factories import EmailFactory


def test_ticket_and_options_limits(samba, conference, conference_user):
    conf = conference['conference_page']
    conf.conference_number_of_tickets = 10
    conf.save()
    conf.refresh_from_db()

    tickets = list(conf.conference_tickets.all())

    ticket_options = list(conf.conference_options.all())
    ticket_options[0].limit = 2
    ticket_options[1].limit = 3

    [x.save() for x in ticket_options]
    [x.refresh_from_db() for x in ticket_options]

    samba.login(conference_user['user_info'])
    samba.goto_page(conference['form_pages']['register'])

    for ticket in list(conf.conference_tickets.all()) + ticket_options:
        samba.selenium.assert_text(str(ticket))

    tickets[0].available = False
    tickets[0].save()
    [x.refresh_from_db() for x in ticket_options]

    samba.goto_page(conference['form_pages']['register'])

    for ticket in list(conf.conference_tickets.all()) + ticket_options:
        if ticket == tickets[0]:
            samba.selenium.assert_text_not_visible(str(ticket))
        else:
            samba.selenium.assert_text(str(ticket))

    ticket_options[0].available = False
    ticket_options[0].save()

    samba.goto_page(conference['form_pages']['register'])

    for ticket in list(conf.conference_tickets.all()) + ticket_options:
        if ticket in (tickets[0], ticket_options[0]):
            samba.selenium.assert_text_not_visible(str(ticket))
        else:
            samba.selenium.assert_text(str(ticket))

    home.factories.Registration.create_batch(
        size=3,
        conference=conf,
        ticket_options=[ticket_options[1]]
    )

    samba.goto_page(conference['form_pages']['register'])

    for ticket in list(conf.conference_tickets.all()) + ticket_options:
        if ticket in (tickets[0], ticket_options[0], ticket_options[1]):
            samba.selenium.assert_text_not_visible(str(ticket))
        else:
            samba.selenium.assert_text(str(ticket))


@pytest.mark.parametrize('chosen_ticket', [0, 1])
@pytest.mark.parametrize('chosen_ticket_options', [[], [0], [1], [0, 1]])
def test_show_ticket_and_ticket_options(samba, conference, conference_user,
                                        admin_user, mailoutbox,
                                        chosen_ticket, chosen_ticket_options):
    conf = conference['conference_page']
    tickets = list(conf.conference_tickets.all())
    ticket_options = list(conf.conference_options.all())

    assert 'Thanks for registering.' in conf.email_has_registered[0].value['body'].source

    conf.email_has_registered = EmailFactory(
        subject='You have registered!',
        body='Thanks for registering. '
        '<p>You bought these tickets</p>'
        '<p>{{ ticket_bought }}</p>'
        '<p>{{ ticket_options }}</p>'
    )
    conf.save()

    reg_page = conference['registration_page']
    reg_page.registered_payment_pending[0] = ('paragraph', RichText(
        '<p>You bought these tickets</p>'
        '<p>{{ ticket_bought }}</p>'
        '<p>{{ ticket_options }}</p>'
    ))
    reg_page.save()

    samba.login(conference_user['user_info'])

    reg = home.factories.Registration.create_in_browser(
        samba=samba,
        conference=conf,
        user=conference_user['user'],
        ticket=tickets[chosen_ticket],
        ticket_options=[ticket_options[i] for i in chosen_ticket_options]
    )

    samba.goto_page(conference['registration_page'])

    samba.selenium.assert_text('You bought these tickets')
    for idx, ticket in enumerate(tickets):
        if idx == chosen_ticket:
            samba.selenium.assert_text(str(ticket))
            assert str(ticket) in mailoutbox[-1].body
        else:
            samba.selenium.assert_text_not_visible(str(ticket))
            assert str(ticket) not in mailoutbox[-1].body

    for idx, option in enumerate(ticket_options):
        if idx in chosen_ticket_options:
            samba.selenium.assert_text(str(option))
            assert str(option) in mailoutbox[-1].body
        else:
            samba.selenium.assert_text_not_visible(str(option))
            assert str(option) not in mailoutbox[-1].body

    samba.login_with_role('manager', conference, conference_user)

    samba.goto_page(conference['registration_management_page'])
    for idx, ticket in enumerate(tickets):
        if idx == chosen_ticket:
            samba.selenium.assert_text(str(ticket), selector='tbody')
        else:
            samba.selenium.assert_text_not_visible(str(ticket), selector='tbody')

    for idx, option in enumerate(ticket_options):
        if idx in chosen_ticket_options:
            samba.selenium.assert_text(str(option), selector='tbody')
        else:
            samba.selenium.assert_text_not_visible(str(option), selector='tbody')

    samba.selenium.click('View Details', by='link text')

    for idx, ticket in enumerate(tickets):
        if idx == chosen_ticket:
            samba.selenium.assert_text(str(ticket))
        else:
            samba.selenium.assert_text_not_visible(str(ticket))

    for idx, option in enumerate(ticket_options):
        if idx in chosen_ticket_options:
            samba.selenium.assert_text(str(option))
        else:
            samba.selenium.assert_text_not_visible(str(option))

    home.factories.Payment(registration=reg,
                           amount=reg.amount_owed)

    samba.goto_page(conference['checkin_page'])
    samba.selenium.get_element('table').find_elements(By.TAG_NAME, 'button')[0].click()  # noqa

    for idx, ticket in enumerate(tickets):
        if idx == chosen_ticket:
            samba.selenium.assert_text(str(ticket))
        else:
            samba.selenium.assert_text_not_visible(str(ticket))

    for idx, option in enumerate(ticket_options):
        if idx in chosen_ticket_options:
            samba.selenium.assert_text(str(option))
        else:
            samba.selenium.assert_text_not_visible(str(option))
