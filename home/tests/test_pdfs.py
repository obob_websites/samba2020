#  samba2020 - Webpage for https://samba.ccns.sbg.ac.at
#  Copyright (C) 2021. Thomas Hartmann <thomas.hartmann@th-ht.de>
#
#  This file is part of samba2020.
#
#  samba2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  samba2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
from time import sleep

import pytest
import tempfile
from django.urls import reverse
import ftfy
from selenium.webdriver.common.by import By


@pytest.mark.parametrize('user', (None, 'conference_user',
                                  'manager', 'admin'))
@pytest.mark.parametrize('view', ('payment-confirmation-preview',
                                  'participation-confirmation-preview'))
def test_pdf_permissions(samba, conference, conference_user, user,
                         view):
    samba.login_with_role(user, conference, conference_user)

    url = reverse(view, kwargs={'conference_pk': None})
    response = samba.get_response_from_url(url).content

    if user == 'admin':
        assert 'PDF'.encode('utf-8') in response
    else:
        assert ('Login'.encode('utf-8') in response or
                'Forbidden'.encode('utf-8') in response)


def test_download_buttons(samba, conference):
    samba.login_with_role('superuser', conference, None)

    conf = conference['conference_page']
    edit_conf_url = reverse('wagtailadmin_pages:edit',
                            kwargs={'page_id': conf.id})
    samba.goto_url(edit_conf_url + '#tab-pdfs')

    all_spans = samba.selenium.find_elements('span')

    for cur_span in all_spans:
        if 'Payment confirmed' in cur_span.text:
            payment_span = cur_span
        if 'Participation confirmed' in cur_span.text:
            participation_span = cur_span

    payment_span.click()
    samba.selenium.driver.switch_to.active_element.send_keys(
        'This is a payment confirmation!'
    )
    sleep(1)
    samba.selenium.click('.payment-conf-preview')
    sleep(1)

    pdf_url = reverse('payment-confirmation-preview',
                      kwargs={'conference_pk': conf.id})
    with tempfile.NamedTemporaryFile(suffix='.pdf') as pdf_file:
        samba.download_from_url(pdf_url, pdf_file.name)
        pdf_text = ftfy.fix_text(samba.selenium.get_pdf_text(
            pdf_file.name))
        assert 'This is a payment confirmation!' in pdf_text
        assert 'Preview' in pdf_text

        samba.selenium.driver.switch_to.window(
            samba.selenium.driver.window_handles[1]
        )
        samba.goto_url(reverse('validate_pdf'))
        upload_pdf_input = samba.selenium.get_element('pdf_file', By.NAME)
        upload_pdf_input.send_keys(pdf_file.name)
        samba.selenium.click('#button-id-validate')
        samba.selenium.assert_text_not_visible(
            'PDF Validated', '#id_results', timeout=30
        )

    samba.selenium.driver.switch_to.window(
        samba.selenium.driver.window_handles[0]
    )

    participation_span.click()
    samba.selenium.driver.switch_to.active_element.send_keys(
        'This is a participant confirmation!'
    )
    sleep(1)
    samba.selenium.click('.participation-conf-preview')
    sleep(1)

    pdf_url = reverse('participation-confirmation-preview',
                      kwargs={'conference_pk': conf.id})
    with tempfile.NamedTemporaryFile(suffix='.pdf') as pdf_file:
        samba.download_from_url(pdf_url, pdf_file.name)
        pdf_text = ftfy.fix_text(samba.selenium.get_pdf_text(
            pdf_file.name))
        assert 'This is a participant confirmation!' in pdf_text
        assert 'Preview' in pdf_text

        samba.selenium.driver.switch_to.window(
            samba.selenium.driver.window_handles[1]
        )
        samba.goto_url(reverse('validate_pdf'))
        upload_pdf_input = samba.selenium.get_element('pdf_file', By.NAME)
        upload_pdf_input.send_keys(pdf_file.name)
        samba.selenium.click('#button-id-validate')
        samba.selenium.assert_text_not_visible(
            'PDF Validated', '#id_results', timeout=30
        )

    samba.selenium.driver.switch_to.window(
        samba.selenium.driver.window_handles[0]
    )
    samba.selenium.driver.close()
    samba.selenium.driver.switch_to.window(
        samba.selenium.driver.window_handles[1]
    )
    samba.goto_url(edit_conf_url + '#tab-pdfs')
    samba.selenium.assert_text_not_visible(
        'This is a participant confirmation!'
    )
    samba.selenium.assert_text_not_visible(
        'This is a payment confirmation!'
    )
