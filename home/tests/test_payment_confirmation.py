
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import pytest
from django.urls import reverse
import ftfy
import tempfile

from pdfminer.pdfparser import PDFSyntaxError
from endesive import pdf
from selenium.webdriver.common.by import By

import home.factories
import home.models


@pytest.mark.parametrize('user', [None, 'conference', 'manager', 'reviewer',
                                  'extended_reviewer', 'owner'])
@pytest.mark.parametrize('payment_status', ['pending', 'complete'])
def test_payment_confirmation(samba, conference, conference_user, user,
                              payment_status):
    conf = conference['conference_page']

    reg_user = home.factories.UserWithConfirmedEmail.create(
        raw_password='pw'
    )

    registration = home.factories.Registration.create(
        conference=conf,
        payment_status=payment_status,
        user=reg_user
    )

    if user == 'conference':
        samba.login(conference_user['user_info'])
    elif user == 'manager':
        this_user = home.factories.UserWithConfirmedEmail(
            raw_password='pw'
        )

        conference['conference_page'].manager_group.user_set.add(
            this_user)
        samba.login(this_user.email, 'pw')
    elif user == 'reviewer':
        this_user = home.factories.UserWithConfirmedEmail(
            raw_password='pw'
        )

        conference['conference_page'].abstract_reviewer_group.user_set.add(
            this_user)
        samba.login(this_user.email, 'pw')
    elif user == 'extended_reviewer':
        this_user = home.factories.UserWithConfirmedEmail(
            raw_password='pw'
        )

        (conference['conference_page']
         .extended_abstract_reviewer_group.user_set.add(this_user))
        samba.login(this_user.email, 'pw')
    elif user == 'owner':
        samba.login(reg_user.email, 'pw')

    pdf_url = reverse('payment-confirmation', args=[registration.pk])

    with tempfile.NamedTemporaryFile(suffix='.pdf') as pdf_file:
        samba.download_from_url(pdf_url, pdf_file.name)
        if user in ('owner', 'manager') and payment_status == 'complete':
            pdf_text = ftfy.fix_text(samba.selenium.get_pdf_text(
                pdf_file.name))
            assert 'Payment confirmed' in pdf_text
            assert 'Preview' not in pdf_text

            assert_text = '%s %s' % (
                reg_user.first_name,
                reg_user.last_name
            )
            assert assert_text in pdf_text

            with open(pdf_file.name, 'rb') as f:
                (hashok, signatureok, certok) = pdf.verify(f.read())[0]

            assert signatureok
            assert hashok

        else:
            with pytest.raises(PDFSyntaxError):
                samba.selenium.get_pdf_text(pdf_file.name)


def test_validate_payment_confirmation(samba, conference):
    conf = conference['conference_page']

    home.factories.UserWithPayedRegistration.create(
        raw_password='pw',
        parent_conference=conf
    )

    registration = home.models.Registration.objects.first()

    this_user = home.factories.UserWithConfirmedEmail(
        raw_password='pw'
    )

    conference['conference_page'].manager_group.user_set.add(
        this_user)
    samba.login(this_user.email, 'pw')

    pdf_url = reverse('payment-confirmation', args=[registration.pk])

    with tempfile.NamedTemporaryFile(suffix='.pdf') as pdf_file:
        samba.download_from_url(pdf_url, pdf_file.name)
        samba.logout()

        samba.goto_url(reverse('validate_pdf'))
        upload_pdf_input = samba.selenium.get_element('pdf_file', By.NAME)
        upload_pdf_input.send_keys(pdf_file.name)
        samba.selenium.click('#button-id-validate')
        samba.selenium.assert_text('PDF Validated', '#id_results', timeout=30)

        pdf_file.seek(50)
        pdf_file.write(b'0')
        pdf_file.flush()

        samba.goto_url(reverse('validate_pdf'))
        upload_pdf_input = samba.selenium.get_element('pdf_file', By.NAME)
        upload_pdf_input.send_keys(pdf_file.name)
        samba.selenium.click('#button-id-validate')
        samba.selenium.assert_text(
            'The content of this PDF may have been altered!',
            '#id_results', timeout=30)
