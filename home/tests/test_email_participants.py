
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from importlib import import_module
from itertools import cycle

import pytest
from django.contrib.auth import get_user_model

import home.factories
import home.models
from selenium.webdriver.common.by import By


@pytest.mark.parametrize('user', [None, 'conference', 'manager', 'reviewer',
                                  'extended_reviewer'])
def test_email_permissions(samba, conference, conference_user, user, settings):
    conf = conference['conference_page']
    email_page = conference['email_participants_page']
    parent_url = email_page.url
    samba.login_with_role(user, conference, conference_user)

    samba.goto_url(parent_url)

    SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
    cookies = samba.get_cookies()

    conf_users = home.factories.UserWithPayedRegistration.create_batch(
        size=20,
        parent_conference=conf
    )

    sub_views = [
        'choose_recipients',
        'write_email',
        'verify_email',
        'email_result'
    ]

    faked_session = {
        'email_recipients': [u.id for u in conf_users],
        'email_content': {
            'subject': 'Subject',
            'content': 'Content'
        }
    }

    for cur_view in sub_views:
        if user is not None:
            session = SessionStore(cookies['sessionid'])
            for key, val in faked_session.items():
                session[key] = val

            session.save()
        samba.goto_url(parent_url + email_page.reverse_subpage(cur_view))

        if user == 'manager':
            samba.selenium.assert_text_not_visible('Forbidden')
        elif user is None:
            samba.selenium.assert_text(
                'In order to mitigate spam attacks by bots')
        else:
            samba.selenium.assert_text('Forbidden')


class TestEmails(object):
    def assert_names(self, samba, participants):
        table_text = (samba.selenium
                      .get_element('table > tbody')
                      .text.split('\n'))

        all_names = []

        for cur_text in table_text:
            cur_split = cur_text.split(' ')
            all_names.append(
                {
                    'first': cur_split[0],
                    'last': cur_split[1]
                }
            )

        assert len(all_names) == 20
        conference_users_pks = [u.pk for u in participants]
        conference_users_qs = get_user_model().objects.filter(
            pk__in=conference_users_pks)
        for cur_name in all_names:
            assert (
                conference_users_qs.filter(
                    first_name=cur_name['first'],
                    last_name=cur_name['last']).exists())

    def send_and_check_emails(self, samba, mailoutbox, participants):
        mailoutbox.clear()

        samba.selenium.click_link(
            'Click here to write to participants matching the filters')

        if not participants:
            samba.selenium.assert_text('You have not chosen any recipients. '
                                       'Please go back and fix this.')
            return

        samba.selenium.assert_text_not_visible('You have not chosen any '
                                               'recipients. Please go back '
                                               'and fix this.')
        samba.selenium.update_text('#id_subject', 'Subject')
        samba.selenium.update_text('#id_content', 'Content')
        samba.selenium.submit('#id_content')

        samba.selenium.submit('#id_content')

        assert (len(mailoutbox) == len(participants))
        conference_users_pks = [u.pk for u in participants]
        conference_users_qs = get_user_model().objects.filter(
            pk__in=conference_users_pks)

        for cur_mail in mailoutbox:
            assert (
                conference_users_qs.filter(
                    email=cur_mail.to[0]
                ).exists()
            )

        all_emails = [m.to[0] for m in mailoutbox]

        for cur_user in conference_users_qs:
            assert cur_user.email in all_emails

        samba.selenium.assert_text('Emails successfully sent to Participants')

    def test_email_two_conferences(self, samba, conference,
                                   conference_user, mailoutbox):
        manager = samba.login_with_role('manager', conference, conference_user)
        first_conference = conference['conference_page']

        second_conference = home.factories.ConferenceFactory.create(
            title='Waltz 1960',
            parent=conference['index_page']
        )
        management_base_page = home.factories.ManagementBasePage.create(
            parent=second_conference
        )
        email_participants_page = (
            home.factories.EmailParticipantsPage.create(
                parent=management_base_page
            )
        )

        second_conference.manager_group.user_set.add(manager)

        first_conference_participants = home.factories.UserWithPayedRegistration.create_batch(  # noqa
            size=20,
            parent_conference=first_conference
        )

        second_conference_participants = home.factories.UserWithPayedRegistration.create_batch(  # noqa
            size=20,
            parent_conference=second_conference
        )

        # check first conference....
        samba.goto_url(conference['email_participants_page'].url)

        self.assert_names(samba, first_conference_participants)
        self.send_and_check_emails(samba, mailoutbox,
                                   first_conference_participants)

        # check second conference....
        samba.goto_url(email_participants_page.url)

        self.assert_names(samba, second_conference_participants)
        self.send_and_check_emails(samba, mailoutbox,
                                   second_conference_participants)

    def test_pagination_harmful(self, samba, conference,
                                conference_user, mailoutbox):
        samba.login_with_role('manager', conference, conference_user)
        conf = conference['conference_page']
        conf.conference_number_of_tickets = 50
        conf.social_number_of_tickets = 50
        conf.save()

        participants = home.factories.UserWithPayedRegistration.create_batch(
            size=50,
            parent_conference=conf
        )

        samba.goto_url(conference['email_participants_page'].url)
        self.send_and_check_emails(samba, mailoutbox, participants)

    @pytest.mark.parametrize('has_paid', ['All', 'Yes', 'No'])
    @pytest.mark.parametrize('is_registered', ['All', 'Yes', 'No'])
    @pytest.mark.parametrize('has_submitted_abstract', ['All', 'Yes', 'No'])
    def test_filters(self, samba, conference, conference_user, mailoutbox,
                     has_paid, is_registered, has_submitted_abstract):
        samba.login_with_role('manager', conference, conference_user)
        conf = conference['conference_page']
        conf.conference_number_of_tickets = 50
        conf.social_number_of_tickets = 50
        conf.save()

        paid_users = home.factories.UserWithPayedRegistration.create_batch(
            size=10,
            parent_conference=conf
        )

        pay_at_conference_users = home.factories.UserPaysRestAtConferenceRegistration.create_batch(  # noqa
            size=10,
            parent_conference=conf
        )

        unpaid_users = home.factories.UserWithUnpaidRegistration.create_batch(
            size=10,
            parent_conference=conf
        )

        unregistered_users = home.factories.UserWithConfirmedEmail.create_batch(  # noqa
            size=10,
        )

        unregistered_with_abstract_users = unregistered_users = home.factories.UserWithConfirmedEmail.create_batch(  # noqa
            size=10,
        )

        abstract_statuses = cycle(
            (
                'pending',
                'accepted',
                'rejected'
            )
        )

        for cur_user in unregistered_with_abstract_users:
            home.factories.Abstract.create(
                user=cur_user,
                conference=conf,
                review_status=next(abstract_statuses)
            )

        samba.goto_url(conference['email_participants_page'].url)
        samba.selenium.select_option_by_text('#id_has_paid', has_paid)
        samba.selenium.select_option_by_text('#id_is_registered',
                                             is_registered)
        samba.selenium.select_option_by_text('#id_abstract_submitted',
                                             has_submitted_abstract)
        samba.selenium.submit('#id_abstract_submitted')

        email_users = list()
        if (
                has_paid in ('All', 'Yes') and
                is_registered in ('All', 'Yes') and
                has_submitted_abstract in ('All', 'No')
        ):
            email_users.extend(paid_users)
            email_users.extend(pay_at_conference_users)

        if (
                has_paid in ('All', 'No') and
                is_registered in ('All', 'Yes') and
                has_submitted_abstract in ('All', 'No')
        ):
            email_users.extend(unpaid_users)

        if (
                has_paid in ('All', 'No') and
                is_registered in ('All', 'No') and
                has_submitted_abstract in ('All', 'Yes')
        ):
            email_users.extend(unregistered_with_abstract_users)

        self.send_and_check_emails(samba, mailoutbox, email_users)


def test_exposed_content_in_mail(samba, conference, mailoutbox):
    samba.login_with_role('manager', conference)
    conf = conference['conference_page']

    participants = home.factories.UserWithPayedRegistration.create_batch(
        size=5,
        parent_conference=conf
    )

    mailoutbox.clear()

    samba.goto_url(conference['email_participants_page'].url)
    samba.selenium.click_link(
        'Click here to write to participants matching the filters')
    samba.selenium.update_text('#id_subject', 'Subject')
    samba.selenium.update_text('#id_content', 'Hello {{ full_name }}')
    samba.selenium.submit('#id_content')

    content_element = samba.selenium.get_element('#id_content')
    first_participant = participants[0]
    assert (content_element.text == 'Hello %s %s' % (
        first_participant.first_name,
        first_participant.last_name
    ))

    samba.selenium.submit('#id_content')

    assert len(mailoutbox) == 5

    conference_users_pks = [u.pk for u in participants]
    conference_users_qs = get_user_model().objects.filter(
        pk__in=conference_users_pks)

    for cur_mail in mailoutbox:
        cur_user = conference_users_qs.get(email=cur_mail.to[0])
        cur_fullname = '%s %s' % (cur_user.first_name, cur_user.last_name)

        assert cur_fullname in cur_mail.body

    samba.selenium.assert_text('Emails successfully sent to Participants')


def test_write_single_participant(samba, conference, mailoutbox):
    conf = conference['conference_page']

    participants = home.factories.UserWithPayedRegistration.create_batch(
        size=5,
        parent_conference=conf
    )

    mailoutbox.clear()
    samba.login_with_role('manager', conference)
    samba.goto_url(conference['email_participants_page'].url)

    first_participant = participants[0]

    button = samba.selenium.get_element(
        'a[href*="write_email/%d"]' % (first_participant.id, ))

    row_text = button.find_element(By.XPATH, '../..').text

    assert first_participant.first_name in row_text
    assert first_participant.last_name in row_text

    button.click()

    samba.selenium.update_text('#id_subject', 'Subject')
    samba.selenium.update_text('#id_content', 'Hello {{ full_name }}')
    samba.selenium.submit('#id_content')

    content_element = samba.selenium.get_element('#id_content')

    assert (content_element.text == 'Hello %s %s' % (
        first_participant.first_name,
        first_participant.last_name
    ))

    samba.selenium.submit('#id_content')

    assert len(mailoutbox) == 1

    assert first_participant.first_name in mailoutbox[0].body
    assert first_participant.last_name in mailoutbox[0].body
