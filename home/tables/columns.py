
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django_tables2 import Column, BooleanColumn


class ConstantValueColumn(Column):
    def __init__(self, const_value, *args, **kwargs):
        self.const_value = const_value
        if 'accessor' not in kwargs:
            kwargs['accessor'] = 'pk'

        kwargs['orderable'] = False

        super().__init__(*args, **kwargs)

    def render(self, value):
        return self.const_value


class AbstractStatusColumn(Column):
    def __init__(self, *args, **kwargs):
        kwargs['orderable'] = False
        kwargs['linkify'] = self._linkify
        kwargs['default'] = 'not submitted'

        super().__init__(*args, **kwargs)

    def _linkify(self, record, table):
        if record.abstract:
            return (table.abstract_manage_page.url +
                    table.abstract_manage_page.reverse_subpage(
                        'show_abstract_detail', args=(record.abstract.pk, )))

    def render(self, value):
        if value:
            return value.review_status


class PosterPrizeStatusColumn(Column):
    def __init__(self, *args, **kwargs):
        kwargs['orderable'] = False
        kwargs['linkify'] = self._linkify
        kwargs['default'] = 'not submitted'

        super().__init__(*args, **kwargs)

    def _linkify(self, record, table):
        if record.extended_abstract:
            return (table.extended_abstract_manage_page.url +
                    table.extended_abstract_manage_page.reverse_subpage(
                        'show_abstract_detail', args=(
                            record.extended_abstract.pk, )))

    def render(self, value):
        if value:
            return value.review_status


class RegistrationStatusColumn(Column):
    def __init__(self, *args, **kwargs):
        kwargs['orderable'] = False
        kwargs['linkify'] = self._linkify
        kwargs['default'] = 'not registered'

        super().__init__(*args, **kwargs)

    def _linkify(self, record, table):
        if record.registration:
            return (table.registration_manage_page.url +
                    table.registration_manage_page.reverse_subpage(
                        'show_registration_detail', args=(
                            record.registration.pk, )))

    def render(self, value):
        if value:
            return value.payment_status


class TakesPartInSocialColumn(BooleanColumn):
    def _get_bool_value(self, record, value, bound_column):
        return value == 'Yes'
