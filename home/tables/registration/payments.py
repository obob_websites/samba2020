
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import home.models
from django_tables2 import tables, Column

notes_attrs = {
    'class': 'w-25',
}


class PaymentTable(tables.Table):
    class Meta:
        model = home.models.Payment
        fields = ('datetime_added', 'amount', 'fee',
                  'kind_of_payment', 'notes')
        orderable = False
        attrs = {
            'class': 'table-sm'
        }

    notes = Column(attrs={
        'th': notes_attrs,
        'td': notes_attrs
    })
