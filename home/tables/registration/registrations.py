
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.functional import cached_property
from django_tables2 import tables, Column, BooleanColumn
import home.models
from home.tables.columns import (ConstantValueColumn, AbstractStatusColumn,
                                 PosterPrizeStatusColumn)


def get_detail_link(record, table):
    return table.context['page'].reverse_subpage(
        'show_registration_detail', args=[record.pk])


class RegistrationsTable(tables.Table):
    class Meta:
        model = home.models.Registration
        fields = ('last_name', 'first_name', 'degree', 'affiliation',
                  'ticket', 'ticket_options',
                  'payment_status', 'is_checked_in',
                  'payment_deadline',
                  'abstract_status', 'poster_prize_status', 'view_details')

    last_name = Column(accessor='user__last_name')
    first_name = Column(accessor='user__first_name')
    view_details = ConstantValueColumn(const_value='View Details',
                                       linkify=get_detail_link,
                                       attrs={'a': {
                                           'class': 'btn btn-primary'
                                       }})
    abstract_status = AbstractStatusColumn(accessor='abstract')
    poster_prize_status = PosterPrizeStatusColumn(accessor='extended_abstract')
    payment_deadline = Column(accessor='unregister_date',
                              verbose_name='Payment deadline')
    is_checked_in = BooleanColumn(orderable=False)

    @cached_property
    def abstract_manage_page(self):
        return (home.models.AbstractManagementPage.objects
                .descendant_of(self.data.data.first().conference)).first()

    @cached_property
    def extended_abstract_manage_page(self):
        return (home.models.ExtendedAbstractManagementPage.objects
                .descendant_of(self.data.data.first().conference)).first()
