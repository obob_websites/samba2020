
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth import get_user_model
from django_tables2 import tables, BooleanColumn

from home.tables.columns import ConstantValueColumn


def get_email_link(record, table):
    return table.context['page'].reverse_subpage(
        'write_email',
        kwargs={'user_id': record.id}
    )


class EmailRecipientsTable(tables.Table):
    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name', 'has_paid',
                  'is_registered', 'abstract_submitted',
                  'write_email')

    has_paid = BooleanColumn()
    is_registered = BooleanColumn()
    abstract_submitted = BooleanColumn()
    write_email = ConstantValueColumn(const_value='Write Email',
                                      linkify=get_email_link,
                                      attrs={'a': {
                                          'class': 'btn btn-primary'
                                      }})
