
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.functional import cached_property
from django_tables2 import tables, Column
import home.models
from home.tables.columns import (ConstantValueColumn, RegistrationStatusColumn,
                                 PosterPrizeStatusColumn)
from home.tables.registration.base import get_detail_link, get_review_link


class AbstractsManagementTable(tables.Table):
    class Meta:
        model = home.models.Abstract
        fields = ('poster_number', 'first_name', 'last_name', 'title',
                  'registration_status', 'poster_prize_status',
                  'review_status', 'view_details')

    poster_number = Column(accessor='poster_number')
    last_name = Column(accessor='user__last_name')
    first_name = Column(accessor='user__first_name')
    view_details = ConstantValueColumn(const_value='View Details',
                                       linkify=get_detail_link,
                                       attrs={'a': {
                                           'class': 'btn btn-primary'
                                       }})
    review_status = Column(orderable=False)
    poster_prize_status = PosterPrizeStatusColumn(accessor='extended_abstract')
    registration_status = RegistrationStatusColumn(accessor='registration')

    @cached_property
    def registration_manage_page(self):
        return (home.models.RegistrationManagementPage.objects
                .descendant_of(self.data.data.first().conference)).first()

    @cached_property
    def extended_abstract_manage_page(self):
        return (home.models.ExtendedAbstractManagementPage.objects
                .descendant_of(self.data.data.first().conference)).first()


class AbstractsReviewTable(tables.Table):
    class Meta:
        model = home.models.Abstract
        fields = ('first_name', 'last_name', 'title', 'review_status',
                  'review')

    last_name = Column(accessor='user__last_name')
    first_name = Column(accessor='user__first_name')
    review = ConstantValueColumn(const_value='Review',
                                 linkify=get_review_link,
                                 attrs={'a': {
                                     'class': 'btn btn-primary'
                                 }})
