#  Copyright (c) 2016-2021, Thomas Hartmann
#
#  This file is part of the SAMBA2020 Project
#  (https://gitlab.com/obob_websites/samba2020)
#
#  SAMBA2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SAMBA2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SAMBA2020. If not, see <http://www.gnu.org/licenses/>.
from django_tables2 import tables, Column, TemplateColumn
import home.models


class ShowParticipantsToCheckinTable(tables.Table):
    class Meta:
        model = home.models.Registration
        fields = ('last_name', 'first_name',
                  'affiliation', 'checkin')
        order_by = ('last_name', 'first_name')

    last_name = Column(accessor='user__last_name')
    first_name = Column(accessor='user__first_name')
    checkin = TemplateColumn(
        verbose_name='Checkin',
        orderable=False,
        template_name='samba_home/checkin/checkin_button.html'
    )
