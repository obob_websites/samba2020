
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView
from django.utils.safestring import mark_safe
from extra_views import CreateWithInlinesView

from .base import OnPageViewMixin, AddConferenceAndUserMixin
from home.forms.registration import RegistrationForm
from home.forms.abstract import (AbstractForm, AbstractAuthorFormSet,
                                 AbstractAuthorFormSetHelper,
                                 ExtendedAbstractForm)
import home.models


class NewRegistration(LoginRequiredMixin,
                      OnPageViewMixin,
                      AddConferenceAndUserMixin,
                      CreateView):
    form_class = RegistrationForm
    template_name = 'samba_home/register_for_conference.html'

    extra_context = {
        'extra_js': ['home/new_registration.js']
    }

    def dispatch(self, request, *args, **kwargs):
        if not self.conference.can_user_register(self.request.user):
            return self.render_to_response({
                'error_message': 'Conference Registration not possible!'
            })

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_js_first'] = True
        context['submit_text'] = 'Register for %s' % (self.conference.title, )

        return context


class NewAbstract(LoginRequiredMixin,
                  OnPageViewMixin,
                  AddConferenceAndUserMixin,
                  CreateWithInlinesView):

    form_class = AbstractForm
    inlines = [AbstractAuthorFormSet]
    model = home.models.Abstract

    extra_context = {
        'extra_js': ('home/new_abstract.js', )
    }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['author_form_helper'] = AbstractAuthorFormSetHelper()
        context['form_title'] = 'Abstract'
        context['formset_title'] = 'Authors'
        context['formset_extra_html'] = mark_safe(
            '<p>Please enter one author at a time and use the '
            '<i class="fas fa-plus" data-formset-add style="font-size: 18px;">'
            '</i> sign to add more.</p>'
        )

        return context

    def forms_valid(self, form, inlines):
        for inline in inlines:
            inline.instance = form.instance
            for idx_author, cur_form in enumerate(inline.forms):
                cur_form.instance.abstract = form.instance
                cur_form.instance.position = idx_author

        return super().forms_valid(form, inlines)

    def dispatch(self, request, *args, **kwargs):
        if not self.conference.can_submit_abstract(self.request.user):
            return self.render_to_response({
                'error_message': 'Abstract Submission not possible!'
            })

        return super().dispatch(request, *args, **kwargs)


class NewExtendedAbstract(LoginRequiredMixin,
                          OnPageViewMixin,
                          CreateView):

    form_class = ExtendedAbstractForm
    model = home.models.ExtendedAbstract

    extra_context = {
        'extra_js': ('home/new_abstract.js', )
    }

    def get_success_url(self):
        return self.extra_context['page'].get_parent().get_url()

    def get_form_class(self):
        form_class = super().get_form_class()

        class NewFormClass(form_class):
            def save(self, *args, **kwargs):
                self.instance.base_abstract = self.base_abstract
                super().save(*args, **kwargs)

        NewFormClass.conference = self.conference
        NewFormClass.base_abstract = self.base_abstract

        return NewFormClass

    @property
    def conference(self):
        return home.models.Conference.objects.ancestor_of(
            self.extra_context['page']).first()

    @property
    def base_abstract(self):
        return home.models.Abstract.objects.get(
            user=self.request.user,
            conference=self.conference
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['form_js_first'] = True
        return context

    def dispatch(self, request, *args, **kwargs):
        if not self.conference.can_submit_extended_abstract(self.request.user):
            return self.render_to_response({
                'error_message': 'Extended Abstract Submission not possible!'
            })

        return super().dispatch(request, *args, **kwargs)
