
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import home.models


class OnPageViewMixin(object):
    @classmethod
    def get_all_onpage_views(cls):
        for subclass in cls.__subclasses__():
            yield from subclass.get_all_onpage_views()
            yield subclass


class AddConferenceAndUserMixin(object):
    def get_form_class(self):
        form_class = super().get_form_class()

        class NewFormClass(form_class):
            def save(self, *args, **kwargs):
                self.instance.conference = self.conference
                self.instance.user = self.user
                super().save(*args, **kwargs)

        NewFormClass.conference = self.conference
        NewFormClass.user = self.request.user

        return NewFormClass

    def get_success_url(self):
        return self.extra_context['page'].get_parent().get_url()

    @property
    def conference(self):
        return home.models.Conference.objects.ancestor_of(
            self.extra_context['page']).first()
