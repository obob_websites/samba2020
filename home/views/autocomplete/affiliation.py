
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from dal import autocomplete

import home.models


class AffiliationAutocomplete(autocomplete.Select2QuerySetView):
    def create_object(self, text):
        if 'conference_pk' in self.kwargs:
            conference = home.models.Conference.objects.get(
                pk=self.kwargs['conference_pk'])
            return self.get_queryset().get_or_create(
                source=conference.title,
                **{self.create_field: text}
            )[0]

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return home.models.Affiliation.objects.none()

        qs = home.models.Affiliation.objects

        if self.q:
            qs = qs.filter_by_names_and_acronyms(self.q)
        else:
            qs = qs.all()

        return qs

    def get_result_label(self, result):
        return result.string_for_dal

    def post(self, request, *args, **kwargs):
        self.kwargs = kwargs
        self.args = args
        return super().post(request)
