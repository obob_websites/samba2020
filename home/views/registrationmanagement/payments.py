
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.utils.functional import cached_property
from django.views.generic import CreateView
import home.forms.payments
from .base import MustBeManager
import home.models


class NewPayment(MustBeManager, CreateView):
    form_class = home.forms.payments.PaymentForm
    template_name = 'samba_home/add_payment.html'

    @cached_property
    def registration_object(self):
        return home.models.Registration.objects.get(
            pk=self.kwargs['pk_registration'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['registration'] = self.registration_object

        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['registration'] = self.registration_object

        return kwargs

    def get_success_url(self):
        return self.extra_context['page'].url + \
            self.extra_context['page'].reverse_subpage(
                'show_registration_detail',
                args=[self.registration_object.pk])
