
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
from io import BytesIO

import dateutil.parser
from datetime import timedelta
from django.shortcuts import redirect
from django.views.generic import DetailView
from django_tables2 import SingleTableMixin
from django_filters.views import FilterView
from django.contrib import messages
from django_downloadview import VirtualDownloadView, VirtualFile
import django_pandas.io
import pandas as pd
import home.tables.registration
import home.models
from home.views.registrationmanagement.base import (MustBeManager,
                                                    DetailObject,
                                                    DetailObjectsMixin,
                                                    OverviewDetailsMixin)
import home.filters
from home.forms.registration import ExtendDeadlineForm, SetDiscountForm


class Registrations(MustBeManager, SingleTableMixin,
                    OverviewDetailsMixin, FilterView):
    table_class = home.tables.registration.RegistrationsTable
    model = home.models.Registration
    template_name = 'samba_home/registration_list.html'
    table_pagination = {
        "per_page": 10
    }

    filterset_class = home.filters.RegistrationFilter

    def get_queryset(self):
        return super().get_queryset().filter(
            conference=self.extra_context['conference'])


class DownloadExcel(MustBeManager, VirtualDownloadView):
    def get_file(self):
        col_mapping = {
            'id': 'Registration ID',
            'user_id': 'user_id',
            'user__last_name': 'Last Name',
            'user__first_name': 'First Name',
            'user__email': 'Email',
            'ticket': 'Ticket',
            'affiliation': 'Affiliation',
            'affiliation__city': 'Affiliation City',
            'affiliation__country': 'Affiliation Country',
            'affiliation__parent__institution_primary_name': 'Parent Institute',  # noqa
            'payment_status': 'Payment Status',
            'amount_owed': 'Amount to pay at registration',
        }

        abstract_col_mapping = {
            'id': 'Abstract ID',
            'user_id': 'user_id',
            'title': 'Abstract Title',
            'content': 'Abstract Content',
            'poster_number': 'Poster Number'
        }

        valid_registrations = home.models.Registration.objects.filter(
            conference=self.extra_context['conference'],
            payment_status__in=['complete', 'pay_at_conference']
        ).select_properties('payment_status', 'amount_owed')

        df = django_pandas.io.read_frame(
            valid_registrations,
            fieldnames=list(col_mapping.keys())
        )

        df = df.rename(columns=col_mapping)
        df['Ticket Options'] = [x.ticket_options_for_html for x in valid_registrations]  # noqa

        all_abstracts = home.models.Abstract.objects.filter(
            conference=self.extra_context['conference'],
            review_status='accepted',
            user__in=valid_registrations.values_list('user_id', flat=True)
        )

        abstract_df = django_pandas.io.read_frame(
            all_abstracts,
            fieldnames=list(abstract_col_mapping.keys())
        )

        abstract_df = abstract_df.rename(columns=abstract_col_mapping)
        abstract_df['Abstract Content'] = abstract_df['Abstract Content'] \
            .apply(lambda x: x.replace('\r', ''))
        authors = list()
        affiliations = list()
        poster_prize = list()

        for cur_abstract in all_abstracts:
            authors.append(cur_abstract.get_authors_indexed_string())
            affiliations.append(cur_abstract.get_affiliations_indexed_string())
            ex_abstract = cur_abstract.extended_abstract

            if (ex_abstract is not None and
                    ex_abstract.review_status == 'accepted'):
                poster_prize.append('Yes')
            else:
                poster_prize.append('No')

        abstract_df['Abstract Authors'] = authors
        abstract_df['Abstract Affiliations'] = affiliations
        abstract_df['Accepted for Poster Prize'] = poster_prize

        df = pd.merge(df, abstract_df, how='left', on='user_id')

        buf = BytesIO()
        df.to_excel(buf, engine='xlsxwriter', index=False)
        f = VirtualFile(buf, name='samba_data.xls')

        return f


class RegistrationDetail(MustBeManager, DetailObjectsMixin,
                         DetailView):
    model = home.models.Registration

    extra_context = {
        'extra_js': ['home/registration_detail.js']
    }

    def get_fields_and_values(self, instance=None):
        instance = self.get_detail_instance(instance)

        fields_and_values = [
            DetailObject('Degree', instance.degree),
            DetailObject('Full Name', '%s %s' % (instance.user.first_name,
                                                 instance.user.last_name)),
            DetailObject('Email', instance.user.email,
                         template='samba_home/registration_detail/email.html'),
            DetailObject('Affiliation', instance.affiliation),
            DetailObject('Checked in', instance.is_checked_in_str),
            DetailObject('Payment Status',
                         instance.get_payment_status_display()),
            DetailObject('Abstract Status',
                         instance.abstract,
                         template='samba_home/registration_detail/abstract_status.html'),  # noqa
            DetailObject('Extended Abstract Status',
                         instance.extended_abstract,
                         template='samba_home/registration_detail/abstract_status.html'),  # noqa),
            DetailObject('Ticket', instance.ticket),
            DetailObject(
                'Ticket Options',
                instance.ticket_options.all(),
                template='samba_home/registration_detail/ticket_options.html'
            ),
            DetailObject('Total Amount', instance.total_amount),
            DetailObject('Amount Paid', instance.amount_paid),
            DetailObject('Fees Paid', instance.fees_paid),
            DetailObject('Discount', instance.discount),
            DetailObject('Amount Owed', instance.amount_owed),
            DetailObject('Payment deadline', instance.unregister_date),
            DetailObject(
                'Payments',
                home.tables.registration.PaymentTable(
                    instance.payment_set.all()),
                template='samba_home/registration_detail/payments.html')
        ]

        return fields_and_values

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['extend_deadline_form'] = ExtendDeadlineForm(self.object)
        context['set_discount_form'] = SetDiscountForm(self.object)

        return context

    def post(self, request, *args, **kwargs):
        obj = self.get_object()
        try:
            if (
                    request.POST['set_discount'] ==
                    f'set_discount_{obj.user.first_name}_{obj.user.last_name}'
            ):
                obj.discount.amount = float(request.POST['discount_0'])
                obj.save()
        except KeyError:
            pass
        try:
            if (request.POST['pay_at_conference'] ==
                    'pay_at_conference_%s_%s' % (
                        obj.user.first_name, obj.user.last_name)):
                obj.pay_at_conference = True
                obj.save()
        except KeyError:
            pass

        try:
            if (request.POST['extend_deadline'] ==
                    'extend_deadline_%s_%s' % (
                        obj.user.first_name, obj.user.last_name)):
                new_deadline = dateutil.parser.parse(
                    request.POST['extend_deadline_until']).date()
                if new_deadline < obj.unregister_date:
                    messages.add_message(request,
                                         messages.ERROR,
                                         'The new deadline must be later '
                                         'than the old one.')
                else:
                    days_extended = (new_deadline - obj.unregister_date).days
                    if obj.send_first_payment_reminder_date:
                        obj.send_first_payment_reminder_date += timedelta(
                            days_extended
                        )
                    if obj.send_second_payment_reminder_date:
                        obj.send_second_payment_reminder_date += timedelta(
                            days_extended
                        )
                    if obj.unregister_date:
                        obj.unregister_date += timedelta(
                            days_extended
                        )

                    obj.save(update_fields=[
                        'send_first_payment_reminder_date',
                        'send_second_payment_reminder_date',
                        'unregister_date'
                    ])
                    messages.add_message(request,
                                         messages.INFO,
                                         'Deadline adjusted.')
        except KeyError:
            pass

        return redirect(request.path_info)
