
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.views.generic import DetailView, UpdateView
from django_tables2 import SingleTableMixin
from django_filters.views import FilterView
import home.tables.registration
import home.models
import home.forms.abstract
import home.filters
from home.views.registrationmanagement.base import (MustBeManager,
                                                    DetailObject,
                                                    MustBeAbstractReviewer,
                                                    DetailObjectsMixin,
                                                    OverviewDetailsMixin)


class AbstractsBaseView(SingleTableMixin, FilterView):
    model = home.models.Abstract
    filterset_class = home.filters.AbstractFilter
    template_name = 'samba_home/abstract_list.html'

    def get_queryset(self):
        return super().get_queryset().filter(
            conference=self.extra_context['conference'])


class AbstractsManagerListView(MustBeManager, OverviewDetailsMixin,
                               AbstractsBaseView):
    table_class = home.tables.registration.AbstractsManagementTable


class AbstractsReviewListView(MustBeAbstractReviewer, AbstractsBaseView):
    table_class = home.tables.registration.AbstractsReviewTable


class AbstractDetailsFieldsMixin(DetailObjectsMixin):
    def get_fields_and_values(self, instance=None):
        instance = self.get_detail_instance(instance)
        poster_number = instance.poster_number
        if poster_number is None:
            poster_number = '-'
        else:
            poster_number = str(poster_number)

        return [
            DetailObject('Full Name of submitting user', '%s %s' % (
                instance.user.first_name,
                instance.user.last_name)),
            DetailObject('Poster Number', poster_number),
            DetailObject('Title', instance.title),
            DetailObject('Authors',
                         instance.get_authors_indexed_list(),
                         'samba_home/abstract_detail/authors.html'),
            DetailObject('Affiliations',
                         instance.get_affiliations_indexed_list(),
                         'samba_home/abstract_detail/affiliations.html'),
            DetailObject('Content', instance.content,
                         'samba_home/abstract_detail/content.html'),
        ]


class AbstractDetail(MustBeManager, AbstractDetailsFieldsMixin, DetailView):
    model = home.models.Abstract

    def get_fields_and_values(self, instance=None):
        if instance is None:
            instance = self.get_object()

        fields = super().get_fields_and_values(instance)
        fields.extend([
            DetailObject('Registration Status',
                         instance.registration,
                         template='samba_home/abstract_detail/registration_status.html'),  # noqa
            DetailObject('Poster Prize Status',
                         instance.extended_abstract,
                         template='samba_home/abstract_detail/posterprize_status.html'),  # noqa
            DetailObject('Review Status',
                         instance.get_review_status_display()),
            DetailObject('Reviewed by', instance.reviewed_by),
            DetailObject('Review Notes', instance.review_notes)
        ])

        return fields


class ReviewAbstractBaseView(UpdateView):
    template_name = 'samba_home/review_abstract.html'

    def get_success_url(self):
        return self.extra_context['page'].url

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()

        if self.request.method == 'GET':
            kwargs['data'] = {
                'review_status': ''
            }

        return kwargs

    def get_form_class(self):
        form_class = super().get_form_class()
        form_class.request = self.request

        return form_class


class ReviewAbstractView(MustBeAbstractReviewer, AbstractDetailsFieldsMixin,
                         ReviewAbstractBaseView):
    model = home.models.Abstract
    form_class = home.forms.abstract.AbstractReviewForm
