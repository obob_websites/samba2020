
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.mixins import UserPassesTestMixin
from django.template.loader import render_to_string
from django.utils.html import format_html
from django.utils.text import slugify
import home.models


class MustBeManager(UserPassesTestMixin):
    def test_func(self):
        manager_group = self.extra_context['conference'] \
            .specific.manager_group
        return manager_group.user_set.all().filter(
            pk=self.request.user.pk).exists()


class MustBeAbstractReviewer(UserPassesTestMixin):
    def test_func(self):
        reviewer_group = self.extra_context['conference'] \
            .specific.abstract_reviewer_group
        return reviewer_group.user_set.all().filter(
            pk=self.request.user.pk).exists()


class MustBeExtendedAbstractReviewer(UserPassesTestMixin):
    def test_func(self):
        reviewer_group = self.extra_context['conference'] \
            .specific.extended_abstract_reviewer_group
        return reviewer_group.user_set.all().filter(
            pk=self.request.user.pk).exists()


class DetailObjectsMixin(object):
    def get_detail_instance(self, instance=None):
        if instance is None:
            instance = self.get_object()

        return instance

    def get_fields_and_values(self, instance=None):
        raise NotImplementedError

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['fields_and_values'] = self.get_fields_and_values()

        return context


class DetailObject(object):
    def __init__(self, description, value, template=None):
        self.description = description
        self._value = value
        self.template = template

    def render(self, context):
        if self.template is None:
            rendered = self._value
        else:
            context['detail_object'] = self._value
            rendered = render_to_string(self.template, context.flatten())

        return format_html('<div id="{}"> {} </div>',
                           slugify(self.description),
                           rendered)


class OverviewDetailsMixin(DetailObjectsMixin):
    def get_fields_and_values(self, instance=None):
        conference = self.extra_context['conference'].specific
        fields_and_values = [
            DetailObject('Total registrations', '%s/%s' % (
                conference.n_registered_for_conference,
                conference.conference_number_of_tickets)),
            DetailObject('Tickets sold', conference.conference_tickets.all(),
                         template='samba_home/registration_overview/tickets_sold.html'),
            DetailObject('Ticket Options sold', conference.conference_options.all(),
                         template='samba_home/registration_overview/ticket_options_sold.html'),
            DetailObject('Payment complete',
                         conference.n_registered_and_paid_for_conference),
            DetailObject('Checked in', '{}/{}'.format(
                conference.n_checked_in,
                conference.n_registered_and_paid_for_conference
            )),
            DetailObject('Abstracts submitted',
                         home.models.Abstract.objects.filter(
                             conference=conference
                         ).count()),
            DetailObject('Abstracts pending review',
                         home.models.Abstract.objects.filter(
                             conference=conference,
                             review_status='pending'
                         ).count()),
            DetailObject('Extended Abstracts submitted',
                         home.models.ExtendedAbstract.objects.filter(
                             base_abstract__conference=conference
                         ).count())
        ]

        return fields_and_values
