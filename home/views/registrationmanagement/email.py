
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import send_mass_mail
from django.db.models import (OuterRef, Subquery, When, Case, BooleanField,
                              Exists)
from django.template import Template, Context
from django.views.generic import FormView, TemplateView
from django_filters.views import FilterView
from django_tables2 import SingleTableMixin

import home.models
from extra.expose_to_template.mixins.expose_to_template import \
    (ExposeToTemplateViewMixin, ExposeContextItem)
from home.filters.email import EmailRecipientFilter
from home.forms.email_participants import ComposeEmailForm, VerifyEmailForm
from home.models.exposeitems import ExposeFullnameToContext
from home.tables.registration import EmailRecipientsTable
from home.views.registrationmanagement.base import MustBeManager


class ChooseRecipients(MustBeManager, SingleTableMixin, FilterView):
    template_name = 'samba_home/emails_chooserecipients.html'
    table_class = EmailRecipientsTable
    model = get_user_model()
    filterset_class = EmailRecipientFilter

    def get_queryset(self):
        qs = super().get_queryset()
        conference = self.extra_context['conference'].specific

        part_group = conference.participant_group

        qs = qs.filter(groups__name__in=[part_group])

        # try to annotate with registration....
        registrations = home.models.Registration.objects.filter(
            user_id=OuterRef('id'),
            conference=conference
        )

        abstracts = home.models.Abstract.objects.filter(
            user_id=OuterRef('id'),
            conference=conference
        )

        qs = qs.annotate(
            payment_status=Subquery(
                registrations.select_properties(
                    'payment_status').values('payment_status')[:1]
            )
        )

        qs = qs.annotate(
            has_paid=Case(
                When(payment_status__in=['complete', 'pay_at_conference'],
                     then=True),
                default=False,
                output_field=BooleanField()
            )
        )

        qs = qs.annotate(
            is_registered=Exists(registrations)
        )

        qs = qs.annotate(
            abstract_submitted=Exists(abstracts)
        )

        return qs

    def get(self, request, *args, **kwargs):
        rval = super().get(request, *args, **kwargs)

        request.session['email_recipients'] = [u.id for u in self.object_list]

        return rval


class EmailExposedItemsMixin(ExposeToTemplateViewMixin):
    expose_fields = [
        ExposeFullnameToContext(description='Full name of participant'),
        ExposeContextItem(
            tag='conference_title',
            description='Title of the conference',
            getfun_or_field=lambda o, c: o.extra_context['conference'].title
        ),
    ]


class WriteEmail(MustBeManager, EmailExposedItemsMixin, FormView):
    template_name = 'samba_home/emails_write.html'
    form_class = ComposeEmailForm

    def get(self, request, *args, **kwargs):
        request.session['email_content'] = None
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        self.request.session['email_content'] = {
            'subject': form.cleaned_data['subject'],
            'content': form.cleaned_data['content'],
        }

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        all_recipients = get_user_model().objects.filter(
            id__in=self.request.session['email_recipients']
        )

        context['recipients'] = all_recipients

        return context

    def get_success_url(self):
        page = self.extra_context['page']
        return page.url + page.reverse_subpage('verify_email')


class VerifyEmail(MustBeManager, EmailExposedItemsMixin, FormView):
    template_name = 'samba_home/emails_verify.html'
    form_class = VerifyEmailForm

    def get_initial(self):
        content = self.request.session['email_content']
        email_template = Template(content['content'])
        exposed_context = self.get_expose_context(
            {
                'user': self.get_all_recipients()[0]
            }
        )

        return {
            'subject': content['subject'],
            'content': email_template.render(Context(exposed_context))
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        all_recipients = get_user_model().objects.filter(
            id__in=self.request.session['email_recipients']
        )

        context['recipients'] = all_recipients

        return context

    def get_success_url(self):
        page = self.extra_context['page']
        return page.url + page.reverse_subpage('email_result')

    def get_all_recipients(self):
        return get_user_model().objects.filter(
            id__in=self.request.session['email_recipients']
        )

    def form_valid(self, form):
        subject = self.request.session['email_content']['subject']
        content = self.request.session['email_content']['content']
        from_address = settings.DEFAULT_FROM_EMAIL

        content_list = []
        email_template = Template(content)

        for user in self.get_all_recipients():
            exposed_context = self.get_expose_context(
                {
                    'user': user
                }
            )
            this_content = email_template.render(Context(exposed_context))

            content_list.append(
                (subject, this_content, from_address, (user.email, ))
            )

        send_mass_mail(content_list, fail_silently=False)

        del self.request.session['email_recipients']
        del self.request.session['email_content']

        return super().form_valid(form)


class EmailSuccess(MustBeManager, TemplateView):
    template_name = 'samba_home/emails_success.html'
