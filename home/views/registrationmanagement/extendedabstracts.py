
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.views.generic import DetailView
from django_tables2 import SingleTableMixin
from django_filters.views import FilterView
import home.tables.registration
import home.models
import home.forms.abstract
import home.filters
from home.views.registrationmanagement.abstracts import (
    AbstractDetailsFieldsMixin,
    ReviewAbstractBaseView
)
from home.views.registrationmanagement.base import (
    MustBeManager,
    DetailObject,
    MustBeExtendedAbstractReviewer
)


class ExtendedAbstractsBaseView(SingleTableMixin, FilterView):
    model = home.models.ExtendedAbstract
    filterset_class = home.filters.ExtendedAbstractFilter
    template_name = 'samba_home/extendedabstract_list.html'

    def get_queryset(self):
        return super().get_queryset().filter(
            base_abstract__conference=self.extra_context['conference'])


class ExtendedAbstractsManagerListView(MustBeManager,
                                       ExtendedAbstractsBaseView):
    table_class = home.tables.registration.ExtendedAbstractsManagementTable


class ExtendedAbstractsReviewListView(MustBeExtendedAbstractReviewer,
                                      ExtendedAbstractsBaseView):
    table_class = home.tables.registration.ExtendedAbstractsReviewTable


class ExtendedAbstractsDetailsFieldsMixin(AbstractDetailsFieldsMixin):
    def get_fields_and_values(self, instance=None):
        instance = self.get_object()

        fields = super().get_fields_and_values(instance.base_abstract)
        fields[-1] = DetailObject('Content', instance.content,
                                  'samba_home/abstract_detail/content.html')
        fields.extend([
            DetailObject('Figure',
                         instance.figure,
                         template='samba_home/abstract_detail/figure.html'),
            DetailObject('Methods Keywords',
                         instance.study_method,
                         template='samba_home/abstract_detail/'
                                  'many2many.html'),
            DetailObject('Field Keywords',
                         instance.study_field,
                         template='samba_home/abstract_detail/'
                                  'many2many.html')
        ])

        return fields


class ExtendedAbstractsDetail(MustBeManager,
                              ExtendedAbstractsDetailsFieldsMixin,
                              DetailView):
    model = home.models.ExtendedAbstract

    def get_fields_and_values(self, instance=None):
        instance = self.get_object()

        fields = super().get_fields_and_values()
        fields.extend([
            DetailObject('Review Status',
                         instance.get_review_status_display()),
            DetailObject('Review Notes', instance.review_notes)
        ])

        return fields


class ReviewExtendedAbstractView(MustBeExtendedAbstractReviewer,
                                 ExtendedAbstractsDetailsFieldsMixin,
                                 ReviewAbstractBaseView):
    model = home.models.ExtendedAbstract
    form_class = home.forms.abstract.ExtendedAbstractReviewForm
    template_name = 'samba_home/review_extendedabstract.html'
