
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.views.generic import TemplateView
from django.conf import settings
from pathlib import Path
import orjson

import git


class About(TemplateView):
    template_name = 'samba_home/about.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context['git'] = git.Repo(
                Path(settings.PROJECT_DIR).parent
            ).active_branch.commit
        except git.exc.InvalidGitRepositoryError:
            context['git'] = 'unknown'

        with open(settings.PYTHON_PACKAGES_JSON_FILE, 'r') as json_file:
            context['python_packages'] = orjson.loads(json_file.read())

        with open(settings.JS_PACKAGES_JSON_FILE, 'r') as json_file:
            context['js_packages'] = orjson.loads(json_file.read())

        return context
