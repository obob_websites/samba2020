
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.models import AnonymousUser
from private_storage.views import PrivateStorageDetailView
import home.models


class ExtendedAbstractPrivateStorageView(PrivateStorageDetailView):
    model = home.models.ExtendedAbstract
    model_file_field = 'figure'

    def get_queryset(self):
        # Make sure only certain objects can be accessed.
        return super().get_queryset()

    def can_access_file(self, private_file):
        # When the object can be accessed, the file may be downloaded.
        # This overrides PRIVATE_STORAGE_AUTH_FUNCTION
        if private_file.parent_object.user == self.request.user:
            return True

        if isinstance(self.request.user, AnonymousUser):
            return False

        if self.request.user.is_staff or self.request.user.is_superuser:
            return True

        conference = private_file.parent_object.conference

        if (conference.extended_abstract_reviewer_group.user_set.filter(
                pk=self.request.user.pk).exists()):
            return True

        if (conference.manager_group.user_set.filter(
                pk=self.request.user.pk).exists()):
            return True

        return False
