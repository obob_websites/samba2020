#  Copyright (c) 2016-2021, Thomas Hartmann
#
#  This file is part of the SAMBA2020 Project
#  (https://gitlab.com/obob_websites/samba2020)
#
#  SAMBA2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SAMBA2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SAMBA2020. If not, see <http://www.gnu.org/licenses/>.
from django_tables2 import SingleTableMixin
from django_filters.views import FilterView

from home.forms.checkin import CheckinForm, CheckinAndPayForm
from home.views.registrationmanagement.base import MustBeManager
import home.models
from home.tables.checkin import ShowParticipantsToCheckinTable
from home.filters.checkin import ShowParticipantsToCheckinFilter
from django.shortcuts import redirect


class ShowParticipantsToCheckin(MustBeManager, SingleTableMixin,
                                FilterView):
    model = home.models.Registration
    template_name = 'samba_home/checkin/show_participants_to_checkin.html'
    table_class = ShowParticipantsToCheckinTable
    filterset_class = ShowParticipantsToCheckinFilter

    extra_context = {
        'extra_js': ['home/show_participants_to_checkin.js']
    }

    def get_queryset(self):
        qs = super().get_queryset().filter(
            conference=self.extra_context['conference'],
            display_for_checkin=True
        )

        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['checkin_form'] = CheckinForm()
        context['checkin_and_pay_form'] = CheckinAndPayForm()

        return context

    def post(self, request, *args, **kwargs):
        if 'checkin_reg_id' in request.POST:
            checkin = home.models.Checkin(
                registration_id=int(request.POST['checkin_reg_id'])
            )
            checkin.save()
        elif 'checkin_and_pay_reg_id' in request.POST:
            if request.POST['participant_has_paid'] == 'on':
                registration = home.models.Registration.objects.get(
                    id=int(request.POST['checkin_and_pay_reg_id'])
                )
                payment = home.models.Payment(amount=registration.amount_owed,
                                              kind_of_payment='cash',
                                              notes='payed at checkin',
                                              registration=registration)
                payment.save()
                checkin = home.models.Checkin(
                    registration=registration)
                checkin.save()

        return redirect(request.path_info)
