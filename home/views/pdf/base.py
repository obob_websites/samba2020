import datetime

import OpenSSL.SSL
import pem
from OpenSSL.SSL import Context, TLSv1_METHOD
from OpenSSL.crypto import load_privatekey, FILETYPE_PEM, load_certificate
from django.conf import settings
from django.http import QueryDict
from django_weasyprint import WeasyTemplateResponse
from endesive import pdf
from moneyed import Money

import home.factories
import home.models


class SignedPdfTemplateResponse(WeasyTemplateResponse):
    @property
    def rendered_content(self):
        tspurl = "http://public-qlts.certum.pl/qts-17"
        content = super().rendered_content

        with open(settings.KEY_FILE, 'rb') as f:
            key = load_privatekey(FILETYPE_PEM, f.read())

        all_certs = pem.parse_file(settings.CERT_FILE)
        cert = []
        for cur_cert in all_certs:
            tmp_cert = load_certificate(FILETYPE_PEM, str(cur_cert))
            ctx = Context(TLSv1_METHOD)
            ctx.use_privatekey(key)
            ctx.use_certificate(tmp_cert)

            try:
                ctx.check_privatekey()
                cert = tmp_cert
            except OpenSSL.SSL.Error:
                pass

        date = datetime.datetime.utcnow() - datetime.timedelta(hours=12)
        date = date.strftime('%Y%m%d%H%M%S+00\'00\'')

        dct = {
            'sigflags': 3,
            'location': '',
            'signingdate': date,
            'reason': '',
            'contact': settings.DEFAULT_FROM_EMAIL
        }

        signed = pdf.cms.sign(content, dct,
                              key.to_cryptography_key(),
                              cert.to_cryptography(),
                              [], 'sha256',
                              None, tspurl)

        return content + signed


class PreviewMixin(object):
    response_class = WeasyTemplateResponse
    session_key_prefix = 'wagtail-preview-'

    def test_func(self):
        return self.request.user.has_perm('wagtailadmin.access_admin')

    @property
    def session_key(self):
        return self.session_key_prefix + str(
            self.kwargs.get('conference_pk', '')
        )

    @property
    def conference(self):
        if self.kwargs['conference_pk'] is not None:
            conf = home.models.Conference.objects.get(
                id=self.kwargs['conference_pk']
            ).get_latest_revision_as_page()
        else:
            conf = home.factories.ConferenceFactory.build()

        post_data, timestamp = self.request.session.get(self.session_key,
                                                        (None, None))

        if isinstance(post_data, str):
            form = self.get_form(conf, QueryDict(post_data))

            form.is_valid()

            # we ignore any form errors.
            form._errors = {}

            form.save(commit=False)

        return conf

    @property
    def registration_page(self):
        if self.conference.get_parent() is not None:
            return super().registration_page
        else:
            return home.factories.RegisterAndAbstractOverview.build(
                parent=self.conference
            )

    def get_object(self, queryset=None):
        user = home.factories.UserWithConfirmedEmail.build()
        reg = home.factories.Registration.build(
            conference=self.conference,
            user=user
        )

        reg.__dict__['amount_owed'] = Money(0, 'EUR')
        reg.__dict__['amount_paid'] = Money(200, 'EUR')
        reg.__dict__['total_amount'] = Money(200, 'EUR')
        reg.__dict__['fees_paid'] = Money(0, 'EUR')

        return reg

    def get_form(self, page, query_dict):
        form_class = page.get_edit_handler().get_form_class()
        parent_page = getattr(page.get_parent(), 'specific', None)

        if self.session_key not in self.request.session:
            # Session key not in session, returning null form
            return form_class(instance=page, parent_page=parent_page)

        return form_class(query_dict, instance=page, parent_page=parent_page)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['watermark'] = 'Preview'

        return context
