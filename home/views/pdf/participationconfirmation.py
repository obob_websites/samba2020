
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.mixins import UserPassesTestMixin
from django.urls import reverse
from django.views.generic import DetailView
from django_weasyprint import WeasyTemplateResponseMixin, WeasyTemplateResponse

import home.models
from home.views.pdf.base import SignedPdfTemplateResponse, PreviewMixin


class ParticipationConfirmationView(UserPassesTestMixin,
                                    WeasyTemplateResponseMixin,
                                    DetailView):
    model = home.models.Registration
    template_name = 'samba_home/pdf/participation_confirmation.html'
    response_class = SignedPdfTemplateResponse
    pdf_filename = 'participation_confirmation.pdf'

    def test_func(self):
        instance = self.get_object()
        if not instance.is_checked_in:
            return False

        if instance.user == self.request.user:
            return True

        manager_group = instance.conference.manager_group
        return manager_group.user_set.all().filter(
            pk=self.request.user.pk).exists()

    @property
    def conference(self):
        return self.get_object().conference

    @property
    def registration_page(self):
        return home.models.RegisterAndAbstractOverview.objects.descendant_of(
            self.conference
        ).first()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page'] = self.registration_page
        context['verify_pdf_url'] = self.request.build_absolute_uri(
            reverse('validate_pdf'))

        return context


class ParticipationConfirmationPreviewView(PreviewMixin,
                                           ParticipationConfirmationView):
    response_class = WeasyTemplateResponse
