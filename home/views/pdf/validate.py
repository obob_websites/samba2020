
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.http import JsonResponse
from django.views.generic import FormView
from django.conf import settings

import home.forms.validate_pdf
from endesive import pdf
import pem
import certifi


class ValidatePdfView(FormView):
    form_class = home.forms.validate_pdf.ValidatePDFForm
    template_name = 'samba_home/validate_pdf.html'

    extra_context = {
        'extra_js': ['home/validate_pdf.js']
    }

    def form_valid(self, form):
        validated = False
        message = 'This PDF could not be validated'

        certs = pem.parse_file(settings.CERT_FILE)
        certifi_certs = pem.parse_file(certifi.where())
        certs = [str(c) for c in certs]
        certifi_certs = [str(c) for c in certifi_certs]

        certs = certs + certifi_certs

        form.files['pdf_file'].file.seek(0)
        data = form.files['pdf_file'].file.read()
        try:
            (hashok, signatureok, certok) = pdf.verify(data, certs)[0]

            if hashok and signatureok and certok:
                message = 'PDF Validated'
                validated = True
            elif hashok and signatureok:
                message = 'The content of the PDF was not altered. But it ' \
                          'was not created at this website.'
            else:
                message = 'The content of this PDF may have been altered!'
        except AssertionError:
            pass

        return JsonResponse({
            'message': message,
            'validated': validated
        })
