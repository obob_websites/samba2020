#  Copyright (c) 2016-2021, Thomas Hartmann
#
#  This file is part of the SAMBA2020 Project
#  (https://gitlab.com/obob_websites/samba2020)
#
#  SAMBA2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SAMBA2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SAMBA2020. If not, see <http://www.gnu.org/licenses/>.
from django_filters.views import FilterView
import home.models
import home.filters


class AbstractPageView(FilterView):
    model = home.models.Abstract
    paginate_by = 10
    template_name = 'samba_home/abstracts_page.html'
    filterset_class = home.filters.abstract.AbstractPageFilter

    def get_queryset(self):
        return home.models.Abstract.objects.filter(
            conference=self.conference,
            review_status='accepted',
            poster_number__isnull=False
        ).order_by('poster_number')
