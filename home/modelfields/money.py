
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from djmoney.models.fields import MoneyField
from moneyed import Money


class DefaultMoneyField(MoneyField):
    def __init__(self, *args, **kwargs):
        default_kwargs = {
            'max_digits': 10,
            'decimal_places': 2,
            'default_currency': 'EUR',
            'default': 0
        }

        for key in kwargs.keys():
            if key in default_kwargs:
                del default_kwargs[key]

        super().__init__(*args,
                         **default_kwargs,
                         **kwargs)

    def from_db_value(self, value, expression, connection):
        return Money(value, 'EUR')
