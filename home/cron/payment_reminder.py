#  Copyright (c) 2016-2020, Thomas Hartmann
#
#  This file is part of the SAMBA2020 Project
#  (https://gitlab.com/obob_websites/samba2020)
#
#  SAMBA2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SAMBA2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SAMBA2020. If not, see <http://www.gnu.org/licenses/>.

from home.receivers.registration import send_email
from home.models.registration import Registration
import datetime


def process_payment_reminders():
    today = datetime.date.today()

    # process first warning
    first_warning_registrations = Registration.objects.filter(
        send_first_payment_reminder_date__lte=today
    )

    for cur_registration in first_warning_registrations:
        send_email('email_first_payment_warning', cur_registration)
        cur_registration.send_first_payment_reminder_date = None
        cur_registration.save()

    # process second warning
    second_warning_registrations = Registration.objects.filter(
        send_second_payment_reminder_date__lte=today
    )

    for cur_registration in second_warning_registrations:
        send_email('email_second_payment_warning', cur_registration)
        cur_registration.send_second_payment_reminder_date = None
        cur_registration.save()

    # process unregister
    unregister_registrations = Registration.objects.filter(
        unregister_date__lte=today
    )

    for cur_registration in unregister_registrations:
        send_email('email_unregister_no_payment', cur_registration)
        cur_registration.delete()
