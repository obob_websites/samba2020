#  Copyright (c) 2016-2021, Thomas Hartmann
#
#  This file is part of the SAMBA2020 Project
#  (https://gitlab.com/obob_websites/samba2020)
#
#  SAMBA2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SAMBA2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SAMBA2020. If not, see <http://www.gnu.org/licenses/>.
from django import forms
from django.template.loader import get_template
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, HTML, Hidden, Button


class CheckinMetaForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.general_information_html = HTML(
            get_template(
                'samba_home/checkin/checkin_information.html'
            ).render()
        )


class CheckinForm(CheckinMetaForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            self.general_information_html,
            HTML('<div class="alert alert-primary">Please confirm that you want to checkin <span class="font-weight-bold checkin_name"></span>!</div>'),  # noqa
            Hidden('checkin_reg_id', 'checkin_dummy'),
            Submit('submit_btn_checkin', 'Checkin'),
            Button('cancel_btn', 'Cancel',
                   data_dismiss='modal', css_class='btn btn-secondary')
        )
        self.helper.form_action = '.'


class CheckinAndPayForm(CheckinMetaForm):
    participant_has_paid = forms.BooleanField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            self.general_information_html,
            HTML('<div class="alert alert-danger"><h3><span class="checkin_name"></span> needs to pay <span class="amount_owed"></span>!</h3></div>'),  # noqa
            HTML('<p>Please collect <span class="font-weight-bold amount_owed"></span> before you continue! </p>'),  # noqa
            'participant_has_paid',
            Hidden('checkin_and_pay_reg_id', 'checkin_dummy'),
            Submit('submit_btn_checkin_and_pay', 'Checkin', disabled=True),
            Button('cancel_btn', 'Cancel',
                   data_dismiss='modal', css_class='btn btn-secondary')
        )
        self.helper.form_action = '.'
