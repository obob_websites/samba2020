
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.core.exceptions import ValidationError
from django.forms import fields
from ..widgets import MaxWordcountWidget
import re


class MaxWordcountField(fields.CharField):
    widget = MaxWordcountWidget

    def __init__(self, max_word_count, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.max_word_count = max_word_count

    @property
    def max_word_count(self):
        return self._max_word_count

    @max_word_count.setter
    def max_word_count(self, max_word_count):
        self._max_word_count = max_word_count
        self.widget.max_words = max_word_count

    def validate(self, value):
        n_words = len(re.split(r'[^\s]+', value)) - 1
        if n_words > self.max_word_count:
            raise ValidationError(
                'You cannot use more than %d words.' % (self.max_word_count, ))
