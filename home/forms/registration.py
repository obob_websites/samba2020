
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from dal import autocomplete
from django.forms import ModelForm, ModelChoiceField, Form
from django import forms
from django.urls import reverse
from django.utils.html import format_html
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Field, HTML, Hidden, Button
from djmoney.forms import MoneyField

import home.models
from home.forms.widgets.moneychoice import (MoneyRadioSelect,
                                            MoneyCheckboxSelectMultiple)
from django.core.exceptions import ValidationError

from home.signals import user_has_registered


class RegistrationForm(ModelForm):
    class Meta:
        model = home.models.Registration
        fields = ['degree',
                  'affiliation',
                  'ticket',
                  'ticket_options',
                  'has_read_refund_policy',
                  'consents_to_data_processing']

    field_order = ['first_name',
                   'last_name',
                   'degree']

    first_name = forms.CharField(max_length=255, disabled=True)
    last_name = forms.CharField(max_length=255, disabled=True)
    affiliation = ModelChoiceField(
        queryset=home.models.Affiliation.objects.all(),
        widget=autocomplete.ModelSelect2(
            attrs={
                'data-theme': 'bootstrap4'
            }
        )
    )

    ticket = forms.ModelChoiceField(
        queryset=None,
        widget=MoneyRadioSelect(price_field='ticket_price'),
        required=True
    )

    ticket_options = forms.ModelMultipleChoiceField(
        queryset=None,
        widget=MoneyCheckboxSelectMultiple(price_field='option_price'),
        required=False
    )

    has_read_refund_policy = forms.BooleanField(
        label='I have read and understood the refund policy',
        required=True)

    consents_to_data_processing = forms.BooleanField(
        label='I have read and understood the privacy policy',
        required=True
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_layout_helper()
        self.initial['first_name'] = self.user.first_name
        self.initial['last_name'] = self.user.last_name
        self.fields['has_read_refund_policy'].required = True
        self.fields['consents_to_data_processing'].required = True
        self.fields['affiliation'].widget.url = reverse(
            'affiliation-complete',
            kwargs={'conference_pk': self.conference.pk}
        )
        self.fields['ticket'].queryset = self.conference.conference_tickets.filter(available=True, ticket_in_time_range=True)  # noqa
        self.fields['ticket_options'].queryset = self.conference.conference_options.filter(ticket_options_available__gt=0, available=True)  # noqa

        privacy_q = (self.conference.get_descendants()
                     .type(home.models.PrivacyPolicyPage).live())

        if privacy_q.exists():
            orig_label = self.fields['consents_to_data_processing'].label
            label = orig_label.replace('privacy policy',
                                       format_html('<a href="{}" target="_blank">privacy policy</a>',  # noqa
                                                   privacy_q.first().url))
            self.fields['consents_to_data_processing'].label = label

        refund_q = (self.conference.get_descendants()
                    .type(home.models.RefundPolicyPage).live())

        if refund_q.exists():
            orig_label = self.fields['has_read_refund_policy'].label
            label = orig_label.replace('refund policy',
                                       format_html(
                                           '<a href="{}" target="_blank">refund policy</a>',  # noqa
                                           # noqa
                                           refund_q.first().url))
            self.fields['has_read_refund_policy'].label = label

    def add_layout_helper(self):
        self.helper = FormHelper()
        self.helper.include_media = False
        self.helper.layout = Layout(
            Field('first_name'),
            Field('last_name'),
            Field('degree', placeholder='M.Sc Dr. PhD. Prof.'),
            Field('affiliation', data_placeholder='Search for institutes by '
                                                  'name and acronym'),
            Field('ticket'),
            Field('ticket_options'),
            Field('has_read_refund_policy', id='id_has_read_refund_policy'),
            Field('consents_to_data_processing',
                  id='id_consents_to_data_processing'),
            HTML('<p data-social-fee="{{ social_fee.amount }}" '
                 'data-conference-fee="{{ conference_fee.amount }}" '
                 'id="id_total_amount">Total amount: </p>')
        )
        self.helper.render_required_fields = True
        self.helper.add_input(
            Submit('submit_btn', 'Register for %s' % (self.conference.title, ))
        )
        self.helper.form_tag = False

    def clean_ticket_options(self):
        data = self.cleaned_data['ticket_options']
        for option in data:
            if not option.available:
                raise ValidationError('Ticket Option not available')
            if option.ticket_options_available <= 0:
                raise ValidationError('Ticket Option sold out')

        return data

    def clean_ticket(self):
        data = self.cleaned_data['ticket']
        if not self.conference.tickets_left_for_conference:
            raise ValidationError('No more tickets left')

        return data

    def save(self, commit=True):
        instance = super().save(commit=commit)
        user_has_registered.send(sender=self.__class__,
                                 registration=instance)

        return instance


class ExtendDeadlineForm(Form):
    extend_deadline_until = forms.DateField()

    def __init__(self, object, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['extend_deadline_until'].widget.input_type = 'date'
        self.fields['extend_deadline_until'].initial = object.unregister_date
        self.helper = FormHelper(self)
        self.helper.add_input(Hidden('extend_deadline',
                                     'extend_deadline_%s_%s' % (
                                         object.user.first_name,
                                         object.user.last_name
                                     )))
        self.helper.add_input(Submit('submit_btn', 'Submit'))
        self.helper.add_input(Button('cancel_btn', 'Cancel',
                                     css_class='btn btn-secondary',
                                     data_dismiss='modal'))
        self.helper.form_action = '.'


class SetDiscountForm(Form):
    discount = MoneyField()

    def __init__(self, object, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['discount'].initial = object.discount
        self.helper = FormHelper(self)
        self.helper.add_input(Hidden(
            'set_discount',
            f'set_discount_{object.user.first_name}_{object.user.last_name}')
        )
        self.helper.add_input(Submit('submit_btn', 'Submit'))
        self.helper.add_input(Button('cancel_btn', 'Cancel',
                                     css_class='btn btn-secondary',
                                     data_dismiss='modal'))
        self.helper.form_action = '.'
