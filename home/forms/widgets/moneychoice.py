#  samba2020 - Webpage for https://samba.ccns.sbg.ac.at
#  Copyright (C) 2023. Thomas Hartmann <thomas.hartmann@th-ht.de>
#
#  This file is part of samba2020.
#
#  samba2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  samba2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
from django.forms.widgets import (RadioSelect,
                                  CheckboxSelectMultiple)


class MoneyChoiceMixin(object):
    def __init__(self, *args, price_field=None, **kwargs):
        if price_field is None:
            raise ValueError('price_field cannot not be None')
        self.price_field = price_field

        super().__init__(*args, **kwargs)

    def create_option(
            self, name, value, label, selected, index, subindex=None,
            attrs=None
    ):
        opt = super().create_option(name, value, label, selected, index,
                                    subindex, attrs)
        opt['attrs']['data-price'] = str(
            getattr(value.instance, self.price_field).amount
        )

        return opt


class MoneyRadioSelect(MoneyChoiceMixin, RadioSelect):
    pass


class MoneyCheckboxSelectMultiple(MoneyChoiceMixin, CheckboxSelectMultiple):
    pass
