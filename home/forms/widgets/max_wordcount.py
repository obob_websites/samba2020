
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.forms import widgets


class MaxWordcountWidget(widgets.Textarea):
    template_name = 'samba_home/forms/widgets/max_wordcount.html'

    def __init__(self, attrs=None):
        if attrs and 'max_words' in attrs:
            self.max_words = attrs.pop('max_words')
        else:
            self.max_words = 10

        super().__init__(attrs)

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)

        context['id_wordcount'] = '%s_wordcount' % (
            context['widget']['attrs']['id'], )

        context['max_words'] = self.max_words

        return context
