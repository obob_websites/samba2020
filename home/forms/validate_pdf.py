
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Button, HTML
from django.forms import Form, FileField, widgets


class ValidatePDFForm(Form):
    pdf_file = FileField(widget=widgets.ClearableFileInput(attrs={
        'accept': 'Application/pdf,.pdf'
    }),
        label='PDF file to validate')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_id = 'id_validate-form'
        self.helper.layout = Layout(
            'pdf_file',
            HTML('<p id="id_results">Please choose a file and '
                 'click "Validate".</p>'),
            Button('validate', 'Validate', css_class='btn btn-primary',
                   disabled=True)
        )
