
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.core.exceptions import ObjectDoesNotExist
from django.forms import ModelForm, ModelMultipleChoiceField, FileField
from django.urls import reverse
from extra_views import InlineFormSetFactory
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field, Submit
from extra_crispy.layout import FontAwesome
from dal import autocomplete

import home.models
from .fields import MaxWordcountField, OrderedModelMultipleChoiceField
import home.signals


class ExtendedAbstractForm(ModelForm):
    class Meta:
        model = home.models.ExtendedAbstract
        fields = ['content',
                  'figure',
                  'study_method',
                  'study_field']

    figure = FileField(label='Figure '
                             '(.png or .jpg format only)')
    content = MaxWordcountField(max_word_count=100)
    study_method = ModelMultipleChoiceField(
        queryset=home.models.StudyMethod.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(
            attrs={
                'data-theme': 'bootstrap4'
            }
        )
    )
    study_field = ModelMultipleChoiceField(
        queryset=home.models.StudyField.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(
            attrs={
                'data-theme': 'bootstrap4'
            }
        )
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['content'].max_word_count = \
            self.conference.extended_abstract_max_number_of_words

        self.fields['study_method'].widget.url = reverse(
            'study_method-complete',
            kwargs={'conference_pk': self.conference.pk}
        )

        self.fields['study_field'].widget.url = reverse(
            'study_field-complete',
            kwargs={'conference_pk': self.conference.pk}
        )

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('content', placeholder='Content of the Extended Abstract'),
            Field('figure'),
            Field('study_method', data_placeholder='Choose at least one '
                                                   'method'),
            Field('study_field', data_placeholder='Choose at least one '
                                                  'field')
        )

        self.helper.add_input(Submit('submit_btn', 'Submit Extended Abstract'))
        self.helper.form_tag = False


class AbstractForm(ModelForm):
    class Meta:
        model = home.models.Abstract
        fields = ['title',
                  'content']

    content = MaxWordcountField(max_word_count=100)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['content'].max_word_count = \
            self.conference.abstract_max_number_of_words
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('title', placeholder='Abstract Title'),
            Field('content', placeholder='Abstract Content')
        )
        self.helper.add_input(Submit('submit_btn', 'Submit Abstract'))
        self.helper.render_hidden_fields = True
        self.helper.form_tag = False


class AbstractAuthorForm(ModelForm):
    class Meta:
        model = home.models.AbstractAuthor
        fields = ['full_name', 'affiliations']

    affiliations = OrderedModelMultipleChoiceField(
        queryset=home.models.Affiliation.objects.all(),
        widget=autocomplete.ModelSelect2Multiple(
            attrs={
                'data-theme': 'bootstrap4'
            }
        )
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['affiliations'].widget.url = reverse(
            'affiliation-complete',
            kwargs={'conference_pk': self.conference.pk}
        )
        if (self.fields['affiliations'].initial is None and
                'initial' in kwargs and
                'affiliations' in kwargs['initial']):
            aff = kwargs['initial']['affiliations']
            self.fields['affiliations'].initial = aff

    def save(self, commit=True):
        return super().save(commit=True)


class AbstractAuthorFormSet(InlineFormSetFactory):
    model = home.models.AbstractAuthor
    factory_kwargs = {'extra': 1, 'max_num': None,
                      'can_order': True}
    form_class = AbstractAuthorForm

    def get_form_class(self):
        form_class = self.form_class
        form_class.conference = self.view.conference

        return form_class

    def get_initial(self):
        user = self.request.user
        initial = [
            {
                'full_name': '%s %s' % (user.first_name, user.last_name)
            }
        ]

        try:
            registration = user.registration_set.get(
                conference=self.view.conference)
            initial[0]['affiliations'] = [registration.affiliation_id]
        except ObjectDoesNotExist:
            pass

        return initial


class AbstractAuthorFormSetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        button_css_class = 'mx-2 mb-2'

        self.layout = Layout(
            Div(
                'DELETE',
                'ORDER',
                css_class='hidden_field'
            ),
            Div(
                Div(
                    Field('full_name', placeholder='John Smith'),
                    Field(
                        'affiliations',
                        data_placeholder='Search for institutes by '
                                         'name and acronym'
                    ),
                    FontAwesome('fa-arrow-up', font_size=24,
                                data_formset_move_up_button='',
                                css_class=button_css_class),
                    FontAwesome('fa-arrow-down', font_size=24,
                                data_formset_move_down_button='',
                                css_class=button_css_class),
                    FontAwesome('fa-trash', font_size=24,
                                data_formset_delete_button='',
                                css_class=button_css_class),
                    css_class='card-body'
                ),
                css_class='card mb-2'
            )
        )
        self.form_tag = False
        self.disable_csrf = True
        self.include_media = False


class AbstractReviewForm(ModelForm):
    class Meta:
        model = home.models.Abstract
        fields = ('review_status', 'review_notes')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['review_status'].choices = (
            ('', '----------'),
            ('accepted', 'Accepted'),
            ('rejected', 'Rejected'),
        )

    def save(self, commit=True):
        self.instance.reviewed_by = self.request.user
        super().save(commit)


class ExtendedAbstractReviewForm(AbstractReviewForm):
    class Meta:
        model = home.models.ExtendedAbstract
        fields = ('review_status', 'review_notes')
