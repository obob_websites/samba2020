
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe
from wagtail.admin.panels import Panel


def widget_with_script(widget, script):
    return mark_safe("{0}<script>{1}</script>".format(widget, script))


class PreviewPDFsEditHandler(Panel):
    class BoundPanel(Panel.BoundPanel):
        js_template = 'samba_home/admin/preview_pdfs_edit_handler.js'
        template_name = 'samba_home/admin/preview_pdfs_edit_handler.html'

        def get_context(self):
            return {
                'conference': self.instance
            }

        def render_html(self, parent_context=None):
            panel = render_to_string(self.template_name, self.get_context())
            return widget_with_script(panel, self.render_js())

        def render_js(self):
            return mark_safe(render_to_string(self.js_template, {
                'self': self,
            }))
