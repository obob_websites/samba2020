
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail.admin.menu import Menu, MenuItem
from ..models import IndexPage, Conference
from django.urls import reverse
from django.contrib.contenttypes.models import ContentType


class ConferenceMenu(Menu):
    def __init__(self, construct_hook_name=None):
        super().__init__(None, construct_hook_name)

    def menu_items_for_request(self, request):
        conference_ctype = ContentType.objects.get_for_model(Conference)
        parent = IndexPage.objects.first()
        menuitem_list = list()

        if parent is not None:
            menuitem_list.append(
                MenuItem('Add Conference',
                         reverse('wagtailadmin_pages:add', args=(
                             conference_ctype.app_label,
                             conference_ctype.model,
                             parent.id,
                         )),
                         classnames='icon icon-plus')
            )

        for cur_conference in Conference.objects.all():
            m_item = MenuItem(cur_conference.title,
                              reverse('wagtailadmin_pages:edit', args=(
                                  cur_conference.pk,
                              )),
                              classnames='icon icon-date')
            menuitem_list.append(m_item)

        return menuitem_list


conference_menu = ConferenceMenu()
