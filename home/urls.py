
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from home.views.autocomplete import (AffiliationAutocomplete,
                                     StudyFieldAutocomplete,
                                     StudyMethodAutocomplete)
from home.views.private_storage import ExtendedAbstractPrivateStorageView
from home.views.pdf import (PaymentConfirmationView, ValidatePdfView,
                            ParticipationConfirmationView,
                            PaymentConfirmationPreviewView)
from home.views.about import About
from django.urls import path
from home.converters import PkOrNoneConverter  # noqa
from home.views.pdf.participationconfirmation import \
    ParticipationConfirmationPreviewView

urlpatterns = [
    path('affiliation-complete/<int:conference_pk>/',
         AffiliationAutocomplete.as_view(
             create_field='institution_primary_name'),
         name='affiliation-complete'),
    path('affiliation-complete/',
         AffiliationAutocomplete.as_view(
             create_field='institution_primary_name'),
         name='affiliation-complete'),
    path('studyfield-complete/<int:conference_pk>/',
         StudyFieldAutocomplete.as_view(create_field='field'),
         name='study_field-complete'),
    path('studymethod-complete/<int:conference_pk>/',
         StudyMethodAutocomplete.as_view(create_field='method'),
         name='study_method-complete'),
    path('extendedabstract-figure/<int:pk>/',
         ExtendedAbstractPrivateStorageView.as_view(),
         name='extendedabstract-figure'),
    path('payment-confirmation/<int:pk>/',
         PaymentConfirmationView.as_view(),
         name='payment-confirmation'),
    path('payment-confirmation-preview/<pk_or_none:conference_pk>/',
         PaymentConfirmationPreviewView.as_view(),
         name='payment-confirmation-preview'),
    path('participation-confirmation/<int:pk>/',
         ParticipationConfirmationView.as_view(),
         name='participation-confirmation'),
    path('participation-confirmation-preview/<pk_or_none:conference_pk>/',
         ParticipationConfirmationPreviewView.as_view(),
         name='participation-confirmation-preview'),
    path('about/', About.as_view(), name='about'),
    path('validate_pdf/', ValidatePdfView.as_view(), name='validate_pdf')
]
