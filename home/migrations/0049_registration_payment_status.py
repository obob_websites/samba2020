
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

# Generated by Django 2.2.9 on 2020-01-14 09:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('samba_home', '0048_auto_20200114_0945'),
    ]

    operations = [
        migrations.AddField(
            model_name='registration',
            name='payment_status',
            field=models.CharField(choices=[('pending', 'Payment pending'), ('complete', 'Payment complete'), ('pay_at_conference', 'Pay outstanding balance at conference')], default='pending', max_length=255),
        ),
    ]
