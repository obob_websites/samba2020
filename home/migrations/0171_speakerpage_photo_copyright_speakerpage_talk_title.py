# Generated by Django 4.1.6 on 2023-03-03 08:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("samba_home", "0170_update_affiliations"),
    ]

    operations = [
        migrations.AddField(
            model_name="speakerpage",
            name="photo_copyright",
            field=models.CharField(blank="True", max_length=255),
        ),
        migrations.AddField(
            model_name="speakerpage",
            name="talk_title",
            field=models.CharField(
                default="Title", max_length=255, verbose_name="Title"
            ),
            preserve_default=False,
        ),
    ]
