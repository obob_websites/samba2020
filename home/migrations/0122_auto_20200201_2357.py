
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

# Generated by Django 2.2.9 on 2020-02-01 23:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('samba_home', '0121_remove_abstractauthor_affiliations'),
    ]

    operations = [
        migrations.CreateModel(
            name='AuthorToAffiliation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('affiliation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='samba_home.Affiliation')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='samba_home.AbstractAuthor')),
            ],
        ),
        migrations.AddField(
            model_name='abstractauthor',
            name='affiliations',
            field=models.ManyToManyField(through='samba_home.AuthorToAffiliation', to='samba_home.Affiliation'),
        ),
    ]
