
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

# Generated by Django 2.2.5 on 2019-09-03 14:03

from django.db import migrations


def setup_initial(apps, schema_editor):
    ContentType = apps.get_model('contenttypes.ContentType')
    Site = apps.get_model('wagtailcore', 'Site')
    Page = apps.get_model('wagtailcore', 'Page')
    IndexPage = apps.get_model('samba_home', 'IndexPage')

    page_content_type = ContentType.objects.get_for_model(Page)
    index_content_type = ContentType.objects.get_for_model(IndexPage)

    Site.objects.all().delete()
    Page.objects.exclude(slug='root').delete()

    # Create homepage
    index_page = IndexPage.objects.create(
        title="Welcome to SAMBA!",
        slug='index',
        content_type=index_content_type,
        path='00010001',
        depth=2,
        numchild=0,
        url_path='index/',
    )

    Site.objects.create(
        hostname='localhost',
        root_page_id=index_page.id,
        is_default_site=True
    )


class Migration(migrations.Migration):
    run_before = [
        ('wagtailcore', '0053_locale_model'),  # added for Wagtail 2.11 compatibility
    ]

    dependencies = [
        ('samba_home', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(setup_initial),
    ]
