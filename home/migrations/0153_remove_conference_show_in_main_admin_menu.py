# Generated by Django 3.2.12 on 2022-02-08 09:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('samba_home', '0152_auto_20210827_1300'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='conference',
            name='show_in_main_admin_menu',
        ),
    ]
