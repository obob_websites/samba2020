# Generated by Django 3.2.6 on 2021-08-23 13:08

from django.db import migrations, models
import django.db.models.deletion
import home.models.checkin


class Migration(migrations.Migration):

    dependencies = [
        ('samba_home', '0147_auto_20210823_1139'),
    ]

    operations = [
        migrations.AlterField(
            model_name='checkin',
            name='registration',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='samba_home.registration', validators=[home.models.checkin.can_checkin]),
        ),
    ]
