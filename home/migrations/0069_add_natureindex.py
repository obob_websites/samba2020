
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

# Generated by Django 2.2.9 on 2020-01-17 22:50

from django.db import migrations
import sys
import home.misc.natureindex


def add_natureindex_data(apps, schema_editor):
    is_testing = 'pytest' in sys.modules
    Affiliation = apps.get_model('samba_home', 'Affiliation')
    home.misc.natureindex.add_natureindex_data(Affiliation, not is_testing)


class Migration(migrations.Migration):

    dependencies = [
        ('samba_home', '0068_affiliation_source'),
    ]

    operations = [
        migrations.RunPython(add_natureindex_data),
    ]
