
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import wagtail_factories

from extra.factories.wagtail import PageFactory, SponsorLevelBlockFactory
from home import models
from home.factories import get_default_streamfield_factory
from factory import SubFactory


class SponsorListFactory(PageFactory):
    class Meta:
        model = models.SponsorsListPage

    intro = get_default_streamfield_factory()
    sponsor_list = wagtail_factories.StreamFieldFactory(
        {
            'sponsor_level': SubFactory(SponsorLevelBlockFactory)
        }
    )
