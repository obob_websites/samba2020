#  Copyright (c) 2016-2021, Thomas Hartmann
#
#  This file is part of the SAMBA2020 Project
#  (https://gitlab.com/obob_websites/samba2020)
#
#  SAMBA2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SAMBA2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SAMBA2020. If not, see <http://www.gnu.org/licenses/>.
from factory.django import DjangoModelFactory
from factory import SubFactory
import home.models
from home.factories import Registration
from extra.factories.wagtail import PageFactory


class Checkin(DjangoModelFactory):
    class Meta:
        model = home.models.Checkin

    registration = SubFactory(Registration)


class CheckinPage(PageFactory):
    class Meta:
        model = home.models.CheckinPage

    title = 'Checkin'
