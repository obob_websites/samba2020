
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import home.models
from extra.factories.wagtail import PageFactory


class RegistrationManagementPage(PageFactory):
    class Meta:
        model = home.models.RegistrationManagementPage

    title = 'Manage Registrations'


class AbstractManagementPage(PageFactory):
    class Meta:
        model = home.models.AbstractManagementPage

    title = 'Manage Abstracts'


class ExtendedAbstractManagementPage(PageFactory):
    class Meta:
        model = home.models.ExtendedAbstractManagementPage

    title = 'Manage Extended Abstracts'


class ManagementBasePage(PageFactory):
    class Meta:
        model = home.models.ManagementParentPage

    title = 'Management Parent Page'
    show_in_menus = True


class EmailParticipantsPage(PageFactory):
    class Meta:
        model = home.models.EmailParticipantsPage

    title = 'Email Participants'
