#  samba2020 - Webpage for https://samba.ccns.sbg.ac.at
#  Copyright (C) 2023. Thomas Hartmann <thomas.hartmann@th-ht.de>
#
#  This file is part of samba2020.
#
#  samba2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  samba2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from factory.django import DjangoModelFactory
from factory import Faker
from home import models


class ConferenceTicketFactory(DjangoModelFactory):
    class Meta:
        model = models.ConferenceTicket

    name = Faker('word')
    ticket_price = Faker('random_int', min=20, max=200)
    available = True


class ConferenceTicketOptionFactory(DjangoModelFactory):
    class Meta:
        model = models.ConferenceTicketOption

    name = Faker('word')
    option_price = Faker('random_int', min=20, max=200)
    limit = Faker('random_int', min=30, max=100)
    available = True
