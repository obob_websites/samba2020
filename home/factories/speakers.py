
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import factory
import wagtail_factories
from factory import faker

from extra.factories.wagtail import PageFactory
from home import models
from home.factories import get_default_streamfield_factory
from wagtail_selenium.mixins import WagtailCreateInSeleniumMixin


class SpeakerIndexFactory(WagtailCreateInSeleniumMixin, PageFactory):
    class Meta:
        model = models.SpeakersIndexPage

    title = 'Our Speakers'
    intro = get_default_streamfield_factory()


class SpeakerFactory(WagtailCreateInSeleniumMixin, PageFactory):
    class Meta:
        model = models.SpeakerPage

    full_name = faker.Faker('name')
    talk_title = faker.Faker('sentence')
    affiliation_name = faker.Faker('company')
    affiliation_link = faker.Faker('url')
    speaker_url = faker.Faker('url')
    biography = get_default_streamfield_factory()
    abstract = get_default_streamfield_factory()
    picture = factory.SubFactory(
        wagtail_factories.ImageChooserBlockFactory
    )
