
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import time

from django.forms import model_to_dict
from factory import SubFactory, Faker, post_generation, lazy_attribute
from factory.django import DjangoModelFactory, mute_signals

from extra.factories.wagtail import PageFactory
from wagtail_factories import StructBlockFactory, StreamFieldFactory
from wagtail_factories.blocks import ChooserBlockFactory, CharBlockFactory

from wagtail.core import blocks
import home.models
from home.factories import get_default_streamfield_factory
from extra.factories.wagtail import ParagraphFactory
from wagtail_selenium.mixins import CreateInSeleniumMixin
import home.forms.registration
import home.forms.abstract
from .auth import UserWithConfirmedEmail
import random

from ..signals import user_has_registered
import home.receivers.registration


class PageChooserBlockFactory(ChooserBlockFactory):
    class Meta:
        model = blocks.PageChooserBlock

    value = SubFactory(PageFactory)


class LinkToFormBlockFactory(StructBlockFactory):
    class Meta:
        model = home.models.registration.blocks.LinkToFormBlock

    form_page = PageChooserBlockFactory
    button_caption = CharBlockFactory


def get_registration_bodyblock_factory():
    return StreamFieldFactory({
        'paragraph': SubFactory(ParagraphFactory),
        'link_to_form': SubFactory(LinkToFormBlockFactory)
    })


class RegisterAndAbstractOverview(PageFactory):
    class Meta:
        model = home.models.RegisterAndAbstractOverview

    title = 'Register and Submit Abstracts'
    not_logged_in = get_registration_bodyblock_factory()
    registration_not_yet_started = get_registration_bodyblock_factory()
    registration_open_not_registered = get_registration_bodyblock_factory()
    registration_open_not_logged_in = get_registration_bodyblock_factory()
    registration_full = get_registration_bodyblock_factory()
    registration_deadline_passed = get_registration_bodyblock_factory()
    registered_payment_pending = get_registration_bodyblock_factory()
    registered_payment_complete = get_registration_bodyblock_factory()
    registered_pay_at_conference = get_registration_bodyblock_factory()
    abstract_submission_open_not_logged_in = get_registration_bodyblock_factory()  # noqa
    abstract_submission_open_not_submitted = get_registration_bodyblock_factory()  # noqa
    abstract_submission_deadline_passed = get_registration_bodyblock_factory()
    abstract_submission_limit_reached = get_registration_bodyblock_factory()
    abstract_pending = get_registration_bodyblock_factory()
    abstract_accepted = get_registration_bodyblock_factory()
    abstract_rejected = get_registration_bodyblock_factory()
    abstract_poster_number_display = get_registration_bodyblock_factory()
    extended_abstract_submission_deadline_passed = get_registration_bodyblock_factory()  # noqa
    extended_abstract_submission_requirements_not_fulfilled = get_registration_bodyblock_factory()  # noqa
    extended_abstract_submission_open_not_submitted = get_registration_bodyblock_factory()  # noqa
    extended_abstract_submission_open_not_logged_in = get_registration_bodyblock_factory()  # noqa
    extended_abstract_pending = get_registration_bodyblock_factory()
    extended_abstract_accepted = get_registration_bodyblock_factory()
    extended_abstract_rejected = get_registration_bodyblock_factory()
    registered_checked_in = get_registration_bodyblock_factory()


class RegisterAndAbstractFormPage(PageFactory):
    class Meta:
        model = home.models.RegisterAndAbstractFormPage

    title = 'Registration Form is here'
    intro = get_default_streamfield_factory()
    form = home.models.get_form_choices()[0][0]


class Registration(CreateInSeleniumMixin, DjangoModelFactory):
    class Meta:
        model = home.models.Registration
        do_not_auto_handle = ['ticket', 'ticket_options']

    degree = Faker('prefix')
    user = SubFactory(UserWithConfirmedEmail)
    conference = None
    has_read_refund_policy = True
    consents_to_data_processing = True
    pay_at_conference = False
    discount = 0

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        with mute_signals(user_has_registered):
            obj = super()._create(model_class, *args, **kwargs)

        return obj

    @classmethod
    def _after_postgeneration(cls, obj, create, results=None):
        super()._after_postgeneration(obj, create, results)
        if create:
            user_has_registered.send(sender=home.models.Registration,
                                     registration=obj)

    @lazy_attribute
    def ticket(self):
        if self.conference.conference_tickets.exists():
            return random.choice(self.conference.conference_tickets.all())
        return None

    @post_generation
    def ticket_options(self, created, extracted, **kwargs):
        if created:
            if extracted is None:
                t_o = random.choice(self.conference.conference_options.all())
                self.ticket_options.add(t_o)
            else:
                self.ticket_options.set(extracted)

    @post_generation
    def affiliation(self, create, extracted, **kwargs):
        if not extracted:
            af = home.models.Affiliation.objects.filter(
                institution_primary_name='Newcastle University').first()
            self.affiliation = af

    @post_generation
    def payment_status(self, create, extracted, **kwargs):
        from home.factories import Payment
        if extracted == 'complete':
            Payment.create(
                registration=self,
                amount=self.amount_owed.amount
            )
        if extracted == 'pay_at_conference':
            self.pay_at_conference = True

    @classmethod
    def get_create_url(cls, conference, *args, **kwargs):
        form_page = home.models.RegisterAndAbstractFormPage \
            .objects \
            .descendant_of(conference) \
            .filter(form='home.views.onpage.registration.NewRegistration') \
            .first()
        return form_page.url

    @classmethod
    def get_form(cls, *args, **kwargs):
        return home.forms.registration.RegistrationForm

    @classmethod
    def handle_further_fields(cls, samba, obj, *args, **kwargs):
        samba.selenium.click('#div_id_affiliation > div > span')
        affiliation = home.models.Affiliation.objects.get(
            pk=obj['affiliation']
        )
        samba.selenium.update_text(
            'span.select2-search.select2-search--dropdown > input',
            affiliation.institution_primary_name)
        time.sleep(1)
        samba.selenium.click('#select2-id_affiliation-results > li')

        samba.selenium.js_click_all(
            f'//div[@id="div_id_ticket"]//input[@value="{obj["ticket"]}"]',
            'xpath')

        for ticket_option in obj['ticket_options']:
            samba.selenium.js_click_all(
                f'//div[@id="div_id_ticket_options"]//input[@value="{ticket_option.id}"]'  # noqa
            )

        print('done')

    @classmethod
    def get_template_object(cls, *args, **kwargs):
        obj = cls.create(*args, **kwargs)
        d = model_to_dict(obj)
        obj.delete()
        return d
