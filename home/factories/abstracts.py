
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import time

from django.forms import model_to_dict
from factory import (Faker, post_generation, SubFactory,
                     SelfAttribute, RelatedFactoryList)
from factory.django import ImageField, DjangoModelFactory
from wagtail_factories import PageFactory

import home.forms
import home.models
from home.factories import (UserWithConfirmedEmail,
                            UserWithPayedRegistration,
                            get_default_streamfield_factory)
from wagtail_selenium.mixins import CreateInSeleniumMixin


class AbstractAuthor(DjangoModelFactory):
    class Meta:
        model = home.models.AbstractAuthor

    full_name = Faker('name')
    abstract = None
    position = 0

    @post_generation
    def affiliations(self, create, extracted, **kwargs):
        if not extracted:
            self.affiliations.add(
                home.models.Affiliation.objects.filter(
                    institution_primary_name='Newcastle University').first(),
                through_defaults={'order': 0}
            )


class Abstract(CreateInSeleniumMixin, DjangoModelFactory):
    class Meta:
        model = home.models.Abstract

    user = SubFactory(UserWithConfirmedEmail)
    conference = None
    title = Faker('sentence')
    content = Faker('text')
    review_status = 'pending'

    authors = RelatedFactoryList(AbstractAuthor, 'abstract', size=3)

    @classmethod
    def get_template_object(cls, *args, **kwargs):
        obj = cls.create(*args, **kwargs)
        d = cls.to_dict(obj)
        obj.delete()
        return d

    @classmethod
    def get_create_url(cls, conference, *args, **kwargs):
        form_page = home.models.RegisterAndAbstractFormPage \
            .objects \
            .descendant_of(conference) \
            .filter(form='home.views.onpage.registration.NewAbstract') \
            .first()
        return form_page.url

    @classmethod
    def get_form(cls, *args, **kwargs):
        return home.forms.abstract.AbstractForm

    @classmethod
    def handle_further_fields(cls, samba, obj, *args, **kwargs):
        for idx_author, author in enumerate(obj['abstractauthor']):
            if idx_author > 0:
                samba.selenium.click('#formset > i')
            id_prefix = 'id_abstractauthor_set-%d' % (idx_author, )
            samba.selenium.update_text('#%s-full_name' % (id_prefix, ),
                                       author['full_name'])
            affiliation = home.models.Affiliation.objects.get(
                pk=author['affiliations'][0]
            )
            samba.selenium.update_text(
                '#div_%s-affiliations input' % (id_prefix, ),
                affiliation.institution_primary_name)
            time.sleep(1)
            samba.selenium.click(
                '#select2-%s-affiliations-results > li' % (id_prefix, ))

    @post_generation
    def add_author_positions(self, create, extracted, **kwargs):
        for idx_author, author in enumerate(self.abstractauthor_set.all()):
            author.position = idx_author
            if create:
                author.save()


class ReviewParentPage(PageFactory):
    class Meta:
        model = home.models.ReviewParentPage

    title = 'Review'
    show_in_menus = True


class ReviewAbstractsPage(PageFactory):
    class Meta:
        model = home.models.ReviewAbstractsPage

    title = 'Review Abstracts'
    show_in_menus = True


class ReviewExtendedAbstractsPage(PageFactory):
    class Meta:
        model = home.models.ReviewExtendedAbstractsPage

    title = 'Review Extended Abstracts'
    show_in_menus = True


class ExtendedAbstract(CreateInSeleniumMixin, DjangoModelFactory):
    class Meta:
        model = home.models.ExtendedAbstract

    class Params:
        parent_conference = None
        parent_user = SubFactory(
            UserWithPayedRegistration,
            parent_conference=SelfAttribute('..parent_conference')
        )

    base_abstract = SubFactory(
        Abstract,
        conference=SelfAttribute('..parent_conference'),
        user=SelfAttribute('..parent_user'),
        review_status='accepted'
    )
    content = Faker('text')
    figure = ImageField()

    @post_generation
    def study_method(self, create, extracted, **kwargs):
        if not extracted:
            self.study_method.add(
                home.models.StudyMethod.objects.order_by('?').first())

    @post_generation
    def study_field(self, create, extracted, **kwargs):
        if not extracted:
            self.study_field.add(
                home.models.StudyField.objects.order_by('?').first())

    @classmethod
    def get_template_object(cls, *args, **kwargs):
        obj = cls.create(*args, **kwargs)
        d = model_to_dict(obj)
        obj.delete()
        return d

    @classmethod
    def get_create_url(cls, parent_conference, *args, **kwargs):
        form_page = (
            home.models.RegisterAndAbstractFormPage
                .objects
                .descendant_of(parent_conference)
                .filter(form='home.views.onpage.registration.NewExtendedAbstract')  # noqa
                .first()
        )
        return form_page.url

    @classmethod
    def get_form(cls, *args, **kwargs):
        return home.forms.abstract.ExtendedAbstractForm

    @classmethod
    def handle_further_fields(cls, samba, obj, *args, **kwargs):
        samba.selenium.update_text(
            '#div_id_study_method input', obj['study_method'][0].method)
        time.sleep(1)
        samba.selenium.click('#select2-id_study_method-results > li')

        samba.selenium.update_text(
            '#div_id_study_field input', obj['study_field'][0].field)
        time.sleep(1)
        samba.selenium.click('#select2-id_study_field-results > li')


class AbstractsPage(PageFactory):
    class Meta:
        model = home.models.AbstractsPage

    title = 'Poster Abstracts'
    intro = get_default_streamfield_factory()
