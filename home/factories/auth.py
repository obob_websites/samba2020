
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.db.models.signals import post_save
from factory import post_generation
from factory.django import DjangoModelFactory
import factory
import allauth.account.models


@factory.django.mute_signals(post_save)
class AllAuthEmail(DjangoModelFactory):
    class Meta:
        model = allauth.account.models.EmailAddress

    user = factory.SubFactory('home.factories.auth.UserWithConfirmedEmail')
    email = None
    verified = True
    primary = True


@factory.django.mute_signals(post_save)
class UserWithConfirmedEmail(DjangoModelFactory):
    class Meta:
        model = get_user_model()

    class Params:
        raw_password = factory.Faker('password')

    username = factory.Faker('user_name')
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    password = factory.LazyAttribute(lambda o: make_password(o.raw_password))
    email = factory.Faker('email')

    emailaddress = factory.RelatedFactory(AllAuthEmail,
                                          'user',
                                          email=factory.SelfAttribute(
                                              '..email'))


@factory.django.mute_signals(post_save)
class UserWithPayedRegistration(UserWithConfirmedEmail):
    class Params:
        parent_conference = None

    registration = factory.RelatedFactory(
        'home.factories.registration.Registration',
        'user',
        conference=factory.SelfAttribute(
            '..parent_conference'
        ),
        payment_status='complete'
    )


@factory.django.mute_signals(post_save)
class UserWithUnpaidRegistration(UserWithConfirmedEmail):
    class Params:
        parent_conference = None

    registration = factory.RelatedFactory(
        'home.factories.registration.Registration',
        'user',
        conference=factory.SelfAttribute(
            '..parent_conference'
        ),
        payment_status='pending'
    )


@factory.django.mute_signals(post_save)
class UserPaysRestAtConferenceRegistration(UserWithConfirmedEmail):
    class Params:
        parent_conference = None

    registration = factory.RelatedFactory(
        'home.factories.registration.Registration',
        'user',
        conference=factory.SelfAttribute(
            '..parent_conference'
        ),
        payment_status='pay_at_conference'
    )


@factory.django.mute_signals(post_save)
class CheckedInUser(UserWithPayedRegistration):
    @post_generation
    def checkin(self, create, extracted, **kwargs):
        from home.factories import Checkin
        Checkin.create(registration=self.registration_set.first())
