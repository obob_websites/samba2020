
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail_factories import StructBlockFactory
from wagtail.core.rich_text import RichText
from factory import Faker, Factory

import extra.blocks.blocks
from wagtail.core import blocks


class EmailBlock(StructBlockFactory):
    class Meta:
        model = extra.blocks.blocks.EmailContentBlock

    subject = Faker('sentence', nb_words=3)
    body = Faker('text', max_nb_chars=100)


class EmailFactory(Factory):
    class Meta:
        model = extra.blocks.blocks.EmailBlock

    subject = Faker('sentence', nb_words=3)
    body = Faker('text', max_nb_chars=100)

    @classmethod
    def _generate(cls, strategy, params):
        result = super()._generate(strategy, params)

        for name, cur_dec in cls._meta.declarations.items():
            if name not in params:
                val = cur_dec
                if isinstance(val, Faker):
                    val = val.generate()

                params[name] = val

        if not isinstance(params['body'], RichText):
            params['body'] = RichText(params['body'])

        return blocks.StreamValue(
            result, (
                ('email', EmailBlock.create(**params)), )
        )
