
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import datetime

import factory

from extra.factories.wagtail import PageFactory
from home import models
from wagtail_selenium.mixins import WagtailCreateInSeleniumMixin

from .misc import get_default_streamfield_factory
from .emails import EmailFactory
from .tickets import ConferenceTicketFactory, ConferenceTicketOptionFactory


class ConferenceFactory(WagtailCreateInSeleniumMixin, PageFactory):
    class Meta:
        model = models.Conference

    title = 'ChaCha 2020'
    start_date = factory.Faker('date_object')
    end_date = factory.LazyAttribute(
        lambda o: o.start_date + datetime.timedelta(3))

    body = get_default_streamfield_factory()
    payment_confirmation_body = get_default_streamfield_factory()
    participant_confirmation_body = get_default_streamfield_factory()
    email_has_registered = EmailFactory(
        subject='You have registered!',
        body='Thanks for registering. '
             '{{ full_name }} '
             '{{ conference_title }}'
    )
    email_pay_at_conference = EmailFactory(
        subject='Pay at the conference',
        body='You pay the rest at the conference '
             '{{ full_name }} '
             '{{ conference_title }}'
    )
    email_payment_complete = EmailFactory(
        subject='You have paid!',
        body='Thanks for paying. '
             '{{ full_name }} '
             '{{ conference_title }}'
    )

    email_abstract_submitted = EmailFactory(
        subject='Abstract submitted',
        body='Thanks for submitting your abstract to {{ conference_title }} '
             '{{ full_name }}'
    )

    email_abstract_accepted = EmailFactory(
        subject='Abstract accepted',
        body='Abstract accepted {{ conference_title }} '
             '{{ full_name }}'
    )

    email_abstract_rejected = EmailFactory(
        subject='Abstract rejected',
        body='Abstract rejected {{ conference_title }} '
             '{{ full_name }}'
    )

    email_poster_number_assigned = EmailFactory(
        subject='Poster number assigned',
        body='Poster number {{ poster_number }}'
    )

    email_extended_abstract_submitted = EmailFactory(
        subject='Extended Abstract submitted',
        body='Extended Abstract submitted {{ conference_title }} '
             '{{ full_name }}'
    )

    email_extended_abstract_accepted = EmailFactory(
        subject='Extended Abstract accepted',
        body='Extended Abstract accepted {{ conference_title }} '
             '{{ full_name }}'
    )

    email_extended_abstract_rejected = EmailFactory(
        subject='Extended Abstract rejected',
        body='Extended Abstract rejected {{ conference_title }} '
             '{{ full_name }}'
    )

    email_first_payment_warning = EmailFactory(
        subject='First Payment Warning',
        body='First Payment Warning {{ conference_title }} '
             '{{ full_name }}'
    )

    email_second_payment_warning = EmailFactory(
        subject='Second Payment Warning',
        body='Second Payment Warning {{ conference_title }} '
             '{{ full_name }}'
    )

    email_unregister_no_payment = EmailFactory(
        subject='Unregistered because no payment',
        body='Unregistered because no payment {{ conference_title }} '
             '{{ full_name }}'
    )

    email_checked_in = EmailFactory(
        subject='You have checked in',
        body='{{ full_name }} has checked in for {{ conference_title }}.'
    )

    conference_number_of_tickets = 20
    abstract_max_number_of_submissions = 15
    registration_start_date = factory.LazyFunction(
        lambda: datetime.date.today() - datetime.timedelta(3))
    registration_deadline = factory.LazyAttribute(
        lambda o: o.registration_start_date + datetime.timedelta(3)
    )
    abstract_deadline = factory.LazyAttribute(
        lambda o: o.registration_start_date + datetime.timedelta(5)
    )
    abstract_max_number_of_words = 100
    extended_abstract_deadline = factory.LazyAttribute(
        lambda o: o.registration_start_date + datetime.timedelta(5)
    )
    extended_abstract_max_number_of_words = 200
    payment_first_warning_after_n_days = 0
    payment_second_warning_after_n_days = 0
    payment_unregister_after_n_days = 0

    conference_tickets = factory.RelatedFactoryList(ConferenceTicketFactory,
                                                    'page',
                                                    size=2)
    conference_options = factory.RelatedFactoryList(
        ConferenceTicketOptionFactory,
        'page',
        size=2
    )
