
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import django_filters
from django.contrib.auth import get_user_model
from django.forms import NullBooleanSelect


class CustomBoolSelect(NullBooleanSelect):
    def __init__(self, attrs=None):
        super().__init__(attrs)
        self.choices = (
            ('unknown', 'All'),
            ('true', 'Yes'),
            ('false', 'No')
        )


class CustomBooleanFilter(django_filters.BooleanFilter):
    def __init__(self, *args, **kwargs):
        kwargs['widget'] = CustomBoolSelect()
        super().__init__(*args, **kwargs)


class EmailRecipientFilter(django_filters.FilterSet):
    class Meta:
        model = get_user_model()
        fields = ('has_paid', 'is_registered', 'abstract_submitted', 'name')

    has_paid = CustomBooleanFilter(label='Has paid')
    is_registered = CustomBooleanFilter(label='Is registered')
    abstract_submitted = CustomBooleanFilter(label='Has submitted abstract')
    name = django_filters.CharFilter(label='Name', method='filter_name')

    def filter_name(self, queryset, name, value):
        return (queryset.filter(first_name__icontains=value) |
                queryset.filter(last_name__icontains=value))
