
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import django_filters
import home.models


class RegistrationStatusFilter(django_filters.ChoiceFilter):
    @property
    def field(self):
        field_name = 'payment_status'
        if self.parent.queryset.first() is not None:
            qs = home.models.Registration.objects.filter(
                conference=self.parent.queryset.first().conference)
            if qs.exists():
                qs = qs.select_properties(field_name)\
                    .order_by(field_name)\
                    .values_list(field_name, flat=True)
                qs = qs.distinct()
                self.extra['choices'] = [(o, o) for o in qs]
                self.extra['choices'].append(
                    ('not registered', 'not registered')
                )
        return super().field

    def filter(self, qs, value):
        all_registrations = home.models.Registration.objects.filter(
            conference=self.parent.queryset.first().conference)

        if value == '':
            return qs
        if value == 'not registered':
            valid_registrations = all_registrations
            valid_registrations_pks = list(
                valid_registrations.values_list('pk',
                                                flat=True))
            qs = qs.exclude(user__registration__in=valid_registrations_pks)
        else:
            valid_registrations = all_registrations.filter(
                payment_status=value)
            valid_registrations_pks = list(
                valid_registrations.values_list('pk',
                                                flat=True))
            qs = qs.filter(user__registration__in=valid_registrations_pks)

        return qs


class AbstractFilter(django_filters.FilterSet):
    class Meta:
        model = home.models.Abstract
        fields = ['author_name',
                  'title',
                  'registration_status',
                  'review_status']

    author_name = django_filters.CharFilter(label='Author Name',
                                            method='filter_author')

    title = django_filters.CharFilter('title', 'icontains',
                                      label='Title')
    registration_status = RegistrationStatusFilter(label='Registration Status')

    def filter_author(self, queryset, name, value):
        return (queryset.filter(user__first_name__icontains=value) |
                queryset.filter(user__last_name__icontains=value) |
                queryset.filter(abstractauthor__full_name__icontains=value)
                )


class ExtendedAbstractFilter(django_filters.FilterSet):
    class Meta:
        model = home.models.ExtendedAbstract
        fields = ['author_name',
                  'title',
                  'review_status']

    author_name = django_filters.CharFilter(label='Author Name',
                                            method='filter_author')
    title = django_filters.CharFilter('base_abstract__title', 'icontains',
                                      label='Title')

    def filter_author(self, queryset, name, value):
        return (
                queryset.filter(  # noqa
                    base_abstract__user__first_name__icontains=value) |
                queryset.filter(
                    base_abstract__user__last_name__icontains=value) |
                queryset.filter(
                    base_abstract__abstractauthor__full_name__icontains=value)  # noqa
        )


class AbstractPageFilter(django_filters.FilterSet):
    class Meta:
        model = home.models.Abstract
        fields = ['authors', 'title']

    title = django_filters.CharFilter('title', 'icontains',
                                      label='Title')
    authors = django_filters.CharFilter(label='Author Name',
                                        method='filter_author')

    def filter_author(self, queryset, name, value):
        return (
            queryset.filter(abstractauthor__full_name__icontains=value)
        )
