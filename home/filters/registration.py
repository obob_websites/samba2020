
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import django_filters
import home.models


class TicketOptionsFilter(django_filters.ChoiceFilter):
    @property
    def field(self):
        if self.parent.queryset.first() is not None:
            ticket_options = self.parent.queryset.order_by('ticket_options') \
                .values_list('ticket_options', flat=True) \
                .distinct()
            self.extra['choices'] = [(o, home.models.ConferenceTicketOption.objects.get(pk=o).name) for o in ticket_options if o is not None]  # noqa
        return super().field


class PaymentStatusFilter(django_filters.ChoiceFilter):
    @property
    def field(self):
        if self.parent.queryset.first() is not None:
            payment_status_choices = self.parent.queryset.select_properties(
                'payment_status'
            ).order_by('payment_status')\
                .values_list('payment_status', flat=True)\
                .distinct()
            self.extra['choices'] = [(o, o) for o in payment_status_choices]
        return super().field


class AbstractStatusFilter(django_filters.ChoiceFilter):
    @property
    def field(self):
        if self.parent.queryset.first() is not None:
            field_name = 'review_status'
            qs = home.models.Abstract.objects.filter(
                conference=self.parent.queryset.first().conference)
            qs = qs.order_by(field_name).values_list(field_name,
                                                     flat=True)
            qs = qs.distinct()
            self.extra['choices'] = [(o, o) for o in qs]
            self.extra['choices'].append(('not submitted', 'not submitted'))
        return super().field

    def filter(self, qs, value):
        all_abstracts = home.models.Abstract.objects.filter(
            conference=self.parent.queryset.first().conference)

        if value == '':
            return qs
        if value == 'not submitted':
            valid_abstracts = all_abstracts
            valid_abstracts_pks = list(valid_abstracts.values_list('pk',
                                                                   flat=True))
            qs = qs.exclude(user__abstract__in=valid_abstracts_pks)
        else:
            valid_abstracts = all_abstracts.filter(review_status=value)
            valid_abstracts_pks = list(valid_abstracts.values_list('pk',
                                                                   flat=True))
            qs = qs.filter(user__abstract__in=valid_abstracts_pks)

        return qs


class ExtendedAbstractStatusFilter(django_filters.ChoiceFilter):
    @property
    def field(self):
        if self.parent.queryset.first() is not None:
            field_name = 'review_status'
            qs = home.models.ExtendedAbstract.objects.filter(
                base_abstract__conference=self.parent.queryset.first().conference)  # noqa
            qs = qs.order_by(field_name).values_list(field_name,
                                                     flat=True)
            qs = qs.distinct()
            self.extra['choices'] = [(o, o) for o in qs]
            self.extra['choices'].append(('not submitted', 'not submitted'))
        return super().field

    def filter(self, qs, value):
        all_abstracts = home.models.ExtendedAbstract.objects.filter(
            base_abstract__conference=self.parent.queryset.first().conference)

        if value == '':
            return qs
        if value == 'not submitted':
            valid_abstracts = all_abstracts
            valid_abstracts_pks = list(valid_abstracts.values_list('pk',
                                                                   flat=True))
            qs = qs.exclude(
                user__abstract__extendedabstract__in=valid_abstracts_pks)
        else:
            valid_abstracts = all_abstracts.filter(review_status=value)
            valid_abstracts_pks = list(valid_abstracts.values_list('pk',
                                                                   flat=True))
            qs = qs.filter(
                user__abstract__extendedabstract__in=valid_abstracts_pks)

        return qs


class RegistrationFilter(django_filters.FilterSet):
    class Meta:
        model = home.models.Registration
        fields = ['name', 'abstract_status', 'payment_status',
                  'ticket', 'ticket_options',
                  'poster_prize_status', 'is_checked_in']

    name = django_filters.CharFilter(label='Name', method='filter_name')
    abstract_status = AbstractStatusFilter(label='Abstract Status')
    poster_prize_status = ExtendedAbstractStatusFilter(
        label='Poster Prize Status'
    )
    is_checked_in = django_filters.BooleanFilter(label='Checked in',
                                                 method='filter_checked_in')

    payment_status = PaymentStatusFilter(label='Payment Status')
    ticket_options = TicketOptionsFilter(label='Ticket Options')

    def filter_name(self, queryset, name, value):
        return (queryset.filter(user__first_name__icontains=value) |
                queryset.filter(user__last_name__icontains=value))

    def filter_checked_in(self, queryset, name, value):
        return queryset.filter(checkin__isnull=not value)
