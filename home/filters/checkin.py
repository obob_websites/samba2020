#  Copyright (c) 2016-2021, Thomas Hartmann
#
#  This file is part of the SAMBA2020 Project
#  (https://gitlab.com/obob_websites/samba2020)
#
#  SAMBA2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  SAMBA2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with SAMBA2020. If not, see <http://www.gnu.org/licenses/>.
import django_filters
import home.models


class AffiliationFilter(django_filters.ModelChoiceFilter):
    def get_queryset(self, request):
        affiliation_ids = self.parent.queryset.values_list(
            'affiliation', flat=True
        )
        qs = home.models.Affiliation.objects.filter(
            id__in=affiliation_ids
        ).order_by('institution_primary_name')

        return qs


class ShowParticipantsToCheckinFilter(django_filters.FilterSet):
    class Meta:
        model = home.models.Registration
        fields = ('name', 'affiliation')

    name = django_filters.CharFilter(label='Name', method='filter_name')
    affiliation = AffiliationFilter()

    def filter_name(self, queryset, name, value):
        return (queryset.filter(user__first_name__icontains=value) |
                queryset.filter(user__last_name__icontains=value))
