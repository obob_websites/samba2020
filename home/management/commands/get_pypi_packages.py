#  samba2020 - Webpage for https://samba.ccns.sbg.ac.at
#  Copyright (C) 2021. Thomas Hartmann <thomas.hartmann@th-ht.de>
#
#  This file is part of samba2020.
#
#  samba2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  samba2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
import orjson

import subprocess
from django.core.management.base import BaseCommand
from django.conf import settings
from pathlib import Path
import yaml
import re
from pip._internal.commands.show import search_packages_info


class Command(BaseCommand):
    help = 'Get current list of python packages used'

    def _parse_python_deps(self, deps):
        js_p = {}

        for pkg in deps.keys():
            result = subprocess.run(f'npm view {pkg} homepage',
                                    capture_output=True,
                                    shell=True)
            hp = result.stdout.decode('utf-8').strip()

            js_p[pkg] = hp

        return js_p

    def handle(self, *args, **kwargs):
        package_regexp = r'(?P<package>[a-zA-Z0-9-_.]+)(?:\s*(?:[<>=][^\s]+)?)'
        with open(Path(settings.PROJECT_DIR).parent / 'environment.yml',
                  'r') as r_file:  # noqa
            env = yaml.safe_load(r_file)
            dependencies = env['dependencies']
            all_names = [x for x in dependencies if
                         type(x) == str and x != 'pip']  # noqa
            pip_names_raw = dependencies[-1]['pip']
            pip_names = []
            for i in pip_names_raw:
                if '#egg' in i:
                    pip_names.append(i.split('#egg=')[1])
                else:
                    pip_names.append(i)
            all_names = [re.match(package_regexp, x).group('package') for x in
                         all_names + pip_names]  # noqa
            all_infos = search_packages_info(all_names)
            all_urls = [info.homepage for info in all_infos]
            python_packages = dict(zip(all_names, all_urls))

        with open(settings.PYTHON_PACKAGES_JSON_FILE, 'w') as json_file:
            json_file.write(orjson.dumps(python_packages).decode('utf-8'))
