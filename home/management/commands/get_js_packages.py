#  samba2020 - Webpage for https://samba.ccns.sbg.ac.at
#  Copyright (C) 2021. Thomas Hartmann <thomas.hartmann@th-ht.de>
#
#  This file is part of samba2020.
#
#  samba2020 is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  samba2020 is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
import orjson

import subprocess
from django.core.management.base import BaseCommand
from django.conf import settings
from pathlib import Path


class Command(BaseCommand):
    help = 'Get current list of javascript packages used'

    def _parse_js_deps(self, deps):
        js_p = {}

        for pkg in deps.keys():
            result = subprocess.run(f'npm view {pkg} homepage',
                                    capture_output=True,
                                    shell=True)
            hp = result.stdout.decode('utf-8').strip()

            js_p[pkg] = hp

        return js_p

    def handle(self, *args, **kwargs):
        with open(Path(settings.PROJECT_DIR).parent / 'package.json',
                  'r') as p_file:
            raw_data = orjson.loads(p_file.read())

            js_p = self._parse_js_deps(raw_data['dependencies'])
            js_p.update(self._parse_js_deps(raw_data['devDependencies']))

        with open(settings.JS_PACKAGES_JSON_FILE, 'w') as json_file:
            json_file.write(orjson.dumps(js_p).decode('utf-8'))
