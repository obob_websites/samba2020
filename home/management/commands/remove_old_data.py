
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand
from django.conf import settings
from django.contrib.auth import get_user_model
import home.models


class Command(BaseCommand):
    help = 'Remove old data'

    def handle(self, *args, **options):
        settings.EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
        home.models.Registration.objects.all().delete()
        home.models.Abstract.objects.all().delete()
        get_user_model().objects.filter(is_staff=False).delete()
