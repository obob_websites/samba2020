/*
 * samba2020 - Webpage for https://samba.ccns.sbg.ac.at
 * Copyright (C) 2021. Thomas Hartmann <thomas.hartmann@th-ht.de>
 *
 * This file is part of samba2020.
 *
 * samba2020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * samba2020 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
 */

function update_wordcounter(wordcount_element) {
    const id_target = wordcount_element.dataset.wordcountObjectId;
    const max_words = parseInt(wordcount_element.dataset.wordcountMaxwords);
    const n_words = wordcount_element.value.split(/[^\s]+/).length - 1;
    const text_element = document.getElementById(id_target);
    text_element.innerHTML = 'Word count: ' + n_words + '/' + max_words;
    if(n_words <= max_words) {
        text_element.style.color = 'green';
    }
    else {
        text_element.style.color = 'red';
    }
}

document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll("[data-wordcount-object-id]").forEach((obj) => {
        ['change', 'keyup',  'paste'].forEach((evt) => {
            obj.addEventListener(evt, () => update_wordcounter(obj));
        });
        update_wordcounter(obj);
    });
});