/*
 * samba2020 - Webpage for https://samba.ccns.sbg.ac.at
 * Copyright (C) 2021. Thomas Hartmann <thomas.hartmann@th-ht.de>
 *
 * This file is part of samba2020.
 *
 * samba2020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * samba2020 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'jquery';

window.jQuery = $;
window.$ = $;

document.addEventListener('DOMContentLoaded', () => {
    const ticket_div = document.getElementById('div_id_ticket');
    const options_div = document.getElementById('div_id_ticket_options');
    const total_amount_element = $('#id_total_amount');

    function update_total_amount() {
        let total_amount = 0;
        const selected_ticket = document.querySelector("input[name=ticket]:checked");
        if(selected_ticket !== null) {
            total_amount = parseInt(selected_ticket.dataset.price);
        }
        document.querySelectorAll("input[name=ticket_options]:checked").forEach((e) => {
            total_amount += parseInt(e.dataset.price);
        });

        if(total_amount > 0) {
            total_amount_element.text('Total amount: € ' + total_amount.toFixed(2));
        }
        else {
            total_amount_element.text('Please select your ticket.')
        }
    }

    update_total_amount();

    ticket_div.addEventListener('change', () => update_total_amount());
    options_div.addEventListener('change', () => update_total_amount());

});