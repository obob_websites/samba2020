/*
 * samba2020 - Webpage for https://samba.ccns.sbg.ac.at
 * Copyright (C) 2021. Thomas Hartmann <thomas.hartmann@th-ht.de>
 *
 * This file is part of samba2020.
 *
 * samba2020 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * samba2020 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with samba2020.  If not, see <http://www.gnu.org/licenses/>.
 */

import 'bootstrap';

function add_content(event, input_name) {
    const button = event.relatedTarget;
    const modal = event.target;
    const registration_id = button.dataset.registrationId;
    const full_name = button.dataset.fullName;
    const amount_owed = button.dataset.amountOwed;
    const affiliation = button.dataset.affiliation;
    const poster_title = button.dataset.poster_title;
    const poster_number = button.dataset.poster_number;
    const poster_prize = button.dataset.poster_prize;
    const ticket = button.dataset.ticket;
    const ticket_options = button.dataset.ticket_options;

    modal.querySelectorAll('.checkin_name').forEach((el) => el.innerHTML = full_name);
    modal.querySelector(`input[name="${input_name}"]`).value = registration_id;
    modal.querySelectorAll('.amount_owed').forEach((el) => el.innerHTML = amount_owed);
    modal.querySelectorAll('.takes_part_in_social').forEach((el) => el.innerHTML = takes_part_in_social);
    modal.querySelectorAll('.affiliation').forEach((el) => el.innerHTML = affiliation);
    modal.querySelectorAll('.poster_title').forEach((el) => el.innerHTML = poster_title);
    modal.querySelectorAll('.poster_number').forEach((el) => el.innerHTML = poster_number);
    modal.querySelectorAll('.poster_prize').forEach((el) => el.innerHTML = poster_prize);
    modal.querySelectorAll('.ticket').forEach((el) => el.innerHTML = ticket);
    modal.querySelectorAll('.ticket_options').forEach((el) => el.innerHTML = ticket_options);
}

document.addEventListener('DOMContentLoaded', () => {
    $('#id_checkin_modal').on('show.bs.modal', (event) => {
        add_content(event, 'checkin_reg_id');
    });

    $('#id_checkin_and_pay_modal').on('show.bs.modal', (event) => {
        const modal = event.target;

        add_content(event, 'checkin_and_pay_reg_id');

        modal.querySelector('#id_participant_has_paid').addEventListener('click', (event) => {
            modal.querySelector('#submit-id-submit_btn_checkin_and_pay').disabled = !event.target.checked;
        });
    });
});