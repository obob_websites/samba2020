
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.dispatch import receiver
from home.signals import (abstract_submitted, abstract_accepted,
                          abstract_rejected, extended_abstract_submitted,
                          extended_abstract_rejected,
                          extended_abstract_accepted,
                          poster_number_assigned)
from extra.blocks.blocks import EmailBlock


def send_email(email_field, abstract):
    EmailBlock.send(
        getattr(abstract.conference, email_field),
        [abstract.user.email],
        extra_context=abstract.conference.get_expose_context(
            {'user': abstract.user})
    )


@receiver(abstract_submitted)
def send_registration_mail(sender, abstract, **kwargs):
    send_email('email_abstract_submitted', abstract)


@receiver(abstract_accepted)
def send_abstract_accepted_mail(sender, abstract, **kwargs):
    send_email('email_abstract_accepted', abstract)


@receiver(abstract_rejected)
def send_abstract_rejected_mail(sender, abstract, **kwargs):
    send_email('email_abstract_rejected', abstract)


@receiver(extended_abstract_submitted)
def send_extended_abstract_submitted_mail(sender, extended_abstract, **kwargs):
    send_email('email_extended_abstract_submitted', extended_abstract)


@receiver(extended_abstract_accepted)
def send_extended_abstract_accepted_mail(sender, extended_abstract, **kwargs):
    send_email('email_extended_abstract_accepted', extended_abstract)


@receiver(extended_abstract_rejected)
def send_extended_abstract_rejected_mail(sender, extended_abstract, **kwargs):
    send_email('email_extended_abstract_rejected', extended_abstract)


@receiver(poster_number_assigned)
def send_poster_number_assigned_mail(sender, abstract, **kwargs):
    send_email('email_poster_number_assigned', abstract)
