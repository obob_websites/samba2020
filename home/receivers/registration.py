
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from django.dispatch import receiver
from home.signals import (user_has_registered, payment_status_changed,
                          poster_number_assigned)
from extra.blocks.blocks import EmailBlock


def send_email(email_field, registration):
    EmailBlock.send(
        getattr(registration.conference, email_field),
        [registration.user.email],
        extra_context=registration.conference.get_expose_context(
            {'user': registration.user})
    )


@receiver(user_has_registered)
def send_registration_mail(sender, registration, **kwargs):
    send_email('email_has_registered', registration)


@receiver(payment_status_changed)
def update_payment_deadlines(sender, registration,
                             new_payment_status, **kwargs):
    if new_payment_status == 'pending':
        registration._reset_payment_deadlines()
    else:
        registration.send_first_payment_reminder_date = None
        registration.send_second_payment_reminder_date = None
        registration.unregister_date = None

    registration.save(update_fields=[
        'send_first_payment_reminder_date',
        'send_second_payment_reminder_date',
        'unregister_date'
    ])

    if new_payment_status == 'pay_at_conference':
        send_email('email_pay_at_conference', registration)

    if new_payment_status == 'complete':
        send_email('email_payment_complete', registration)

    if (new_payment_status in ('complete', 'pay_at_conference') and
            registration.abstract is not None and
            registration.abstract.review_status == 'accepted' and
            registration.abstract.poster_number is None):
        registration.abstract.poster_number = registration.conference.highest_poster_number + 1  # noqa
        registration.abstract.save()
        poster_number_assigned.send(sender=sender,
                                    abstract=registration.abstract)
