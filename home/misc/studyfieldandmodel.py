
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import csv
from django.conf import settings
import os


def add_studyfield_data(StudyField, n_items=None):
    with open(os.path.join(
            os.path.dirname(settings.PROJECT_DIR),
            'home/data/keywords.csv')) as f:
        data = csv.reader(f)
        if n_items is False:
            return

        if isinstance(n_items, int) and not isinstance(n_items, bool):
            data = list(data)[0:n_items]
        for row in data:
            StudyField.objects.get_or_create(field=row[0],
                                             source='cambridge')


def add_studymethod_data(StudyMethod, n_items=None):
    with open(os.path.join(
            os.path.dirname(settings.PROJECT_DIR),
            'home/data/methods.csv')) as f:
        data = csv.reader(f)
        if n_items is False:
            return

        if isinstance(n_items, int) and not isinstance(n_items, bool):
            data = list(data)[0:n_items]
        for row in data:
            StudyMethod.objects.get_or_create(method=row[0],
                                              source='own')
