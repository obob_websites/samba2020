# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import orjson
import pyzstd
from pathlib import Path

from bulk_sync import bulk_sync
from django.conf import settings


def get_ror_data(f_name, n_items=None):
    with pyzstd.open(f_name) as data_file:
        data_s = data_file.read()
        data = orjson.loads(data_s)

    data = [d for d in data if 'name' in d]

    if isinstance(n_items, int) and not isinstance(n_items, bool):
        data = list(data)[0:n_items]

    return {x['id']: x for x in data}


def add_ror_data(Affiliation, n_items=None):
    if n_items is False:
        return

    acronym_model = Affiliation.affiliationacronym_set.field.model
    alt_names_model = Affiliation.affiliationalternativename_set.field.model

    data = get_ror_data(Path(settings.PROJECT_DIR).parent / Path('home', 'data', 'v1.19-2023-02-16-ror-data.json.zst'),
                        n_items=n_items)

    new_models = {}

    for row in data.values():
        new_models[row['id']] = Affiliation(
            ror_id=row['id'],
            institution_primary_name=row['name'],
            city=row['addresses'][0]['city'],
            country=row['country']['country_name'],
            source='v1.19-2023-02-16'
        )

    ret = bulk_sync(new_models=list(new_models.values()),
                    key_fields=('ror_id', ),
                    filters=None,
                    skip_deletes=True)

    print('Results of bulk_sync: '
          '{created} created, {updated} updated, {deleted} deleted.'
          .format(**ret['stats']))

    new_alt_names = []
    new_acronyms = []
    affiliations_to_update = []

    for row in data.values():
        affiliation = new_models[row['id']]

        for cur_alternative in row['labels']:
            new_alt_names.append(
                alt_names_model(
                    alternative_name=cur_alternative['label'],
                    language_code=cur_alternative['iso639'],
                    affiliation=affiliation
                )
            )

        for cur_alternative in row['aliases']:
            new_alt_names.append(
                alt_names_model(
                    alternative_name=cur_alternative,
                    affiliation=affiliation
                )
            )

        for cur_acronym in row['acronyms']:
            new_acronyms.append(
                acronym_model(
                    acronym=cur_acronym,
                    affiliation=affiliation
                )
            )

        try:
            cur_parents = [data[r['id']] for r in row['relationships'] if r['type'] == 'Parent']
            if cur_parents:
                parents_n_children = {x['id']: len([y for y in x['relationships'] if y['type'] == 'Child']) for x in cur_parents}  # noqa
                best_parent = sorted(parents_n_children.items(), key=lambda x: x[1])[0]

                affiliation.parent = new_models[best_parent[0]]
                affiliations_to_update.append(affiliation)
        except KeyError:
            pass

    if affiliations_to_update:
        ret = bulk_sync(new_models=affiliations_to_update,
                        key_fields=('id', ),
                        filters=None,
                        skip_deletes=True)

        assert ret['stats']['updated'] == len(affiliations_to_update)
        print('Results of bulk_sync: '
              '{created} created, {updated} updated, {deleted} deleted.'
              .format(**ret['stats']))

    alt_names_model.objects.bulk_create(new_alt_names)
    acronym_model.objects.bulk_create(new_acronyms)
