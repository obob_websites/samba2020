
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import orjson
from django.conf import settings
import os
import pyzstd
from pathlib import Path
from bulk_sync import bulk_sync
from django.core.exceptions import ObjectDoesNotExist

from shelved_cache import PersistentCache
from cachetools import LRUCache, cached

country_code_subsitutes = {
    'at': 'de',
    'ch': 'de'
}


def get_grid_data(f_name, n_items=None):
    with pyzstd.open(f_name) as data_file:
        data_s = data_file.read()
        data = orjson.loads(data_s)['institutes']

    data = [d for d in data if 'name' in d]

    if isinstance(n_items, int) and not isinstance(n_items, bool):
        data = list(data)[0:n_items]

    return data


if 'TEST_BASE_DIR' in os.environ:
    pc = PersistentCache(LRUCache, str(Path(os.environ['TEST_BASE_DIR'],
                                            'cache')),
                         maxsize=10)
    get_grid_data = cached(pc)(get_grid_data)


def row_to_dict(row):
    primary_name = ''
    alternative_names = row['aliases']
    english_name_idx = None
    acronyms = row['acronyms']

    country_code = row['addresses'][0]['country_code'].lower()
    country_code = country_code_subsitutes.get(country_code,
                                               country_code)

    for label in row['labels']:
        if label['iso639'] == country_code:
            primary_name = label['label']
        else:
            alternative_names.append(label['label'])

    if not primary_name:
        primary_name = row['name']
    else:
        alternative_names.append(row['name'])
        english_name_idx = len(alternative_names) - 1

    return {
        'primary_name': primary_name,
        'alternative_names': alternative_names,
        'english_name_idx': english_name_idx,
        'acronyms': acronyms,
        'country_code': country_code,
        'grid_id': row['id']
    }


def add_grid_ids(Affiliations, n_items=None):
    data = get_grid_data(os.path.join(os.path.dirname(settings.PROJECT_DIR),
                                      'home/data/grid191210.json.zst'),
                         n_items=n_items)

    all_affiliations = Affiliations.objects.all()
    if all_affiliations.count() == 0:
        return

    for idx, row in enumerate(data):
        row_data = row_to_dict(row)

        cur_affiliation = all_affiliations[idx]

        assert cur_affiliation.institution_primary_name == row_data['primary_name']  # noqa

        cur_affiliation.grid_id = row['id']

        cur_affiliation.save()


def add_grid_data(Affiliation, n_items=None):
    return


def update_grid_data(Affiliation, n_items=None):
    if n_items is False:
        return

    data = get_grid_data(os.path.join(os.path.dirname(settings.PROJECT_DIR),
                                      'home/data/grid201209.json.zst'),
                         n_items=n_items)

    acronym_model = Affiliation.affiliationacronym_set.field.model
    alt_names_model = Affiliation.affiliationalternativename_set.field.model

    acronym_model.objects.all().delete()
    alt_names_model.objects.all().delete()

    new_models = []

    for row in data:
        row_data = row_to_dict(row)

        new_models.append(
            Affiliation(
                grid_id=row_data['grid_id'],
                institution_primary_name=row_data['primary_name'],
                source='grid201209'
            )
        )

    ret = bulk_sync(new_models=new_models,
                    key_fields=('grid_id', ),
                    filters=None,
                    skip_deletes=True)

    print('Results of bulk_sync: '
          '{created} created, {updated} updated, {deleted} deleted.'
          .format(**ret['stats']))

    new_alt_names = []
    new_acronyms = []

    for row in data:
        row_data = row_to_dict(row)

        affiliation = Affiliation.objects.get(grid_id=row_data['grid_id'])

        for idx, cur_alternative in enumerate(row_data['alternative_names']):
            alternative_model = (affiliation
                                 .affiliationalternativename_set
                                 .model)
            new_alt_names.append(
                alternative_model(
                    alternative_name=cur_alternative,
                    put_in_string=(idx == row_data['english_name_idx']),
                    affiliation=affiliation
                )
            )

        for cur_acronym in row_data['acronyms']:
            acronym_model = (affiliation
                             .affiliationacronym_set
                             .model)
            new_acronyms.append(
                acronym_model(
                    acronym=cur_acronym,
                    affiliation=affiliation
                )
            )

    alt_names_model.objects.bulk_create(new_alt_names)
    acronym_model.objects.bulk_create(new_acronyms)


def grid_add_city_and_country(Affiliation, n_items=None):
    data_new = get_grid_data(os.path.join(
        os.path.dirname(settings.PROJECT_DIR),
        'home/data/grid201209.json.zst'
    ), n_items=n_items)
    data_old = get_grid_data(
        os.path.join(os.path.dirname(settings.PROJECT_DIR),
                     'home/data/grid191210.json.zst'),
        n_items=n_items)

    new_objects = {}

    for data in [data_old, data_new]:
        for cur_data in data:
            cur_affiliation = getattr(new_objects, cur_data['id'], None)
            if cur_affiliation is None:
                try:
                    cur_affiliation = Affiliation.objects.get(
                        grid_id=cur_data['id']
                    )
                except ObjectDoesNotExist:
                    continue

            cur_affiliation.city = cur_data['addresses'][0]['city']
            cur_affiliation.country = cur_data['addresses'][0]['country']

            new_objects[cur_data['id']] = cur_affiliation

    ret = bulk_sync(new_models=list(new_objects.values()),
                    key_fields=('grid_id', ),
                    filters=None,
                    skip_deletes=True)

    print('Results of bulk_sync: '
          '{created} created, {updated} updated, {deleted} deleted.'
          .format(**ret['stats']))


def grid_add_parents(Affiliation, n_items=None):
    data_new = get_grid_data(os.path.join(
        os.path.dirname(settings.PROJECT_DIR),
        'home/data/grid201209.json.zst'
    ), n_items=n_items)
    data_old = get_grid_data(
        os.path.join(os.path.dirname(settings.PROJECT_DIR),
                     'home/data/grid191210.json.zst'),
        n_items=n_items)

    for data in [data_old, data_new]:
        for cur_data in data:
            done = False
            for cur_relationship in cur_data['relationships']:
                if cur_relationship['type'] == 'Parent' and not done:
                    try:
                        cur_affiliation = Affiliation.objects.get(
                            grid_id=cur_data['id']
                        )
                        this_parent = Affiliation.objects.get(
                            grid_id=cur_relationship['id']
                        )
                    except ObjectDoesNotExist:
                        continue

                    cur_affiliation.parent = this_parent
                    cur_affiliation.save()
                    done = True
                    continue
