
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import wagtail_factories
from factory import faker, SubFactory

from ..factoryblocks.blocks import RichtextBlockFactory
from wagtail.core import models
from home import models as home_models


class DummyArgMixin(object):
    @classmethod
    def _adjust_kwargs(cls, **kwargs):
        if 'dummy' in kwargs:
            del kwargs['dummy']

        return super()._adjust_kwargs(**kwargs)


class HeaderFactory(DummyArgMixin, wagtail_factories.CharBlockFactory):
    value = faker.Faker('word')


class ParagraphFactory(DummyArgMixin, RichtextBlockFactory):
    value = faker.Faker('text', max_nb_chars=100)


class PageFactory(wagtail_factories.PageFactory):
    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        if 'parent' not in kwargs or kwargs['parent'] is None:
            kwargs['parent'] = models.Page.objects.get(slug='root')

        return super()._create(model_class, *args, **kwargs)


class SponsorBlockFactory(wagtail_factories.StructBlockFactory):
    name = faker.Faker('company')
    logo = SubFactory(
        wagtail_factories.ImageChooserBlockFactory
    )
    url = faker.Faker('url')

    class Meta:
        model = home_models.SponsorBlock


class SponsorLevelBlockFactory(wagtail_factories.StructBlockFactory):
    level_name = faker.Faker('word')
    sponsors = wagtail_factories.ListBlockFactory(SponsorBlockFactory)

    class Meta:
        model = home_models.SponsorLevelBlock
