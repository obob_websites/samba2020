
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import pytest
from .expose_to_template_test_app import factories
import wagtail_factories

from django.utils.formats import date_format


@pytest.mark.django_db
def test_test_page_models(samba, admin_user):
    template_string = 'The date is {{ date_field }}. ' \
                      'The title of my parent is {{ parent_title }}.'

    index_factory_kwargs = dict(
        title='My Page',
        body__0__paragraph__dummy=None,
        body__1__paragraph__dummy=None,
    )

    samba.login_admin()

    index_page = factories.IndexPageFactory.create(
        **index_factory_kwargs)

    wagtail_factories.SiteFactory(root_page=index_page,
                                  is_default_site=True)

    sub_page = factories.ExposeToTemplatePageFactory.create(
        parent=index_page,
        title='My Sub Page',
        body__0__paragraph__value=template_string)

    sub_page_obj = sub_page

    samba.selenium.open(samba.live_server + sub_page.url)

    samba.selenium.assert_text('The date is %s' % (
        date_format(sub_page_obj.date_field),
    ))

    samba.selenium.assert_text('Date: %s' % (
        date_format(sub_page_obj.date_field), ))

    samba.selenium.assert_text('The title of my parent is My Page.')


@pytest.mark.django_db
def test_help_text(samba, admin_user):
    samba.login_admin()

    index_page = factories.IndexPageFactory.create()

    wagtail_factories.SiteFactory(root_page=index_page,
                                  is_default_site=True)

    sub_dummy = factories.ExposeToTemplatePageFactory.build()

    samba.selenium.open(
        samba.live_server + factories.ExposeToTemplatePageFactory.get_add_url(
            sub_dummy, index_page
        ))

    for key, item in sub_dummy.get_expose_descriptions(
            include_invalid=True).items():

        assert_text = '{{ %s }}: %s' % (key, item)

        if ' not ' in item:
            samba.selenium.assert_text_not_visible(assert_text)
        else:
            samba.selenium.assert_text(assert_text)


@pytest.mark.django_db
def test_recursive_expose(samba, admin_user):
    template_string = 'The parent date is {{ date_field }}. ' \
                      'The title of my parent is {{ parent_title }}.' \
                      'The child date is {{ child_date_field }}.'

    samba.login_admin()
    index_page = factories.IndexPageFactory.create()

    wagtail_factories.SiteFactory(root_page=index_page,
                                  is_default_site=True)

    parent_page = factories.ExposeToTemplatePageFactory.create(
        parent=index_page
    )

    sub_dummy = factories.ExposeToTemplateChildPageFactory.build()

    samba.selenium.open(
        samba.live_server + factories.ExposeToTemplatePageFactory.get_add_url(
            sub_dummy, parent_page
        ))

    for page in (parent_page, sub_dummy):
        for key, item in page.get_expose_descriptions().items():
            assert_text = '{{ %s }}: %s' % (key, item)
            samba.selenium.assert_text(assert_text)

    sub_page = factories.ExposeToTemplateChildPageFactory.create(
        parent=parent_page,
        body__0__paragraph__value=template_string
    )

    samba.selenium.open(samba.live_server + sub_page.url)

    samba.selenium.assert_text('The child date is %s' % (
        date_format(sub_page.date_field),
    ))

    samba.selenium.assert_text('Date: %s' % (
        date_format(sub_page.date_field),
    ))

    samba.selenium.assert_text('The title of my parent is %s' % (
        parent_page.title,
    ))

    samba.selenium.assert_text('The parent date is %s' % (
        date_format(parent_page.date_field),
    ))
