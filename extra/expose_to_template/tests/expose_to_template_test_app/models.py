
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import home.models.mixins
from home import models as original_models
from wagtail.core.models import Page
from wagtail.core import blocks
from wagtail.core.fields import StreamField
from extra.expose_to_template.mixins.expose_to_template import (
    ExposeToTemplatePageMixin, ExposeContextItem)
from extra.expose_to_template.blocks import ExposedRichTextBlock

from wagtail.admin.edit_handlers import FieldPanel

from django.db import models as dj_models


def get_default_streamfield():
    sf = StreamField(block_types=[
        ('h2', blocks.TextBlock(template='home/blocks/h2.html')),
        ('h3', blocks.TextBlock(template='home/blocks/h3.html')),
        ('h4', blocks.TextBlock(template='home/blocks/h4.html')),
        ('paragraph', ExposedRichTextBlock()),
    ])

    return sf


class IndexPage(original_models.IndexPage):
    subpage_types = original_models.IndexPage.subpage_types + [
        'expose_to_template_test_app.ExposeToTemplatePage']


class ExposeToTemplatePage(home.models.mixins.NotRootMixin,
                           ExposeToTemplatePageMixin,
                           Page):
    body = get_default_streamfield()
    date_field = dj_models.DateField('Choose a date')

    content_panels = Page.content_panels + [
        FieldPanel('date_field'),
        FieldPanel('body')
    ]

    expose_fields = ExposeToTemplatePageMixin.expose_fields + [
        ExposeContextItem(tag='date_field',
                          description='The current date'),
        ExposeContextItem(
            tag='parent_title',
            description='The title of the parent page',
            getfun_or_field=lambda o, c: c['page'].get_parent().title),
        ExposeContextItem(
            tag='invalid_item_lambda',
            description='This should not be there',
            getfun_or_field=lambda o, c: 'oh no!',
            is_valid=lambda o, c: False
        ),
        ExposeContextItem(
            tag='invalid_item_value',
            description='This should also not be there',
            getfun_or_field=lambda o, c: 'oh no!',
            is_valid=False
        ),
        ExposeContextItem(
            tag='valid_item_lambda',
            description='This should be there',
            getfun_or_field=lambda o, c: 'oh yes!',
            is_valid=lambda o, c: True
        ),
        ExposeContextItem(
            tag='valid_item_value',
            description='This should totally be there',
            getfun_or_field=lambda o, c: 'oh yes!',
            is_valid=True
        )
    ]


class ExposeToTemplateChildPage(home.models.mixins.NotRootMixin,
                                ExposeToTemplatePageMixin,
                                Page):
    body = get_default_streamfield()
    date_field = dj_models.DateField('Choose a date')

    content_panels = Page.content_panels + [
        FieldPanel('date_field'),
        FieldPanel('body')
    ]

    expose_fields = ExposeToTemplatePageMixin.expose_fields + [
        ExposeContextItem(tag='child_date_field',
                          description='The date of the child',
                          getfun_or_field='date_field')
    ]
