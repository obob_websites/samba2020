
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail.core.blocks import RichTextBlock
from django.template import Context, Template
from ..mixins import ExposeToTemplatePageMixin


class ExposedRichTextBlock(RichTextBlock):
    def __init__(self, *args, ignore_tag=None, **kwargs):
        self.ignore_tag = ignore_tag
        super().__init__(*args, **kwargs)

    def render(self, value, context=None):
        if (context is not None and
                isinstance(context['page'], ExposeToTemplatePageMixin)):
            value_template = Template(value)
            ex_context = Context(context['page'].get_expose_context(
                context, [self.ignore_tag]))

            value = value_template.render(ex_context)

        return super().render(value, context)
