
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail.admin.panels import PanelGroup


class ObjectListWithHelp(PanelGroup):
    class BoundPanel(PanelGroup.BoundPanel):
        template_name = 'expose_to_template/object_list_with_help.html'


class ExposeContextItem(object):
    def __init__(self, tag, description,
                 getfun_or_field=None, is_valid=True,
                 get_from_context=False):
        self.tag = tag
        self.description = description

        if getfun_or_field is None:
            getfun_or_field = tag

        if isinstance(getfun_or_field, str):
            if get_from_context:
                self.getfun = lambda o, c: c.get(getfun_or_field, '')
            else:
                self.getfun = lambda o, c: getattr(o, getfun_or_field, '')
        else:
            self.getfun = getfun_or_field

        if callable(is_valid):
            self._is_valid = is_valid
        else:
            self._is_valid = lambda o, c: is_valid

    def get(self, object, context):
        return self.getfun(object, context)

    def is_valid(self, object, context):
        return self._is_valid(object, context)


class ExposeToTemplateMixin(object):
    expose_fields = []

    def get_expose_context(self, context, ignore_fields=None):
        expose_context = dict()

        if ignore_fields is None:
            ignore_fields = []

        for field in self.expose_fields:
            if (field.is_valid(self, context) and
                    field.tag not in ignore_fields):
                expose_context[field.tag] = field.get(self, context)

        return expose_context

    def get_expose_descriptions(self, include_invalid=False):
        desc = dict()
        for field in self.expose_fields:
            if not include_invalid and not field.is_valid(self, {}):
                continue

            desc[field.tag] = field.description

        return desc


class ExposeToTemplateViewMixin(ExposeToTemplateMixin):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['expose_descriptions'] = self.get_expose_descriptions()

        return context


class ExposeToTemplatePageMixin(ExposeToTemplateMixin):
    def get_expose_descriptions(self, include_invalid=False):
        parent_desc = dict()
        if isinstance(self._get_parent(), ExposeToTemplatePageMixin):
            parent_desc = self._get_parent().get_expose_descriptions(
                include_invalid)

        desc = super().get_expose_descriptions(include_invalid)

        desc.update(parent_desc)

        return desc

    def get_expose_context(self, context, ignore_fields=None):
        if 'page' not in context:
            context['page'] = self

        if hasattr(self, 'get_parent') and self.get_parent() is not None:
            this_parent = self.get_parent().specific
        else:
            this_parent = None

        expose_context = super().get_expose_context(context, ignore_fields)

        if isinstance(this_parent, ExposeToTemplatePageMixin):
            expose_context.update(this_parent.get_expose_context(
                context, ignore_fields))

        return expose_context

    @classmethod
    def get_edit_handler(cls):
        edit_handlers = super().get_edit_handler()
        for i, cur_handler in enumerate(edit_handlers.children):
            if str(cur_handler.heading) in ('Content', 'Emails'):
                edit_handlers.children[i] = ObjectListWithHelp(
                    **cur_handler.clone_kwargs()).bind_to_model(cls)
        return edit_handlers

    def _get_parent(self):
        if not hasattr(self, 'get_parent'):
            return None

        parent = getattr(self, 'parent_for_desc', self.get_parent())

        if parent == self:
            parent = self.get_parent()

        return getattr(parent, 'specific', parent)
