
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail.core.blocks import StructBlock, IntegerBlock, StreamBlock

from .card import CardBlock
from extra.expose_to_template.blocks import ExposedRichTextBlock
from wagtail.contrib.table_block.blocks import TableBlock


class BootstrapContentBlock(StreamBlock):
    def __init__(self, *args, content_blocks=None, **kwargs):
        super().__init__(*args, local_blocks=content_blocks, **kwargs)


class BootstrapColumnBlock(StructBlock):
    class Meta:
        icon = 'fa-columns'
    n_cols = IntegerBlock(min_value=1, max_value=12,
                          help_text='Amount of columns to span')

    def __init__(self, *args, content_blocks=None, **kwargs):
        super().__init__(*args, local_blocks=(
            ('content', BootstrapContentBlock(
                content_blocks=content_blocks), ),
        ), **kwargs)


class BootstrapRowBlock(StreamBlock):
    class Meta:
        template = 'extra_blocks/bootstrap_row.html'
        icon = 'fa-paragraph'

    def __init__(self, *args, ignore_tag=None, content_blocks=None, **kwargs):
        if content_blocks is None:
            content_blocks = (
                ('paragraph', ExposedRichTextBlock(ignore_tag=ignore_tag)),
                ('table', TableBlock(table_options={
                    'renderer': 'html',
                })),
                ('card', CardBlock(ignore_tag=ignore_tag))
            )

        super().__init__(*args, local_blocks=(
            ('column', BootstrapColumnBlock(content_blocks=content_blocks)),
        ), **kwargs)
