
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

from wagtail.core.blocks import StructBlock, CharBlock, StreamBlock

from extra.expose_to_template.blocks import ExposedRichTextBlock
from wagtail.contrib.table_block.blocks import TableBlock


class CardBlock(StructBlock):
    class Meta:
        template = 'extra_blocks/card.html'
        icon = 'fa-newspaper-o'

    header = CharBlock()

    def __init__(self, *args, ignore_tag=None, **kwargs):
        from .bootstraprow import BootstrapRowBlock
        row_blocks = (
            ('paragraph', ExposedRichTextBlock(ignore_tag=ignore_tag)),
        )

        local_blocks = (
            ('content', StreamBlock(
                (
                    ('paragraph', ExposedRichTextBlock(ignore_tag=ignore_tag)),
                    ('table', TableBlock(table_options={
                        'renderer': 'html',
                    })),
                    ('row', BootstrapRowBlock(ignore_tag=ignore_tag,
                                              content_blocks=row_blocks))
                )
            )),
        )
        super().__init__(*args, local_blocks=local_blocks, **kwargs)
