
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import warnings

from bs4 import BeautifulSoup
from django.conf import settings
from django.core import mail
from django.template import Context, Template
from wagtail.core import blocks
from wagtail.core.blocks import StreamBlock
from wagtail.core.templatetags.wagtailcore_tags import richtext

from extra.expose_to_template.blocks import ExposedRichTextBlock


class EmailContentBlock(blocks.StructBlock):
    subject = blocks.CharBlock()
    body = ExposedRichTextBlock()


class EmailBlock(StreamBlock):
    email = EmailContentBlock()

    class Meta:
        max_num = 1
        min_num = 0
        required = False

    @classmethod
    def send(cls, stream_value, to_addresses,
             from_address=None, extra_context=None):
        if from_address is None:
            from_address = settings.DEFAULT_FROM_EMAIL

        context = {}

        if extra_context is not None:
            context.update(extra_context)

        context = Context(context)

        if len(stream_value) < 1:
            warnings.warn('Email not set. Sending aborted', RuntimeWarning)
            return

        email_subject = stream_value[0].value['subject']
        email_body_template = Template(
            richtext(stream_value[0].value['body']))
        email_body_html = email_body_template.render(context)
        email_body_txt = BeautifulSoup(email_body_html, 'html5lib').get_text()

        mail.send_mail(subject=email_subject,
                       message=email_body_txt,
                       from_email=from_address,
                       recipient_list=to_addresses,
                       html_message=email_body_html)
