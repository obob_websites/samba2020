
# samba2020 - Webpage for https://samba.ccns.sbg.ac.at
# Copyright (C) 2019-2021 Thomas Hartmann <thomas.hartmann@th-ht.de>
#
# This file is part of samba2020.
#
# samba2020 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# samba2020 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with samba2020.  If not, see <http://www.gnu.org/licenses/>.

import wagtail_factories
from factory import Factory
from wagtail.core import blocks
from wagtail.core.rich_text import RichText
from factory.base import FactoryMetaClass


class RichtextBlockFactory(wagtail_factories.CharBlockFactory):
    class Meta:
        model = blocks.RichTextBlock

    @classmethod
    def _build(cls, model_class, value):
        return model_class().clean(RichText(value))

    @classmethod
    def _create(cls, model_class, value):
        return model_class().clean(RichText(value))


class StreamBlockFactory(Factory):
    class Meta:
        model = blocks.StreamBlock

    @classmethod
    def _build(cls, model_class, *args, **kwargs):
        block = model_class()
        stream_data = []
        for name, child_block in block.child_blocks.items():
            this_factory = getattr(cls, name, None)
            if this_factory is not None:
                if isinstance(kwargs[name], FactoryMetaClass):
                    value = kwargs[name]()
                else:
                    value = kwargs[name]

                this_value = this_factory(value=value)
            else:
                this_value = child_block.get_default()

            stream_data.append((name, this_value))

        return blocks.StreamValue(block, stream_data)

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        return cls._build(model_class, *args, **kwargs)
